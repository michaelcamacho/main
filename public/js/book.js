$(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
  $('button[data-dismiss="modal"]').click(function(){ $(this).parent().parent().parent().parent().modal('hide'); })
});
        var table = $('#book').DataTable( {
        retrieve: true,
        responsive: true,
        "scrollX": true,
        dom: 'Bfrtip',
        buttons: [
        'excel', 'pdf','csv', 
        
        ]
        
    });

         var table = $('#sale').DataTable( {
        retrieve: true,
        responsive: true,
        "scrollX": true,
        dom: 'Bfrtip',
        buttons: [
        'excel', 'pdf','csv', 
        
        ]
        
    });

$(document.body).on('click','.a_edit',function(){
  window.edit_id = $(this).attr('id').replace('a_edit_','');
  branch = $('#a_branch_'+edit_id).text();
  program = $('#a_program_'+edit_id).text();
  book_title = $('#a_book_title_'+edit_id).text();
  book_author = $('#a_book_author_'+edit_id).text();
  price = $('#a_price_'+edit_id).text();
  available = $('#a_available_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_book_title').val(book_title);
  $('#update_author').val(book_author);
  $('#update_price').val(price);
  $('#update_available').val(available);
  $('#stockInput').attr("max" , available);
  
  $('#update_book_title').empty();
  $('#update_book_title').append('<option value="'+ book_title +'" disable="true" selected="true">'+ book_title +'</option>');
  $.getJSON('https://beta.cbrc.solutions/api/main/books',function(data){
            
        $.each(data, function (i) {
            $.each(this, function (key, value) {
            
                console.log(program);
                console.log(value.program_name);
                if(value.program_name == program ){
                    

                  $('#update_book_title').append('<option value="'+ value.Title +'">'+  value.Title +' </option>');
                }
            
            });
        });
        

      });//end of on change getJSON
  });

$(document.body).on('click','.b_edit',function(){
  window.edit_id = $(this).attr('id').replace('b_edit_','');
  branch = $('#b_branch_'+edit_id).text();
  program = $('#b_program_'+edit_id).text();
  book_title = $('#b_book_title_'+edit_id).text();
  price = $('#b_price_'+edit_id).text();
  available = $('#b_available_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_book_title').val(book_title);
  $('#update_price').val(price);
  $('#update_available').val(available);
  
  $('#update_book_title').empty();
  $('#update_book_title').append('<option value="0" disable="true" selected="true">--- Select Book ---</option>');

  });
$(document.body).on('click','.c_edit',function(){
  window.edit_id = $(this).attr('id').replace('c_edit_','');
  branch = $('#c_branch_'+edit_id).text();
  program = $('#c_program_'+edit_id).text();
  book_title = $('#c_book_title_'+edit_id).text();
  price = $('#c_price_'+edit_id).text();
  available = $('#c_available_'+edit_id).text();

  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_book_title').val(book_title);
  $('#update_price').val(price);
  $('#update_available').val(available);
  
  $('#update_book_title').empty();
  $('#update_book_title').append('<option value="0" disable="true" selected="true">--- Select Book ---</option>');

});



$(document.body).on('click','.d_edit',function(){
  window.edit_id = $(this).attr('id').replace('d_edit_','');
  branch = $('#d_branch_'+edit_id).text();
  program = $('#d_program_'+edit_id).text();
  book_title = $('#d_book_title_'+edit_id).text();
  price = $('#d_price_'+edit_id).text();
  available = $('#d_available_'+edit_id).text();
  available = $('#d_newStock_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_book_title').val(book_title);
  $('#update_price').val(price);
  $('#update_available').val(available);
  $('#update_newStock').val(available);


  $('#update_book_title').empty();
  $('#update_book_title').append('<option value="0" disable="true" selected="true">--- Select Book ---</option>');

  });

$(document.body).on('click','.e_edit',function(){
  window.edit_id = $(this).attr('id').replace('e_edit_','');
  branch = $('#e_branch_'+edit_id).text();
  program = $('#e_program_'+edit_id).text();
  book_title = $('#e_book_title_'+edit_id).text();
  price = $('#e_price_'+edit_id).text();
  available = $('#e_available_'+edit_id).text();
  
  $('#update_id').val(edit_id);
  $('#update_branch').val(branch);
  $('#update_program').val(program);
  $('#update_book_title').val(book_title);
  $('#update_price').val(price);
  $('#update_available').val(available);
  

  });
  