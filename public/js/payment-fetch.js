$('#program').on('change', function(e){
            console.log(e);

            var program = e.target.value;

            $.get('/json-student?program=' + program,function(data){
                console.log(data);

                 $('#student').empty();
                 $('#student').append('<option value="0" disable="true" selected="true">--- Select Student ---</option>');

                 
                 $('#facilitation').empty();
                 $('#facilitation').append('<option value="0" disable="true" selected="true">--- Select Facilitation Fee ---</option>');

                 $('#discount').empty();
                 $('#discount').append('<option value="0" disable="true" selected="true">--- Select Discount ---</option>');

                 $('#category').prop('selectedIndex',0);
                 
                 $('#facilitation').val('');
                 $('#tuition').val('');
                 $('#total_amount').val('');
                 $('#balance').val('');
                 $('#id').val('');

                  $.each(data, function(index, studentObj){
                    $('#student').append('<option value="'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'*'+ studentObj.id+'*'+ studentObj.contact_no +'*'+ studentObj.cbrc_id +'*'+ studentObj.school +'*'+ studentObj.major +'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
                    
                });
                  $.each(data, function(index, tuitionObj){
                    $('#id').val(tuitionObj.id);
                });

            });
        });

        $('#student').on('change', function(e){
            console.log(e);

            

            var id = $( "#id" ).val();
            var program = $("#program").val();

            $.get('/json-balance?id=' + id + '&program=' + program,function(data){
                console.log(data);
                $("#total_amount").val('');
                 $('#balance').val('');
                 $('#id').val('');
                
                  $.each(data, function(index, tuitionObj){
                    $('#balance').val(tuitionObj.balance);
                });
               
            });


        });

         $('#category').on('change', function(e){
            console.log(e);

            var category = e.target.value;

            var program = $( "#program" ).val();

            $.get('/json-tuition?program=' + program + '&category=' + category,function(data){
                console.log(data);

                 $('#facilitation').val('');
                 $('#season').val('');
                 $('#tuition').val('');
                 $('#total_amount').val('');

                  $.each(data, function(index, tuitionObj){
                    $('#season').val(tuitionObj.season);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#tuition').val(tuitionObj.tuition_fee);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#facilitation').val(tuitionObj.facilitation_fee);
                })

            });


        });

          $('#category').on('change', function(e){
            console.log(e);

            var category = e.target.value;

            var program = $( "#program" ).val();

            $.get('/json-discount?program=' + program + '&category=' + category,function(data){
                console.log(data);

                 $('#total_amount').val('');

                 $('#discount').empty();
                 $('#discount').append('<option value="0" disable="true" selected="true">--- Select Discount ---</option>');

                 $.each(data, function(index, studentObj){
                    $('#discount').append('<option value="'+ studentObj.discount_amount +','+ studentObj.discount_category +'">'+ studentObj.discount_amount+' ---- '+ studentObj.discount_category +'</option>');
                })
            });


        });

         

$('#category').on('change', function(){


            var category = $("#category").val();

            if(category != null){
              $(".val").addClass("input-has-value");
            }

            else{
              $(".val").removeClass("input-has-value");
            }
        });

$('#student').on('change', function(){

              $(".balance").addClass("input-has-value");
             
            
        });


$('#compute').on('click', function(){

            var tuition = parseInt($("#tuition").val());
            var facilitation = parseInt($("#facilitation").val());
            var discount = parseInt($("#discount").val());
            var balance = parseInt($("#balance").val());
            
            if (isNaN(tuition) === true && isNaN(facilitation) === true && isNaN(balance) === false){

                $("#total_amount").val(balance);
                $(".total").addClass("input-has-value");
            }
            
            if (isNaN(tuition) === false && isNaN(facilitation) === false && isNaN(balance) === true) {
                total_amount = tuition + facilitation - discount;
                $("#total_amount").val(total_amount);
                $(".total").addClass("input-has-value");
              }

            if (isNaN(tuition) === false && isNaN(facilitation) === false && isNaN(balance) === false){
                total_amount = tuition + facilitation + balance - discount;
                $("#total_amount").val(total_amount);
                $(".total").addClass("input-has-value");
            }

            if (isNaN(tuition) === true && isNaN(facilitation) === true && isNaN(balance) === true){
                $("#total_amount").val('');
            }
                    
            
      });
            
$(".readonly").keydown(function(e){
        e.preventDefault();
    });

$('#discount').on('click', function(){
    $('#total_amount').val('');
    });

