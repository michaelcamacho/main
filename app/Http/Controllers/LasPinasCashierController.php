<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\LasPinas\LasPinasAgri;
use App\Model\LasPinas\LasPinasBookCash;
use App\Model\LasPinas\LasPinasBooksInventorie;
use App\Model\LasPinas\LasPinasBooksSale;
use App\Model\LasPinas\LasPinasBudget;
use App\Model\LasPinas\LasPinasCivil;
use App\Model\LasPinas\LasPinasCrim;
use App\Model\LasPinas\LasPinasDiscount;
use App\Model\LasPinas\LasPinasDropped;
use App\Model\LasPinas\LasPinasExpense;
use App\Model\LasPinas\LasPinasIelt;
use App\Model\LasPinas\LasPinasLet;
use App\Model\LasPinas\LasPinasMid;
use App\Model\LasPinas\LasPinasNclex;
use App\Model\LasPinas\LasPinasNle;
use App\Model\LasPinas\LasPinasOnline;
use App\Model\LasPinas\LasPinasPsyc;
use App\Model\LasPinas\LasPinasReceivable;
use App\Model\LasPinas\LasPinasS1Sale;
use App\Model\LasPinas\LasPinasS2Sale;
use App\Model\LasPinas\LasPinasS1Cash;
use App\Model\LasPinas\LasPinasS2Cash;
use App\Model\LasPinas\LasPinasScholar;
use App\Model\LasPinas\LasPinasSocial;
use App\Model\LasPinas\LasPinasTuition;
use App\Model\LasPinas\LasPinasPettyCash;
use App\Model\LasPinas\LasPinasRemit;
use App\Model\LasPinas\LasPinasReservation;
use App\Model\LasPinas\LasPinasEmployee;
use App\Model\LasPinas\LasPinasScoreCards;
use App\Model\LasPinas\LasPinasBookTransfer;

use App\Model\LasPinas\LasPinasLecturerAEvaluation;
use App\Model\LasPinas\LasPinasLecturerBEvaluation;
use App\Model\LasPinas\LasPinasComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class LasPinasCashierController extends Controller
{
    private $branch = "LasPinas";

    private $sbranch = "las_pinas";

public function __construct()
    {

         $this->middleware('role:las_pinas_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = LasPinasBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = LasPinasBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = LasPinasBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             LasPinasBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = LasPinasBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        LasPinasBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = LasPinasBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        LasPinasBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('las_pinas-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('las_pinas-cashier/book-payment');
        }
}
public function books_table(){

$book = LasPinasBooksInventorie::all();
$sale = LasPinasBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }