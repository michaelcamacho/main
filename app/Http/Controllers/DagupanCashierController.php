<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Dagupan\DagupanAgri;
use App\Model\Dagupan\DagupanBookCash;
use App\Model\Dagupan\DagupanBooksInventorie;
use App\Model\Dagupan\DagupanBooksSale;
use App\Model\Dagupan\DagupanBudget;
use App\Model\Dagupan\DagupanCivil;
use App\Model\Dagupan\DagupanCrim;
use App\Model\Dagupan\DagupanDiscount;
use App\Model\Dagupan\DagupanDropped;
use App\Model\Dagupan\DagupanExpense;
use App\Model\Dagupan\DagupanIelt;
use App\Model\Dagupan\DagupanLet;
use App\Model\Dagupan\DagupanMid;
use App\Model\Dagupan\DagupanNclex;
use App\Model\Dagupan\DagupanNle;
use App\Model\Dagupan\DagupanOnline;
use App\Model\Dagupan\DagupanPsyc;
use App\Model\Dagupan\DagupanReceivable;
use App\Model\Dagupan\DagupanS1Sale;
use App\Model\Dagupan\DagupanS2Sale;
use App\Model\Dagupan\DagupanS1Cash;
use App\Model\Dagupan\DagupanS2Cash;
use App\Model\Dagupan\DagupanScholar;
use App\Model\Dagupan\DagupanSocial;
use App\Model\Dagupan\DagupanTuition;
use App\Model\Dagupan\DagupanPettyCash;
use App\Model\Dagupan\DagupanRemit;
use App\Model\Dagupan\DagupanReservation;
use App\Model\Dagupan\DagupanEmployee;
use App\Model\Dagupan\DagupanScoreCards;
use App\Model\Dagupan\DagupanBookTransfer;

use App\Model\Dagupan\DagupanLecturerAEvaluation;
use App\Model\Dagupan\DagupanLecturerBEvaluation;
use App\Model\Dagupan\DagupanComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class DagupanCashierController extends Controller
{
    private $branch = "Dagupan";

    private $sbranch = "dagupan";

public function __construct()
    {

         $this->middleware('role:dagupan_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = DagupanBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = DagupanBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = DagupanBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             DagupanBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = DagupanBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        DagupanBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = DagupanBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        DagupanBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('dagupan-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('dagupan-cashier/book-payment');
        }
}
public function books_table(){

$book = DagupanBooksInventorie::all();
$sale = DagupanBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }