<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Model\AdminSettings;

use App\Branch;
use App\Model\Launion\LaunionAgri;
use App\Model\Launion\LaunionBookCash;
use App\Model\Launion\LaunionBooksInventorie;
use App\Model\Launion\LaunionBooksSale;
use App\Model\Launion\LaunionBudget;
use App\Model\Launion\LaunionCivil;
use App\Model\Launion\LaunionCrim;
use App\Model\Launion\LaunionDiscount;
use App\Model\Launion\LaunionDropped;
use App\Model\Launion\LaunionExpense;
use App\Model\Launion\LaunionIelt;
use App\Model\Launion\LaunionLet;
use App\Model\Launion\LaunionMid;
use App\Model\Launion\LaunionNclex;
use App\Model\Launion\LaunionNle;
use App\Model\Launion\LaunionOnline;
use App\Model\Launion\LaunionPsyc;
use App\Model\Launion\LaunionReceivable;
use App\Model\Launion\LaunionS1Sale;
use App\Model\Launion\LaunionS2Sale;
use App\Model\Launion\LaunionS1Cash;
use App\Model\Launion\LaunionS2Cash;
use App\Model\Launion\LaunionScholar;
use App\Model\Launion\LaunionSocial;
use App\Model\Launion\LaunionTuition;
use App\Model\Launion\LaunionPettyCash;
use App\Model\Launion\LaunionRemit;
use App\Model\Launion\LaunionReservation;
use App\Model\Launion\LaunionEmployee;
use App\Model\Launion\LaunionScoreCards;
use App\Model\Launion\LaunionBookTransfer;

use App\Model\Launion\LaunionLecturerAEvaluation;
use App\Model\Launion\LaunionLecturerBEvaluation;
use App\Model\Launion\LaunionComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


use App\Model\ExpenseSetup;

use Auth;
use User;
use DB;
use File;




class LaunionController extends Controller
{
    private $branch = "Launion";

    private $sbranch = "launion";

public function __construct()
    {

         $this->middleware('role:launion|admin');
        
    }

public function index(){

        $sid = AdminSettings::where('Account','=','admin')->value('id');
        $setting = AdminSettings::findorfail($sid);

        $facilitation = facilitation::where('Year','=',$setting->Year)->sum('facilitation');

        $pettycash = LaunionPettyCash::where('id','=','1')->value('petty_cash');

        $s1_sale = LaunionS1Sale::where('Year','=',$setting->Year)->sum('amount_paid');

        $s2_sale = LaunionS2Sale::where('Year','=',$setting->Year)->sum('amount_paid');

        $s1_cash = LaunionS1Cash::where('id','=','1')->value('cash');

        $s2_cash = LaunionS2Cash::where('id','=','1')->value('cash');

        $receivable = LaunionReceivable::sum('balance');

        $expense = LaunionExpense::where('Season','=',$setting->Season)
        ->where('Year','=',$setting->Year)->sum('amount');

        $enrollee = LaunionLet::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionNle::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionCrim::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionCivil::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionPsyc::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionNclex::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionIelt::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionSocial::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionAgri::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionMid::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count()
                  + LaunionOnline::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->where('status','=','Enrolled')->count();

        $unpaid = LaunionReceivable::count();
        
        $s1_dis = LaunionS1Sale::sum('discount');

        $s2_dis = LaunionS2Sale::sum('discount');

        $discount = $s1_dis + $s2_dis;

        $book = LaunionBooksSale::sum('amount');

        $book_cash = LaunionBookCash::where('id','=','1')->value('cash');

        return view ('member.member-dashboard')
        ->with('pettycash',$pettycash)
        ->with('s1_sale',$s1_sale)
        ->with('s2_sale',$s2_sale)
        ->with('s1_cash',$s1_cash)
        ->with('s2_cash',$s2_cash)
        ->with('receivable', $receivable)
        ->with('expense',$expense)
        ->with('enrollee',$enrollee)
        ->with('unpaid',$unpaid)
        ->with('discount',$discount)
        ->with('book',$book)
        ->with('book_cash',$book_cash)
        ->with('facilitation',$facilitation);
    }

// Register New Enrollee

public function add_enrollee(){

        $branch= $this->branch; 

        $program = Program::all();

        return view('member.add-enrollee')->with('branch',$branch)->with('program',$program);
}


public function insert_enrollee(Request $request){

        $input = $request->except(['_token']);
        $branch = $this->branch;
        $course = $input['program'];

        if($course == 'lets'){
            $course = 'LET';
        }
        if($course == 'nles'){
            $course = 'NLE';
        }
        if($course == 'crims'){
            $course = 'Criminology';
        }
        if($course == 'civils'){
            $course = 'Civil Service';
        }

        if($course == 'psycs'){
            $course = 'Psychometrician';
        }
        if($course == 'nclexes'){
            $course = 'NCLEX';
        }
        if($course == 'ielts'){
            $course = 'IELTS';
        }
        if($course == 'socials'){
            $course = 'Social Work';
        }
        if($course == 'agris'){
            $course = 'Agriculture';
        }
        if($course == 'mids'){
            $course = 'Midwifery';
        }
        if($course == 'onlines'){
            $course = 'Online Only';
        }

        $program = $this->sbranch.'_'.$input['program'];

        $lastname = strtoupper($input['last_name']);
        $firstname = strtoupper($input['first_name']);
        $middlename = strtoupper($input['middle_name']);

        $existent = DB::table($program)->where('last_name','=',$lastname)->where('first_name','=',$firstname)->where('middle_name','=',$middlename)->first();

        if($existent != null){
            Alert::error('Failed!', 'This name is already registered.');
            return redirect ($this->sbranch.'/add-enrollee');
        }

        if($existent == null){
        DB::table($program)->insert([

            'cbrc_id'       => $input['cbrc_id'],
            'section'       => $input['section'],
            'last_name'     => strtoupper($input['last_name']),
            'first_name'    => strtoupper($input['first_name']),
            'middle_name'   => strtoupper($input['middle_name']),
            'username'      => $input['username'],
            'password'      => $input['password'],
            'course'        => $course,
            'major'         => $input['major'],
            'program'       => $input['program'],
            'school'        => $input['school'],
            'noa_no'        => $input['noa_no'],
            'take'          => $input['take'],
            'branch'        => $branch,
            'birthdate'     => $input['birthdate'],
            'contact_no'    => $input['contact_no'],
            'email'         => $input['email'],
            'address'       => $input['address'],
            'contact_person'=> $input['contact_person'],
            'contact_details'=> $input['contact_details'],
            'registration'  => 'Walk-in',
            'created_at'    => date('Y-m-d'),
        ]);
        Alert::success('Success!', 'New student has been registered.');
        return redirect ($this->sbranch.'/add-enrollee');

    }
    }

public function update_enrollee(Request $request){
    $input = $request->except(['_token']);
        $branch = $this->branch;
        $program = $input['program'];
        $id = $input['id'];

        if($program == 'LET'){
            $program = 'lets';
        }
        if($program == 'NLE'){
            $program = 'nles';
        }
        if($program == 'Criminology'){
            $program = 'crims';
        }
        if($program == 'Civil Service'){
            $program = 'civils';
        }

        if($program == 'Psychometrician'){
            $program = 'psycs';
        }
        if($program == 'NCLEX'){
            $program = 'nclexes';
        }
        if($program == 'IELTS'){
            $program = 'ielts';
        }
        if($program == 'Social Work'){
            $program = 'socials';
        }
        if($program == 'Agriculture'){
            $program = 'agris';
        }
        if($program == 'Midwifery'){
            $program = 'mids';
        }
        if($program == 'Online Only'){
            $program = 'onlines';
        }

        $program = $this->sbranch.'_'.$program;

        DB::table($program)->where('id','=',$id)->update([

            'cbrc_id'       => $input['cbrc_id'],
            'last_name'     => $input['last_name'],
            'first_name'    => $input['first_name'],
            'middle_name'   => $input['middle_name'],
            'username'      => $input['username'],
            'password'      => $input['password'],
            'major'         => $input['major'],
            'school'        => $input['school'],
            'noa_no'        => $input['noa_no'],
            'take'          => $input['take'],
            'birthdate'     => $input['birthdate'],
            'contact_no'    => $input['contact_no'],
            'email'         => $input['email'],
            'address'       => $input['address'],
            'contact_person'=> $input['contact_person'],
            'contact_details'=> $input['contact_details'],
            'section'        => $input['section'],
        ]);
       
       //login to api
       $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
       $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
           'form_params' => [
               "email"=>"admin@main.cbrc.solutions",
               "password"=>"main@dmin"
           ]
       ]);
   //insert data to api
  
       if ($res->getStatusCode() == 200) { // 200 OK
           $response_data = json_decode($res->getBody()->getContents());
        
           
               //save student info to api
               $studentForApi =  DB::table($program)->where('id','=' , $id)->where('status','=','Enrolled')->first();
               $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
               ,[
               'form_params' => [
                   "_method" => "PUT",
                   "BranchStdID" => $studentForApi->id,
                   "Branch_Name" =>lcfirst($this->sbranch),
                   "cbrc_id" =>     $input['cbrc_id'],
                   "Lastname" => $input['last_name'],
                   "Firstname" =>$input['first_name'],
                   "Middlename" =>$input['middle_name'],
                   "Birthday" =>$input['birthdate'],
                   "Contact_Number" =>$input['contact_no'],
                   "Address" =>$input['address'],
                   "Email" =>$input['email'],
                   "Username" =>$input['username'],
                   "Password" =>$input['password'],
                   "School" => $input['school'],
                   "Program" =>$studentForApi->program,
                   "Section" =>$input['section'],
                   "Major" =>$input['major'],
                   "Take" =>$input['take'],
                   "Noa" =>$input['noa_no'],
                   "Category" =>$studentForApi->category,
                   "Status" =>$studentForApi->status,
                   "Contact_Person" =>$input['contact_person'],
                   "Contact_Details" =>$input['contact_details'],
                   "Facilitation" =>$studentForApi->facilitation,
                   "Season" =>$studentForApi->season,
                   "Year" =>$studentForApi->year
           
               ]
               ]);
               // return dd($studentForApi);


       }//end of 200 ok

        return response()->json([
            'success' => true,
            'message' => 'Enrollee has been updated',
            // 'response_data1 '=>   $response_data1, 
            'input '=>   $input, 
            // 'responsed '=>   $responsed,
            // 'studentForApi '=>   $studentForApi,
        ]);

    }

    public function delete_enrollee(Request $request){
    $input = $request->except(['_token']);
        $branch = $this->branch;
        $program = $input['program'];
        $id = $input['id'];

        if($program == 'LET'){
            $program = 'lets';
        }
        if($program == 'NLE'){
            $program = 'nles';
        }
        if($program == 'Criminology'){
            $program = 'crims';
        }
        if($program == 'Civil Service'){
            $program = 'civils';
        }

        if($program == 'Psychometrician'){
            $program = 'psycs';
        }
        if($program == 'NCLEX'){
            $program = 'nclexes';
        }
        if($program == 'IELTS'){
            $program = 'ielts';
        }
        if($program == 'Social Work'){
            $program = 'socials';
        }
        if($program == 'Agriculture'){
            $program = 'agris';
        }
        if($program == 'Midwifery'){
            $program = 'mids';
        }
        if($program == 'Online Only'){
            $program = 'onlines';
        }

        $program = $this->sbranch.'_'.$program;

        DB::table($program)->where('id','=',$id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Enrollee has been deleted',
            
        ]);

    }

// Add New Payment

public function new_payment(){

        $branch= $this->branch; 
        $date = date('M-d-Y');
        $program = Program::all();
        return view('member.new-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }

public function fetch_student(){

        $program = Input::get('program');

        if ($program == 'lets' ){

        $student = LaunionLet::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = LaunionNle::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = LaunionCrim::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = LaunionCivil::orderBy('last_name')->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = LaunionPsyc::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = LaunionNclex::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = LaunionIelt::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = LaunionSocial::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = LaunionAgri::orderBy('last_name')->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = LaunionMid::orderBy('last_name')->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = LaunionOnline::orderBy('last_name')->get();
            return response()->json($student);
        }       
    }

public function fetch_tuition(){

        $category = Input::get('category');
        $program = Input::get('program');


        if ($program == 'lets' ){

        $tuition = LaunionTuition::where('program','=', 'LET')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'nles' ){
            $tuition = LaunionTuition::where('program','=', 'NLE')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'crims' ){
            $tuition = LaunionTuition::where('program','=', 'Criminology')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'civils' ){
            $tuition = LaunionTuition::where('program','=', 'Civil Service')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

         if ($program == 'psycs' ){
            $tuition = LaunionTuition::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'nclexes' ){
            $tuition = LaunionTuition::where('program','=', 'NCLEX')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'ielts' ){
            $tuition = LaunionTuition::where('program','=', 'IELTS')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'socials' ){
            $tuition = LaunionTuition::where('program','=', 'Social Work')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'agris' ){
            $tuition = LaunionTuition::where('program','=', 'Agriculture')->where('category','=', $category)->get();
            return response()->json($tuition);
        }

        if ($program == 'mids' ){
            $tuition = LaunionTuition::where('program','=', 'Midwifery')->where('category','=', $category)->get();
            return response()->json($tuition);
        }  

        if ($program == 'onlines' ){
            $tuition = LaunionTuition::where('program','=', 'Online Only')->where('category','=', $category)->get();
            return response()->json($tuition);
        }          
    }

public function fetch_discount(){

        $program = Input::get('program');
        $category = Input::get('category');


        if ($program == 'lets' ){

        $discount = LaunionDiscount::where('program','=', 'LET')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'nles' ){
            $discount = LaunionDiscount::where('program','=', 'NLE')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'crims' ){
            $discount = LaunionDiscount::where('program','=', 'Criminology')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'civils' ){
            $discount = LaunionDiscount::where('program','=', 'Civil Service')->where('category','=', $category)->get();
        return response()->json($discount);
        }

         if ($program == 'psycs' ){
            $discount = LaunionDiscount::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'nclexes' ){
            $discount = LaunionDiscount::where('program','=', 'NCLEX')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'ielts' ){
            $discount = LaunionDiscount::where('program','=', 'IELTS')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'socials' ){
            $discount = LaunionDiscount::where('program','=', 'Social Work')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'agris' ){
            $discount = LaunionDiscount::where('program','=', 'Agriculture')->where('category','=', $category)->get();
            return response()->json($discount);
        }  

        if ($program == 'mids' ){
            $discount = LaunionDiscount::where('program','=', 'Midwifery')->where('category','=', $category)->get();
            return response()->json($discount);
        } 

        if ($program == 'onlines' ){
            $discount = LaunionDiscount::where('program','=', 'Online Only')->where('category','=', $category)->get();
            return response()->json($discount);
        }       


    }
    public function add_new_payment(Request $request){
    
            $input = $request->except(['_token']);
            $season = $input['season'];
            $total_amount = $input['total_amount'];
            $amount_paid = $input['amount_paid'];
            $student = explode('*',$input['name']);
            $balance = $total_amount - $amount_paid;
            $firstPayment =0;       
            
            //-----------------for scorecard payment starts here 
            $curr_date = date('M-d-Y');
            
            $program = $this->sbranch.'_'.$input['program'];
            
            $check = DB::table($program)->where('id','=',$student[1])->where('status','=','Enrolled')->first();
            $statusforapi = DB::table($program)->where('id','=',$student[1])->value('status');
            $dis_category = explode(',',$input['discount']);
            $scoreSeason = 0;
    
            //check what season
            if($season == "Season 1"){
                $scoreSeason = '1';
            }
            if($season == "Season 2"){
                $scoreSeason = '2';
            }//end check what season
    
            //check if retaker or first timer
            
            $remark = '1stTimer';
            
            if(count($dis_category) == 2){
                if (strpos(strtolower($dis_category[1]), 'bounce') == false || strpos(strtolower($dis_category[1]), 'retaker') == false|| strpos(strtolower($dis_category[1]), 'retake') == false) {
                    $remark = 'retake';
                }//end of check if retaker or first timer
            }
            
            //reffer to views/js/payment.blade.php for "student[5]" 
            //skip this if already made any payment   
            if($check == null || $input['tuition_fee'] == null){
            $firstPayment = 1;
            $check_date = LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$curr_date)->first();
               //if let
            if($input['program'] == "lets")
                {
                    if($check_date == null)
                    {         
                    LaunionScoreCards::create([
                        'year' => date('Y',strtotime($curr_date)),
                        'date' => $curr_date,
                        'season' => $scoreSeason,
                        str_replace(' ', '', strtolower($student[5])) => 1,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev[ str_replace(' ', '', strtolower($student[5]))] + 1;
                        
                            LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                str_replace(' ', '', strtolower($student[5]))  => $score,
                        ]);
                    }     
                }//end of if lets
    
    
                //if nles
                elseif($input['program'] == "nles")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'nles_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['nles_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'nles_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                    'date' => $curr_date,
                                    'nles_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['nles_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'nles_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if nles
    
                //if crims
                elseif($input['program'] == "crims")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'crims_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['crims_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'crims_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)), 
                                    'date' => $curr_date,
                                    'crims_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['crims_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'crims_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if civils
    
                elseif($input['program'] == "civils")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'civils_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['civils_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'civils_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)),
                                    'date' => date('Y-m-d'),
                                    'civils_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['civils_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'civils_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if civils 
    
                elseif($input['program'] == "psycs")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'psycs_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['psycs_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'psycs_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)),
                                    'date' => $curr_date,
                                    'psycs_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['psycs_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'psycs_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if psycs

                elseif($input['program'] == "ielts")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'ielts_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['ielts_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'ielts_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)),
                                    'date' => $curr_date,
                                    'ielts_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['ielts_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'ielts_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if ielts

                elseif($input['program'] == "socials")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'socials_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['socials_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'socials_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)),
                                    'date' => $curr_date,
                                    'socials_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['socials_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'socials_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if socials

                elseif($input['program'] == "agris")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'agris_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['agris_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'agris_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)),
                                    'date' => $curr_date,
                                    'agris_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['agris_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'agris_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if agris

               
                
                elseif($input['program'] == "mids")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'mids_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['mids_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'mids_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)),
                                    'date' => $curr_date,
                                    'mids_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['mids_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'mids_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if mids

                elseif($input['program'] == "onlines")
                {
                    if($remark == "retake")
                    {
                        if($check_date == null)
                        {         
                        LaunionScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'onlines_retakers' => 1,
                                'season' => $scoreSeason,
                        ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['onlines_retakers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'onlines_retakers' => $score,
                            ]);
                        }
                    }
                    else
                    {
                        if($check_date == null)
                        {  
                             LaunionScoreCards::create([
                                    'year' => date('Y',strtotime($curr_date)),
                                    'date' => $curr_date,
                                    'onlines_1stTimers' => 1,
                                    'season' => $scoreSeason,
                            ]);
                        }
                        else// do this if there is existing payment of the current date
                        {
                            $prev =  LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                            $score = $prev['onlines_1stTimers'] + 1;
                                LaunionScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                    'onlines_1stTimers'  => $score,
                            ]);
                        }
                    }
                }//end of if onlines
                
                else
                {
    
                }
    
    
    
//--add facilitation amount to facilitation table
facilitation::create([
    'year' => date('Y',strtotime($curr_date)),
    'season' => $season,
    'branch' => "Launion",
    'facilitation' => $input['facilitation'],
]);
//--end of add facilitation amount to facilitation table

    
} //end of skip this if already made any payment  

//-----------end of score card update in payment
//-----------end of score card update in payment
//-----------end of score card update in payment
    
    


            $reserve = $input['reserve'];
    
            if($balance < 0 ){
                $balance = 0;
            }
            $discount = explode(',',$input['discount']);
            $prog = $input['program'];
            if ($prog == 'lets' ){
            $prog = 'LET';
            }
            if ($prog == 'nles' ){
                $prog = 'NLE';
            }
            if ($prog == 'crims' ){
                $prog = 'Criminology';
            }
            if ($prog == 'civils' ){
                $prog = 'Civil Service';
            }
             if ($prog == 'psycs' ){
                $prog = 'Psychometrician';
            }
            if ($prog == 'nclexes' ){
                $prog = 'NCLEX';
            }
            if ($prog == 'ielts' ){
                $prog = 'IELTS';
            }
            if ($prog == 'socials' ){
                $prog = 'Social Work';
            }
            if ($prog == 'agris' ){
                $prog = 'Agriculture';
            }
            if ($prog == 'mids' ){
                $prog = 'Midwifery';
            }
    
            if ($prog == 'onlines' ){
                $prog = 'Online Only';
            }
            $discount = explode(',',$input['discount']);
    
            if($discount[0] == 0) {
                $discount_amount = null;
                $discount_category = null;
            } 
            else {
                
                $discount_amount = $discount[0];
                $discount_category = $discount[1];
            }
    
            if($input['tuition_fee'] != null){
            if ($season == 'Season 1'){
            DB::table($this->sbranch.'_s1_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'season'  => $season,
                'year'  => $input['year'],
                'created_at'    => date('Y-m-d'),
            ]);
            $ini = LaunionS1Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            LaunionS1Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
    
            $program = $this->sbranch.'_'.$input['program'];
    
            DB::table($program)->where('id','=',$student[1])->update([
    
                'category' => $input['category'],
                'status'   => 'Enrolled',
                'facilitation' => $input['facilitation'],
                'year'  => $input['year'],
                'season' => $season
            ]);
            }
            if ($season == 'Season 2'){
            DB::table($this->sbranch.'_s2_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'season' => $season,
                'year'  => $input['year'],
                 'created_at'    => date('Y-m-d'),
            ]);
    
            $ini = LaunionS2Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            LaunionS2Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
    
            $program = $this->sbranch.'_'.$input['program'];
    
            DB::table($program)->where('id','=',$student[1])->update([
    
                'category' => $input['category'],
                'status'   => 'Enrolled',
                'facilitation' => $input['facilitation'],
                'year'  => $input['year'],
                'season' => $season
            ]);
            }
    
            Alert::success('Success!', 'Payment has been submitted.');
        }
            if($input['tuition_fee'] == null){
                
                //get year
                $ryear = DB::table($program)->where('id','=',$student[1])->value('year');
                
                if ($input['rseason'] == 'Season 1'){
                    
                

            DB::table($this->sbranch.'_s1_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'year'  => $ryear,
                 'created_at'    => date('Y-m-d'),
            ]);
    
            $ini = LaunionS1Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            LaunionS1Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
    
            }
            if ($input['rseason'] == 'Season 2'){
            DB::table($this->sbranch.'_s2_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'year'  => $ryear,
                'season'  => "Season 2",
                 'created_at'    => date('Y-m-d'),
            ]);
    
            $ini = LaunionS2Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            LaunionS2Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
            }
            Alert::success('Success!', 'Payment has been submitted.');
            }
    
           
    
            if($balance > 0){
    
                if($input['balance'] == null){
                    LaunionReceivable::insert([
    
                        'enrollee_id' => $student[1],
                        'name'        => $student[0],
                        'program'     => $prog,
                        'contact_no'  => $student[2],
                        'season'      => $season,
                        'balance'     => $balance,
                    ]);
                }
                }
                if($input['balance'] != null && $input['tuition_fee'] == null){
    
                    if($amount_paid >= $total_amount){
                        LaunionReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                        }
                    if($amount_paid < $total_amount){
                    $remaining = $input['balance'];
                    $present_balance = $remaining - $amount_paid;
                    LaunionReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                        'balance' => $present_balance,
                    ]);
                    }
                }
                if($input['balance'] != null && $input['tuition_fee'] != null){
    
                    if($amount_paid >= $total_amount){
                        LaunionReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                        }
                    else{
                    LaunionReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                        'balance' => $balance,
                    ]);
                    }
                }
    
               if($input['reserve'] != null){
    
                LaunionReservation::where('enrollee_id','=',$student[1])->where('program','=',$input['program'])->delete();
               }




            //check if not enrolled
    if($statusforapi  != "Enrolled"){
        
            if($firstPayment == 1){
               //get last inserted payment
               //check what season
              
            if($season === "Season 1"){
                $lastRec = DB::table($this->sbranch.'_s1_sales')->where('program',$prog)->where('student_id',$student[1])->orderby('id','desc')->get();
               
            }
            if($season === "Season 2"){
                $lastRec = DB::table($this->sbranch.'_s2_sales')->where('program',$prog)->where('student_id',$student[1])->orderby('id','desc')->get();
               
            }//end check what season
            

            foreach($lastRec as $data){
                $lastRecDate  = $data->date;
                $lastRecName  = $data->name;
                $lastRecstudent_id  = $data->student_id;
                $lastRecdiscout_category  = $data->discount_category;
                $lastRectuition_fee  = $data->tuition_fee;
                $lastRecfacilitation_fee  = $data->facilitation_fee;
                $lastRecyear  = $data->year;
            }
           
        //login to api
        $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
        $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
            'form_params' => [
                "email"=>"admin@main.cbrc.solutions",
                "password"=>"main@dmin"
            ]
        ]);
    //insert data to api
   
        if ($res->getStatusCode() == 200) { // 200 OK
            $response_data = json_decode($res->getBody()->getContents());
         //save first payment to api   
            $sendPayment = $client->request('POST', 'https://cbrc.solutions/api/main/payment?token='.$response_data->access_token
            ,[
            'form_params' => [
                
                        "Branch" => ucwords($this->sbranch),
                        "Season" => $season,
                        "Date" => $lastRecDate,
                        "Name" => $lastRecName,
                        "Stdid" => $lastRecstudent_id,
                        "Program" => $input['program'],
                        "Category" => $input['category'],
                        "Discount_category" => $lastRecdiscout_category,
                        "Tuition_fee" =>  $lastRectuition_fee,
                        "Facilitation_fee" => $lastRecfacilitation_fee,
                        "year" => $lastRecyear
                        ]
            ]);
            
                //save student info to api
                $studentForApi =  DB::table($program)->where('id','=',$student[1])->where('status','=','Enrolled')->first();
                $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
                ,[
                'form_params' => [
                    "BranchStdID" => $studentForApi->id,
                    "Branch_Name" =>lcfirst($this->sbranch),
                    "cbrc_id" =>$studentForApi->cbrc_id,
                    "Lastname" =>$studentForApi->last_name,
                    "Firstname" =>$studentForApi->first_name,
                    "Middlename" =>$studentForApi->middle_name,
                    "Birthday" =>$studentForApi->birthdate,
                    "Contact_Number" =>$studentForApi->contact_no,
                    "Address" =>$studentForApi->address,
                    "Email" =>$studentForApi->email,
                    "Username" =>$studentForApi->username,
                    "Password" =>$studentForApi->password,
                    "School" =>$studentForApi->school,
                    "Program" =>$studentForApi->program,
                    "Section" =>$studentForApi->section,
                    "Major" =>$studentForApi->major,
                    "Take" =>$studentForApi->take,
                    "Noa" =>$studentForApi->noa_no,
                    "Category" =>$studentForApi->category,
                    "Status" =>$studentForApi->status,
                    "Contact_Person" =>$studentForApi->contact_person,
                    "Contact_Details" =>$studentForApi->contact_details,
                    "Facilitation" =>$studentForApi->facilitation,
                    "Season" =>$studentForApi->season,
                    "Year" =>$studentForApi->year
            
                ]
                ]);
                // return dd($studentForApi);


        }//end of 200 ok
    }//end of firstPayment save to api    

    }//end of check if enrolled

               return redirect ($this->sbranch.'/new-payment');
        }

public function add_expense(){
        $branch=$this->branch; 
        $program = Program::all();
        $date = date('M-d-Y');
        return view('member.add-expense')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }

public function fetch_expense(){
        $category = Input::get('category');
        $sub_category = Expense::where('category','=', $category)->get();
        return response()->json($sub_category);
    }

public function fetch_id(){

    $id = Input::get('id');
    $program = Input::get('program');

    if ($program == 'lets' ){

        $student = LaunionLet::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = LaunionNle::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = LaunionCrim::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = LaunionCivil::where('id','=',$id)->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = LaunionPsyc::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = LaunionNclex::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = LaunionIelt::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = LaunionSocial::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = LaunionAgri::where('id','=',$id)->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = LaunionMid::where('id','=',$id)->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = LaunionOnline::where('id','=',$id)->get();
            return response()->json($student);
        }  
}

public function fetch_balance(){

        $id = Input::get('id');
        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 

        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 

        if ($program == 'onlines' ){
            $program = 'Online Only';
        }

        

        $balance = LaunionReceivable::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        return response()->json($balance);
    }

public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = LaunionBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             LaunionBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = LaunionBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        LaunionBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ($this->sbranch.'/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ($this->sbranch.'/book-payment');
        }
}
public function add_new_expense(Request $request){

        $input = $request->except(['_token']);
        $author = Auth::user()->name;
        $ubranch = "Launion";
        $expid = ExpenseSetup::where('branch','=',$ubranch)->value('id');
        $expset = ExpenseSetup::findorfail($expid);
        
        LaunionExpense::insert([
            'date'=>$input['date'],
            'branch'=>$this->branch,
            'program'=>$input['program'],
            'category'=>$input['category'],
            'sub_category'=>$input['sub_category'],
            'amount'=>$input['amount'],
            'remarks'=>$input['remarks'],
            'author' => $author,
            'Season' => $expset->Season,
            'Year' => $expset->Year
            ]);
        $pettycash = LaunionPettyCash::where('id','=','1')->value('petty_cash');

        $updated_pettycash = $pettycash - $input['amount'];

        LaunionPettyCash::where('id','=','1')->update([
            'petty_cash' => $updated_pettycash,
        ]);

        $chk = $input['category'];
        if ($chk == 'Facilitation') {
            # code...
            $amt = $input['amount'];

            $facilitation = facilitation::where('id','=','1')->value('facilitation');
            $new_faci = $facilitation - $amt;
            facilitation::where('id','=','1')->update(['facilitation' => $new_faci]);
            //d2ako
        }
        
        Alert::success('Success!', 'New expense has been added.');
        return redirect ($this->sbranch.'/add-expense');
    }

public function add_budget(){
    $branch=$this->branch;
    $date = date('M-d-Y');
    return view('member.add-budget')->with('branch',$branch)->with('date',$date);
}

public function insert_new_budget(Request $request){
    $input = $request->except(['_token']);
    LaunionBudget::insert($input);

    $initial = LaunionPettyCash::where('id','=','1')->value('petty_cash');

    $amount = $input['amount'];

    $total = $initial + $amount;

    LaunionPettyCash::where('id','=','1')->update([
        'petty_cash' => $total,
    ]);
    Alert::success('Success!', 'New budget has been added.');
    return redirect ($this->sbranch.'/add-budget');
}

public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}

public function let_table(){
    $prog = 'LET';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = LaunionLet::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nle_table(){
    $prog = 'NLE';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = LaunionNle::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function crim_table(){
    $prog = 'Criminology';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = LaunionCrim::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function civil_table(){
    $prog = 'Civil Service';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = LaunionCivil::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function psyc_table(){
    $prog = 'Psychometrician';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = LaunionPsyc::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nclex_table(){
    $prog = 'NCLEX';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = LaunionNclex::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function ielts_table(){
    $prog = 'IELTS';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = LaunionIelt::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function social_table(){
    $prog = 'Social Work';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = LaunionSocial::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function agri_table(){
    $prog = 'Agriculture';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = LaunionAgri::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function mid_table(){
    $prog = 'Midwifery';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = LaunionMid::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}

public function online_table(){
    $prog = 'Online Only';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = LaunionOnline::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function scholar_table(){

    $let = LaunionLet::where('category','=','Scholar')->get();
    $nle = LaunionNle::where('category','=','Scholar')->get();
    $crim = LaunionCrim::where('category','=','Scholar')->get();
    $civil = LaunionCivil::where('category','=','Scholar')->get();
    $psyc = LaunionPsyc::where('category','=','Scholar')->get();
    $nclex = LaunionNclex::where('category','=','Scholar')->get();
    $ielt = LaunionIelt::where('category','=','Scholar')->get();
    $social = LaunionSocial::where('category','=','Scholar')->get();
    $agri = LaunionAgri::where('category','=','Scholar')->get();
    $mid = LaunionMid::where('category','=','Scholar')->get();
    $online = LaunionOnline::where('category','=','Scholar')->get();

    return view ('member.scholar')->with('let',$let)->with('nle',$nle)->with('crim',$crim)->with('civil',$civil)->with('psyc',$psyc)->with('nclex',$nclex)->with('ielt',$ielt)->with('social',$social)->with('agri',$agri)->with('mid',$mid)->with('online',$online);

}
public function enrolled_table(){

    $let = LaunionLet::where('status','=','Enrolled')->get();
    $nle = LaunionNle::where('status','=','Enrolled')->get();
    $crim = LaunionCrim::where('status','=','Enrolled')->get();
    $civil = LaunionCivil::where('status','=','Enrolled')->get();
    $psyc = LaunionPsyc::where('status','=','Enrolled')->get();
    $nclex = LaunionNclex::where('status','=','Enrolled')->get();
    $ielt = LaunionIelt::where('status','=','Enrolled')->get();
    $social = LaunionSocial::where('status','=','Enrolled')->get();
    $agri = LaunionAgri::where('status','=','Enrolled')->get();
    $mid = LaunionMid::where('category','=','Enrolled')->get();
    $online = LaunionOnline::where('category','=','Enrolled')->get();

    $sale1 = LaunionS1Sale::all();

    $sale2 = LaunionS2Sale::all();
    $tot_amount_paid=0;
    $total_balance=0;
    return view ('member.enrolled')
                ->with('let',$let)
                ->with('nle',$nle)
                ->with('crim',$crim)
                ->with('civil',$civil)
                ->with('psyc',$psyc)
                ->with('nclex',$nclex)
                ->with('ielt',$ielt)
                ->with('social',$social)
                ->with('agri',$agri)
                ->with('mid',$mid)
                ->with('online',$online)
                ->with('tot_amount_paid',$tot_amount_paid)
                ->with('total_balance',$total_balance)
                ->with('sale1',$sale1)
                ->with('sale2',$sale2);

}
public function dropped_table(){

    $let = LaunionLet::where('status','=','Dropped')->get();
    $nle = LaunionNle::where('status','=','Dropped')->get();
    $crim = LaunionCrim::where('status','=','Dropped')->get();
    $civil = LaunionCivil::where('status','=','Dropped')->get();
    $psyc = LaunionPsyc::where('status','=','Dropped')->get();
    $nclex = LaunionNclex::where('status','=','Dropped')->get();
    $ielt = LaunionIelt::where('status','=','Dropped')->get();
    $social = LaunionSocial::where('status','=','Dropped')->get();
    $agri = LaunionAgri::where('status','=','Dropped')->get();
    $mid = LaunionMid::where('category','=','Dropped')->get();
    $online = LaunionOnline::where('category','=','Dropped')->get();

    return view ('member.dropped')->with('let',$let)->with('nle',$nle)->with('crim',$crim)->with('civil',$civil)->with('psyc',$psyc)->with('nclex',$nclex)->with('ielt',$ielt)->with('social',$social)->with('agri',$agri)->with('mid',$mid)->with('online',$online);

}

public function drop_student(Request $request){
    $input = $request->except(['_token']);

    $id = $input['id'];
    $program = $input['program'];

    if($program == 'lets'){

        LaunionLet::where('id','=',$id)->update([
            'status' => 'Dropped',
        ]);

        return response()->json([
            'success' => true,
            'message' => '1 Student has beed dropped.',
        ]);
    }


}
public function tuition_table(){

    $tuition = LaunionTuition::where('branch','=',$this->branch)->get();

    $discount = LaunionDiscount::where('branch','=',$this->branch)->get();

    return view ('member.tuition')->with('tuition',$tuition)->with('discount',$discount);

}

public function expense_table(){

    $expense = LaunionExpense::all();

    return view ('member.expense')->with('expense',$expense);

}
public function sales_enrollee_table(){

    $sale = LaunionS1Sale::all();

    $sale2 = LaunionS2Sale::all();

    return view ('member.sales-enrollee')->with('sale',$sale)->with('sale2',$sale2);

}

public function sales_program_table(){

    $let_1_sale=LaunionS1Sale::where('program','=','LET')->sum('amount_paid');
    $nle_1_sale=LaunionS1Sale::where('program','=','NLE')->sum('amount_paid');
    $crim_1_sale=LaunionS1Sale::where('program','=','Criminology')->sum('amount_paid');
    $civil_1_sale=LaunionS1Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psyc_1_sale=LaunionS1Sale::where('program','=','Psychometrician')->sum('amount_paid');
    $nclex_1_sale=LaunionS1Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ielts_1_sale=LaunionS1Sale::where('program','=','IELTS')->sum('amount_paid');
    $social_1_sale=LaunionS1Sale::where('program','=','Social')->sum('amount_paid');
    $agri_1_sale=LaunionS1Sale::where('program','=','Agriculture')->sum('amount_paid');
    $mid_1_sale=LaunionS1Sale::where('program','=','Midwifery')->sum('amount_paid');
    $online_1_sale=LaunionS1Sale::where('program','=','Online Only')->sum('amount_paid');

    $let_2_sale=LaunionS2Sale::where('program','=','LET')->sum('amount_paid');
    $nle_2_sale=LaunionS2Sale::where('program','=','NLE')->sum('amount_paid');
    $crim_2_sale=LaunionS2Sale::where('program','=','Criminology')->sum('amount_paid');
    $civil_2_sale=LaunionS2Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psyc_2_sale=LaunionS2Sale::where('program','=','Psychometrician')->sum('amount_paid');
    $nclex_2_sale=LaunionS2Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ielts_2_sale=LaunionS2Sale::where('program','=','IELTS')->sum('amount_paid');
    $social_2_sale=LaunionS2Sale::where('program','=','Social')->sum('amount_paid');
    $agri_2_sale=LaunionS2Sale::where('program','=','Agriculture')->sum('amount_paid');
    $mid_2_sale=LaunionS1Sale::where('program','=','Midwifery')->sum('amount_paid');
    $online_2_sale=LaunionS1Sale::where('program','=','Online Only')->sum('amount_paid');

    $let =  LaunionLet::where('status','=','Enrolled')->count();
    $nle =  LaunionNle::where('status','=','Enrolled')->count();
    $crim = LaunionCrim::where('status','=','Enrolled')->count();
    $civil= LaunionCivil::where('status','=','Enrolled')->count();
    $psyc = LaunionPsyc::where('status','=','Enrolled')->count();
    $nclex = LaunionNclex::where('status','=','Enrolled')->count();
    $ielts = LaunionIelt::where('status','=','Enrolled')->count();
    $social = LaunionSocial::where('status','=','Enrolled')->count();
    $agri = LaunionAgri::where('status','=','Enrolled')->count();
    $mid = LaunionMid::where('status','=','Enrolled')->count();
    $online = LaunionOnline::where('status','=','Enrolled')->count();

   

    return view ('member.sales-program')
    ->with('let_1_sale',$let_1_sale)
    ->with('nle_1_sale',$nle_1_sale)
    ->with('crim_1_sale',$crim_1_sale)
    ->with('civil_1_sale',$civil_1_sale)
    ->with('psyc_1_sale',$psyc_1_sale)
    ->with('nclex_1_sale',$nclex_1_sale)
    ->with('ielts_1_sale',$ielts_1_sale)
    ->with('social_1_sale',$social_1_sale)
    ->with('agri_1_sale',$agri_1_sale)
    ->with('mid_1_sale',$mid_1_sale)
    ->with('online_1_sale',$online_1_sale)
    ->with('let_2_sale',$let_2_sale)
    ->with('nle_2_sale',$nle_2_sale)
    ->with('crim_2_sale',$crim_2_sale)
    ->with('civil_2_sale',$civil_2_sale)
    ->with('psyc_2_sale',$psyc_2_sale)
    ->with('nclex_2_sale',$nclex_2_sale)
    ->with('ielts_2_sale',$ielts_2_sale)
    ->with('social_2_sale',$social_2_sale)
    ->with('agri_2_sale',$agri_2_sale)
    ->with('mid_2_sale',$mid_2_sale)
    ->with('online_2_sale',$online_2_sale)
    ->with('let',$let)
    ->with('nle',$nle)
    ->with('crim',$crim)
    ->with('civil',$civil)
    ->with('psyc',$psyc)
    ->with('nclex',$nclex)
    ->with('ielts',$ielts)
    ->with('social',$social)
    ->with('agri',$agri)
    ->with('mid',$mid)
    ->with('online',$online);

}

public function receivable_enrollee_table(){

    $receivable = LaunionReceivable::all();
    return view ('member.receivable-enrollee')->with('receivable',$receivable);

}
public function receivable_program_table(){

    $let_receivable=LaunionReceivable::where('program','=','LET')->sum('balance');
    $nle_receivable=LaunionReceivable::where('program','=','NLE')->sum('balance');
    $crim_receivable=LaunionReceivable::where('program','=','Criminology')->sum('balance');
    $civil_receivable=LaunionReceivable::where('program','=','Civil Service')->sum('balance');
    $psyc_receivable=LaunionnReceivable::where('program','=','Psychometrician')->sum('balance');
    $nclex_receivable=LaunionReceivable::where('program','=','NCLEX')->sum('balance');
    $ielts_receivable=LaunionReceivable::where('program','=','IELTS')->sum('balance');
    $social_receivable=LaunionReceivable::where('program','=','Social')->sum('balance');
    $agri_receivable=LaunionReceivable::where('program','=','Agriculture')->sum('balance');
    $mid_receivable=LaunionReceivable::where('program','=','Midwifery')->sum('balance');
    $online_receivable=LaunionReceivable::where('program','=','Online Only')->sum('balance');

    $let=LaunionReceivable::where('program','=','LET')->count();
    $nle=LaunionReceivable::where('program','=','NLE')->count();
    $crim=LaunionReceivable::where('program','=','Criminology')->count();
    $civil=LaunionReceivable::where('program','=','Civil Service')->count();
    $psyc=LaunionReceivable::where('program','=','Psychometrician')->count();
    $nclex=LaunionReceivable::where('program','=','NCLEX')->count();
    $ielts=LaunionReceivable::where('program','=','IELTS')->count();
    $social=LaunionReceivable::where('program','=','Social')->count();
    $agri=LaunionReceivable::where('program','=','Agriculture')->count();
    $mid=LaunionReceivable::where('program','=','Midwifery')->count();
    $online=LaunionReceivable::where('program','=','Online Only')->count();

    return view ('member.receivable-program')
    ->with('let_receivable',$let_receivable)
    ->with('nle_receivable',$nle_receivable)
    ->with('crim_receivable',$crim_receivable)
    ->with('civil_receivable',$civil_receivable)
    ->with('psyc_receivable',$psyc_receivable)
    ->with('nclex_receivable',$nclex_receivable)
    ->with('ielts_receivable',$ielts_receivable)
    ->with('social_receivable',$social_receivable)
    ->with('agri_receivable',$agri_receivable)
    ->with('mid_receivable',$mid_receivable)
    ->with('online_receivable',$online_receivable)
    ->with('let',$let)
    ->with('nle',$nle)
    ->with('crim',$crim)
    ->with('civil',$civil)
    ->with('psyc',$psyc)
    ->with('nclex',$nclex)
    ->with('ielts',$ielts)
    ->with('social',$social)
    ->with('agri',$agri)
    ->with('mid',$mid)
    ->with('online',$online);
}

public function books_table(){

$book = LaunionBooksInventorie::all();
$sale = LaunionBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}
public function new_remit(){
    $date = date('M-d-Y');
    $branch = $this->branch;
    return view ('member.new-remit')->with('branch',$branch)->with('date',$date);
}

public function insert_remit(Request $request){

    $input = $request->except(['_token']);
    $author = Auth::user()->name;
    

    if($input['category'] == 'Sales' && $input['season'] == 'Season 1'){
        
        LaunionRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'season'    => $input['season'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);


        $available = LaunionS1Cash::where('id','=','1')->value('cash');

        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }
        else{
            LaunionS1Cash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from season 1 has been remitted.');
    }

    if($input['category'] == 'Sales' && $input['season'] == 'Season 2'){

        LaunionRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'season'    => $input['season'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);

        $available = LaunionS2Cash::where('id','=','1')->value('cash');
        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }

        else{
            LaunionS2Cash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from season 2 has been remitted.');
    }

    if($input['category'] == 'Books'){

        LaunionRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);

        $available = LaunionBookCash::where('id','=','1')->value('cash');
        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }

        else{
            LaunionBookCash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from books has been remitted.');
    }

    return redirect ($this->sbranch.'/new-remit');

}


public function remit(){

    $remit = LaunionRemit::all();

    return view ('member.remit')->with('remit',$remit);
}

public function clear_enrollee(Request $request){

    $input = $request->except(['_token']);
    $program = $input['program'];

    DB::table($this->sbranch.'_'.$program)->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}
public function clear_sale_season1(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_s1_sales')->truncate();

    LaunionS1Cash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_sale_season2(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_s2_sales')->truncate();

    LaunionS2Cash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_receivable(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_receivables')->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_expense(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_expenses')->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_book(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_books_sales')->truncate();

    LaunionBookCash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function new_reservation(){
    $branch= $this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view ('member.new-reservation')->with('branch',$branch)->with('date',$date)->with('program',$program);
}

public function insert_reservation(Request $request){

    $input = $request->except(['_token']);
    $details = explode('*',$input['name']);

    $name = $details[0];
    $school = $details[1];
    $email = $details[2];
    $contact_no = $details[3];
    $id = $details[4];

    $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psychometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }

    $existent = LaunionReservation::where('enrollee_id','=',$id)->first();

        if($existent != null){
           
        $old = LaunionReservation::where('enrollee_id','=',$id)->value('reservation_fee');
        
        $new = $old + $input['amount_paid'];

        LaunionReservation::where('enrollee_id','=',$id)->update([
            'reservation_fee' => $new,
        ]);

         $season = $input['season'];
        if ($season == 'Season 1'){

        LaunionS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = LaunionS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        LaunionS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        LaunionS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = LaunionS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        LaunionS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', '1 student has been updated.');
    return redirect ($this->sbranch.'/new-reservation');
        }

    if($existent == null)
    {

    LaunionReservation::create([
        'enrollee_id'    =>     $id,
        'name'           =>     $name,
        'branch'         =>     $this->branch,
        'program'        =>     $input['program'],
        'prog'           =>     $prog,
        'school'         =>     $school,
        'email'          =>     $email,
        'contact_no'     =>     $contact_no,
        'reservation_fee'=>     $input['amount_paid'],
    ]);

    $season = $input['season'];
    if ($season == 'Season 1'){

        LaunionS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = LaunionS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        LaunionS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        LaunionS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = LaunionS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        LaunionS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', 'New student has been reserved.');
    return redirect ($this->sbranch.'/new-reservation');
}
}

public function reservation_table(){

    $reserve = LaunionReservation::all();

    return view ('member.reservation')->with('reserve',$reserve);
}

public function fetch_reserved(){

    $id = Input::get('id');
    $program = Input::get('program');
    

    $fee = LaunionReservation::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        
        return response()->json($fee);

}

public function clear_reservation(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_reservations')->truncate();


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}


/* Lecturer Evaluation */

public function add_lec(){

    $program = Program::all();

    return view ('member.add-lecturer')->with('program',$program);
}

public function fetch_class(){
    $program = Input::get('program');

        $class = DB::table('classes')->where('program','=',$program)->get();

        return response()->json($class);
        
}

public function fetch_subject(){
    $program = Input::get('program');
    $class = Input::get('class');

        $subject = DB::table('subjects')->where('program','=',$program)->where('class','=',$class)->get();

        return response()->json($subject);
        
}

public function fetch_section(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');

        $section = Section::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('branch','=',$this->branch)->get();

        return response()->json($section);
        
}

public function fetch_lecturer(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');
    $section = Input::get('section');

        $lecturer = LaunionLecturerAEvaluation::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('section','=',$section)->get();

        return response()->json($lecturer);
        
}

public function fetch_lecturerb(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');
    $section = Input::get('section');

        $lecturer = LaunionLecturerBEvaluation::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('section','=',$section)->get();

        return response()->json($lecturer);
        
}

public function insert_lecturer(Request $request){

    $input = $request->except(['_token']);

    $class   = explode('*',$input['class']);
    $subject = explode('*',$input['subject']);

    $existent1 = LaunionLecturerAEvaluation::where('lecturer','=',strtoupper($input['lecturer']))->where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->first();

    $existent2 = LaunionLecturerBEvaluation::where('lecturer','=',strtoupper($input['lecturer']))
        ->where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->first();

    if($existent1 != null && $existent2 != null){

        Alert::error('Failed!', 'This lecturer was already assigned.');
        return redirect ($this->sbranch.'/add-lecturer');

    }
    if($existent1 == null && $existent2 == null){
    LaunionLecturerAEvaluation::create([
        'date'      => $input['date'],
        'lecturer'  => strtoupper($input['lecturer']),
        'branch'    => $this->branch,
        'program'   => $input['program'],
        'section'   => $input['section'],
        'class'     => $class[1],
        'aka_class' => $class[0],
        'subject'   => $subject[1],
        'aka_subject'=>$subject[0],
        'review_ambassador'=>strtoupper($input['ambassador']),
        'excellentA'    => 0,
        'goodA'         => 0,
        'fairA'         => 0,
        'poorA'         => 0,
        'verypoorA'     => 0,
        'excellentB'    => 0,
        'goodB'         => 0,
        'fairB'         => 0,
        'poorB'         => 0,
        'verypoorB'     => 0,
        'excellentC'    => 0,
        'goodC'         => 0,
        'fairC'         => 0,
        'poorC'         => 0,
        'verypoorC'     => 0,
        'excellentD'    => 0,
        'goodD'         => 0,
        'fairD'         => 0,
        'poorD'         => 0,
        'verypoorD'     => 0,
        'excellentE'    => 0,
        'goodE'         => 0,
        'fairE'         => 0,
        'poorE'         => 0,
        'verypoorE'     => 0,
        'excellentF'    => 0,
        'goodF'         => 0,
        'fairF'         => 0,
        'poorF'         => 0,
        'verypoorF'     => 0,
        'excellentG'    => 0,
        'goodG'         => 0,
        'fairG'         => 0,
        'poorG'         => 0,
        'verypoorG'     => 0,
    ]);

    LaunionLecturerBEvaluation::create([
        'date'      => $input['date'],
        'lecturer'  => strtoupper($input['lecturer']),
        'branch'    => $this->branch,
        'program'   => $input['program'],
        'section'   => $input['section'],
        'class'     => $class[1],
        'aka_class' => $class[0],
        'subject'   => $subject[1],
        'aka_subject'=>$subject[0],
        'review_ambassador'=>strtoupper($input['ambassador']),
        'excellentH'    => 0,
        'goodH'         => 0,
        'fairH'         => 0,
        'poorH'         => 0,
        'verypoorH'     => 0,
        'excellentI'    => 0,
        'goodI'         => 0,
        'fairI'         => 0,
        'poorI'         => 0,
        'verypoorI'     => 0,
        'excellentJ'    => 0,
        'goodJ'         => 0,
        'fairJ'         => 0,
        'poorJ'         => 0,
        'verypoorJ'     => 0,
        'excellentK'    => 0,
        'goodK'         => 0,
        'fairK'         => 0,
        'poorK'         => 0,
        'verypoorK'     => 0,
        'excellentL'    => 0,
        'goodL'         => 0,
        'fairL'         => 0,
        'poorL'         => 0,
        'verypoorL'     => 0,
        'excellentM'    => 0,
        'goodM'         => 0,
        'fairM'         => 0,
        'poorM'         => 0,
        'verypoorM'     => 0,
        'excellentN'    => 0,
        'goodN'         => 0,
        'fairN'         => 0,
        'poorN'         => 0,
        'verypoorN'     => 0,
        'excellentO'    => 0,
        'goodO'         => 0,
        'fairO'         => 0,
        'poorO'         => 0,
        'verypoorO'     => 0,
        'excellentP'    => 0,
        'goodP'         => 0,
        'fairP'         => 0,
        'poorP'         => 0,
        'verypoorP'     => 0,
        'excellentQ'    => 0,
        'goodQ'         => 0,
        'fairQ'         => 0,
        'poorQ'         => 0,
        'verypoorQ'     => 0,
    ]);

    $validate = Section::where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->where('branch','=',$this->branch)
        ->first();
    if($validate == null){
        Section::create([
            'branch'        =>  $this->branch,
            'program'       =>  $input['program'],
            'aka_class'     =>  $class[0],
            'aka_subject'   =>  $subject[0],
            'section'       =>  $input['section'],
        ]);
    }
    Alert::success('Success!', 'New lecturer has been added.');
    return redirect ($this->sbranch.'/add-lecturer');
}
}

public function insert_eval(Request $request){

    $input = $request->except(['_token']); 
    
    $id = $input['id'];
    $qA = $input['qA'];
    $qB = $input['qB'];
    $qC = $input['qC'];
    $qD = $input['qD'];
    $qE = $input['qE'];
    $qF = $input['qF'];
    $qG = $input['qG'];
    $qH = $input['qH'];
    $qI = $input['qI'];
    $qJ = $input['qJ'];
    $qK = $input['qK'];
    $qL = $input['qL'];
    $qM = $input['qM'];
    $qN = $input['qN'];
    $qO = $input['qO'];
    $qP = $input['qP'];
    $qQ = $input['qQ'];

    $evalA = LaunionLecturerAEvaluation::find($id);
    $evalA->$qA += 1;
    $evalA->$qB += 1;
    $evalA->$qC += 1;
    $evalA->$qD += 1;
    $evalA->$qE += 1;
    $evalA->$qF += 1;
    $evalA->$qG += 1;
    $evalA->save();

    $evalB = LaunionLecturerBEvaluation::find($id);
    $evalB->$qH += 1;
    $evalB->$qI += 1;
    $evalB->$qJ += 1;
    $evalB->$qK += 1;
    $evalB->$qL += 1;
    $evalB->$qM += 1;
    $evalB->$qN += 1;
    $evalB->$qO += 1;
    $evalB->$qP += 1;
    $evalB->$qQ += 1;
    $evalB->save();

    LaunionComment::create([
        'name'      =>  $input['student'],
        'lecturer'  =>  $input['lecturer'],
        'like'      =>  $input['like'],
        'dislike'   =>  $input['dislike'],
        'comment'   =>  $input['comment'],
        'branch'    =>  $this->branch,
        'program'   =>  $input['program'],
        'section'   =>  $input['section'],
        'class'     =>  $input['class'],
        'subject'   =>  $input['subject'],
        'date'      =>  $input['date'],
    ]);

    return response()->json([
            'success' => true,
            'message' => '1 has been lecturer has been evaluated',
        ]);

}

public function eval_lec(){

    $program = Program::all();

    return view ('member.evaluate-lecturer')->with('program',$program);;
}

public function lec_eval(){

    $program = Program::all();
    $comment = LaunionComment::all();
    return view ('member.lecturer-evaluation')->with('program',$program)->with('comment',$comment);
}

public function clear_lecturers(Request $request){

    $input = $request->except(['_token']);

    LaunionLecturerAEvaluation::truncate();
    LaunionLecturerBEvaluation::truncate();
    LaunionComment::truncate();
    Section::where('branch','=','Launion')->delete();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}





public function today(){

    $branch= $this->branch; 
    $date = date('M-d-Y');

    $let_sale = LaunionS1Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid');

    $let_book = LaunionBooksSale::where('program','=','LET')->where('date','=',$date)->sum('amount');

    $nle_sale = LaunionS1Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid');

    $nle_book = LaunionBooksSale::where('program','=','NLE')->where('date','=',$date)->sum('amount');

    $crim_sale = LaunionS1Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid');

    $crim_book = LaunionBooksSale::where('program','=','Criminology')->where('date','=',$date)->sum('amount');

    $civil_sale = LaunionS1Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid');

    $civil_book = LaunionBooksSale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount');

    $psyc_sale = LaunionS1Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid');

    $psyc_book = LaunionBooksSale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount');

    $nclex_sale = LaunionS1Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid');

    $nclex_book = LaunionBooksSale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount');

    $ielts_sale = LaunionS1Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid');

    $ielts_book = LaunionBooksSale::where('program','=','IELTS')->where('date','=',$date)->sum('amount');

    $social_sale = LaunionS1Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid');

    $social_book = LaunionBooksSale::where('program','=','Social Work')->where('date','=',$date)->sum('amount');

    $agri_sale = LaunionS1Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid');

    $agri_book = LaunionBooksSale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount');

    $mid_sale = LaunionS1Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid');

    $mid_book = LaunionBooksSale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount');

    $online_sale = LaunionS1Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid');

    $online_book = LaunionBooksSale::where('program','=','Online Only')->where('date','=',$date)->sum('amount');

    return view ('member.today-report')->with('branch',$branch)->with('date',$date)
    ->with('let_sale',$let_sale)->with('let_book',$let_book)
    ->with('nle_sale',$nle_sale)->with('nle_book',$nle_book)
    ->with('crim_sale',$crim_sale)->with('crim_book',$crim_book)
    ->with('civil_sale',$civil_sale)->with('civil_book',$civil_book)
    ->with('psyc_sale',$psyc_sale)->with('psyc_book',$psyc_book)
    ->with('nclex_sale',$nclex_sale)->with('nclex_book',$nclex_book)
    ->with('ielts_sale',$ielts_sale)->with('ielts_book',$ielts_book)
    ->with('social_sale',$social_sale)->with('social_book',$social_book)
    ->with('agri_sale',$agri_sale)->with('agri_book',$agri_book)
    ->with('mid_sale',$mid_sale)->with('mid_book',$mid_book)
    ->with('online_sale',$online_sale)->with('online_book',$online_book);

}

public function yesterday(){
 
 $branch= $this->branch; 
    $date = date('M-d-Y',strtotime("-1 days"));


    $let_sale = LaunionS1Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid');

    $let_book = LaunionBooksSale::where('program','=','LET')->where('date','=',$date)->sum('amount');

    $nle_sale = LaunionS1Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid');

    $nle_book = LaunionBooksSale::where('program','=','NLE')->where('date','=',$date)->sum('amount');

    $crim_sale = LaunionS1Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid');

    $crim_book = LaunionBooksSale::where('program','=','Criminology')->where('date','=',$date)->sum('amount');

    $civil_sale = LaunionS1Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid');

    $civil_book = LaunionBooksSale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount');

    $psyc_sale = LaunionS1Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid');

    $psyc_book = LaunionBooksSale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount');

    $nclex_sale = LaunionS1Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid');

    $nclex_book = LaunionBooksSale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount');

    $ielts_sale = LaunionS1Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid');

    $ielts_book = LaunionBooksSale::where('program','=','IELTS')->where('date','=',$date)->sum('amount');

    $social_sale = LaunionS1Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid');

    $social_book = LaunionBooksSale::where('program','=','Social Work')->where('date','=',$date)->sum('amount');

    $agri_sale = LaunionS1Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid');

    $agri_book = LaunionBooksSale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount');

    $mid_sale = LaunionS1Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid');

    $mid_book = LaunionBooksSale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount');

    $online_sale = LaunionS1Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid') + LaunionS2Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid');

    $online_book = LaunionBooksSale::where('program','=','Online Only')->where('date','=',$date)->sum('amount');

    return view ('member.yesterday-report')->with('branch',$branch)->with('date',$date)
    ->with('let_sale',$let_sale)->with('let_book',$let_book)
    ->with('nle_sale',$nle_sale)->with('nle_book',$nle_book)
    ->with('crim_sale',$crim_sale)->with('crim_book',$crim_book)
    ->with('civil_sale',$civil_sale)->with('civil_book',$civil_book)
    ->with('psyc_sale',$psyc_sale)->with('psyc_book',$psyc_book)
    ->with('nclex_sale',$nclex_sale)->with('nclex_book',$nclex_book)
    ->with('ielts_sale',$ielts_sale)->with('ielts_book',$ielts_book)
    ->with('social_sale',$social_sale)->with('social_book',$social_book)
    ->with('agri_sale',$agri_sale)->with('agri_book',$agri_book)
    ->with('mid_sale',$mid_sale)->with('mid_book',$mid_book)
    ->with('online_sale',$online_sale)->with('online_book',$online_book);

}

public function budget_record(){

    $branch= $this->branch; 
    $budget = LaunionBudget::all();

    return view ('member.budget')->with('budget',$budget)->with('branch',$branch);
}

public function clear_budget(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_budgets')->truncate();


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function delete_sale1($id){

    $val = LaunionS1Sale::where('id','=',$id)->value('amount_paid');
    $ini = LaunionS1Cash::where('id','=','1')->value('cash');
    $bal = LaunionS1Sale::where('id','=',$id)->value('balance');
    $name = LaunionS1Sale::where('id','=',$id)->value('name');
    $id1 = LaunionS1Sale::where('id','=', $id)->value('student_id');
    //program
    $program1 = LaunionS1Sale::where('id','=',$id)->value('program');
    $program = Program::where('program_name','=',$program1)->value('aka');
    $studentIds1 = DB::table($this->branch.'_'. $program)->where('id','=', $id1)->value('id');

          //save student info to api
          $studentForApi =  DB::table($this->branch."_".$program)->where('id','=',$studentIds1)->where('status','=','Enrolled')->first();
        

    //date for scorecard table
    $transDate = LaunionS1Sale::where('id','=',$id)->value('date');
    
    //if retake or 1st timer
    $dis_category = LaunionS1Sale::where('id','=',$id)->value('discount_category');
   
    

    if ($bal != null){

        $receivable = LaunionReceivable::where('name','=',$name)->where('season','=','Season 1')->value('balance');
        
        $new_bal = $receivable + $val;

        LaunionReceivable::where('name','=',$name)->where('season','=','Season 1')->update([
        'balance' => $new_bal,
        ]);
         
    }
    else{
        $reservation = LaunionReservation::where('name','=',$name)->value('reservation_fee');

        $new_bal = $reservation - $val;

        $reservation = LaunionReservation::where('name','=',$name)->update([
            'reservation_fee' => $new_bal,
        ]);///end for reservation
    }//end of else 

   
    if($val <= $ini){

        $sale = LaunionS1Sale::findorfail($id);
        $sale->delete();
    
        $total = $ini - $val;
    
        LaunionS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

     



    //update score card
    //check student overall transaction in launionS1sale
    $student_allBal = LaunionS1Sale::where('student_id','=',$studentIds1)->count();

    
  
    //major
    $studentInfo = DB::table($this->branch.'_'. $program)->where('id','=', $studentIds1)->value('major');

    //end of check if retaker or first timer
    $remark = "1st timer";
        if (strpos(strtolower($dis_category), 'bounce') !== false || strpos(strtolower($dis_category), 'retaker') !== false|| strpos(strtolower($dis_category), 'retake') !== false) {
            $remark = 'retake';
    }//end of check if retaker or first timer


    //if  stud_allball == 0
    if($student_allBal === 0){

    
        LaunionReceivable::where('name','=',$name)->where('season','=','Season 1')->update([
            'balance' => 0,
            ]);
    //major
    DB::table($this->branch.'_'. $program)->where('id','=', $studentIds1)->update([
        'status' => null,
    ]);
    
   
// return $program1 . " " . $program . " " . $studentInfo;
        if($program1 == "LET"){
            $prev =  LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
            $score = $prev[strtolower($studentInfo)] - 1;
            
                LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                    strtolower($studentInfo)  => $score,
            ]);
        }
        else{

            if($remark == "retake") {
                $prev =  LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_retakers'] - 1;
                    LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_retakers' => $score,
                ]);
            }else{
                $prev =  LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_1stTimers'] - 1;
                    LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_1stTimers' => $score,
                ]);
            }
        }
   

  //login to api
  $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
  $client2  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
  $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
      'form_params' => [
          "email"=>"admin@main.cbrc.solutions",
          "password"=>"main@dmin"
      ]
  ]);
//insert data to api

  if ($res->getStatusCode() == 200) { // 200 OK
      $response_data = json_decode($res->getBody()->getContents());
 
      
          $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
          ,[
          'form_params' => [
              
            "_method" => "PUT",
            "BranchStdID" => $studentForApi->id,
            "Branch_Name" =>lcfirst($this->sbranch),
            "cbrc_id" =>"",
            "Lastname" =>$studentForApi->last_name,
            "Firstname" =>$studentForApi->first_name,
            "Middlename" =>$studentForApi->middle_name,
            "Birthday" =>$studentForApi->birthdate,
            "Contact_Number" =>$studentForApi->contact_no,
            "Address" =>$studentForApi->address,
            "Email" =>$studentForApi->email,
            "Username" =>$studentForApi->username,
            "Password" =>$studentForApi->password,
            "School" =>$studentForApi->school,
            "Program" =>$studentForApi->program,
            "Section" =>$studentForApi->section,
            "Major" =>$studentForApi->major,
            "Take" =>$studentForApi->take,
            "Noa" =>$studentForApi->noa_no,
            "Category" =>$studentForApi->category,
            "Status" =>"",
            "Contact_Person" =>$studentForApi->contact_person,
            "Contact_Details" =>$studentForApi->contact_details,
            "Facilitation" =>"",
            "Season" =>$studentForApi->season,
            "Year" =>$studentForApi->year
      
          ]
          ]);


          $updatePayment = $client2->request('POST', 'https://cbrc.solutions/api/main/payment?token='.$response_data->access_token
          ,[
          'form_params' => [
              
                      "_method" => "PUT",
                      "Branch" => ucwords($this->sbranch),
                      "Season" => $studentForApi->season,
                      "Date" => "",
                      "Name" => "",
                      "Stdid" =>  $studentForApi->id,
                      "Program" => $studentForApi->program,
                      "Category" => "",
                      "Discount_category" => "",
                      "Tuition_fee" =>  0.00,
                      "Facilitation_fee" => 0.00,
                      "year" => $studentForApi->year
                      ]
          ]);
          
          }


        }//end of if  student balance == 0
//end of update scorecard
  



    Alert::success('Success!', '1 sales record has been deleted.');
    }//end of if $val <= $ini
    else{
    Alert::error('Failed!', 'Insufficient Cash.');

    }

    

    return redirect ($this->sbranch.'/sales-enrollee');
}

public function delete_sale2($id){
    $val = LaunionS2Sale::where('id','=',$id)->value('amount_paid');
    $ini = LaunionS2Cash::where('id','=','1')->value('cash');
    $bal = LaunionS2Sale::where('id','=',$id)->value('balance');
    $name = LaunionS2Sale::where('id','=',$id)->value('name');
    $id1 = LaunionS2Sale::where('id','=', $id)->value('student_id');
    //program
    $program1 = LaunionS2Sale::where('id','=',$id)->value('program');
    $program = Program::where('program_name','=',$program1)->value('aka');
    $studentIdS2 = DB::table($this->branch.'_'. $program)->where('id','=', $id1)->value('id');

          //save student info to api
          $studentForApi =  DB::table($this->branch."_".$program)->where('id','=',$studentIdS2)->where('status','=','Enrolled')->first();
        

    //date for scorecard table
    $transDate = LaunionS2Sale::where('id','=',$id)->value('date');
    
    //if retake or 1st timer
    $dis_category = LaunionS2Sale::where('id','=',$id)->value('discount_category');
   
    

    if ($bal != null){

        $receivable = LaunionReceivable::where('name','=',$name)->where('season','=','Season 2')->value('balance');
        
        $new_bal = $receivable + $val;

        LaunionReceivable::where('name','=',$name)->where('season','=','Season 2')->update([
        'balance' => $new_bal,
        ]);
         
    }
    else{
        $reservation = LaunionReservation::where('name','=',$name)->value('reservation_fee');

        $new_bal = $reservation - $val;

        $reservation = LaunionReservation::where('name','=',$name)->update([
            'reservation_fee' => $new_bal,
        ]);///end for reservation
    }//end of else 

   
    if($val <= $ini){

        $sale = LaunionS2Sale::findorfail($id);
        $sale->delete();
    
        $total = $ini - $val;
    
        LaunionS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

     



    //update score card
    //check student overall transaction in launionS2sale
    $student_allBal = LaunionS2Sale::where('student_id','=',$studentIdS2)->count();

    
  
    //major
    $studentInfo = DB::table($this->branch.'_'. $program)->where('id','=', $studentIdS2)->value('major');

    //end of check if retaker or first timer
    $remark = "1st timer";
        if (strpos(strtolower($dis_category), 'bounce') !== false || strpos(strtolower($dis_category), 'retaker') !== false|| strpos(strtolower($dis_category), 'retake') !== false) {
            $remark = 'retake';
    }//end of check if retaker or first timer


    //if  stud_allball == 0
    if($student_allBal === 0){

    
        LaunionReceivable::where('name','=',$name)->where('season','=','Season 2')->update([
            'balance' => 0,
            ]);
    //major
    DB::table($this->branch.'_'. $program)->where('id','=', $studentIdS2)->update([
        'status' => null,
    ]);
    
   
// return $program1 . " " . $program . " " . $studentInfo;
        if($program1 == "LET"){
            $prev =  LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
            $score = $prev[strtolower($studentInfo)] - 1;
            
                LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                    strtolower($studentInfo)  => $score,
            ]);
        }
        else{

            if($remark == "retake") {
                $prev =  LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_retakers'] - 1;
                    LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_retakers' => $score,
                ]);
            }else{
                $prev =  LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_1stTimers'] - 1;
                    LaunionScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_1stTimers' => $score,
                ]);
            }
        }
   

  //login to api
  $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
  $client2  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
  $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
      'form_params' => [
          "email"=>"admin@main.cbrc.solutions",
          "password"=>"main@dmin"
      ]
  ]);
//insert data to api

  if ($res->getStatusCode() == 200) { // 200 OK
      $response_data = json_decode($res->getBody()->getContents());
 
      
          $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
          ,[
          'form_params' => [
              
            "_method" => "PUT",
            "BranchStdID" => $studentForApi->id,
            "Branch_Name" =>lcfirst($this->sbranch),
            "cbrc_id" =>"",
            "Lastname" =>$studentForApi->last_name,
            "Firstname" =>$studentForApi->first_name,
            "Middlename" =>$studentForApi->middle_name,
            "Birthday" =>$studentForApi->birthdate,
            "Contact_Number" =>$studentForApi->contact_no,
            "Address" =>$studentForApi->address,
            "Email" =>$studentForApi->email,
            "Username" =>$studentForApi->username,
            "Password" =>$studentForApi->password,
            "School" =>$studentForApi->school,
            "Program" =>$studentForApi->program,
            "Section" =>$studentForApi->section,
            "Major" =>$studentForApi->major,
            "Take" =>$studentForApi->take,
            "Noa" =>$studentForApi->noa_no,
            "Category" =>$studentForApi->category,
            "Status" =>"",
            "Contact_Person" =>$studentForApi->contact_person,
            "Contact_Details" =>$studentForApi->contact_details,
            "Facilitation" =>"",
            "Season" =>$studentForApi->season,
            "Year" =>$studentForApi->year
      
          ]
          ]);


          $updatePayment = $client2->request('POST', 'https://cbrc.solutions/api/main/payment?token='.$response_data->access_token
          ,[
          'form_params' => [
              
                      "_method" => "PUT",
                      "Branch" => ucwords($this->sbranch),
                      "Season" => $studentForApi->season,
                      "Date" => "",
                      "Name" => "",
                      "Stdid" =>  $studentForApi->id,
                      "Program" => $studentForApi->program,
                      "Category" => "",
                      "Discount_category" => "",
                      "Tuition_fee" =>  0.00,
                      "Facilitation_fee" => 0.00,
                      "year" => $studentForApi->year
                      ]
          ]);
          
          }


        }//end of if  student balance == 0
//end of update scorecard
  



    Alert::success('Success!', '1 sales record has been deleted.');
    }//end of if $val <= $ini
    else{
    Alert::error('Failed!', 'Insufficient Cash.');

    }

    

    return redirect ($this->sbranch.'/sales-enrollee');
    
}
public function csv_enrollee(Request $request){

            $input = $request->except(['_token']);
            $program = $input['program'];
            $prog = "";
            if($program == 'LET'){
            $prog = 'let';
            $program = 'lets';
            $db = 'LaunionLet';
            }
            if($program == 'NLE'){
                $prog = 'nle';
                $program = 'nles';
                $db = 'LaunionNle';
            }
            if($program == 'Criminology'){
                $prog = 'crim';
                $program = 'crims';
                $db = 'LaunionCrim';
            }
            if($program == 'Civil Service'){
                $prog = 'civil';
                $program = 'civils';
                $db = 'LaunionCivil';
            }

            if($program == 'Psychometrician'){
                $prog = 'psyc';
                $program = 'psycs';
                $db = 'LaunionPsyc';
            }
            if($program == 'NCLEX'){
                $prog = 'nclex';
                $program = 'nclexes';
                $db = 'LaunionPsyc';
            }
            if($program == 'IELTS'){
                $prog = 'ielt';
                $program = 'ielts';
                $db = 'LaunionIelt';
            }
            if($program == 'Social Work'){
                $prog = 'social';
                $program = 'socials';
                $db = 'LaunionSocial';
            }
            if($program == 'Agriculture'){
                $prog = 'agri';
                $program = 'agris';
                $db = 'LaunionAgri';
            }
            if($program == 'Midwifery'){
                $prog = 'mid';
                $program = 'mids';
                $db = 'LaunionMid';
            }
            if($program == 'Online Only'){
                $prog = 'online';
                $program = 'onlines';
                $db = 'LaunionOnline';
            }



            $input['csv_enrollee'] = null;
            $csv="";
            $file = "";
            if($request->hasFile('csv'))
            {
            $extension = $request->csv;
            $extension = $request->csv->getClientOriginalExtension(); // getting excel extension
            $uploader = Auth::user()->branch;
            $input['csv'] = 'uploads/csv/'.''.$uploader;
            $csv = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
            $request->csv->move($input['csv'], $csv); 

            $file = $input['csv']."/".$csv;
             }
            else{
                $file = Auth::user()->csv;
            }

            if ($input['program'] == 'LET'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionLet ();
                    $cbrc_id = LaunionLet::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){
                     
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                    else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'NLE'){
            if (($handle = fopen (public_path () .'/uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionNle ();
                    $cbrc_id = LaunionNle::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }

                fclose ( $handle );
            }
        }


        if ($input['program'] == 'Criminology'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionCrim ();
                      $cbrc_id = LaunionCrim::where('cbrc_id','=',$data[0])->first();
                        if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }

                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Civil Service'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionCivil ();
                     $cbrc_id = LaunionCivil::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Psychometrician'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionPsyc ();
                    $cbrc_id = LaunionPsyc::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'NCLEX'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionNclex ();
                     $cbrc_id = LaunionNclex::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'IELTS'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionIelt ();
                     $cbrc_id = LaunionIelt::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Social Work'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionSocial ();
                     $cbrc_id = LaunionSocial::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Agriculture'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionAgri ();
                       $cbrc_id = LaunionAgri::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Midwifery'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionMid ();
                      $cbrc_id = LaunionMid::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Online Only'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new LaunionOnline ();
                     $cbrc_id = LaunionOnline::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

            
            Alert::success('Success!', 'Your file has been successfully imported.');

            return redirect ()->back(); 
    }




// =================== Register New Employee =============================

public function add_employee(){
    $branch= $this->branch; 
    $program = Program::all();
    $branch_name = Branch::all();
    return view('member.add-employee')->with('branch',$branch)->with('program',$program)->with('branch_name',$branch_name);
}

//================ edit information of employee  ===================================

public function update_employee(Request $request){ 
    $input = $request->except(['_token']);    

     if ($request->hasfile('cover_image') == true) {
    // # code...

     $raw = LaunionEmployee::find($input['update_id']);
     $oldpath = $raw->cover_image;

   
    File::Delete('cover_images',$oldpath);
    //new image upload
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = $filename. '_'.time().'.webp';
            $path = $request->file('cover_image')->move('cover_images',$fileNameToStore);


            DB::table($this->branch.'_employees')->where('id','=',$input['update_id'])->update([

            'last_name'             => $input['last_name'],
            'first_name'            => $input['first_name'],
            'middle_name'           => $input['middle_name'],
            'birthdate'             =>  $input['birthdate'],
            'gender'                => $input['gender'],
            'status'                => $input['status'],
            'address'               => $input['address'],
            'email'                 => $input['email'],
            'contact_no'            => $input['contact_no'],
            'contact_person'        => $input['contact_person'],
            'contact_details'       => $input['contact_details'],
            'position'              =>$input['position'],
            'employment_status'     =>$input['employment_status'],
            'rate'                  => $input['rate'],
            'sss'                   => $input['sss'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'],
            'branch_name'           => $input['branch_name'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'],
            'cover_image'           => $fileNameToStore

            ]);

            Alert::success('Success!', 'Updated employee Record');

    }

    else {
            DB::table($this->branch.'_employees')->where('id','=',$input['update_id'])->update([

            'last_name'             => $input['last_name'],
            'first_name'            => $input['first_name'],
            'middle_name'           => $input['middle_name'],
            'birthdate'             =>  $input['birthdate'],
            'gender'                => $input['gender'],
            'status'                => $input['status'],
            'address'               => $input['address'],
            'email'                 => $input['email'],
            'contact_no'            => $input['contact_no'],
            'contact_person'        => $input['contact_person'],
            'contact_details'       => $input['contact_details'],
            'employment_status'     =>$input['employment_status'],
            'position'              =>$input['position'],
            'rate'                  => $input['rate'],
            'sss'                   => $input['sss'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'],
            'branch_name'           => $input['branch_name'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'] 

        ]); 

        Alert::success('Success!', 'Updated employee Record');
     
    }
}

// =================== View Employeee Record =================================
public function employee_record(){
        $employee = LaunionEmployee::all();
        $branch_name = Branch::all();
        return view('member.employee-record')->with('employee',$employee)->with('branch_name',$branch_name);
}


// ================ insert data of employee ======================================  

public function insert_employee(Request $request) {
    $input = $request->except(['_token']); 
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = $filename. '_'.time().'.webp';
            $path = $request->file('cover_image')->move('cover_images',$fileNameToStore);
        }
        else{
            $path = 'NO IMAGE';
        }
            $data = new LaunionEmployee;
            $data->employee_no = $input['employee_no'];
            $data->last_name = strtoupper($input['last_name']);
            $data->first_name = strtoupper($input['first_name']);
            $data->middle_name = strtoupper($input['middle_name']);
            $data->birthdate = $input['birthdate'];
            $data->gender = strtoupper($input['gender']);
            $data->status = strtoupper($input['status']);
            $data->address = strtoupper($input['address']);
            $data->email = strtoupper($input['email']);
            $data->contact_no = $input['contact_no'];
            $data->contact_person = strtoupper($input['contact_person']);
            $data->contact_details = strtoupper($input['contact_details']);
            $data->position = strtoupper($input['position']);
            $data->employment_status = strtoupper($input['employmentStatus']);
            $data->rate = $input['rate'];
            $data->date_hired = $input['date_hired'];
            $data->sss = $input['sss'];
            $data->phil_health = $input['phil_health'];
            $data->pag_ibig = $input['pag_ibig'];
            $data->tin = $input['tin']; 
            $data->branch_name = $input['branch_name']; 
            $data->cover_image = $fileNameToStore;
            $data->save();
            if ($data->save()){
               $test =  LaunionEmployee::latest()->first();
               return response()->json($test);
                Alert::success('Success!', 'Added employee Record');

    }
}

// ============================= delete Employee Record  ==================================

    public function delete_employee($id) {
    $data = LaunionEmployee::findorfail($id);

    $img = $data->cover_image;
    File::delete('cover_images/'.$img);
    $data->delete();
    Alert::success('Success!', ' Successfully record deleted');
    return redirect()->back();
    }

    //======================= View employee Member Dashboard  =====================

    public function view_employee(){
         $employee = LaunionEmployee::all();
        return view('member.view-employee-record')->with('employee',$employee);
    
    } 
    //====================== Student Generate Id's ===================================
    public function student_id(){
         $branch= $this->branch; 
        $program = Program::all();


        return view('admin.student-id')->with('branch',$branch)->with('program',$program);
    }


    // =================== Bulletin Board ===============================================
        public function Bulletin(){  
            return view('admin.bulletin');

        }

// =================== taskHistory ====================================
         public function taskHistory(){
               return view ('admin.taskHistory'); 
        }

    //====================== Book transfer and purchase  ===============================
        public function fetch_branches($bn){
            $branch = Branch::where('branch_name','!=',$bn)->get();
            return response()->json($branch);
        }

    //======== mike ScoreCard and Finacial Report Controller  ==============================



public function scorecard($season){
    
    $scorecard = LaunionScoreCards::all()->where('season',$season);
    $scorecard_last = LaunionScoreCards::where('season',$season)->orderby('id','desc')->first();
    //this is sunday variable
    $Sun = date("D" , strtotime('2/17/2019'));

    $weekly_beed = 0;
    $weekly_bsed = 0;
    $weekly_let = 0;
    $weekly_math = 0;
    $weekly_tle = 0;
    $weekly_english = 0;
    $weekly_filipino = 0;
    $weekly_biosci = 0;
    $weekly_physci = 0;
    $weekly_socsci = 0;
    $weekly_mapeh = 0;
    $weekly_values = 0;
    $weekly_afa = 0;
    $weekly_ufo = 0;

    $weekly_tot_nle = 0;
    $weekly_tot_nle_1stTimers = 0;
    $weekly_tot_nle_retakers = 0;
    
    $weekly_tot_crim = 0;
    $weekly_tot_crim_1stTimers = 0;
    $weekly_tot_crim_retakers = 0;
    
    $weekly_tot_civil = 0;
    $weekly_tot_civil_1stTimers = 0;
    $weekly_tot_civil_retakers = 0;
    
    $weekly_tot_psyc = 0;
    $weekly_tot_psyc_1stTimers = 0;
    $weekly_tot_psyc_retakers = 0;
    
    $weekly_tot_nclex = 0;
    $weekly_tot_nclex_1stTimers = 0;
    $weekly_tot_nclex_retakers = 0;
    
    $weekly_tot_ielts = 0;
    $weekly_tot_ielts_1stTimers = 0;
    $weekly_tot_ielts_retakers = 0;
    
    $weekly_tot_social = 0;
    $weekly_tot_social_1stTimers = 0;
    $weekly_tot_social_retakers = 0;
    
    $weekly_tot_agri = 0;
    $weekly_tot_agri_1stTimers = 0;
    $weekly_tot_agri_retakers = 0;
    
    $weekly_tot_mid = 0;
    $weekly_tot_mid_1stTimers = 0;
    $weekly_tot_mid_retakers = 0;
    
    $weekly_tot_online = 0;
    $weekly_tot_online_1stTimers = 0;
    $weekly_tot_online_retakers = 0;

    $tot_beed = 0;
    $fin_tot_let = 0;
    $tot_bsed = 0;
    $fin_tot_bsed = 0;
    $tot_math = 0;
    $tot_tle = 0;
    $tot_english = 0;
    $tot_filipino= 0;
    $tot_biosci = 0;
    $tot_physci = 0;
    $tot_mapeh = 0;
    $tot_socsci = 0;
    $tot_values= 0;
    $tot_afa = 0;
    $tot_ufo = 0;
    $tot_let = 0;

    $tot_nle_1stTimers = 0;
    $tot_nle_retakers = 0;
    $tot_nle = 0;

    $tot_crim_1stTimers = 0;
    $tot_crim_retakers = 0;
    $tot_crim = 0;

    $tot_civil_1stTimers = 0;
    $tot_civil_retakers = 0;
    $tot_civil = 0;

    $tot_psyc_1stTimers = 0;
    $tot_psyc_retakers = 0;
    $tot_psyc = 0;

    $tot_nclex_1stTimers = 0;
    $tot_nclex_retakers = 0;
    $tot_nclex = 0;

    $tot_ielts_1stTimers = 0;
    $tot_ielts_retakers = 0;
    $tot_ielts = 0;

    $tot_social_1stTimers = 0;
    $tot_social_retakers = 0;
    $tot_social = 0;    

    $tot_agri_1stTimers = 0;
    $tot_agri_retakers = 0;
    $tot_agri = 0;
    
    $tot_mid_1stTimers = 0;
    $tot_mid_retakers = 0;
    $tot_mid = 0;
    
    $tot_online_1stTimers = 0;
    $tot_online_retakers = 0;
    $tot_online = 0;

    foreach($scorecard as $sub_score){

        
        $tot_beed = $tot_beed + $sub_score['beed'];
        $tot_math = $tot_math + $sub_score['math'];
        $tot_tle = $tot_tle + $sub_score['tle'];
        $tot_english = $tot_english + $sub_score['english'];
        $tot_filipino = $tot_filipino + $sub_score['filipino'];
        $tot_biosci = $tot_biosci + $sub_score['biosci'];
        $tot_physci = $tot_physci + $sub_score['physci'];
        $tot_mapeh = $tot_mapeh + $sub_score['mapeh'];
        $tot_socsci = $tot_socsci + $sub_score['socsci'];
        $tot_values = $tot_values + $sub_score['values'];
        $tot_afa = $tot_afa + $sub_score['afa'];
        $tot_ufo = $tot_ufo + $sub_score['ufo'];
        
        $tot_bsed =     $sub_score['math']    +
                        $sub_score['tle']     +
                        $sub_score['english'] +
                        $sub_score['filipino']+
                        $sub_score['biosci']  +
                        $sub_score['physci']  +
                        $sub_score['socsci']  +
                        $sub_score['mapeh']   +
                        $sub_score['values']  +
                        $sub_score['afa']     +
                        $sub_score['ufo'];


        $tot_let =  $sub_score['beed'] + $tot_bsed;

        $fin_tot_bsed = $fin_tot_bsed + $tot_bsed;
        $fin_tot_let = $fin_tot_let +  $tot_let;

        $tot_nle_1stTimers += $sub_score['nles_1stTimers'];
        $tot_nle_retakers += $sub_score['nles_retakers'];
        $tot_nle = $tot_nle_1stTimers + $tot_nle_retakers;

        $tot_crim_1stTimers += $sub_score['crims_1stTimers'];
        $tot_crim_retakers += $sub_score['crims_retakers'];
        $tot_crim = $tot_crim_1stTimers + $tot_crim_retakers;

        $tot_civil_1stTimers += $sub_score['civils_1stTimers'];
        $tot_civil_retakers += $sub_score['civils_retakers'];
        $tot_civil = $tot_civil_1stTimers + $tot_civil_retakers;

        $tot_psyc_1stTimers += $sub_score['psycs_1stTimers'];
        $tot_psyc_retakers += $sub_score['psycs_retakers'];
        $tot_psyc = $tot_psyc_1stTimers + $tot_psyc_retakers;

        $tot_nclex_1stTimers += $sub_score['nclexes_1stTimers'];
        $tot_nclex_retakers += $sub_score['nclexes_retakers'];
        $tot_nclex = $tot_nclex_1stTimers + $tot_nclex_retakers;

        $tot_ielts_1stTimers += $sub_score['ielts_1stTimers'];
        $tot_ielts_retakers += $sub_score['ielts_retakers'];
        $tot_ielts = $tot_ielts_1stTimers + $tot_ielts_retakers;

        $tot_social_1stTimers += $sub_score['socials_1stTimers'];
        $tot_social_retakers += $sub_score['socials_retakers'];
        $tot_social = $tot_social_1stTimers + $tot_social_retakers;

        $tot_agri_1stTimers += $sub_score['agris_1stTimers'];
        $tot_agri_retakers += $sub_score['agris_retakers'];
        $tot_agri = $tot_agri_1stTimers + $tot_agri_retakers;

        $tot_mid_1stTimers += $sub_score['mids_1stTimers'];
        $tot_mid_retakers += $sub_score['mids_retakers'];
        $tot_mid = $tot_mid_1stTimers + $tot_mid_retakers;

        $tot_online_1stTimers += $sub_score['onlines_1stTimers'];
        $tot_online_retakers += $sub_score['onlines_retakers'];
        $tot_online = $tot_online_1stTimers + $tot_online_retakers;
    }

    $branch= $this->branch; 
    $date = date('M-d-Y');

   

    return view ('member.scorecard-season'.$season)
                ->with('scorecard',$scorecard)
                ->with('scorecard_last',$scorecard_last)
                ->with('branch',$branch)
                ->with('date',$date)
                ->with('Sun',$Sun)
                ->with('season',$season)
                

                ->with('weekly_beed',$weekly_beed)
                ->with('weekly_bsed',$weekly_bsed)
                ->with('weekly_let',$weekly_let)
                ->with('weekly_math',$weekly_math)
                ->with('weekly_tle',$weekly_tle)
                ->with('weekly_english',$weekly_english)
                ->with('weekly_filipino',$weekly_filipino)
                ->with('weekly_biosci',$weekly_biosci)
                ->with('weekly_physci',$weekly_physci)
                ->with('weekly_socsci',$weekly_socsci)
                ->with('weekly_mapeh',$weekly_mapeh)
                ->with('weekly_values',$weekly_values)
                ->with('weekly_afa',$weekly_afa)
                ->with('weekly_ufo',$weekly_ufo)

                ->with('weekly_tot_nle',$weekly_tot_nle)
                ->with('weekly_tot_nle_1stTimers',$weekly_tot_nle_1stTimers)
                ->with('weekly_tot_nle_retakers',$weekly_tot_nle_retakers)

                ->with('weekly_tot_crim',$weekly_tot_crim)
                ->with('weekly_tot_crim_1stTimers',$weekly_tot_crim_1stTimers)
                ->with('weekly_tot_crim_retakers',$weekly_tot_crim_retakers)

                ->with('weekly_ufo',$weekly_tot_civil)
                ->with('weekly_tot_civil_1stTimers',$weekly_tot_civil_1stTimers)
                ->with('weekly_tot_civil_retakers',$weekly_tot_civil_retakers)

                ->with('weekly_tot_psyc',$weekly_tot_psyc)
                ->with('weekly_tot_psyc_1stTimers',$weekly_tot_psyc_1stTimers)
                ->with('weekly_tot_psyc_retakers',$weekly_tot_psyc_retakers)

                ->with('weekly_tot_nclex',$weekly_tot_nclex)
                ->with('weekly_tot_nclex_1stTimers',$weekly_tot_nclex_1stTimers)
                ->with('weekly_tot_nclex_retakers',$weekly_tot_nclex_retakers)

                ->with('weekly_tot_ielts',$weekly_tot_ielts)
                ->with('weekly_tot_ielts_1stTimers',$weekly_tot_ielts_1stTimers)
                ->with('weekly_tot_ielts_retakers',$weekly_tot_ielts_retakers)
                
                ->with('weekly_tot_social',$weekly_tot_social)
                ->with('weekly_tot_social_1stTimers',$weekly_tot_social_1stTimers)
                ->with('weekly_tot_social_retakers',$weekly_tot_social_retakers)
                
                ->with('weekly_tot_agri',$weekly_tot_agri)
                ->with('weekly_tot_agri_1stTimers',$weekly_tot_agri_1stTimers)
                ->with('weekly_tot_agri_retakers',$weekly_tot_agri_retakers)
                
                ->with('weekly_tot_mid',$weekly_tot_mid)
                ->with('weekly_tot_mid_1stTimers',$weekly_tot_mid_1stTimers)
                ->with('weekly_tot_mid_retakers',$weekly_tot_mid_retakers)
                
                ->with('weekly_tot_online',$weekly_tot_online)
                ->with('weekly_tot_online_1stTimers',$weekly_tot_online_1stTimers)
                ->with('weekly_tot_online_retakers',$weekly_tot_online_retakers)
                
                ->with('tot_beed',$tot_beed)
                ->with('fin_tot_let',$fin_tot_let)
                ->with('tot_bsed',$tot_bsed)
                ->with('fin_tot_bsed',$fin_tot_bsed)
                ->with('tot_math',$tot_math)
                ->with('tot_tle',$tot_tle)
                ->with('tot_english',$tot_english)
                ->with('tot_filipino',$tot_filipino)
                ->with('tot_biosci',$tot_biosci)
                ->with('tot_physci',$tot_physci)
                ->with('tot_mapeh',$tot_mapeh)
                ->with('tot_socsci',$tot_socsci)
                ->with('tot_values',$tot_values)
                ->with('tot_afa',$tot_afa)
                ->with('tot_ufo',$tot_ufo)
                ->with('tot_let',$tot_let)
                
                ->with('tot_nle_1stTimers',$tot_nle_1stTimers)
                ->with('tot_nle_retakers',$tot_nle_retakers)
                ->with('tot_nle',$tot_nle)

                ->with('tot_crim_1stTimers',$tot_crim_1stTimers)
                ->with('tot_crim_retakers',$tot_crim_retakers)
                ->with('tot_crim',$tot_crim)
                
                ->with('tot_civil_1stTimers',$tot_civil_1stTimers)
                ->with('tot_civil_retakers',$tot_civil_retakers)
                ->with('tot_civil',$tot_civil)
                
                ->with('tot_psyc_1stTimers',$tot_psyc_1stTimers)
                ->with('tot_psyc_retakers',$tot_psyc_retakers)
                ->with('tot_psyc',$tot_psyc)
                
                ->with('tot_nclex_1stTimers',$tot_nclex_1stTimers)
                ->with('tot_nclex_retakers',$tot_nclex_retakers)
                ->with('tot_nclex',$tot_nclex)
                
                ->with('tot_ielts_1stTimers',$tot_ielts_1stTimers)
                ->with('tot_ielts_retakers',$tot_ielts_retakers)
                ->with('tot_ielts',$tot_ielts)
                
                ->with('tot_social_1stTimers',$tot_social_1stTimers)
                ->with('tot_social_retakers',$tot_social_retakers)
                ->with('tot_social',$tot_social)
                
                ->with('tot_agri_1stTimers',$tot_agri_1stTimers)
                ->with('tot_agri_retakers',$tot_agri_retakers)
                ->with('tot_agri',$tot_agri)
                
                ->with('tot_mid_1stTimers',$tot_mid_1stTimers)
                ->with('tot_mid_retakers',$tot_mid_retakers)
                ->with('tot_mid',$tot_mid)

                ->with('tot_online_1stTimers',$tot_online_1stTimers)
                ->with('tot_online_retakers',$tot_online_retakers)
                ->with('tot_online',$tot_online);
}



public function financialreport(){
 //branch name
    $branch= $this->branch;
//over all total    
$revenue = 0;

//sales book
//total books
$bookNCLEX = 0;
$bookPsycho = 0;
$bookLET = 0;
$bookNLE = 0;
$bookCrim = 0;
$bookCS = 0;
$bookIELTS = 0;
$bookSW = 0;
$bookAgri = 0;
$bookMid = 0;
$bookOnline =0;

$booksSale = 0;

//total season 1 and 2  
$totSeason1 = 0;
$totSeason2 = 0;
//total season 1 and 2  

//Season 1
    $letS1 = 0;
    $nleS1 = 0;
    $crimS1 = 0;
    $civilS1 = 0;
    $psychoS1 = 0;
    $nclexS1 = 0;
    $ieltsS1 = 0;
    $socialsS1 = 0;
    $agriS1 = 0;
    $midwiferyS1 = 0;
    $onlineS1 = 0;
 
//Season 2  
    $letS2 = 0;
    $nleS2 = 0;
    $crimS2 = 0;
    $civilS2 = 0;
    $psychoS2 = 0;
    $nclexS2 = 0;
    $ieltsS2 = 0;
    $socialsS2 = 0;
    $agriS2 = 0;
    $midwiferyS2 = 0;
    $onlineS2 = 0;

//Disbursement 
    
    $lecProfFee = 0;
    $lecTax = 0;
    $lecAllow = 0;
    $lecTranspo = 0;
    $lecReimburse = 0;
    $lecAccommodation   = 0;
    $utilVenue   = 0;
    $mktingAllowance = 0;
    $mktingGasoline = 0;
    $mktingMeals = 0;
    $mktingProfFee = 0;
    $mktingTranspo = 0;
    $mktingDailyExpWaybill = 0;
    $mktingPostSignageFlyers = 0;
    $mktingGifts = 0;
    $admnsalary = 0;
    $contributionsss = 0;
    $contributionpagibig = 0;
    $contributionphilhealth = 0;
    $utilityrent = 0;
    $utilitywater = 0;
    $utilityelectricity = 0;
    $utilitysound = 0;
    $utilityinternet = 0;
    $utilitytelephone = 0;
    $dailyexpload = 0;
    $dailyexpmaintenance = 0;
    $dailyexptranspo = 0;
    $dailyexpgasoline = 0;
    $dailyexpofficeSupply = 0;
    $dailyexpgrocery = 0;
    $taxAndLicense = 0;
    $insurance = 0;
    $investOfficeEquip = 0;
    $investFurnitureAndfixture = 0;
    $dailyexpOthers = 0;
    $dailyexpMeals = 0;
    $dailyexpAllow = 0;
    $totDisbursement = 0;
    $faciFee = 0;
    $finalCoaching = 0;
    $totBookDisbursement = 0;
    $totLoanDisbursment = 0;
    //end Distribution  


    $letS1=LaunionS1Sale::where('program','=','LET')->sum('amount_paid');
    $nleS1=LaunionS1Sale::where('program','=','NLE')->sum('amount_paid');
    $crimS1=LaunionS1Sale::where('program','=','Criminology')->sum('amount_paid');
    $civilS1=LaunionS1Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psychoS1=LaunionS1Sale::where('program','=','Psychometrician')->sum('amount_paid');
    $nclexS1=LaunionS1Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ieltsS1=LaunionS1Sale::where('program','=','IELTS')->sum('amount_paid');
    $socialsS1=LaunionS1Sale::where('program','=','Social')->sum('amount_paid');
    $agriS1=LaunionS1Sale::where('program','=','Agriculture')->sum('amount_paid');
    $midwiferyS1=LaunionS1Sale::where('program','=','Midwifery')->sum('amount_paid');
    $onlineS1=LaunionS1Sale::where('program','=','Online Only')->sum('amount_paid');

    $letS2=LaunionS2Sale::where('program','=','LET')->sum('amount_paid');
    $nleS2=LaunionS2Sale::where('program','=','NLE')->sum('amount_paid');
    $crimS2=LaunionS2Sale::where('program','=','Criminology')->sum('amount_paid');
    $civilS2=LaunionS2Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psychoS2=LaunionS2Sale::where('program','=','Psychometrician')->sum('amount_paid');
    $nclexS2=LaunionS2Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ieltsS2=LaunionS2Sale::where('program','=','IELTS')->sum('amount_paid');
    $socialsS2=LaunionS2Sale::where('program','=','Social')->sum('amount_paid');
    $agriS2=LaunionS2Sale::where('program','=','Agriculture')->sum('amount_paid');
    $midwiferyS2=LaunionS2Sale::where('program','=','Midwifery')->sum('amount_paid');
    $onlineS2=LaunionS2Sale::where('program','=','Online Only')->sum('amount_paid');
    
    
    $finalCoaching= LaunionS1Sale::where('category','=','Final Coaching')->sum('tuition_fee') + LaunionS2Sale::where('category','=','Final Coaching')->sum('tuition_fee'); 

    $faciFee= LaunionS1Sale::sum('facilitation_fee') + LaunionS2Sale::sum('facilitation_fee'); 

    //start of Distribution
    $totLoanDisbursment= LaunionExpense::where('category','=', 'Loans')->sum('amount'); 
    $totBookDisbursement= LaunionExpense::where('category','=', 'Books')->sum('amount'); 
    $insurance= LaunionExpense::where('category' ,'=', "Insurance")->sum('amount'); 
    $taxAndLicense= LaunionExpense::where('category','=','Tax')->sum('amount'); 

    $lecProfFee= LaunionExpense::where('category','=',"Lecturer")->where('sub_category','=',"Professional Fee")->sum('amount'); 
    $lecTax= LaunionExpense::where('category','=',"Lecturer")->where('sub_category','=',"Tax")->sum('amount'); 
    $lecTranspo= LaunionExpense::where('category','=',"Lecturer")->where('sub_category','=',"Transportation")->sum('amount'); 
    $lecReimburse= LaunionExpense::where('category','=',"Lecturer")->where('sub_category','=',"Reimbursement")->sum('amount'); 
    $lecAllow= LaunionExpense::where('category','=',"Lecturer")->where('sub_category','=',"Allowance")->sum('amount'); 
    $lecAccommodation= LaunionExpense::where('category','=',"Lecturer")->where('sub_category','=',"Accommodation")->sum('amount'); 
    $utilVenue= LaunionExpense::where('category','=',"Utilities")->where('sub_category','=',"Venue")->sum('amount'); 
    
    $mktingAllowance= LaunionExpense::where('category','=',"Marketing")->where('sub_category','=',"Allowance")->sum('amount'); 
    $mktingGasoline= LaunionExpense::where('category','=',"Marketing")->where('sub_category','=',"Gasoline")->sum('amount'); 
    $mktingMeals= LaunionExpense::where('category','=',"Marketing")->where('sub_category','=',"Meals")->sum('amount'); 
    $mktingProfFee= LaunionExpense::where('category','=',"Marketing")->where('sub_category','=',"Professional Fee")->sum('amount'); 
    $mktingTranspo= LaunionExpense::where('category','=',"Marketing")->where('sub_category','=',"Transportation")->sum('amount'); 
    $mktingPostSignageFlyers= LaunionExpense::where('category','=',"Marketing")->where('sub_category','=',"Posters | Signage | Flyers")->sum('amount'); 
    $mktingGifts= LaunionExpense::where('category','=',"Marketing")->where('sub_category','=',"Gifts")->sum('amount'); 
    
    $mktingDailyExpWaybill= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Waybill")->sum('amount'); 
    $dailyexpload= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Load")->sum('amount'); 
    $dailyexpmaintenance= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Maintenance")->sum('amount'); 
    $dailyexptranspo= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Transportation")->sum('amount'); 
    $dailyexpgasoline= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Gasoline")->sum('amount'); 
    $dailyexpofficeSupply= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Office Supply")->sum('amount'); 
    $dailyexpgrocery= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Grocery")->sum('amount'); 
    $dailyexpAllow= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Allowance")->sum('amount'); 
    $dailyexpOthers= LaunionExpense::where('category','=',"Daily Expense")->where('sub_category','=',"Others")->sum('amount'); 
    
    $admnsalary= LaunionExpense::where('category','=',"Salary")->sum('amount'); 
    
    $contributionsss= LaunionExpense::where('category','=',"Contribution")->where('sub_category','=',"SSS")->sum('amount'); 
    $contributionpagibig= LaunionExpense::where('category','=',"Contribution")->where('sub_category','=',"Pag-ibig")->sum('amount'); 
    $contributionphilhealth= LaunionExpense::where('category','=',"Contribution")->where('sub_category','=',"PhilHealth")->sum('amount'); 
    
    $utilityrent= LaunionExpense::where('category','=',"Utilities")->where('sub_category','=',"Rent")->sum('amount'); 
    $utilitywater= LaunionExpense::where('category','=',"Utilities")->where('sub_category','=',"Water")->sum('amount'); 
    $utilityelectricity= LaunionExpense::where('category','=',"Utilities")->where('sub_category','=',"Electricity")->sum('amount'); 
    $utilitysound= LaunionExpense::where('category','=',"Utilities")->where('sub_category','=',"Sound System")->sum('amount'); 
    $utilityinternet= LaunionExpense::where('category','=',"Utilities")->where('sub_category','=',"Internet")->sum('amount'); 
    $utilitytelephone= LaunionExpense::where('category','=',"Utilities")->where('sub_category','=',"Telephone")->sum('amount'); 
    
    $investOfficeEquip= LaunionExpense::where('category','=',"Investment")->where('sub_category','=',"Office Equipment")->sum('amount'); 
    $investFurnitureAndfixture= LaunionExpense::where('category','=',"Investment")->where('sub_category','=',"Furniture & Fixture")->sum('amount'); 
    $dailyexpMeals= LaunionExpense::where('category','=',"Investment")->where('sub_category','=',"Meals")->sum('amount'); 
  
    //books
    $bookNCLEX = LaunionBooksSale::where('program','=',"NCLEX")->sum('amount');
    $bookLET = LaunionBooksSale::where('program','=',"LET")->sum('amount');
    $bookNLE = LaunionBooksSale::where('program','=',"NLE")->sum('amount');
    $bookCrim = LaunionBooksSale::where('program','=',"Criminology")->sum('amount');
    $bookCS = LaunionBooksSale::where('program','=',"Civil Service")->sum('amount');
    $bookIELTS = LaunionBooksSale::where('program','=',"IELTS")->sum('amount');
    $bookSW = LaunionBooksSale::where('program','=',"Social Work")->sum('amount');
    $bookAgri = LaunionBooksSale::where('program','=',"Agriculture")->sum('amount');
    $bookMid = LaunionBooksSale::where('program','=',"Midwifery")->sum('amount');
    $bookOnline = LaunionBooksSale::where('program','=',"Online Only")->sum('amount');
    $bookPsycho = LaunionBooksSale::where('program','=',"Psychometrician")->sum('amount');

    $booksSale= LaunionBooksSale::sum('amount'); 
    //end of books

 
    // compute  total disbursement   
    $totDisbursement = $lecProfFee + $lecTax + $lecAllow + $lecTranspo  + $faciFee
                     + $lecReimburse + $lecAccommodation + $utilVenue + $mktingAllowance
                     + $mktingGasoline + $mktingMeals + $mktingProfFee + $mktingTranspo
                     + $mktingDailyExpWaybill + $mktingPostSignageFlyers + $mktingGifts + $admnsalary
                     + $contributionsss + $contributionpagibig + $contributionphilhealth + $utilityrent
                     + $utilitywater + $utilityelectricity + $utilitysound + $utilityinternet
                     + $utilitytelephone + $dailyexpload + $dailyexpmaintenance + $dailyexptranspo
                     + $dailyexpgasoline + $dailyexpofficeSupply + $dailyexpgrocery + $taxAndLicense
                     + $insurance + $investOfficeEquip + $investFurnitureAndfixture + $dailyexpOthers
                     + $dailyexpMeals + $dailyexpAllow + $totBookDisbursement + $totLoanDisbursment
                     ;

// total season 1
$totSeason1 =  $letS1 + $nleS1 + $crimS1 + $civilS1 + $psychoS1 + $nclexS1 + $ieltsS1 + $socialsS1 + $agriS1 + $midwiferyS1 + $onlineS1;

//total season 2
$totSeason2 =  $letS2 + $nleS2 + $crimS2 + $civilS2 + $psychoS2 + $nclexS2 + $ieltsS2 + $socialsS2 + $agriS2 + $midwiferyS2 + $onlineS2;
   
//total revenue    
$revenue = $totSeason1 + $totSeason2 + $booksSale;

//netexcess netincome
$netExcessIncome =  $revenue -  $totDisbursement ;

    return view('member.financial-report')
    ->with('branch', $branch)
    
    ->with('totSeason1', $totSeason1)
    ->with('totSeason2', $totSeason2)
    ->with('booksSale', $booksSale)
 
    ->with('bookNCLEX', $bookNCLEX)
    ->with('bookPsycho', $bookPsycho)
    ->with('bookLET', $bookLET)
    ->with('bookNLE', $bookNLE)
    ->with('bookCrim', $bookCrim)
    ->with('bookCS', $bookCS)
    ->with('bookIELTS', $bookIELTS)
    ->with('bookSW', $bookSW)
    ->with('bookAgri', $bookAgri)
    ->with('bookMid', $bookMid)
    ->with('bookOnline', $bookOnline)
    
    ->with('revenue', $revenue)
    ->with('netExcessIncome', $netExcessIncome)

    ->with('totLoanDisbursment', $totLoanDisbursment)
    ->with('totBookDisbursement', $totBookDisbursement)
    
    ->with('finalCoaching', $finalCoaching)

    ->with('lecTax', $lecTax)
    ->with('lecProfFee', $lecProfFee)
    ->with('lecTranspo', $lecTranspo)
    ->with('lecReimburse', $lecReimburse)
    ->with('lecAllow', $lecAllow)
    ->with('lecAccommodation', $lecAccommodation)
    ->with('utilVenue', $utilVenue)
    ->with('mktingAllowance', $mktingAllowance)
    ->with('mktingGasoline', $mktingGasoline)
    ->with('mktingMeals', $mktingMeals)
    ->with('mktingProfFee', $mktingProfFee)
    ->with('mktingTranspo', $mktingTranspo)
    ->with('mktingDailyExpWaybill', $mktingDailyExpWaybill)
    ->with('mktingPostSignageFlyers', $mktingPostSignageFlyers)
    ->with('mktingGifts', $mktingGifts)
    ->with('admnsalary', $admnsalary)
    ->with('contributionsss', $contributionsss)
    ->with('contributionpagibig', $contributionpagibig)
    ->with('contributionphilhealth', $contributionphilhealth)
    ->with('utilityrent', $utilityrent)
    ->with('utilitywater', $utilitywater)
    ->with('utilityelectricity', $utilityelectricity)
    ->with('utilitysound', $utilitysound)
    ->with('utilityinternet', $utilityinternet)
    ->with('utilitytelephone', $utilitytelephone)
    ->with('dailyexpload', $dailyexpload)
    ->with('dailyexpmaintenance', $dailyexpmaintenance)
    ->with('dailyexptranspo', $dailyexptranspo)
    ->with('dailyexpgasoline', $dailyexpgasoline)
    ->with('dailyexpofficeSupply', $dailyexpofficeSupply)
    ->with('dailyexpgrocery', $dailyexpgrocery)
    ->with('taxAndLicense', $taxAndLicense)
    ->with('insurance', $insurance)
    ->with('investOfficeEquip', $investOfficeEquip)
    ->with('investFurnitureAndfixture', $investFurnitureAndfixture)
    ->with('dailyexpOthers', $dailyexpOthers)
    ->with('dailyexpMeals', $dailyexpMeals)
    ->with('dailyexpAllow', $dailyexpAllow)
    //season 1 sale
    ->with('letS1', $letS1)
    ->with('nleS1', $nleS1)
    ->with('crimS1', $crimS1)
    ->with('civilS1', $civilS1)
    ->with('psychoS1', $psychoS1)
    ->with('nclexS1', $nclexS1)
    ->with('ieltsS1', $ieltsS1)
    ->with('socialsS1', $socialsS1)
    ->with('agriS1', $agriS1)
    ->with('midwiferyS1', $midwiferyS1)
    ->with('onlineS1', $onlineS1)

    //season 2 sale
    ->with('letS2', $letS2)
    ->with('nleS2', $nleS2)
    ->with('crimS2', $crimS2)
    ->with('civilS2', $civilS2)
    ->with('psychoS2', $psychoS2)
    ->with('nclexS2', $nclexS2)
    ->with('ieltsS2', $ieltsS2)
    ->with('socialsS2', $socialsS2)
    ->with('agriS2', $agriS2)
    ->with('midwiferyS2', $midwiferyS2)
    ->with('onlineS2', $onlineS2)
    ->with('totDisbursement', $totDisbursement)

    ->with('faciFee', $faciFee);
}//end of financial report




public function insert_book_transfer(Request $request){

    $input = $request->except(['_token']);
   
   
    
   $validatedData = $request->validate([
       'remark' => 'required'
   ]);
   
           $quantities = $input['quantities'];
           $available = $input['available'];
           $program = $input['book_program'];
           $author = $input['book_author'];
           $final = $input['final'];
          bookTranferTrans::create([

              'book_date'              => $input['date'],
              'book_remarks'           => $input['remark'],
              'book_branchSender'      => $this->branch,
              'book_branchReciever'    => $input['transfer_to'],
          ]);
          $last_id = bookTranferTrans::orderby('id','desc')->first();
           if(isset($input['book_title_item'])){
               foreach ($input['book_title_item'] as $book => $value ) {

                LaunionBookTransfer::create([
                   'book_transId'             => $last_id['id'],
                   'book_program'           => $program[$book],
                   'book_major'             => null,
                   'book_author'             => $author[$book],
                   'book_title'             => $value,
                   'book_quantity'          => $quantities[$book],
                   'book_stockInitial'      => $available[$book],
                   'book_stockFinal'        => $final[$book]
                ]);
   
   
              
          
   
           LaunionBooksInventorie::where('book_title','=',$value)->update([
   
               'available' => $final[$book],
           ]);
   
               
           }
          
          
   

               
   
               Alert::success('Success!', 'New book tranfer has been submitted.');
            return redirect ($this->sbranch.'/book-transfer');
                   }
   
           else{
               Alert::error('Failed!', 'Oops Something went wrong!, please try again');
               return redirect ($this->sbranch.'/book-transfer');
           }
       
   
           
   }//end of book transfer



   public function book_transfer(){

   
    
        $branch=$this->branch; 
        $date = date('M-d-Y');
        $program = Program::all();
        return view('member.book-transfer')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }
    
    
    public function bookTransfer_table(){
    $book = BookTranferTrans::orderby('id','desc')->get();
    return view ('member.book-transferRecord')->with('book',$book);
    
}
public function bookTransfer_table_fetch($id){
    $data = LaunionBookTransfer::where('book_transId','=',$id)->get();
   return response()->json($data);
    }
    
    public function lecturerView(){
        $Programs = Program::all();
        $sy = date("Y")+1;
        $years = range($sy,2010);

        return view('radashboard.lecturer')
        ->with('Programs',$Programs)
        ->with('years',$years);
    }


    public function onlineCompletion(){
        $sy = date("Y")+1;
        $years = range($sy,2010);
        return view('radashboard.online_completion')
        ->with('years',$years);
    }
    public function delete_expense($id){
        
        
        $expense_amount = LaunionExpense::where('id','=',$id)->value('amount');
        $pettycash = LaunionPettyCash::where('id','=','1')->value('petty_cash');

        LaunionExpense::where('id','=',$id)->delete();
            

        $updated_pettycash = $pettycash + $expense_amount;

        LaunionPettyCash::where('id','=','1')->update([
            'petty_cash' => $updated_pettycash,
        ]);

       
        
        Alert::success('Success!', 'expense deleted.');
        return redirect ($this->sbranch.'/expense');

    }
}
