<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Launion\LaunionAgri;
use App\Model\Launion\LaunionBookCash;
use App\Model\Launion\LaunionBooksInventorie;
use App\Model\Launion\LaunionBooksSale;
use App\Model\Launion\LaunionBudget;
use App\Model\Launion\LaunionCivil;
use App\Model\Launion\LaunionCrim;
use App\Model\Launion\LaunionDiscount;
use App\Model\Launion\LaunionDropped;
use App\Model\Launion\LaunionExpense;
use App\Model\Launion\LaunionIelt;
use App\Model\Launion\LaunionLet;
use App\Model\Launion\LaunionMid;
use App\Model\Launion\LaunionNclex;
use App\Model\Launion\LaunionNle;
use App\Model\Launion\LaunionOnline;
use App\Model\Launion\LaunionPsyc;
use App\Model\Launion\LaunionReceivable;
use App\Model\Launion\LaunionS1Sale;
use App\Model\Launion\LaunionS2Sale;
use App\Model\Launion\LaunionS1Cash;
use App\Model\Launion\LaunionS2Cash;
use App\Model\Launion\LaunionScholar;
use App\Model\Launion\LaunionSocial;
use App\Model\Launion\LaunionTuition;
use App\Model\Launion\LaunionPettyCash;
use App\Model\Launion\LaunionRemit;
use App\Model\Launion\LaunionReservation;
use App\Model\Launion\LaunionEmployee;
use App\Model\Launion\LaunionScoreCards;
use App\Model\Launion\LaunionBookTransfer;

use App\Model\Launion\LaunionLecturerAEvaluation;
use App\Model\Launion\LaunionLecturerBEvaluation;
use App\Model\Launion\LaunionComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class LaunionCashierController extends Controller
{
    private $branch = "Launion";

    private $sbranch = "launion";

public function __construct()
    {

         $this->middleware('role:launion_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = LaunionBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             LaunionBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        LaunionBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = LaunionBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        LaunionBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('launion-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('launion-cashier/book-payment');
        }
}
public function books_table(){

$book = LaunionBooksInventorie::all();
$sale = LaunionBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }