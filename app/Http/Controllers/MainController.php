<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Auth;
use App\User;
use DB;

use App\Major;

class MainController extends Controller
{
    public function __construct()
{
// $this->middleware('auth');
}

    public function index(){

    }

    public function fetch_major(){

        $program = Input::get('program');

        if($program == 'LET'){
            $program = 'lets';
        }
        if($program == 'NLE'){
            $program = 'nles';
        }
        if($program == 'Criminology'){
            $program = 'crims';
        }
        if($program == 'Civil Service'){
            $program = 'civils';
        }

        if($program == 'Psychometrician'){
            $program = 'psycs';
        }
        if($program == 'NCLEX'){
            $program = 'nclexs';
        }
        if($program == 'IELTS'){
            $program = 'ielts';
        }
        if($program == 'Social Work'){
            $program = 'socials';
        }
        if($program == 'Agriculture'){
            $program = 'agris';
        }
        if($program == 'Midwifery'){
            $program = 'mids';
        }
        if($program == 'Online Only'){
            $program = 'onlines';
        }

        $major = Major::where('program','=', $program)->get();

        return response()->json($major);
    }

    public function get_userinfo($uid){

        $userifo = User::where('id','=',$uid)->get();
    
      
    
        return response()->json($userifo);
    }
 


}
