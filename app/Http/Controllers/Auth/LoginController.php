<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectAfterLogout = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated()
    {

    if(auth()->user()->hasRole('ra_novaliches'))
    {
    return redirect('/novaliches/dashboard');
    }  

        $uid = auth()->user()->id;
        if(auth()->user()->hasRole('admin'))
        {
        return redirect('/dashboard')->with('log',$uid);
        } 

    
    
    if(auth()->user()->hasRole('abra'))
    {   return redirect('/abra/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('baguio'))
    {   return redirect('/baguio/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('calapan'))
    {   return redirect('/calapan/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('candon'))
    {   return redirect('/candon/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('dagupan'))
    {   return redirect('/dagupan/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('fairview'))
    {   return redirect('/fairview/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('las_pinas'))
    {   return redirect('/las_pinas/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('launion'))
    {   return redirect('/launion/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('manila'))
    {   return redirect('/manila/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('roxas'))
    {   return redirect('/roxas/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('tarlac'))
    {   return redirect('/tarlac/dashboard')->with('log',$uid);  }
    if(auth()->user()->hasRole('vigan'))
    {   return redirect('/vigan/dashboard')->with('log',$uid);  }
   
    if(auth()->user()->hasRole('abra_enrollment'))
    {  return redirect('/abra-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('baguio_enrollment'))
    {  return redirect('/baguio-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('calapan_enrollment'))
    {  return redirect('/calapan-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('candon_enrollment'))
    {  return redirect('/candon-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('dagupan_enrollment'))
    {  return redirect('/dagupan-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('las_pinas_enrollment'))
    {  return redirect('/las_pinas-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('launion_enrollment'))
    {  return redirect('/launion-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('manila_enrollment'))
    {  return redirect('/manila-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('roxas_enrollment'))
    {  return redirect('/roxas-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('tarlac_enrollment'))
    {  return redirect('/tarlac-enrollment/new-payment')->with('log',$uid); }  
    if(auth()->user()->hasRole('vigan_enrollment'))
    {  return redirect('/vigan-enrollment/new-payment')->with('log',$uid); }  

    if(auth()->user()->hasRole('abra_cashier'))
    {  return redirect('/abra-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('baguio_cashier'))
    {  return redirect('/baguio-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('calapan_cashier'))
    {  return redirect('/calapan-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('candon_cashier'))
    {  return redirect('/candon-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('dagupan_cashier'))
    {  return redirect('/dagupan-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('fairview_cashier'))
    {  return redirect('/fairview-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('las_pinas_cashier'))
    {  return redirect('/las_pinas-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('launion_cashier'))
    {  return redirect('/launion-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('manila_cashier'))
    {  return redirect('/manila-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('roxas_cashier'))
    {  return redirect('/roxas-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('tarlac_cashier'))
    {  return redirect('/tarlac-cashier/book-payment')->with('log',$uid); }
    if(auth()->user()->hasRole('vigan_cashier'))
    {  return redirect('/vigan-cashier/book-payment')->with('log',$uid); }

    
    // else{
       
    //     return auth()->user()->id;
    // } 
   
    
}

public function username(){
    $loginType = request()->input('username');

    $this->username = filter_var($loginType, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

    request()->merge([$this->username => $loginType]);

    return property_exists($this, 'username') ? $this->username : 'email';
}


}
