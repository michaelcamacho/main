<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Vigan\ViganAgri;
use App\Model\Vigan\ViganBookCash;
use App\Model\Vigan\ViganBooksInventorie;
use App\Model\Vigan\ViganBooksSale;
use App\Model\Vigan\ViganBudget;
use App\Model\Vigan\ViganCivil;
use App\Model\Vigan\ViganCrim;
use App\Model\Vigan\ViganDiscount;
use App\Model\Vigan\ViganDropped;
use App\Model\Vigan\ViganExpense;
use App\Model\Vigan\ViganIelt;
use App\Model\Vigan\ViganLet;
use App\Model\Vigan\ViganMid;
use App\Model\Vigan\ViganNclex;
use App\Model\Vigan\ViganNle;
use App\Model\Vigan\ViganOnline;
use App\Model\Vigan\ViganPsyc;
use App\Model\Vigan\ViganReceivable;
use App\Model\Vigan\ViganS1Sale;
use App\Model\Vigan\ViganS2Sale;
use App\Model\Vigan\ViganS1Cash;
use App\Model\Vigan\ViganS2Cash;
use App\Model\Vigan\ViganScholar;
use App\Model\Vigan\ViganSocial;
use App\Model\Vigan\ViganTuition;
use App\Model\Vigan\ViganPettyCash;
use App\Model\Vigan\ViganRemit;
use App\Model\Vigan\ViganReservation;
use App\Model\Vigan\ViganEmployee;
use App\Model\Vigan\ViganScoreCards;
use App\Model\Vigan\ViganBookTransfer;

use App\Model\Vigan\ViganLecturerAEvaluation;
use App\Model\Vigan\ViganLecturerBEvaluation;
use App\Model\Vigan\ViganComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class ViganCashierController extends Controller
{
    private $branch = "Vigan";

    private $sbranch = "vigan";

public function __construct()
    {

         $this->middleware('role:vigan_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = ViganBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = ViganBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = ViganBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             ViganBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = ViganBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        ViganBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = ViganBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        ViganBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('vigan-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('vigan-cashier/book-payment');
        }
}
public function books_table(){

$book = ViganBooksInventorie::all();
$sale = ViganBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }