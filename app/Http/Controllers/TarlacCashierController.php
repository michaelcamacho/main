<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Tarlac\TarlacAgri;
use App\Model\Tarlac\TarlacBookCash;
use App\Model\Tarlac\TarlacBooksInventorie;
use App\Model\Tarlac\TarlacBooksSale;
use App\Model\Tarlac\TarlacBudget;
use App\Model\Tarlac\TarlacCivil;
use App\Model\Tarlac\TarlacCrim;
use App\Model\Tarlac\TarlacDiscount;
use App\Model\Tarlac\TarlacDropped;
use App\Model\Tarlac\TarlacExpense;
use App\Model\Tarlac\TarlacIelt;
use App\Model\Tarlac\TarlacLet;
use App\Model\Tarlac\TarlacMid;
use App\Model\Tarlac\TarlacNclex;
use App\Model\Tarlac\TarlacNle;
use App\Model\Tarlac\TarlacOnline;
use App\Model\Tarlac\TarlacPsyc;
use App\Model\Tarlac\TarlacReceivable;
use App\Model\Tarlac\TarlacS1Sale;
use App\Model\Tarlac\TarlacS2Sale;
use App\Model\Tarlac\TarlacS1Cash;
use App\Model\Tarlac\TarlacS2Cash;
use App\Model\Tarlac\TarlacScholar;
use App\Model\Tarlac\TarlacSocial;
use App\Model\Tarlac\TarlacTuition;
use App\Model\Tarlac\TarlacPettyCash;
use App\Model\Tarlac\TarlacRemit;
use App\Model\Tarlac\TarlacReservation;
use App\Model\Tarlac\TarlacEmployee;
use App\Model\Tarlac\TarlacScoreCards;
use App\Model\Tarlac\TarlacBookTransfer;

use App\Model\Tarlac\TarlacLecturerAEvaluation;
use App\Model\Tarlac\TarlacLecturerBEvaluation;
use App\Model\Tarlac\TarlacComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class TarlacCashierController extends Controller
{
    private $branch = "Tarlac";

    private $sbranch = "tarlac";

public function __construct()
    {

         $this->middleware('role:tarlac_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = TarlacBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = TarlacBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = TarlacBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             TarlacBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = TarlacBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        TarlacBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = TarlacBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        TarlacBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('tarlac-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('tarlac-cashier/book-payment');
        }
}
public function books_table(){

$book = TarlacBooksInventorie::all();
$sale = TarlacBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }