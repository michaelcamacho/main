<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Branch;
use App\Program;

use App\facilitation;
use DB;
use Auth;
use App\User;
use App\Role;
use App\Major;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


use App\Model\ExpenseSetup;
use App\Model\AdminSettings;


/* Abra */
use App\Model\Abra\AbraScoreCards;
use App\Model\Abra\AbraAgri;
use App\Model\Abra\AbraBooksInventorie;
use App\Model\Abra\AbraBooksSale;
use App\Model\Abra\AbraBudget;
use App\Model\Abra\AbraCivil;
use App\Model\Abra\AbraCrim;
use App\Model\Abra\AbraDiscount;
use App\Model\Abra\AbraDropped;
use App\Model\Abra\AbraExpense;
use App\Model\Abra\AbraIelt;
use App\Model\Abra\AbraLet;
use App\Model\Abra\AbraNclex;
use App\Model\Abra\AbraNle;
use App\Model\Abra\AbraPsyc;
use App\Model\Abra\AbraMid;
use App\Model\Abra\AbraOnline;
use App\Model\Abra\AbraReceivable;
use App\Model\Abra\AbraS1Sale;
use App\Model\Abra\AbraS2Sale;
use App\Model\Abra\AbraScholar;
use App\Model\Abra\AbraSocial;
use App\Model\Abra\AbraTuition;
use App\Model\Abra\AbraPettyCash;

/* Abra */


/* Baguio */
use App\Model\Baguio\BaguioScoreCards;
use App\Model\Baguio\BaguioAgri;
use App\Model\Baguio\BaguioBooksInventorie;
use App\Model\Baguio\BaguioBooksSale;
use App\Model\Baguio\BaguioBudget;
use App\Model\Baguio\BaguioCivil;
use App\Model\Baguio\BaguioCrim;
use App\Model\Baguio\BaguioDiscount;
use App\Model\Baguio\BaguioDropped;
use App\Model\Baguio\BaguioExpense;
use App\Model\Baguio\BaguioIelt;
use App\Model\Baguio\BaguioLet;
use App\Model\Baguio\BaguioNclex;
use App\Model\Baguio\BaguioNle;
use App\Model\Baguio\BaguioPsyc;
use App\Model\Baguio\BaguioMid;
use App\Model\Baguio\BaguioOnline;
use App\Model\Baguio\BaguioReceivable;
use App\Model\Baguio\BaguioS1Sale;
use App\Model\Baguio\BaguioS2Sale;
use App\Model\Baguio\BaguioScholar;
use App\Model\Baguio\BaguioSocial;
use App\Model\Baguio\BaguioTuition;
use App\Model\Baguio\BaguioPettyCash;

/* Baguio */

/* Calapan */
use App\Model\Calapan\CalapanScoreCards;
use App\Model\Calapan\CalapanAgri;
use App\Model\Calapan\CalapanBooksInventorie;
use App\Model\Calapan\CalapanBooksSale;
use App\Model\Calapan\CalapanBudget;
use App\Model\Calapan\CalapanCivil;
use App\Model\Calapan\CalapanCrim;
use App\Model\Calapan\CalapanDiscount;
use App\Model\Calapan\CalapanDropped;
use App\Model\Calapan\CalapanExpense;
use App\Model\Calapan\CalapanIelt;
use App\Model\Calapan\CalapanLet;
use App\Model\Calapan\CalapanNclex;
use App\Model\Calapan\CalapanNle;
use App\Model\Calapan\CalapanPsyc;
use App\Model\Calapan\CalapanMid;
use App\Model\Calapan\CalapanOnline;
use App\Model\Calapan\CalapanReceivable;
use App\Model\Calapan\CalapanS1Sale;
use App\Model\Calapan\CalapanS2Sale;
use App\Model\Calapan\CalapanScholar;
use App\Model\Calapan\CalapanSocial;
use App\Model\Calapan\CalapanTuition;
use App\Model\Calapan\CalapanPettyCash;


/* Calapan */

/* Dagupan */
use App\Model\Dagupan\DagupanScoreCards;
use App\Model\Dagupan\DagupanAgri;
use App\Model\Dagupan\DagupanBooksInventorie;
use App\Model\Dagupan\DagupanBooksSale;
use App\Model\Dagupan\DagupanBudget;
use App\Model\Dagupan\DagupanCivil;
use App\Model\Dagupan\DagupanCrim;
use App\Model\Dagupan\DagupanDiscount;
use App\Model\Dagupan\DagupanDropped;
use App\Model\Dagupan\DagupanExpense;
use App\Model\Dagupan\DagupanIelt;
use App\Model\Dagupan\DagupanLet;
use App\Model\Dagupan\DagupanNclex;
use App\Model\Dagupan\DagupanNle;
use App\Model\Dagupan\DagupanPsyc;
use App\Model\Dagupan\DagupanMid;
use App\Model\Dagupan\DagupanOnline;
use App\Model\Dagupan\DagupanReceivable;
use App\Model\Dagupan\DagupanS1Sale;
use App\Model\Dagupan\DagupanS2Sale;
use App\Model\Dagupan\DagupanScholar;
use App\Model\Dagupan\DagupanSocial;
use App\Model\Dagupan\DagupanTuition;
use App\Model\Dagupan\DagupanPettyCash;

/* Dagupan */


/* Candon */
use App\Model\Candon\CandonScoreCards;
use App\Model\Candon\CandonAgri;
use App\Model\Candon\CandonBooksInventorie;
use App\Model\Candon\CandonBooksSale;
use App\Model\Candon\CandonBudget;
use App\Model\Candon\CandonCivil;
use App\Model\Candon\CandonCrim;
use App\Model\Candon\CandonDiscount;
use App\Model\Candon\CandonDropped;
use App\Model\Candon\CandonExpense;
use App\Model\Candon\CandonIelt;
use App\Model\Candon\CandonLet;
use App\Model\Candon\CandonNclex;
use App\Model\Candon\CandonNle;
use App\Model\Candon\CandonPsyc;
use App\Model\Candon\CandonMid;
use App\Model\Candon\CandonOnline;
use App\Model\Candon\CandonReceivable;
use App\Model\Candon\CandonS1Sale;
use App\Model\Candon\CandonS2Sale;
use App\Model\Candon\CandonScholar;
use App\Model\Candon\CandonSocial;
use App\Model\Candon\CandonTuition;
use App\Model\Candon\CandonPettyCash;

/* Candon */


/* Fairview */
use App\Model\Fairview\FairviewScoreCards;
use App\Model\Fairview\FairviewAgri;
use App\Model\Fairview\FairviewBooksInventorie;
use App\Model\Fairview\FairviewBooksSale;
use App\Model\Fairview\FairviewBudget;
use App\Model\Fairview\FairviewCivil;
use App\Model\Fairview\FairviewCrim;
use App\Model\Fairview\FairviewDiscount;
use App\Model\Fairview\FairviewDropped;
use App\Model\Fairview\FairviewExpense;
use App\Model\Fairview\FairviewIelt;
use App\Model\Fairview\FairviewLet;
use App\Model\Fairview\FairviewNclex;
use App\Model\Fairview\FairviewNle;
use App\Model\Fairview\FairviewPsyc;
use App\Model\Fairview\FairviewMid;
use App\Model\Fairview\FairviewOnline;
use App\Model\Fairview\FairviewReceivable;
use App\Model\Fairview\FairviewS1Sale;
use App\Model\Fairview\FairviewS2Sale;
use App\Model\Fairview\FairviewScholar;
use App\Model\Fairview\FairviewSocial;
use App\Model\Fairview\FairviewTuition;
use App\Model\Fairview\FairviewPettyCash;

/* Fairview */


/* LasPinas */
use App\Model\LasPinas\LasPinasScoreCards;
use App\Model\LasPinas\LasPinasAgri;
use App\Model\LasPinas\LasPinasBooksInventorie;
use App\Model\LasPinas\LasPinasBooksSale;
use App\Model\LasPinas\LasPinasBudget;
use App\Model\LasPinas\LasPinasCivil;
use App\Model\LasPinas\LasPinasCrim;
use App\Model\LasPinas\LasPinasDiscount;
use App\Model\LasPinas\LasPinasDropped;
use App\Model\LasPinas\LasPinasExpense;
use App\Model\LasPinas\LasPinasIelt;
use App\Model\LasPinas\LasPinasLet;
use App\Model\LasPinas\LasPinasNclex;
use App\Model\LasPinas\LasPinasNle;
use App\Model\LasPinas\LasPinasPsyc;
use App\Model\LasPinas\LasPinasMid;
use App\Model\LasPinas\LasPinasOnline;
use App\Model\LasPinas\LasPinasReceivable;
use App\Model\LasPinas\LasPinasS1Sale;
use App\Model\LasPinas\LasPinasS2Sale;
use App\Model\LasPinas\LasPinasScholar;
use App\Model\LasPinas\LasPinasSocial;
use App\Model\LasPinas\LasPinasTuition;
use App\Model\LasPinas\LasPinasPettyCash;

/* LasPinas */

/* Launion */
use App\Model\Launion\LaunionScoreCards;
use App\Model\Launion\LaunionAgri;
use App\Model\Launion\LaunionBooksInventorie;
use App\Model\Launion\LaunionBooksSale;
use App\Model\Launion\LaunionBudget;
use App\Model\Launion\LaunionCivil;
use App\Model\Launion\LaunionCrim;
use App\Model\Launion\LaunionDiscount;
use App\Model\Launion\LaunionDropped;
use App\Model\Launion\LaunionExpense;
use App\Model\Launion\LaunionIelt;
use App\Model\Launion\LaunionLet;
use App\Model\Launion\LaunionNclex;
use App\Model\Launion\LaunionNle;
use App\Model\Launion\LaunionPsyc;
use App\Model\Launion\LaunionMid;
use App\Model\Launion\LaunionOnline;
use App\Model\Launion\LaunionReceivable;
use App\Model\Launion\LaunionS1Sale;
use App\Model\Launion\LaunionS2Sale;
use App\Model\Launion\LaunionScholar;
use App\Model\Launion\LaunionSocial;
use App\Model\Launion\LaunionTuition;
use App\Model\Launion\LaunionPettyCash;


/* Launion */

/* Manila */
use App\Model\Manila\ManilaScoreCards;
use App\Model\Manila\ManilaAgri;
use App\Model\Manila\ManilaBooksInventorie;
use App\Model\Manila\ManilaBooksSale;
use App\Model\Manila\ManilaBudget;
use App\Model\Manila\ManilaCivil;
use App\Model\Manila\ManilaCrim;
use App\Model\Manila\ManilaDiscount;
use App\Model\Manila\ManilaDropped;
use App\Model\Manila\ManilaExpense;
use App\Model\Manila\ManilaIelt;
use App\Model\Manila\ManilaLet;
use App\Model\Manila\ManilaNclex;
use App\Model\Manila\ManilaNle;
use App\Model\Manila\ManilaPsyc;
use App\Model\Manila\ManilaMid;
use App\Model\Manila\ManilaOnline;
use App\Model\Manila\ManilaReceivable;
use App\Model\Manila\ManilaS1Sale;
use App\Model\Manila\ManilaS2Sale;
use App\Model\Manila\ManilaScholar;
use App\Model\Manila\ManilaSocial;
use App\Model\Manila\ManilaTuition;
use App\Model\Manila\ManilaPettyCash;

/* Manila */


/* Roxas */
use App\Model\Roxas\RoxasScoreCards;
use App\Model\Roxas\RoxasAgri;
use App\Model\Roxas\RoxasBooksInventorie;
use App\Model\Roxas\RoxasBooksSale;
use App\Model\Roxas\RoxasBudget;
use App\Model\Roxas\RoxasCivil;
use App\Model\Roxas\RoxasCrim;
use App\Model\Roxas\RoxasDiscount;
use App\Model\Roxas\RoxasDropped;
use App\Model\Roxas\RoxasExpense;
use App\Model\Roxas\RoxasIelt;
use App\Model\Roxas\RoxasLet;
use App\Model\Roxas\RoxasNclex;
use App\Model\Roxas\RoxasNle;
use App\Model\Roxas\RoxasPsyc;
use App\Model\Roxas\RoxasMid;
use App\Model\Roxas\RoxasOnline;
use App\Model\Roxas\RoxasReceivable;
use App\Model\Roxas\RoxasS1Sale;
use App\Model\Roxas\RoxasS2Sale;
use App\Model\Roxas\RoxasScholar;
use App\Model\Roxas\RoxasSocial;
use App\Model\Roxas\RoxasTuition;
use App\Model\Roxas\RoxasPettyCash;

/* Roxas */



/* Tarlac */
use App\Model\Tarlac\TarlacScoreCards;
use App\Model\Tarlac\TarlacAgri;
use App\Model\Tarlac\TarlacBooksInventorie;
use App\Model\Tarlac\TarlacBooksSale;
use App\Model\Tarlac\TarlacBudget;
use App\Model\Tarlac\TarlacCivil;
use App\Model\Tarlac\TarlacCrim;
use App\Model\Tarlac\TarlacDiscount;
use App\Model\Tarlac\TarlacDropped;
use App\Model\Tarlac\TarlacExpense;
use App\Model\Tarlac\TarlacIelt;
use App\Model\Tarlac\TarlacLet;
use App\Model\Tarlac\TarlacNclex;
use App\Model\Tarlac\TarlacNle;
use App\Model\Tarlac\TarlacPsyc;
use App\Model\Tarlac\TarlacMid;
use App\Model\Tarlac\TarlacOnline;
use App\Model\Tarlac\TarlacReceivable;
use App\Model\Tarlac\TarlacS1Sale;
use App\Model\Tarlac\TarlacS2Sale;
use App\Model\Tarlac\TarlacScholar;
use App\Model\Tarlac\TarlacSocial;
use App\Model\Tarlac\TarlacTuition;
use App\Model\Tarlac\TarlacPettyCash;

/* Tarlac */


/* Vigan */
use App\Model\Vigan\ViganScoreCards;
use App\Model\Vigan\ViganAgri;
use App\Model\Vigan\ViganBooksInventorie;
use App\Model\Vigan\ViganBooksSale;
use App\Model\Vigan\ViganBudget;
use App\Model\Vigan\ViganCivil;
use App\Model\Vigan\ViganCrim;
use App\Model\Vigan\ViganDiscount;
use App\Model\Vigan\ViganDropped;
use App\Model\Vigan\ViganExpense;
use App\Model\Vigan\ViganIelt;
use App\Model\Vigan\ViganLet;
use App\Model\Vigan\ViganNclex;
use App\Model\Vigan\ViganNle;
use App\Model\Vigan\ViganPsyc;
use App\Model\Vigan\ViganMid;
use App\Model\Vigan\ViganOnline;
use App\Model\Vigan\ViganReceivable;
use App\Model\Vigan\ViganS1Sale;
use App\Model\Vigan\ViganS2Sale;
use App\Model\Vigan\ViganScholar;
use App\Model\Vigan\ViganSocial;
use App\Model\Vigan\ViganTuition;
use App\Model\Vigan\ViganPettyCash;

/* Vigan */




class AdminController extends Controller
{


public function __construct()
	{
		$this->middleware('role:admin');
		
	}


public function dashboard(){
		
		$this->abra_populateScoreCard();
		$this->baguio_populateScoreCard();
		$this->calapan_populateScoreCard();
		$this->candon_populateScoreCard();
		$this->dagupan_populateScoreCard();
		$this->fairview_populateScoreCard();
		$this->las_pinas_populateScoreCard();
		$this->launion_populateScoreCard();
		$this->manila_populateScoreCard();
		$this->roxas_populateScoreCard();
		$this->tarlac_populateScoreCard();
		$this->vigan_populateScoreCard();

	$sid = AdminSettings::where('Account','=','admin')->value('id');
	$setting = AdminSettings::findorfail($sid);

	
	
	$abra_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Abra")->sum('facilitation');
			
	$abra_let_1=AbraS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_nle_1=AbraS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_crim_1=AbraS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_civil_1=AbraS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$abra_psyc_1=AbraS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_nclex_1=AbraS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$abra_ielts_1=AbraS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$abra_social_1=AbraS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_agri_1=AbraS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_mid_1=AbraS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_online_1=AbraS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_let_2=AbraS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_nle_2=AbraS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_crim_2=AbraS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_civil_2=AbraS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_psyc_2=AbraS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_nclex_2=AbraS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_ielts_2=AbraS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_social_2=AbraS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_agri_2=AbraS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_mid_2=AbraS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$abra_online_2=AbraS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');




	
	$baguio_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Baguio")->sum('facilitation');
			
	$baguio_let_1=BaguioS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_nle_1=BaguioS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_crim_1=BaguioS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_civil_1=BaguioS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$baguio_psyc_1=BaguioS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_nclex_1=BaguioS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$baguio_ielts_1=BaguioS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$baguio_social_1=BaguioS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_agri_1=BaguioS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_mid_1=BaguioS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_online_1=BaguioS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_let_2=BaguioS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_nle_2=BaguioS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_crim_2=BaguioS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_civil_2=BaguioS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_psyc_2=BaguioS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_nclex_2=BaguioS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_ielts_2=BaguioS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_social_2=BaguioS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_agri_2=BaguioS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_mid_2=BaguioS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$baguio_online_2=BaguioS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');



	
	$calapan_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Calapan")->sum('facilitation');
			
	$calapan_let_1=CalapanS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_nle_1=CalapanS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_crim_1=CalapanS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_civil_1=CalapanS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$calapan_psyc_1=CalapanS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_nclex_1=CalapanS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$calapan_ielts_1=CalapanS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$calapan_social_1=CalapanS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_agri_1=CalapanS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_mid_1=CalapanS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_online_1=CalapanS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_let_2=CalapanS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_nle_2=CalapanS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_crim_2=CalapanS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_civil_2=CalapanS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_psyc_2=CalapanS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_nclex_2=CalapanS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_ielts_2=CalapanS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_social_2=CalapanS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_agri_2=CalapanS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_mid_2=CalapanS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$calapan_online_2=CalapanS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$candon_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Candon")->sum('facilitation');
			
	$candon_let_1=CandonS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_nle_1=CandonS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_crim_1=CandonS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_civil_1=CandonS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$candon_psyc_1=CandonS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_nclex_1=CandonS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$candon_ielts_1=CandonS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$candon_social_1=CandonS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_agri_1=CandonS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_mid_1=CandonS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_online_1=CandonS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_let_2=CandonS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_nle_2=CandonS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_crim_2=CandonS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_civil_2=CandonS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_psyc_2=CandonS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_nclex_2=CandonS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_ielts_2=CandonS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_social_2=CandonS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_agri_2=CandonS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_mid_2=CandonS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$candon_online_2=CandonS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	
	
	$dagupan_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Dagupan")->sum('facilitation');
			
	$dagupan_let_1=DagupanS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_nle_1=DagupanS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_crim_1=DagupanS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_civil_1=DagupanS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$dagupan_psyc_1=DagupanS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_nclex_1=DagupanS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$dagupan_ielts_1=DagupanS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$dagupan_social_1=DagupanS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_agri_1=DagupanS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_mid_1=DagupanS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_online_1=DagupanS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_let_2=DagupanS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_nle_2=DagupanS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_crim_2=DagupanS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_civil_2=DagupanS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_psyc_2=DagupanS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_nclex_2=DagupanS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_ielts_2=DagupanS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_social_2=DagupanS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_agri_2=DagupanS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_mid_2=DagupanS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$dagupan_online_2=DagupanS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');



	
	$fairview_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Fairview")->sum('facilitation');
			
	$fairview_let_1=FairviewS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_nle_1=FairviewS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_crim_1=FairviewS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_civil_1=FairviewS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$fairview_psyc_1=FairviewS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_nclex_1=FairviewS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$fairview_ielts_1=FairviewS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$fairview_social_1=FairviewS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_agri_1=FairviewS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_mid_1=FairviewS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_online_1=FairviewS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_let_2=FairviewS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_nle_2=FairviewS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_crim_2=FairviewS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_civil_2=FairviewS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_psyc_2=FairviewS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_nclex_2=FairviewS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_ielts_2=FairviewS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_social_2=FairviewS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_agri_2=FairviewS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_mid_2=FairviewS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$fairview_online_2=FairviewS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$las_pinas_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"LasPinas")->sum('facilitation');
			
	$las_pinas_let_1=LasPinasS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_nle_1=LasPinasS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_crim_1=LasPinasS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_civil_1=LasPinasS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$las_pinas_psyc_1=LasPinasS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_nclex_1=LasPinasS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$las_pinas_ielts_1=LasPinasS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$las_pinas_social_1=LasPinasS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_agri_1=LasPinasS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_mid_1=LasPinasS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_online_1=LasPinasS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_let_2=LasPinasS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_nle_2=LasPinasS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_crim_2=LasPinasS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_civil_2=LasPinasS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_psyc_2=LasPinasS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_nclex_2=LasPinasS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_ielts_2=LasPinasS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_social_2=LasPinasS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_agri_2=LasPinasS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_mid_2=LasPinasS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$las_pinas_online_2=LasPinasS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');



	
	$launion_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Launion")->sum('facilitation');
			
	$launion_let_1=LaunionS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_nle_1=LaunionS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_crim_1=LaunionS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_civil_1=LaunionS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$launion_psyc_1=LaunionS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_nclex_1=LaunionS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$launion_ielts_1=LaunionS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$launion_social_1=LaunionS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_agri_1=LaunionS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_mid_1=LaunionS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_online_1=LaunionS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_let_2=LaunionS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_nle_2=LaunionS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_crim_2=LaunionS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_civil_2=LaunionS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_psyc_2=LaunionS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_nclex_2=LaunionS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_ielts_2=LaunionS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_social_2=LaunionS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_agri_2=LaunionS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_mid_2=LaunionS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$launion_online_2=LaunionS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$manila_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Manila")->sum('facilitation');
			
	$manila_let_1=ManilaS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_nle_1=ManilaS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_crim_1=ManilaS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_civil_1=ManilaS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$manila_psyc_1=ManilaS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_nclex_1=ManilaS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$manila_ielts_1=ManilaS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$manila_social_1=ManilaS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_agri_1=ManilaS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_mid_1=ManilaS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_online_1=ManilaS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_let_2=ManilaS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_nle_2=ManilaS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_crim_2=ManilaS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_civil_2=ManilaS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_psyc_2=ManilaS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_nclex_2=ManilaS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_ielts_2=ManilaS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_social_2=ManilaS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_agri_2=ManilaS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_mid_2=ManilaS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$manila_online_2=ManilaS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$roxas_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Roxas")->sum('facilitation');
			
	$roxas_let_1=RoxasS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_nle_1=RoxasS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_crim_1=RoxasS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_civil_1=RoxasS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$roxas_psyc_1=RoxasS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_nclex_1=RoxasS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$roxas_ielts_1=RoxasS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$roxas_social_1=RoxasS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_agri_1=RoxasS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_mid_1=RoxasS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_online_1=RoxasS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_let_2=RoxasS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_nle_2=RoxasS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_crim_2=RoxasS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_civil_2=RoxasS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_psyc_2=RoxasS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_nclex_2=RoxasS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_ielts_2=RoxasS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_social_2=RoxasS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_agri_2=RoxasS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_mid_2=RoxasS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$roxas_online_2=RoxasS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$tarlac_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Tarlac")->sum('facilitation');
			
	$tarlac_let_1=TarlacS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_nle_1=TarlacS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_crim_1=TarlacS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_civil_1=TarlacS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$tarlac_psyc_1=TarlacS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_nclex_1=TarlacS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$tarlac_ielts_1=TarlacS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$tarlac_social_1=TarlacS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_agri_1=TarlacS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_mid_1=TarlacS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_online_1=TarlacS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_let_2=TarlacS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_nle_2=TarlacS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_crim_2=TarlacS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_civil_2=TarlacS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_psyc_2=TarlacS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_nclex_2=TarlacS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_ielts_2=TarlacS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_social_2=TarlacS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_agri_2=TarlacS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_mid_2=TarlacS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$tarlac_online_2=TarlacS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');


	
	$vigan_facilitation = facilitation::where('Year','=',$setting->Year)->where('branch','=',"Vigan")->sum('facilitation');
			
	$vigan_let_1=ViganS1Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_nle_1=ViganS1Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_crim_1=ViganS1Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_civil_1=ViganS1Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$vigan_psyc_1=ViganS1Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_nclex_1=ViganS1Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');
	
	$vigan_ielts_1=ViganS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$vigan_social_1=ViganS1Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_agri_1=ViganS1Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_mid_1=ViganS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_online_1=ViganS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_let_2=ViganS2Sale::where('program','=','LET')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_nle_2=ViganS2Sale::where('program','=','NLE')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_crim_2=ViganS2Sale::where('program','=','Criminology')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_civil_2=ViganS2Sale::where('program','=','Civil Service')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_psyc_2=ViganS2Sale::where('program','=','Psychometrician')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_nclex_2=ViganS2Sale::where('program','=','NCLEX')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_ielts_2=ViganS2Sale::where('program','=','IELTS')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_social_2=ViganS2Sale::where('program','=','Social')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_agri_2=ViganS2Sale::where('program','=','Agriculture')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_mid_2=ViganS1Sale::where('program','=','Midwifery')->where('Year','=',$setting->Year)->sum('amount_paid');

	$vigan_online_2=ViganS1Sale::where('program','=','Online Only')->where('Year','=',$setting->Year)->sum('amount_paid');




		$total_s1 =   $abra_let_1 + $abra_nle_1 + $abra_crim_1 + $abra_civil_1 + $abra_psyc_1 + $abra_nclex_1 + $abra_ielts_1 + $abra_social_1 + $abra_agri_1 + $abra_mid_1 + $abra_online_1
			 		+ $baguio_let_1 + $baguio_nle_1 + $baguio_crim_1 + $baguio_civil_1 + $baguio_psyc_1 + $baguio_nclex_1 + $baguio_ielts_1 + $baguio_social_1 + $baguio_agri_1 + $baguio_mid_1 + $baguio_online_1
					+ $calapan_let_1 + $calapan_nle_1 + $calapan_crim_1 + $calapan_civil_1 + $calapan_psyc_1 + $calapan_nclex_1 + $calapan_ielts_1 + $calapan_social_1 + $calapan_agri_1 + $calapan_mid_1 + $calapan_online_1
					+ $candon_let_1 + $candon_nle_1 + $candon_crim_1 + $candon_civil_1 + $candon_psyc_1 + $candon_nclex_1 + $candon_ielts_1 + $candon_social_1 + $candon_agri_1 + $candon_mid_1 + $candon_online_1
					+ $dagupan_let_1 + $dagupan_nle_1 + $dagupan_crim_1 + $dagupan_civil_1 + $dagupan_psyc_1 + $dagupan_nclex_1 + $dagupan_ielts_1 + $dagupan_social_1 + $dagupan_agri_1 + $dagupan_mid_1 + $dagupan_online_1;
					+ $fairview_let_1 + $fairview_nle_1 + $fairview_crim_1 + $fairview_civil_1 + $fairview_psyc_1 + $fairview_nclex_1 + $fairview_ielts_1 + $fairview_social_1 + $fairview_agri_1 + $fairview_mid_1 + $fairview_online_1
					+ $launion_let_1 + $launion_nle_1 + $launion_crim_1 + $launion_civil_1 + $launion_psyc_1 + $launion_nclex_1 + $launion_ielts_1 + $launion_social_1 + $launion_agri_1 + $launion_mid_1 + $launion_online_1
					+ $manila_let_1 + $manila_nle_1 + $manila_crim_1 + $manila_civil_1 + $manila_psyc_1 + $manila_nclex_1 + $manila_ielts_1 + $manila_social_1 + $manila_agri_1 + $manila_mid_1 + $manila_online_1
					+ $roxas_let_1 + $roxas_nle_1 + $roxas_crim_1 + $roxas_civil_1 + $roxas_psyc_1 + $roxas_nclex_1 + $roxas_ielts_1 + $roxas_social_1 + $roxas_agri_1 + $roxas_mid_1 + $roxas_online_1
					+ $tarlac_let_1 + $tarlac_nle_1 + $tarlac_crim_1 + $tarlac_civil_1 + $tarlac_psyc_1 + $tarlac_nclex_1 + $tarlac_ielts_1 + $tarlac_social_1 + $tarlac_agri_1 + $tarlac_mid_1 + $tarlac_online_1
					+ $vigan_let_1 + $vigan_nle_1 + $vigan_crim_1 + $vigan_civil_1 + $vigan_psyc_1 + $vigan_nclex_1 + $vigan_ielts_1 + $vigan_social_1 + $vigan_agri_1 + $vigan_mid_1 + $vigan_online_1;
		
		$total_s2 =   $abra_let_2 + $abra_nle_2 + $abra_crim_2 + $abra_civil_2 + $abra_psyc_2 + $abra_nclex_2 + $abra_ielts_2 + $abra_social_2 + $abra_agri_2 + $abra_mid_2 + $abra_online_2
					+ $baguio_let_2 + $baguio_nle_2 + $baguio_crim_2 + $baguio_civil_2 + $baguio_psyc_2 + $baguio_nclex_2 + $baguio_ielts_2 + $baguio_social_2 + $baguio_agri_2 + $baguio_mid_2 + $baguio_online_2
					+ $calapan_let_2 + $calapan_nle_2 + $calapan_crim_2 + $calapan_civil_2 + $calapan_psyc_2 + $calapan_nclex_2 + $calapan_ielts_2 + $calapan_social_2 + $calapan_agri_2 + $calapan_mid_2 + $calapan_online_2
				    + $candon_let_2 + $candon_nle_2 + $candon_crim_2 + $candon_civil_2 + $candon_psyc_2 + $candon_nclex_2 + $candon_ielts_2 + $candon_social_2 + $candon_agri_2 + $candon_mid_2 + $candon_online_2
				    + $dagupan_let_2 + $dagupan_nle_2 + $dagupan_crim_2 + $dagupan_civil_2 + $dagupan_psyc_2 + $dagupan_nclex_2 + $dagupan_ielts_2 + $dagupan_social_2 + $dagupan_agri_2 + $dagupan_mid_2 + $dagupan_online_2;
				    + $fairview_let_2 + $fairview_nle_2 + $fairview_crim_2 + $fairview_civil_2 + $fairview_psyc_2 + $fairview_nclex_2 + $fairview_ielts_2 + $fairview_social_2 + $fairview_agri_2 + $fairview_mid_2 + $fairview_online_2
				    + $launion_let_2 + $launion_nle_2 + $launion_crim_2 + $launion_civil_2 + $launion_psyc_2 + $launion_nclex_2 + $launion_ielts_2 + $launion_social_2 + $launion_agri_2 + $launion_mid_2 + $launion_online_2
				    + $manila_let_2 + $manila_nle_2 + $manila_crim_2 + $manila_civil_2 + $manila_psyc_2 + $manila_nclex_2 + $manila_ielts_2 + $manila_social_2 + $manila_agri_2 + $manila_mid_2 + $manila_online_2
				    + $roxas_let_2 + $roxas_nle_2 + $roxas_crim_2 + $roxas_civil_2 + $roxas_psyc_2 + $roxas_nclex_2 + $roxas_ielts_2 + $roxas_social_2 + $roxas_agri_2 + $roxas_mid_2 + $roxas_online_2
				    + $tarlac_let_2 + $tarlac_nle_2 + $tarlac_crim_2 + $tarlac_civil_2 + $tarlac_psyc_2 + $tarlac_nclex_2 + $tarlac_ielts_2 + $tarlac_social_2 + $tarlac_agri_2 + $tarlac_mid_2 + $tarlac_online_2
				    + $vigan_let_2 + $vigan_nle_2 + $vigan_crim_2 + $vigan_civil_2 + $vigan_psyc_2 + $vigan_nclex_2 + $vigan_ielts_2 + $vigan_social_2 + $vigan_agri_2 + $vigan_mid_2 + $vigan_online_2;

					$total_abra_s1 = $abra_let_1 + $abra_nle_1 + $abra_crim_1 + $abra_civil_1 + $abra_psyc_1 + $abra_nclex_1 +  $abra_ielts_1 + $abra_social_1 + $abra_agri_1 + $abra_mid_1 + $abra_online_1;
					$total_abra_s2 = $abra_let_2 + $abra_nle_2 + $abra_crim_2 + $abra_civil_2 + $abra_psyc_2 + $abra_nclex_2 +  $abra_ielts_2 + $abra_social_2 + $abra_agri_2 + $abra_mid_2 + $abra_online_2;
					   
					$total_baguio_s1 = $baguio_let_1 + $baguio_nle_1 + $baguio_crim_1 + $baguio_civil_1 + $baguio_psyc_1 + $baguio_nclex_1 +  $baguio_ielts_1 + $baguio_social_1 + $baguio_agri_1 + $baguio_mid_1 + $baguio_online_1;
					$total_baguio_s2 = $baguio_let_2 + $baguio_nle_2 + $baguio_crim_2 + $baguio_civil_2 + $baguio_psyc_2 + $baguio_nclex_2 +  $baguio_ielts_2 + $baguio_social_2 + $baguio_agri_2 + $baguio_mid_2 + $baguio_online_2;
					   
					$total_calapan_s1 = $calapan_let_1 + $calapan_nle_1 + $calapan_crim_1 + $calapan_civil_1 + $calapan_psyc_1 + $calapan_nclex_1 +  $calapan_ielts_1 + $calapan_social_1 + $calapan_agri_1 + $calapan_mid_1 + $calapan_online_1;
					$total_calapan_s2 = $calapan_let_2 + $calapan_nle_2 + $calapan_crim_2 + $calapan_civil_2 + $calapan_psyc_2 + $calapan_nclex_2 +  $calapan_ielts_2 + $calapan_social_2 + $calapan_agri_2 + $calapan_mid_2 + $calapan_online_2;
					   
					$total_candon_s1 = $candon_let_1 + $candon_nle_1 + $candon_crim_1 + $candon_civil_1 + $candon_psyc_1 + $candon_nclex_1 +  $candon_ielts_1 + $candon_social_1 + $candon_agri_1 + $candon_mid_1 + $candon_online_1;
					$total_candon_s2 = $candon_let_2 + $candon_nle_2 + $candon_crim_2 + $candon_civil_2 + $candon_psyc_2 + $candon_nclex_2 +  $candon_ielts_2 + $candon_social_2 + $candon_agri_2 + $candon_mid_2 + $candon_online_2;
					   
					$total_dagupan_s1 = $dagupan_let_1 + $dagupan_nle_1 + $dagupan_crim_1 + $dagupan_civil_1 + $dagupan_psyc_1 + $dagupan_nclex_1 +  $dagupan_ielts_1 + $dagupan_social_1 + $dagupan_agri_1 + $dagupan_mid_1 + $dagupan_online_1;
					$total_dagupan_s2 = $dagupan_let_2 + $dagupan_nle_2 + $dagupan_crim_2 + $dagupan_civil_2 + $dagupan_psyc_2 + $dagupan_nclex_2 +  $dagupan_ielts_2 + $dagupan_social_2 + $dagupan_agri_2 + $dagupan_mid_2 + $dagupan_online_2;
					   
					$total_fairview_s1 = $fairview_let_1 + $fairview_nle_1 + $fairview_crim_1 + $fairview_civil_1 + $fairview_psyc_1 + $fairview_nclex_1 +  $fairview_ielts_1 + $fairview_social_1 + $fairview_agri_1 + $fairview_mid_1 + $fairview_online_1;
					$total_fairview_s2 = $fairview_let_2 + $fairview_nle_2 + $fairview_crim_2 + $fairview_civil_2 + $fairview_psyc_2 + $fairview_nclex_2 +  $fairview_ielts_2 + $fairview_social_2 + $fairview_agri_2 + $fairview_mid_2 + $fairview_online_2;
					
					$total_las_pinas_s1 = $las_pinas_let_1 + $las_pinas_nle_1 + $las_pinas_crim_1 + $las_pinas_civil_1 + $las_pinas_psyc_1 + $las_pinas_nclex_1 +  $las_pinas_ielts_1 + $las_pinas_social_1 + $las_pinas_agri_1 + $las_pinas_mid_1 + $las_pinas_online_1;
					$total_las_pinas_s2 = $las_pinas_let_2 + $las_pinas_nle_2 + $las_pinas_crim_2 + $las_pinas_civil_2 + $las_pinas_psyc_2 + $las_pinas_nclex_2 +  $las_pinas_ielts_2 + $las_pinas_social_2 + $las_pinas_agri_2 + $las_pinas_mid_2 + $las_pinas_online_2;
					
					$total_launion_s1 = $launion_let_1 + $launion_nle_1 + $launion_crim_1 + $launion_civil_1 + $launion_psyc_1 + $launion_nclex_1 +  $launion_ielts_1 + $launion_social_1 + $launion_agri_1 + $launion_mid_1 + $launion_online_1;
					$total_launion_s2 = $launion_let_2 + $launion_nle_2 + $launion_crim_2 + $launion_civil_2 + $launion_psyc_2 + $launion_nclex_2 +  $launion_ielts_2 + $launion_social_2 + $launion_agri_2 + $launion_mid_2 + $launion_online_2;
					
					$total_manila_s1 = $manila_let_1 + $manila_nle_1 + $manila_crim_1 + $manila_civil_1 + $manila_psyc_1 + $manila_nclex_1 +  $manila_ielts_1 + $manila_social_1 + $manila_agri_1 + $manila_mid_1 + $manila_online_1;
					$total_manila_s2 = $manila_let_2 + $manila_nle_2 + $manila_crim_2 + $manila_civil_2 + $manila_psyc_2 + $manila_nclex_2 +  $manila_ielts_2 + $manila_social_2 + $manila_agri_2 + $manila_mid_2 + $manila_online_2;
					
					$total_roxas_s1 = $roxas_let_1 + $roxas_nle_1 + $roxas_crim_1 + $roxas_civil_1 + $roxas_psyc_1 + $roxas_nclex_1 +  $roxas_ielts_1 + $roxas_social_1 + $roxas_agri_1 + $roxas_mid_1 + $roxas_online_1;
					$total_roxas_s2 = $roxas_let_2 + $roxas_nle_2 + $roxas_crim_2 + $roxas_civil_2 + $roxas_psyc_2 + $roxas_nclex_2 +  $roxas_ielts_2 + $roxas_social_2 + $roxas_agri_2 + $roxas_mid_2 + $roxas_online_2;
					
					$total_tarlac_s1 = $tarlac_let_1 + $tarlac_nle_1 + $tarlac_crim_1 + $tarlac_civil_1 + $tarlac_psyc_1 + $tarlac_nclex_1 +  $tarlac_ielts_1 + $tarlac_social_1 + $tarlac_agri_1 + $tarlac_mid_1 + $tarlac_online_1;
					$total_tarlac_s2 = $tarlac_let_2 + $tarlac_nle_2 + $tarlac_crim_2 + $tarlac_civil_2 + $tarlac_psyc_2 + $tarlac_nclex_2 +  $tarlac_ielts_2 + $tarlac_social_2 + $tarlac_agri_2 + $tarlac_mid_2 + $tarlac_online_2;
					
					$total_vigan_s1 = $vigan_let_1 + $vigan_nle_1 + $vigan_crim_1 + $vigan_civil_1 + $vigan_psyc_1 + $vigan_nclex_1 +  $vigan_ielts_1 + $vigan_social_1 + $vigan_agri_1 + $vigan_mid_1 + $vigan_online_1;
					$total_vigan_s2 = $vigan_let_2 + $vigan_nle_2 + $vigan_crim_2 + $vigan_civil_2 + $vigan_psyc_2 + $vigan_nclex_2 +  $vigan_ielts_2 + $vigan_social_2 + $vigan_agri_2 + $vigan_mid_2 + $vigan_online_2;
					   

					
			 
/* end total sale */



$abra_let = AbraLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_nle = AbraNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_crim = AbraCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_civil = AbraCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_psyc = AbraPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_nclex = AbraNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_ielts = AbraIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_social = AbraSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_agri = AbraAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_mid = AbraMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$abra_online = AbraOnline::where('status','=','Enrolled')->count();

$total_abra = $abra_let + $abra_nle + $abra_crim + $abra_civil + $abra_psyc + $abra_nclex + $abra_ielts + $abra_social + $abra_agri + $abra_mid + $abra_online;




$baguio_let = BaguioLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_nle = BaguioNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_crim = BaguioCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_civil = BaguioCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_psyc = BaguioPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_nclex = BaguioNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_ielts = BaguioIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_social = BaguioSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_agri = BaguioAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_mid = BaguioMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$baguio_online = BaguioOnline::where('status','=','Enrolled')->count();

$total_baguio = $baguio_let + $baguio_nle + $baguio_crim + $baguio_civil + $baguio_psyc + $baguio_nclex + $baguio_ielts + $baguio_social + $baguio_agri + $baguio_mid + $baguio_online;





$calapan_let = CalapanLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_nle = CalapanNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_crim = CalapanCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_civil = CalapanCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_psyc = CalapanPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_nclex = CalapanNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_ielts = CalapanIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_social = CalapanSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_agri = CalapanAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_mid = CalapanMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$calapan_online = CalapanOnline::where('status','=','Enrolled')->count();

$total_calapan = $calapan_let + $calapan_nle + $calapan_crim + $calapan_civil + $calapan_psyc + $calapan_nclex + $calapan_ielts + $calapan_social + $calapan_agri + $calapan_mid + $calapan_online;




$candon_let = CandonLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_nle = CandonNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_crim = CandonCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_civil = CandonCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_psyc = CandonPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_nclex = CandonNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_ielts = CandonIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_social = CandonSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_agri = CandonAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_mid = CandonMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$candon_online = CandonOnline::where('status','=','Enrolled')->count();

$total_candon = $candon_let + $candon_nle + $candon_crim + $candon_civil + $candon_psyc + $candon_nclex + $candon_ielts + $candon_social + $candon_agri + $candon_mid + $candon_online;


$dagupan_let = DagupanLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_nle = DagupanNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_crim = DagupanCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_civil = DagupanCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_psyc = DagupanPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_nclex = DagupanNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_ielts = DagupanIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_social = DagupanSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_agri = DagupanAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_mid = DagupanMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$dagupan_online = DagupanOnline::where('status','=','Enrolled')->count();

$total_dagupan = $dagupan_let + $dagupan_nle + $dagupan_crim + $dagupan_civil + $dagupan_psyc + $dagupan_nclex + $dagupan_ielts + $dagupan_social + $dagupan_agri + $dagupan_mid + $dagupan_online;




$fairview_let = FairviewLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_nle = FairviewNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_crim = FairviewCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_civil = FairviewCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_psyc = FairviewPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_nclex = FairviewNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_ielts = FairviewIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_social = FairviewSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_agri = FairviewAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_mid = FairviewMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$fairview_online = FairviewOnline::where('status','=','Enrolled')->count();

$total_fairview = $fairview_let + $fairview_nle + $fairview_crim + $fairview_civil + $fairview_psyc + $fairview_nclex + $fairview_ielts + $fairview_social + $fairview_agri + $fairview_mid + $fairview_online;




$las_pinas_let = LasPinasLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_nle = LasPinasNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_crim = LasPinasCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_civil = LasPinasCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_psyc = LasPinasPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_nclex = LasPinasNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_ielts = LasPinasIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_social = LasPinasSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_agri = LasPinasAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_mid = LasPinasMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$las_pinas_online = LasPinasOnline::where('status','=','Enrolled')->count();

$total_las_pinas = $las_pinas_let + $las_pinas_nle + $las_pinas_crim + $las_pinas_civil + $las_pinas_psyc + $las_pinas_nclex + $las_pinas_ielts + $las_pinas_social + $las_pinas_agri + $las_pinas_mid + $las_pinas_online;





$launion_let = LaunionLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_nle = LaunionNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_crim = LaunionCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_civil = LaunionCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_psyc = LaunionPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_nclex = LaunionNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_ielts = LaunionIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_social = LaunionSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_agri = LaunionAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_mid = LaunionMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$launion_online = LaunionOnline::where('status','=','Enrolled')->count();

$total_launion = $launion_let + $launion_nle + $launion_crim + $launion_civil + $launion_psyc + $launion_nclex + $launion_ielts + $launion_social + $launion_agri + $launion_mid + $launion_online;




$manila_let = ManilaLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_nle = ManilaNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_crim = ManilaCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_civil = ManilaCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_psyc = ManilaPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_nclex = ManilaNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_ielts = ManilaIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_social = ManilaSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_agri = ManilaAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_mid = ManilaMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$manila_online = ManilaOnline::where('status','=','Enrolled')->count();

$total_manila = $manila_let + $manila_nle + $manila_crim + $manila_civil + $manila_psyc + $manila_nclex + $manila_ielts + $manila_social + $manila_agri + $manila_mid + $manila_online;


$roxas_let = RoxasLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_nle = RoxasNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_crim = RoxasCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_civil = RoxasCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_psyc = RoxasPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_nclex = RoxasNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_ielts = RoxasIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_social = RoxasSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_agri = RoxasAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_mid = RoxasMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$roxas_online = RoxasOnline::where('status','=','Enrolled')->count();

$total_roxas = $roxas_let + $roxas_nle + $roxas_crim + $roxas_civil + $roxas_psyc + $roxas_nclex + $roxas_ielts + $roxas_social + $roxas_agri + $roxas_mid + $roxas_online;



$tarlac_let = TarlacLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_nle = TarlacNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_crim = TarlacCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_civil = TarlacCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_psyc = TarlacPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_nclex = TarlacNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_ielts = TarlacIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_social = TarlacSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_agri = TarlacAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_mid = TarlacMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$tarlac_online = TarlacOnline::where('status','=','Enrolled')->count();

$total_tarlac = $tarlac_let + $tarlac_nle + $tarlac_crim + $tarlac_civil + $tarlac_psyc + $tarlac_nclex + $tarlac_ielts + $tarlac_social + $tarlac_agri + $tarlac_mid + $tarlac_online;


$vigan_let = ViganLet::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_nle = ViganNle::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_crim = ViganCrim::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_civil = ViganCivil::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_psyc = ViganPsyc::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_nclex = ViganNclex::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_ielts = ViganIelt::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_social = ViganSocial::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_agri = ViganAgri::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_mid = ViganMid::where('status','=','Enrolled')->where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->count();

$vigan_online = ViganOnline::where('status','=','Enrolled')->count();

$total_vigan = $vigan_let + $vigan_nle + $vigan_crim + $vigan_civil + $vigan_psyc + $vigan_nclex + $vigan_ielts + $vigan_social + $vigan_agri + $vigan_mid + $vigan_online;




$total_enrollee = $total_abra + $total_baguio + $total_calapan + $total_candon + $total_fairview + $total_las_pinas + $total_launion + $total_manila + $total_roxas + $total_tarlac + $total_vigan;



$id = ExpenseSetup::value('id');

$setup = ExpenseSetup::findorfail($id);



$abra_expense = AbraExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$baguio_expense = BaguioExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$calapan_expense = CalapanExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$candon_expense = CandonExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$dagupan_expense = DagupanExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$fairview_expense = FairviewExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$las_pinas_expense = LasPinasExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$launion_expense = LaunionExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$manila_expense = ManilaExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$roxas_expense = RoxasExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$tarlac_expense = TarlacExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');
$vigan_expense = ViganExpense::where('Season','=',$setting->Season)->where('Year','=',$setting->Year)->sum('amount');

$total_expense = $abra_expense + $baguio_expense + $calapan_expense + $candon_expense + $dagupan_expense + $fairview_expense + $las_pinas_expense + $launion_expense + $manila_expense + $roxas_expense + $tarlac_expense + $vigan_expense;


$abra_receivable = AbraReceivable::sum('balance');
$baguio_receivable = BaguioReceivable::sum('balance');
$calapan_receivable = CalapanReceivable::sum('balance');
$candon_receivable = CandonReceivable::sum('balance');
$fairview_receivable = FairviewReceivable::sum('balance');
$las_pinas_receivable = LasPinasReceivable::sum('balance');
$launion_receivable = LaunionReceivable::sum('balance');
$manila_receivable = ManilaReceivable::sum('balance');
$roxas_receivable = RoxasReceivable::sum('balance');
$tarlac_receivable = TarlacReceivable::sum('balance');
$vigan_receivable = ViganReceivable::sum('balance');

$total_receivable = $abra_receivable + $baguio_receivable + $calapan_receivable + $candon_receivable + $fairview_receivable + $las_pinas_receivable + $launion_receivable + $manila_receivable + $roxas_receivable + $tarlac_receivable + $vigan_receivable;


$abra_s1_dis = AbraS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$abra_s2_dis = AbraS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
   // $discount = $s1_dis + $s2_dis;

$baguio_s1_dis = BaguioS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$baguio_s2_dis = BaguioS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
   
$calapan_s1_dis = CalapanS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$calapan_s2_dis = CalapanS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	   
$candon_s1_dis = CandonS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$candon_s2_dis = CandonS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	
$dagupan_s1_dis = DagupanS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$dagupan_s2_dis = DagupanS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
$fairview_s1_dis = FairviewS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$fairview_s2_dis = FairviewS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
   // $discount = $s1_dis + $s2_dis;

$las_pinas_s1_dis = LasPinasS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$las_pinas_s2_dis = LasPinasS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
   
$launion_s1_dis = LaunionS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$launion_s2_dis = LaunionS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	   
$manila_s1_dis = ManilaS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$manila_s2_dis = ManilaS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	
$roxas_s1_dis = RoxasS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$roxas_s2_dis = RoxasS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
$tarlac_s1_dis = TarlacS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$tarlac_s2_dis = TarlacS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	
$vigan_s1_dis = ViganS1Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
$vigan_s2_dis = ViganS2Sale::where('Year','=',$setting->Year)->where('Season','=',$setting->Season)->sum('discount');
	// $discount = $s1_dis + $s2_dis;
	

	if($setting->Season == 'Season 1'){
		 $discount = $abra_s1_dis;
		 $discount = $baguio_s1_dis;
		 $discount = $calapan_s1_dis;
		 $discount = $candon_s1_dis;
		 $discount = $dagupan_s1_dis;
		 $discount = $fairview_s1_dis;
		 $discount = $las_pinas_s1_dis;
		 $discount = $launion_s1_dis;
		 $discount = $manila_s1_dis;
		 $discount = $roxas_s1_dis;
		 $discount = $tarlac_s1_dis;
		 $discount = $vigan_s1_dis;
	}
	else{
		$discount = $abra_s2_dis;
		$discount = $baguio_s2_dis;
		$discount = $calapan_s2_dis;
		$discount = $candon_s2_dis;
		$discount = $dagupan_s2_dis;
		$discount = $fairview_s2_dis;
		$discount = $las_pinas_s2_dis;
		$discount = $launion_s2_dis;
		$discount = $manila_s2_dis;
		$discount = $roxas_s2_dis;
		$discount = $tarlac_s2_dis;
		$discount = $vigan_s2_dis;
	}


    	return view ('/admin.dashboard')
    	->with('total_s1',$total_s1)
		->with('total_s2',$total_s2)
		->with('total_expense',$total_expense)
		->with('total_enrollee',$total_enrollee)
		->with('total_receivable',$total_receivable)
		->with('discount',$discount)
		
		->with('abra_facilitation',$abra_facilitation)
		->with('baguio_facilitation',$baguio_facilitation)
		->with('calapan_facilitation',$calapan_facilitation)
		->with('candon_facilitation',$candon_facilitation)
		->with('dagupan_facilitation',$dagupan_facilitation)
		->with('fairview_facilitation',$fairview_facilitation)
		->with('las_pinas_facilitation',$las_pinas_facilitation)
		->with('launion_facilitation',$launion_facilitation)
		->with('manila_facilitation',$manila_facilitation)
		->with('roxas_facilitation',$roxas_facilitation)
		->with('tarlac_facilitation',$tarlac_facilitation)
		->with('vigan_facilitation',$vigan_facilitation)

    	->with('total_abra_s1',$total_abra_s1)
		->with('total_abra_s2',$total_abra_s2)
		
    	->with('total_baguio_s1',$total_baguio_s1)
		->with('total_baguio_s2',$total_baguio_s2)
		
    	->with('total_calapan_s1',$total_calapan_s1)
		->with('total_calapan_s2',$total_calapan_s2)
		
    	->with('total_candon_s1',$total_candon_s1)
		->with('total_candon_s2',$total_candon_s2)
		
    	->with('total_dagupan_s1',$total_dagupan_s1)
		->with('total_dagupan_s2',$total_dagupan_s2)
		
		->with('total_fairview_s1',$total_fairview_s1)
		->with('total_fairview_s2',$total_fairview_s2)
		
    	->with('total_las_pinas_s1',$total_las_pinas_s1)
		->with('total_las_pinas_s2',$total_las_pinas_s2)
		
    	->with('total_launion_s1',$total_launion_s1)
		->with('total_launion_s2',$total_launion_s2)
		
    	->with('total_manila_s1',$total_manila_s1)
		->with('total_manila_s2',$total_manila_s2)
		
    	->with('total_roxas_s1',$total_roxas_s1)
		->with('total_roxas_s2',$total_roxas_s2)
		
    	->with('total_tarlac_s1',$total_tarlac_s1)
		->with('total_tarlac_s2',$total_tarlac_s2)
		
    	->with('total_vigan_s1',$total_vigan_s1)
		->with('total_vigan_s2',$total_vigan_s2)
		
		->with('abra_expense',$abra_expense)
		->with('baguio_expense',$baguio_expense)
		->with('calapan_expense',$calapan_expense)
		->with('candon_expense',$candon_expense)
		->with('dagupan_expense',$dagupan_expense)
		->with('fairview_expense',$fairview_expense)
		->with('las_pinas_expense',$las_pinas_expense)
		->with('launion_expense',$launion_expense)
		->with('manila_expense',$manila_expense)
		->with('roxas_expense',$roxas_expense)
		->with('tarlac_expense',$tarlac_expense)
		->with('vigan_expense',$vigan_expense)

		->with('total_abra',$total_abra)
		->with('total_baguio',$total_baguio)
		->with('total_calapan',$total_calapan)
		->with('total_candon',$total_candon)
		->with('total_dagupan',$total_dagupan)
		->with('total_fairview',$total_fairview)
		->with('total_las_pinas',$total_las_pinas)
		->with('total_launion',$total_launion)
		->with('total_manila',$total_manila)
		->with('total_roxas',$total_roxas)
		->with('total_tarlac',$total_tarlac)
		->with('total_vigan',$total_vigan);

    }

public function total_program(){

	$abra_let_1_sale=AbraS1Sale::where('program','=','LET')->sum('amount_paid');

	$abra_nle_1_sale=AbraS1Sale::where('program','=','NLE')->sum('amount_paid');

	$abra_crim_1_sale=AbraS1Sale::where('program','=','Criminology')->sum('amount_paid');

	$abra_civil_1_sale=AbraS1Sale::where('program','=','Civil Service')->sum('amount_paid');

	$abra_psyc_1_sale=AbraS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

	$abra_nclex_1_sale=AbraS1Sale::where('program','=','NCLEX')->sum('amount_paid');

	$abra_ielts_1_sale=AbraS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$abra_social_1_sale=AbraS1Sale::where('program','=','Social')->sum('amount_paid');

	$abra_agri_1_sale=AbraS1Sale::where('program','=','Agriculture')->sum('amount_paid');

	$abra_mid_1_sale=AbraS1Sale::where('program','=','Midwifery')->sum('amount_paid');

	$abra_online_1_sale=AbraS1Sale::where('program','=','Online Only')->sum('amount_paid');

	$abra_let_2_sale=AbraS2Sale::where('program','=','LET')->sum('amount_paid');

	$abra_nle_2_sale=AbraS2Sale::where('program','=','NLE')->sum('amount_paid');

	$abra_crim_2_sale=AbraS2Sale::where('program','=','Criminology')->sum('amount_paid');

	$abra_civil_2_sale=AbraS2Sale::where('program','=','Civil Service')->sum('amount_paid');

	$abra_psyc_2_sale=AbraS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

	$abra_nclex_2_sale=AbraS2Sale::where('program','=','NCLEX')->sum('amount_paid');

	$abra_ielts_2_sale=AbraS2Sale::where('program','=','IELTS')->sum('amount_paid');

	$abra_social_2_sale=AbraS2Sale::where('program','=','Social')->sum('amount_paid');

	$abra_agri_2_sale=AbraS2Sale::where('program','=','Agriculture')->sum('amount_paid');

	$abra_mid_2_sale=AbraS2Sale::where('program','=','Midwifery')->sum('amount_paid');

	$abra_online_2_sale=AbraS2Sale::where('program','=','Online Only')->sum('amount_paid');

	$abra_let = 	AbraLet::where('status','=','Enrolled')->count();

	$abra_nle = 	AbraNle::where('status','=','Enrolled')->count();

	$abra_crim = AbraCrim::where('status','=','Enrolled')->count();

	$abra_civil= AbraCivil::where('status','=','Enrolled')->count();

	$abra_psyc = AbraPsyc::where('status','=','Enrolled')->count();

	$abra_nclex = AbraNclex::where('status','=','Enrolled')->count();

	$abra_ielts = AbraIelt::where('status','=','Enrolled')->count();

	$abra_social = AbraSocial::where('status','=','Enrolled')->count();

	$abra_agri = AbraAgri::where('status','=','Enrolled')->count();

	$abra_mid = AbraMid::where('status','=','Enrolled')->count();

	$abra_online = AbraOnline::where('status','=','Enrolled')->count();


	$baguio_let_1_sale=BaguioS1Sale::where('program','=','LET')->sum('amount_paid');

    	$baguio_nle_1_sale=BaguioS1Sale::where('program','=','NLE')->sum('amount_paid');

		$baguio_crim_1_sale=BaguioS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$baguio_civil_1_sale=BaguioS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$baguio_psyc_1_sale=BaguioS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$baguio_nclex_1_sale=BaguioS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$baguio_ielts_1_sale=BaguioS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$baguio_social_1_sale=BaguioS1Sale::where('program','=','Social')->sum('amount_paid');

		$baguio_agri_1_sale=BaguioS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$baguio_mid_1_sale=BaguioS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$baguio_online_1_sale=BaguioS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$baguio_let_2_sale=BaguioS2Sale::where('program','=','LET')->sum('amount_paid');

    	$baguio_nle_2_sale=BaguioS2Sale::where('program','=','NLE')->sum('amount_paid');

		$baguio_crim_2_sale=BaguioS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$baguio_civil_2_sale=BaguioS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$baguio_psyc_2_sale=BaguioS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$baguio_nclex_2_sale=BaguioS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$baguio_ielts_2_sale=BaguioS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$baguio_social_2_sale=BaguioS2Sale::where('program','=','Social')->sum('amount_paid');

		$baguio_agri_2_sale=BaguioS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$baguio_mid_2_sale=BaguioS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$baguio_online_2_sale=BaguioS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$baguio_let = 	BaguioLet::where('status','=','Enrolled')->count();

		$baguio_nle = 	BaguioNle::where('status','=','Enrolled')->count();

		$baguio_crim = BaguioCrim::where('status','=','Enrolled')->count();

		$baguio_civil= BaguioCivil::where('status','=','Enrolled')->count();

		$baguio_psyc = BaguioPsyc::where('status','=','Enrolled')->count();

		$baguio_nclex = BaguioNclex::where('status','=','Enrolled')->count();

		$baguio_ielts = BaguioIelt::where('status','=','Enrolled')->count();

		$baguio_social = BaguioSocial::where('status','=','Enrolled')->count();

		$baguio_agri = BaguioAgri::where('status','=','Enrolled')->count();

		$baguio_mid = BaguioMid::where('status','=','Enrolled')->count();

		$baguio_online = BaguioOnline::where('status','=','Enrolled')->count();


		$calapan_let_1_sale=CalapanS1Sale::where('program','=','LET')->sum('amount_paid');

    	$calapan_nle_1_sale=CalapanS1Sale::where('program','=','NLE')->sum('amount_paid');

		$calapan_crim_1_sale=CalapanS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$calapan_civil_1_sale=CalapanS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$calapan_psyc_1_sale=CalapanS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$calapan_nclex_1_sale=CalapanS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$calapan_ielts_1_sale=CalapanS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$calapan_social_1_sale=CalapanS1Sale::where('program','=','Social')->sum('amount_paid');

		$calapan_agri_1_sale=CalapanS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$calapan_mid_1_sale=CalapanS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$calapan_online_1_sale=CalapanS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$calapan_let_2_sale=CalapanS2Sale::where('program','=','LET')->sum('amount_paid');

    	$calapan_nle_2_sale=CalapanS2Sale::where('program','=','NLE')->sum('amount_paid');

		$calapan_crim_2_sale=CalapanS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$calapan_civil_2_sale=CalapanS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$calapan_psyc_2_sale=CalapanS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$calapan_nclex_2_sale=CalapanS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$calapan_ielts_2_sale=CalapanS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$calapan_social_2_sale=CalapanS2Sale::where('program','=','Social')->sum('amount_paid');

		$calapan_agri_2_sale=CalapanS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$calapan_mid_2_sale=CalapanS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$calapan_online_2_sale=CalapanS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$calapan_let = 	CalapanLet::where('status','=','Enrolled')->count();

		$calapan_nle = 	CalapanNle::where('status','=','Enrolled')->count();

		$calapan_crim = CalapanCrim::where('status','=','Enrolled')->count();

		$calapan_civil= CalapanCivil::where('status','=','Enrolled')->count();

		$calapan_psyc = CalapanPsyc::where('status','=','Enrolled')->count();

		$calapan_nclex = CalapanNclex::where('status','=','Enrolled')->count();

		$calapan_ielts = CalapanIelt::where('status','=','Enrolled')->count();

		$calapan_social = CalapanSocial::where('status','=','Enrolled')->count();

		$calapan_agri = CalapanAgri::where('status','=','Enrolled')->count();

		$calapan_mid = CalapanMid::where('status','=','Enrolled')->count();

		$calapan_online = CalapanOnline::where('status','=','Enrolled')->count();


		$candon_let_1_sale=CandonS1Sale::where('program','=','LET')->sum('amount_paid');

    	$candon_nle_1_sale=CandonS1Sale::where('program','=','NLE')->sum('amount_paid');

		$candon_crim_1_sale=CandonS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$candon_civil_1_sale=CandonS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$candon_psyc_1_sale=CandonS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$candon_nclex_1_sale=CandonS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$candon_ielts_1_sale=CandonS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$candon_social_1_sale=CandonS1Sale::where('program','=','Social')->sum('amount_paid');

		$candon_agri_1_sale=CandonS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$candon_mid_1_sale=CandonS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$candon_online_1_sale=CandonS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$candon_let_2_sale=CandonS2Sale::where('program','=','LET')->sum('amount_paid');

    	$candon_nle_2_sale=CandonS2Sale::where('program','=','NLE')->sum('amount_paid');

		$candon_crim_2_sale=CandonS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$candon_civil_2_sale=CandonS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$candon_psyc_2_sale=CandonS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$candon_nclex_2_sale=CandonS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$candon_ielts_2_sale=CandonS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$candon_social_2_sale=CandonS2Sale::where('program','=','Social')->sum('amount_paid');

		$candon_agri_2_sale=CandonS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$candon_mid_2_sale=CandonS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$candon_online_2_sale=CandonS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$candon_let = 	CandonLet::where('status','=','Enrolled')->count();

		$candon_nle = 	CandonNle::where('status','=','Enrolled')->count();

		$candon_crim = CandonCrim::where('status','=','Enrolled')->count();

		$candon_civil= CandonCivil::where('status','=','Enrolled')->count();

		$candon_psyc = CandonPsyc::where('status','=','Enrolled')->count();

		$candon_nclex = CandonNclex::where('status','=','Enrolled')->count();

		$candon_ielts = CandonIelt::where('status','=','Enrolled')->count();

		$candon_social = CandonSocial::where('status','=','Enrolled')->count();

		$candon_agri = CandonAgri::where('status','=','Enrolled')->count();

		$candon_mid = CandonMid::where('status','=','Enrolled')->count();

		$candon_online = CandonOnline::where('status','=','Enrolled')->count();

		$dagupan_let_1_sale=DagupanS1Sale::where('program','=','LET')->sum('amount_paid');

    	$dagupan_nle_1_sale=DagupanS1Sale::where('program','=','NLE')->sum('amount_paid');

		$dagupan_crim_1_sale=DagupanS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$dagupan_civil_1_sale=DagupanS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$dagupan_psyc_1_sale=DagupanS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$dagupan_nclex_1_sale=DagupanS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$dagupan_ielts_1_sale=DagupanS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$dagupan_social_1_sale=DagupanS1Sale::where('program','=','Social')->sum('amount_paid');

		$dagupan_agri_1_sale=DagupanS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$dagupan_mid_1_sale=DagupanS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$dagupan_online_1_sale=DagupanS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$dagupan_let_2_sale=DagupanS2Sale::where('program','=','LET')->sum('amount_paid');

    	$dagupan_nle_2_sale=DagupanS2Sale::where('program','=','NLE')->sum('amount_paid');

		$dagupan_crim_2_sale=DagupanS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$dagupan_civil_2_sale=DagupanS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$dagupan_psyc_2_sale=DagupanS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$dagupan_nclex_2_sale=DagupanS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$dagupan_ielts_2_sale=DagupanS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$dagupan_social_2_sale=DagupanS2Sale::where('program','=','Social')->sum('amount_paid');

		$dagupan_agri_2_sale=DagupanS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$dagupan_mid_2_sale=DagupanS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$dagupan_online_2_sale=DagupanS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$dagupan_let = 	DagupanLet::where('status','=','Enrolled')->count();

		$dagupan_nle = 	DagupanNle::where('status','=','Enrolled')->count();

		$dagupan_crim = DagupanCrim::where('status','=','Enrolled')->count();

		$dagupan_civil= DagupanCivil::where('status','=','Enrolled')->count();

		$dagupan_psyc = DagupanPsyc::where('status','=','Enrolled')->count();

		$dagupan_nclex = DagupanNclex::where('status','=','Enrolled')->count();

		$dagupan_ielts = DagupanIelt::where('status','=','Enrolled')->count();

		$dagupan_social = DagupanSocial::where('status','=','Enrolled')->count();

		$dagupan_agri = DagupanAgri::where('status','=','Enrolled')->count();

		$dagupan_mid = DagupanMid::where('status','=','Enrolled')->count();

		$dagupan_online = DagupanOnline::where('status','=','Enrolled')->count();

		$fairview_let_1_sale=FairviewS1Sale::where('program','=','LET')->sum('amount_paid');

	$fairview_nle_1_sale=FairviewS1Sale::where('program','=','NLE')->sum('amount_paid');

	$fairview_crim_1_sale=FairviewS1Sale::where('program','=','Criminology')->sum('amount_paid');

	$fairview_civil_1_sale=FairviewS1Sale::where('program','=','Civil Service')->sum('amount_paid');

	$fairview_psyc_1_sale=FairviewS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

	$fairview_nclex_1_sale=FairviewS1Sale::where('program','=','NCLEX')->sum('amount_paid');

	$fairview_ielts_1_sale=FairviewS1Sale::where('program','=','IELTS')->sum('amount_paid');

	$fairview_social_1_sale=FairviewS1Sale::where('program','=','Social')->sum('amount_paid');

	$fairview_agri_1_sale=FairviewS1Sale::where('program','=','Agriculture')->sum('amount_paid');

	$fairview_mid_1_sale=FairviewS1Sale::where('program','=','Midwifery')->sum('amount_paid');

	$fairview_online_1_sale=FairviewS1Sale::where('program','=','Online Only')->sum('amount_paid');

	$fairview_let_2_sale=FairviewS2Sale::where('program','=','LET')->sum('amount_paid');

	$fairview_nle_2_sale=FairviewS2Sale::where('program','=','NLE')->sum('amount_paid');

	$fairview_crim_2_sale=FairviewS2Sale::where('program','=','Criminology')->sum('amount_paid');

	$fairview_civil_2_sale=FairviewS2Sale::where('program','=','Civil Service')->sum('amount_paid');

	$fairview_psyc_2_sale=FairviewS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

	$fairview_nclex_2_sale=FairviewS2Sale::where('program','=','NCLEX')->sum('amount_paid');

	$fairview_ielts_2_sale=FairviewS2Sale::where('program','=','IELTS')->sum('amount_paid');

	$fairview_social_2_sale=FairviewS2Sale::where('program','=','Social')->sum('amount_paid');

	$fairview_agri_2_sale=FairviewS2Sale::where('program','=','Agriculture')->sum('amount_paid');

	$fairview_mid_2_sale=FairviewS2Sale::where('program','=','Midwifery')->sum('amount_paid');

	$fairview_online_2_sale=FairviewS2Sale::where('program','=','Online Only')->sum('amount_paid');

	$fairview_let = 	FairviewLet::where('status','=','Enrolled')->count();

	$fairview_nle = 	FairviewNle::where('status','=','Enrolled')->count();

	$fairview_crim = FairviewCrim::where('status','=','Enrolled')->count();

	$fairview_civil= FairviewCivil::where('status','=','Enrolled')->count();

	$fairview_psyc = FairviewPsyc::where('status','=','Enrolled')->count();

	$fairview_nclex = FairviewNclex::where('status','=','Enrolled')->count();

	$fairview_ielts = FairviewIelt::where('status','=','Enrolled')->count();

	$fairview_social = FairviewSocial::where('status','=','Enrolled')->count();

	$fairview_agri = FairviewAgri::where('status','=','Enrolled')->count();

	$fairview_mid = FairviewMid::where('status','=','Enrolled')->count();

	$fairview_online = FairviewOnline::where('status','=','Enrolled')->count();


	$las_pinas_let_1_sale=LasPinasS1Sale::where('program','=','LET')->sum('amount_paid');

    	$las_pinas_nle_1_sale=LasPinasS1Sale::where('program','=','NLE')->sum('amount_paid');

		$las_pinas_crim_1_sale=LasPinasS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$las_pinas_civil_1_sale=LasPinasS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$las_pinas_psyc_1_sale=LasPinasS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$las_pinas_nclex_1_sale=LasPinasS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$las_pinas_ielts_1_sale=LasPinasS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$las_pinas_social_1_sale=LasPinasS1Sale::where('program','=','Social')->sum('amount_paid');

		$las_pinas_agri_1_sale=LasPinasS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$las_pinas_mid_1_sale=LasPinasS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$las_pinas_online_1_sale=LasPinasS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$las_pinas_let_2_sale=LasPinasS2Sale::where('program','=','LET')->sum('amount_paid');

    	$las_pinas_nle_2_sale=LasPinasS2Sale::where('program','=','NLE')->sum('amount_paid');

		$las_pinas_crim_2_sale=LasPinasS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$las_pinas_civil_2_sale=LasPinasS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$las_pinas_psyc_2_sale=LasPinasS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$las_pinas_nclex_2_sale=LasPinasS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$las_pinas_ielts_2_sale=LasPinasS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$las_pinas_social_2_sale=LasPinasS2Sale::where('program','=','Social')->sum('amount_paid');

		$las_pinas_agri_2_sale=LasPinasS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$las_pinas_mid_2_sale=LasPinasS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$las_pinas_online_2_sale=LasPinasS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$las_pinas_let = 	LasPinasLet::where('status','=','Enrolled')->count();

		$las_pinas_nle = 	LasPinasNle::where('status','=','Enrolled')->count();

		$las_pinas_crim = LasPinasCrim::where('status','=','Enrolled')->count();

		$las_pinas_civil= LasPinasCivil::where('status','=','Enrolled')->count();

		$las_pinas_psyc = LasPinasPsyc::where('status','=','Enrolled')->count();

		$las_pinas_nclex = LasPinasNclex::where('status','=','Enrolled')->count();

		$las_pinas_ielts = LasPinasIelt::where('status','=','Enrolled')->count();

		$las_pinas_social = LasPinasSocial::where('status','=','Enrolled')->count();

		$las_pinas_agri = LasPinasAgri::where('status','=','Enrolled')->count();

		$las_pinas_mid = LasPinasMid::where('status','=','Enrolled')->count();

		$las_pinas_online = LasPinasOnline::where('status','=','Enrolled')->count();


		$launion_let_1_sale=LaunionS1Sale::where('program','=','LET')->sum('amount_paid');

    	$launion_nle_1_sale=LaunionS1Sale::where('program','=','NLE')->sum('amount_paid');

		$launion_crim_1_sale=LaunionS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$launion_civil_1_sale=LaunionS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$launion_psyc_1_sale=LaunionS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$launion_nclex_1_sale=LaunionS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$launion_ielts_1_sale=LaunionS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$launion_social_1_sale=LaunionS1Sale::where('program','=','Social')->sum('amount_paid');

		$launion_agri_1_sale=LaunionS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$launion_mid_1_sale=LaunionS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$launion_online_1_sale=LaunionS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$launion_let_2_sale=LaunionS2Sale::where('program','=','LET')->sum('amount_paid');

    	$launion_nle_2_sale=LaunionS2Sale::where('program','=','NLE')->sum('amount_paid');

		$launion_crim_2_sale=LaunionS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$launion_civil_2_sale=LaunionS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$launion_psyc_2_sale=LaunionS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$launion_nclex_2_sale=LaunionS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$launion_ielts_2_sale=LaunionS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$launion_social_2_sale=LaunionS2Sale::where('program','=','Social')->sum('amount_paid');

		$launion_agri_2_sale=LaunionS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$launion_mid_2_sale=LaunionS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$launion_online_2_sale=LaunionS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$launion_let = 	LaunionLet::where('status','=','Enrolled')->count();

		$launion_nle = 	LaunionNle::where('status','=','Enrolled')->count();

		$launion_crim = LaunionCrim::where('status','=','Enrolled')->count();

		$launion_civil= LaunionCivil::where('status','=','Enrolled')->count();

		$launion_psyc = LaunionPsyc::where('status','=','Enrolled')->count();

		$launion_nclex = LaunionNclex::where('status','=','Enrolled')->count();

		$launion_ielts = LaunionIelt::where('status','=','Enrolled')->count();

		$launion_social = LaunionSocial::where('status','=','Enrolled')->count();

		$launion_agri = LaunionAgri::where('status','=','Enrolled')->count();

		$launion_mid = LaunionMid::where('status','=','Enrolled')->count();

		$launion_online = LaunionOnline::where('status','=','Enrolled')->count();


		$roxas_let_1_sale=RoxasS1Sale::where('program','=','LET')->sum('amount_paid');

    	$roxas_nle_1_sale=RoxasS1Sale::where('program','=','NLE')->sum('amount_paid');

		$roxas_crim_1_sale=RoxasS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$roxas_civil_1_sale=RoxasS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$roxas_psyc_1_sale=RoxasS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$roxas_nclex_1_sale=RoxasS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$roxas_ielts_1_sale=RoxasS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$roxas_social_1_sale=RoxasS1Sale::where('program','=','Social')->sum('amount_paid');

		$roxas_agri_1_sale=RoxasS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$roxas_mid_1_sale=RoxasS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$roxas_online_1_sale=RoxasS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$roxas_let_2_sale=RoxasS2Sale::where('program','=','LET')->sum('amount_paid');

    	$roxas_nle_2_sale=RoxasS2Sale::where('program','=','NLE')->sum('amount_paid');

		$roxas_crim_2_sale=RoxasS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$roxas_civil_2_sale=RoxasS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$roxas_psyc_2_sale=RoxasS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$roxas_nclex_2_sale=RoxasS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$roxas_ielts_2_sale=RoxasS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$roxas_social_2_sale=RoxasS2Sale::where('program','=','Social')->sum('amount_paid');

		$roxas_agri_2_sale=RoxasS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$roxas_mid_2_sale=RoxasS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$roxas_online_2_sale=RoxasS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$roxas_let = 	RoxasLet::where('status','=','Enrolled')->count();

		$roxas_nle = 	RoxasNle::where('status','=','Enrolled')->count();

		$roxas_crim = RoxasCrim::where('status','=','Enrolled')->count();

		$roxas_civil= RoxasCivil::where('status','=','Enrolled')->count();

		$roxas_psyc = RoxasPsyc::where('status','=','Enrolled')->count();

		$roxas_nclex = RoxasNclex::where('status','=','Enrolled')->count();

		$roxas_ielts = RoxasIelt::where('status','=','Enrolled')->count();

		$roxas_social = RoxasSocial::where('status','=','Enrolled')->count();

		$roxas_agri = RoxasAgri::where('status','=','Enrolled')->count();

		$roxas_mid = RoxasMid::where('status','=','Enrolled')->count();

		$roxas_online = RoxasOnline::where('status','=','Enrolled')->count();

		$manila_let_1_sale=ManilaS1Sale::where('program','=','LET')->sum('amount_paid');

    	$manila_nle_1_sale=ManilaS1Sale::where('program','=','NLE')->sum('amount_paid');

		$manila_crim_1_sale=ManilaS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$manila_civil_1_sale=ManilaS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$manila_psyc_1_sale=ManilaS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$manila_nclex_1_sale=ManilaS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$manila_ielts_1_sale=ManilaS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$manila_social_1_sale=ManilaS1Sale::where('program','=','Social')->sum('amount_paid');

		$manila_agri_1_sale=ManilaS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$manila_mid_1_sale=ManilaS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$manila_online_1_sale=ManilaS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$manila_let_2_sale=ManilaS2Sale::where('program','=','LET')->sum('amount_paid');

    	$manila_nle_2_sale=ManilaS2Sale::where('program','=','NLE')->sum('amount_paid');

		$manila_crim_2_sale=ManilaS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$manila_civil_2_sale=ManilaS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$manila_psyc_2_sale=ManilaS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$manila_nclex_2_sale=ManilaS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$manila_ielts_2_sale=ManilaS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$manila_social_2_sale=ManilaS2Sale::where('program','=','Social')->sum('amount_paid');

		$manila_agri_2_sale=ManilaS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$manila_mid_2_sale=ManilaS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$manila_online_2_sale=ManilaS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$manila_let = 	ManilaLet::where('status','=','Enrolled')->count();

		$manila_nle = 	ManilaNle::where('status','=','Enrolled')->count();

		$manila_crim = ManilaCrim::where('status','=','Enrolled')->count();

		$manila_civil= ManilaCivil::where('status','=','Enrolled')->count();

		$manila_psyc = ManilaPsyc::where('status','=','Enrolled')->count();

		$manila_nclex = ManilaNclex::where('status','=','Enrolled')->count();

		$manila_ielts = ManilaIelt::where('status','=','Enrolled')->count();

		$manila_social = ManilaSocial::where('status','=','Enrolled')->count();

		$manila_agri = ManilaAgri::where('status','=','Enrolled')->count();

		$manila_mid = ManilaMid::where('status','=','Enrolled')->count();

		$manila_online = ManilaOnline::where('status','=','Enrolled')->count();

		

		$tarlac_let_1_sale=TarlacS1Sale::where('program','=','LET')->sum('amount_paid');

    	$tarlac_nle_1_sale=TarlacS1Sale::where('program','=','NLE')->sum('amount_paid');

		$tarlac_crim_1_sale=TarlacS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$tarlac_civil_1_sale=TarlacS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$tarlac_psyc_1_sale=TarlacS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$tarlac_nclex_1_sale=TarlacS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$tarlac_ielts_1_sale=TarlacS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$tarlac_social_1_sale=TarlacS1Sale::where('program','=','Social')->sum('amount_paid');

		$tarlac_agri_1_sale=TarlacS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$tarlac_mid_1_sale=TarlacS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$tarlac_online_1_sale=TarlacS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$tarlac_let_2_sale=TarlacS2Sale::where('program','=','LET')->sum('amount_paid');

    	$tarlac_nle_2_sale=TarlacS2Sale::where('program','=','NLE')->sum('amount_paid');

		$tarlac_crim_2_sale=TarlacS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$tarlac_civil_2_sale=TarlacS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$tarlac_psyc_2_sale=TarlacS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$tarlac_nclex_2_sale=TarlacS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$tarlac_ielts_2_sale=TarlacS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$tarlac_social_2_sale=TarlacS2Sale::where('program','=','Social')->sum('amount_paid');

		$tarlac_agri_2_sale=TarlacS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$tarlac_mid_2_sale=TarlacS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$tarlac_online_2_sale=TarlacS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$tarlac_let = 	TarlacLet::where('status','=','Enrolled')->count();

		$tarlac_nle = 	TarlacNle::where('status','=','Enrolled')->count();

		$tarlac_crim = TarlacCrim::where('status','=','Enrolled')->count();

		$tarlac_civil= TarlacCivil::where('status','=','Enrolled')->count();

		$tarlac_psyc = TarlacPsyc::where('status','=','Enrolled')->count();

		$tarlac_nclex = TarlacNclex::where('status','=','Enrolled')->count();

		$tarlac_ielts = TarlacIelt::where('status','=','Enrolled')->count();

		$tarlac_social = TarlacSocial::where('status','=','Enrolled')->count();

		$tarlac_agri = TarlacAgri::where('status','=','Enrolled')->count();

		$tarlac_mid = TarlacMid::where('status','=','Enrolled')->count();

		$tarlac_online = TarlacOnline::where('status','=','Enrolled')->count();

		$vigan_let_1_sale=ViganS1Sale::where('program','=','LET')->sum('amount_paid');

    	$vigan_nle_1_sale=ViganS1Sale::where('program','=','NLE')->sum('amount_paid');

		$vigan_crim_1_sale=ViganS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$vigan_civil_1_sale=ViganS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$vigan_psyc_1_sale=ViganS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$vigan_nclex_1_sale=ViganS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$vigan_ielts_1_sale=ViganS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$vigan_social_1_sale=ViganS1Sale::where('program','=','Social')->sum('amount_paid');

		$vigan_agri_1_sale=ViganS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$vigan_mid_1_sale=ViganS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$vigan_online_1_sale=ViganS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$vigan_let_2_sale=ViganS2Sale::where('program','=','LET')->sum('amount_paid');

    	$vigan_nle_2_sale=ViganS2Sale::where('program','=','NLE')->sum('amount_paid');

		$vigan_crim_2_sale=ViganS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$vigan_civil_2_sale=ViganS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$vigan_psyc_2_sale=ViganS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$vigan_nclex_2_sale=ViganS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$vigan_ielts_2_sale=ViganS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$vigan_social_2_sale=ViganS2Sale::where('program','=','Social')->sum('amount_paid');

		$vigan_agri_2_sale=ViganS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$vigan_mid_2_sale=ViganS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$vigan_online_2_sale=ViganS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$vigan_let = 	ViganLet::where('status','=','Enrolled')->count();

		$vigan_nle = 	ViganNle::where('status','=','Enrolled')->count();

		$vigan_crim = ViganCrim::where('status','=','Enrolled')->count();

		$vigan_civil= ViganCivil::where('status','=','Enrolled')->count();

		$vigan_psyc = ViganPsyc::where('status','=','Enrolled')->count();

		$vigan_nclex = ViganNclex::where('status','=','Enrolled')->count();

		$vigan_ielts = ViganIelt::where('status','=','Enrolled')->count();

		$vigan_social = ViganSocial::where('status','=','Enrolled')->count();

		$vigan_agri = ViganAgri::where('status','=','Enrolled')->count();

		$vigan_mid = ViganMid::where('status','=','Enrolled')->count();

		$vigan_online = ViganOnline::where('status','=','Enrolled')->count();

	return view ('admin.total-program')
	->with('abra_let_1_sale',$abra_let_1_sale)
	->with('abra_nle_1_sale',$abra_nle_1_sale)
	->with('abra_crim_1_sale',$abra_crim_1_sale)
	->with('abra_civil_1_sale',$abra_civil_1_sale)
	->with('abra_psyc_1_sale',$abra_psyc_1_sale)
	->with('abra_nclex_1_sale',$abra_nclex_1_sale)
	->with('abra_ielts_1_sale',$abra_ielts_1_sale)
	->with('abra_social_1_sale',$abra_social_1_sale)
	->with('abra_agri_1_sale',$abra_agri_1_sale)
	->with('abra_mid_1_sale',$abra_mid_1_sale)
	->with('abra_online_1_sale',$abra_online_1_sale)
	->with('abra_let_2_sale',$abra_let_2_sale)
	->with('abra_nle_2_sale',$abra_nle_2_sale)
	->with('abra_crim_2_sale',$abra_crim_2_sale)
	->with('abra_civil_2_sale',$abra_civil_2_sale)
	->with('abra_psyc_2_sale',$abra_psyc_2_sale)
	->with('abra_nclex_2_sale',$abra_nclex_2_sale)
	->with('abra_ielts_2_sale',$abra_ielts_2_sale)
	->with('abra_social_2_sale',$abra_social_2_sale)
	->with('abra_agri_2_sale',$abra_agri_2_sale)
	->with('abra_mid_2_sale',$abra_mid_1_sale)
	->with('abra_online_2_sale',$abra_online_1_sale)
	->with('abra_let',$abra_let)
	->with('abra_nle',$abra_nle)
	->with('abra_crim',$abra_crim)
	->with('abra_civil',$abra_civil)
	->with('abra_psyc',$abra_psyc)
	->with('abra_nclex',$abra_nclex)
	->with('abra_ielts',$abra_ielts)
	->with('abra_social',$abra_social)
	->with('abra_agri',$abra_agri)
	->with('abra_mid',$abra_mid)
	->with('abra_online',$abra_online)

	->with('baguio_let_1_sale',$baguio_let_1_sale)
	->with('baguio_nle_1_sale',$baguio_nle_1_sale)
	->with('baguio_crim_1_sale',$baguio_crim_1_sale)
	->with('baguio_civil_1_sale',$baguio_civil_1_sale)
	->with('baguio_psyc_1_sale',$baguio_psyc_1_sale)
	->with('baguio_nclex_1_sale',$baguio_nclex_1_sale)
	->with('baguio_ielts_1_sale',$baguio_ielts_1_sale)
	->with('baguio_social_1_sale',$baguio_social_1_sale)
	->with('baguio_agri_1_sale',$baguio_agri_1_sale)
	->with('baguio_mid_1_sale',$baguio_mid_1_sale)
	->with('baguio_online_1_sale',$baguio_online_1_sale)
	->with('baguio_let_2_sale',$baguio_let_2_sale)
	->with('baguio_nle_2_sale',$baguio_nle_2_sale)
	->with('baguio_crim_2_sale',$baguio_crim_2_sale)
	->with('baguio_civil_2_sale',$baguio_civil_2_sale)
	->with('baguio_psyc_2_sale',$baguio_psyc_2_sale)
	->with('baguio_nclex_2_sale',$baguio_nclex_2_sale)
	->with('baguio_ielts_2_sale',$baguio_ielts_2_sale)
	->with('baguio_social_2_sale',$baguio_social_2_sale)
	->with('baguio_agri_2_sale',$baguio_agri_2_sale)
	->with('baguio_mid_2_sale',$baguio_mid_1_sale)
	->with('baguio_online_2_sale',$baguio_online_1_sale)
	->with('baguio_let',$baguio_let)
	->with('baguio_nle',$baguio_nle)
	->with('baguio_crim',$baguio_crim)
	->with('baguio_civil',$baguio_civil)
	->with('baguio_psyc',$baguio_psyc)
	->with('baguio_nclex',$baguio_nclex)
	->with('baguio_ielts',$baguio_ielts)
	->with('baguio_social',$baguio_social)
	->with('baguio_agri',$baguio_agri)
	->with('baguio_mid',$baguio_mid)
	->with('baguio_online',$baguio_online)

	->with('calapan_let_1_sale',$calapan_let_1_sale)
	->with('calapan_nle_1_sale',$calapan_nle_1_sale)
	->with('calapan_crim_1_sale',$calapan_crim_1_sale)
	->with('calapan_civil_1_sale',$calapan_civil_1_sale)
	->with('calapan_psyc_1_sale',$calapan_psyc_1_sale)
	->with('calapan_nclex_1_sale',$calapan_nclex_1_sale)
	->with('calapan_ielts_1_sale',$calapan_ielts_1_sale)
	->with('calapan_social_1_sale',$calapan_social_1_sale)
	->with('calapan_agri_1_sale',$calapan_agri_1_sale)
	->with('calapan_mid_1_sale',$calapan_mid_1_sale)
	->with('calapan_online_1_sale',$calapan_online_1_sale)
	->with('calapan_let_2_sale',$calapan_let_2_sale)
	->with('calapan_nle_2_sale',$calapan_nle_2_sale)
	->with('calapan_crim_2_sale',$calapan_crim_2_sale)
	->with('calapan_civil_2_sale',$calapan_civil_2_sale)
	->with('calapan_psyc_2_sale',$calapan_psyc_2_sale)
	->with('calapan_nclex_2_sale',$calapan_nclex_2_sale)
	->with('calapan_ielts_2_sale',$calapan_ielts_2_sale)
	->with('calapan_social_2_sale',$calapan_social_2_sale)
	->with('calapan_agri_2_sale',$calapan_agri_2_sale)
	->with('calapan_mid_2_sale',$calapan_mid_1_sale)
	->with('calapan_online_2_sale',$calapan_online_1_sale)
	->with('calapan_let',$calapan_let)
	->with('calapan_nle',$calapan_nle)
	->with('calapan_crim',$calapan_crim)
	->with('calapan_civil',$calapan_civil)
	->with('calapan_psyc',$calapan_psyc)
	->with('calapan_nclex',$calapan_nclex)
	->with('calapan_ielts',$calapan_ielts)
	->with('calapan_social',$calapan_social)
	->with('calapan_agri',$calapan_agri)
	->with('calapan_mid',$calapan_mid)
	->with('calapan_online',$calapan_online)

	->with('candon_let_1_sale',$candon_let_1_sale)
	->with('candon_nle_1_sale',$candon_nle_1_sale)
	->with('candon_crim_1_sale',$candon_crim_1_sale)
	->with('candon_civil_1_sale',$candon_civil_1_sale)
	->with('candon_psyc_1_sale',$candon_psyc_1_sale)
	->with('candon_nclex_1_sale',$candon_nclex_1_sale)
	->with('candon_ielts_1_sale',$candon_ielts_1_sale)
	->with('candon_social_1_sale',$candon_social_1_sale)
	->with('candon_agri_1_sale',$candon_agri_1_sale)
	->with('candon_mid_1_sale',$candon_mid_1_sale)
	->with('candon_online_1_sale',$candon_online_1_sale)
	->with('candon_let_2_sale',$candon_let_2_sale)
	->with('candon_nle_2_sale',$candon_nle_2_sale)
	->with('candon_crim_2_sale',$candon_crim_2_sale)
	->with('candon_civil_2_sale',$candon_civil_2_sale)
	->with('candon_psyc_2_sale',$candon_psyc_2_sale)
	->with('candon_nclex_2_sale',$candon_nclex_2_sale)
	->with('candon_ielts_2_sale',$candon_ielts_2_sale)
	->with('candon_social_2_sale',$candon_social_2_sale)
	->with('candon_agri_2_sale',$candon_agri_2_sale)
	->with('candon_mid_2_sale',$candon_mid_1_sale)
	->with('candon_online_2_sale',$candon_online_1_sale)
	->with('candon_let',$candon_let)
	->with('candon_nle',$candon_nle)
	->with('candon_crim',$candon_crim)
	->with('candon_civil',$candon_civil)
	->with('candon_psyc',$candon_psyc)
	->with('candon_nclex',$candon_nclex)
	->with('candon_ielts',$candon_ielts)
	->with('candon_social',$candon_social)
	->with('candon_agri',$candon_agri)
	->with('candon_mid',$candon_mid)
	->with('candon_online',$candon_online)

	->with('dagupan_let_1_sale',$dagupan_let_1_sale)
	->with('dagupan_nle_1_sale',$dagupan_nle_1_sale)
	->with('dagupan_crim_1_sale',$dagupan_crim_1_sale)
	->with('dagupan_civil_1_sale',$dagupan_civil_1_sale)
	->with('dagupan_psyc_1_sale',$dagupan_psyc_1_sale)
	->with('dagupan_nclex_1_sale',$dagupan_nclex_1_sale)
	->with('dagupan_ielts_1_sale',$dagupan_ielts_1_sale)
	->with('dagupan_social_1_sale',$dagupan_social_1_sale)
	->with('dagupan_agri_1_sale',$dagupan_agri_1_sale)
	->with('dagupan_mid_1_sale',$dagupan_mid_1_sale)
	->with('dagupan_online_1_sale',$dagupan_online_1_sale)
	->with('dagupan_let_2_sale',$dagupan_let_2_sale)
	->with('dagupan_nle_2_sale',$dagupan_nle_2_sale)
	->with('dagupan_crim_2_sale',$dagupan_crim_2_sale)
	->with('dagupan_civil_2_sale',$dagupan_civil_2_sale)
	->with('dagupan_psyc_2_sale',$dagupan_psyc_2_sale)
	->with('dagupan_nclex_2_sale',$dagupan_nclex_2_sale)
	->with('dagupan_ielts_2_sale',$dagupan_ielts_2_sale)
	->with('dagupan_social_2_sale',$dagupan_social_2_sale)
	->with('dagupan_agri_2_sale',$dagupan_agri_2_sale)
	->with('dagupan_mid_2_sale',$dagupan_mid_1_sale)
	->with('dagupan_online_2_sale',$dagupan_online_1_sale)
	->with('dagupan_let',$dagupan_let)
	->with('dagupan_nle',$dagupan_nle)
	->with('dagupan_crim',$dagupan_crim)
	->with('dagupan_civil',$dagupan_civil)
	->with('dagupan_psyc',$dagupan_psyc)
	->with('dagupan_nclex',$dagupan_nclex)
	->with('dagupan_ielts',$dagupan_ielts)
	->with('dagupan_social',$dagupan_social)
	->with('dagupan_agri',$dagupan_agri)
	->with('dagupan_mid',$dagupan_mid)
	->with('dagupan_online',$dagupan_online)

	->with('fairview_let_1_sale',$fairview_let_1_sale)
	->with('fairview_nle_1_sale',$fairview_nle_1_sale)
	->with('fairview_crim_1_sale',$fairview_crim_1_sale)
	->with('fairview_civil_1_sale',$fairview_civil_1_sale)
	->with('fairview_psyc_1_sale',$fairview_psyc_1_sale)
	->with('fairview_nclex_1_sale',$fairview_nclex_1_sale)
	->with('fairview_ielts_1_sale',$fairview_ielts_1_sale)
	->with('fairview_social_1_sale',$fairview_social_1_sale)
	->with('fairview_agri_1_sale',$fairview_agri_1_sale)
	->with('fairview_mid_1_sale',$fairview_mid_1_sale)
	->with('fairview_online_1_sale',$fairview_online_1_sale)
	->with('fairview_let_2_sale',$fairview_let_2_sale)
	->with('fairview_nle_2_sale',$fairview_nle_2_sale)
	->with('fairview_crim_2_sale',$fairview_crim_2_sale)
	->with('fairview_civil_2_sale',$fairview_civil_2_sale)
	->with('fairview_psyc_2_sale',$fairview_psyc_2_sale)
	->with('fairview_nclex_2_sale',$fairview_nclex_2_sale)
	->with('fairview_ielts_2_sale',$fairview_ielts_2_sale)
	->with('fairview_social_2_sale',$fairview_social_2_sale)
	->with('fairview_agri_2_sale',$fairview_agri_2_sale)
	->with('fairview_mid_2_sale',$fairview_mid_1_sale)
	->with('fairview_online_2_sale',$fairview_online_1_sale)
	->with('fairview_let',$fairview_let)
	->with('fairview_nle',$fairview_nle)
	->with('fairview_crim',$fairview_crim)
	->with('fairview_civil',$fairview_civil)
	->with('fairview_psyc',$fairview_psyc)
	->with('fairview_nclex',$fairview_nclex)
	->with('fairview_ielts',$fairview_ielts)
	->with('fairview_social',$fairview_social)
	->with('fairview_agri',$fairview_agri)
	->with('fairview_mid',$fairview_mid)
	->with('fairview_online',$fairview_online)

	->with('las_pinas_let_1_sale',$las_pinas_let_1_sale)
	->with('las_pinas_nle_1_sale',$las_pinas_nle_1_sale)
	->with('las_pinas_crim_1_sale',$las_pinas_crim_1_sale)
	->with('las_pinas_civil_1_sale',$las_pinas_civil_1_sale)
	->with('las_pinas_psyc_1_sale',$las_pinas_psyc_1_sale)
	->with('las_pinas_nclex_1_sale',$las_pinas_nclex_1_sale)
	->with('las_pinas_ielts_1_sale',$las_pinas_ielts_1_sale)
	->with('las_pinas_social_1_sale',$las_pinas_social_1_sale)
	->with('las_pinas_agri_1_sale',$las_pinas_agri_1_sale)
	->with('las_pinas_mid_1_sale',$las_pinas_mid_1_sale)
	->with('las_pinas_online_1_sale',$las_pinas_online_1_sale)
	->with('las_pinas_let_2_sale',$las_pinas_let_2_sale)
	->with('las_pinas_nle_2_sale',$las_pinas_nle_2_sale)
	->with('las_pinas_crim_2_sale',$las_pinas_crim_2_sale)
	->with('las_pinas_civil_2_sale',$las_pinas_civil_2_sale)
	->with('las_pinas_psyc_2_sale',$las_pinas_psyc_2_sale)
	->with('las_pinas_nclex_2_sale',$las_pinas_nclex_2_sale)
	->with('las_pinas_ielts_2_sale',$las_pinas_ielts_2_sale)
	->with('las_pinas_social_2_sale',$las_pinas_social_2_sale)
	->with('las_pinas_agri_2_sale',$las_pinas_agri_2_sale)
	->with('las_pinas_mid_2_sale',$las_pinas_mid_1_sale)
	->with('las_pinas_online_2_sale',$las_pinas_online_1_sale)
	->with('las_pinas_let',$las_pinas_let)
	->with('las_pinas_nle',$las_pinas_nle)
	->with('las_pinas_crim',$las_pinas_crim)
	->with('las_pinas_civil',$las_pinas_civil)
	->with('las_pinas_psyc',$las_pinas_psyc)
	->with('las_pinas_nclex',$las_pinas_nclex)
	->with('las_pinas_ielts',$las_pinas_ielts)
	->with('las_pinas_social',$las_pinas_social)
	->with('las_pinas_agri',$las_pinas_agri)
	->with('las_pinas_mid',$las_pinas_mid)
	->with('las_pinas_online',$las_pinas_online)

	->with('launion_let_1_sale',$launion_let_1_sale)
	->with('launion_nle_1_sale',$launion_nle_1_sale)
	->with('launion_crim_1_sale',$launion_crim_1_sale)
	->with('launion_civil_1_sale',$launion_civil_1_sale)
	->with('launion_psyc_1_sale',$launion_psyc_1_sale)
	->with('launion_nclex_1_sale',$launion_nclex_1_sale)
	->with('launion_ielts_1_sale',$launion_ielts_1_sale)
	->with('launion_social_1_sale',$launion_social_1_sale)
	->with('launion_agri_1_sale',$launion_agri_1_sale)
	->with('launion_mid_1_sale',$launion_mid_1_sale)
	->with('launion_online_1_sale',$launion_online_1_sale)
	->with('launion_let_2_sale',$launion_let_2_sale)
	->with('launion_nle_2_sale',$launion_nle_2_sale)
	->with('launion_crim_2_sale',$launion_crim_2_sale)
	->with('launion_civil_2_sale',$launion_civil_2_sale)
	->with('launion_psyc_2_sale',$launion_psyc_2_sale)
	->with('launion_nclex_2_sale',$launion_nclex_2_sale)
	->with('launion_ielts_2_sale',$launion_ielts_2_sale)
	->with('launion_social_2_sale',$launion_social_2_sale)
	->with('launion_agri_2_sale',$launion_agri_2_sale)
	->with('launion_mid_2_sale',$launion_mid_1_sale)
	->with('launion_online_2_sale',$launion_online_1_sale)
	->with('launion_let',$launion_let)
	->with('launion_nle',$launion_nle)
	->with('launion_crim',$launion_crim)
	->with('launion_civil',$launion_civil)
	->with('launion_psyc',$launion_psyc)
	->with('launion_nclex',$launion_nclex)
	->with('launion_ielts',$launion_ielts)
	->with('launion_social',$launion_social)
	->with('launion_agri',$launion_agri)
	->with('launion_mid',$launion_mid)
	->with('launion_online',$launion_online)

	->with('manila_let_1_sale',$manila_let_1_sale)
	->with('manila_nle_1_sale',$manila_nle_1_sale)
	->with('manila_crim_1_sale',$manila_crim_1_sale)
	->with('manila_civil_1_sale',$manila_civil_1_sale)
	->with('manila_psyc_1_sale',$manila_psyc_1_sale)
	->with('manila_nclex_1_sale',$manila_nclex_1_sale)
	->with('manila_ielts_1_sale',$manila_ielts_1_sale)
	->with('manila_social_1_sale',$manila_social_1_sale)
	->with('manila_agri_1_sale',$manila_agri_1_sale)
	->with('manila_mid_1_sale',$manila_mid_1_sale)
	->with('manila_online_1_sale',$manila_online_1_sale)
	->with('manila_let_2_sale',$manila_let_2_sale)
	->with('manila_nle_2_sale',$manila_nle_2_sale)
	->with('manila_crim_2_sale',$manila_crim_2_sale)
	->with('manila_civil_2_sale',$manila_civil_2_sale)
	->with('manila_psyc_2_sale',$manila_psyc_2_sale)
	->with('manila_nclex_2_sale',$manila_nclex_2_sale)
	->with('manila_ielts_2_sale',$manila_ielts_2_sale)
	->with('manila_social_2_sale',$manila_social_2_sale)
	->with('manila_agri_2_sale',$manila_agri_2_sale)
	->with('manila_mid_2_sale',$manila_mid_1_sale)
	->with('manila_online_2_sale',$manila_online_1_sale)
	->with('manila_let',$manila_let)
	->with('manila_nle',$manila_nle)
	->with('manila_crim',$manila_crim)
	->with('manila_civil',$manila_civil)
	->with('manila_psyc',$manila_psyc)
	->with('manila_nclex',$manila_nclex)
	->with('manila_ielts',$manila_ielts)
	->with('manila_social',$manila_social)
	->with('manila_agri',$manila_agri)
	->with('manila_mid',$manila_mid)
	->with('manila_online',$manila_online)

	->with('roxas_let_1_sale',$roxas_let_1_sale)
	->with('roxas_nle_1_sale',$roxas_nle_1_sale)
	->with('roxas_crim_1_sale',$roxas_crim_1_sale)
	->with('roxas_civil_1_sale',$roxas_civil_1_sale)
	->with('roxas_psyc_1_sale',$roxas_psyc_1_sale)
	->with('roxas_nclex_1_sale',$roxas_nclex_1_sale)
	->with('roxas_ielts_1_sale',$roxas_ielts_1_sale)
	->with('roxas_social_1_sale',$roxas_social_1_sale)
	->with('roxas_agri_1_sale',$roxas_agri_1_sale)
	->with('roxas_mid_1_sale',$roxas_mid_1_sale)
	->with('roxas_online_1_sale',$roxas_online_1_sale)
	->with('roxas_let_2_sale',$roxas_let_2_sale)
	->with('roxas_nle_2_sale',$roxas_nle_2_sale)
	->with('roxas_crim_2_sale',$roxas_crim_2_sale)
	->with('roxas_civil_2_sale',$roxas_civil_2_sale)
	->with('roxas_psyc_2_sale',$roxas_psyc_2_sale)
	->with('roxas_nclex_2_sale',$roxas_nclex_2_sale)
	->with('roxas_ielts_2_sale',$roxas_ielts_2_sale)
	->with('roxas_social_2_sale',$roxas_social_2_sale)
	->with('roxas_agri_2_sale',$roxas_agri_2_sale)
	->with('roxas_mid_2_sale',$roxas_mid_1_sale)
	->with('roxas_online_2_sale',$roxas_online_1_sale)
	->with('roxas_let',$roxas_let)
	->with('roxas_nle',$roxas_nle)
	->with('roxas_crim',$roxas_crim)
	->with('roxas_civil',$roxas_civil)
	->with('roxas_psyc',$roxas_psyc)
	->with('roxas_nclex',$roxas_nclex)
	->with('roxas_ielts',$roxas_ielts)
	->with('roxas_social',$roxas_social)
	->with('roxas_agri',$roxas_agri)
	->with('roxas_mid',$roxas_mid)
	->with('roxas_online',$roxas_online)
	
	->with('tarlac_let_1_sale',$tarlac_let_1_sale)
	->with('tarlac_nle_1_sale',$tarlac_nle_1_sale)
	->with('tarlac_crim_1_sale',$tarlac_crim_1_sale)
	->with('tarlac_civil_1_sale',$tarlac_civil_1_sale)
	->with('tarlac_psyc_1_sale',$tarlac_psyc_1_sale)
	->with('tarlac_nclex_1_sale',$tarlac_nclex_1_sale)
	->with('tarlac_ielts_1_sale',$tarlac_ielts_1_sale)
	->with('tarlac_social_1_sale',$tarlac_social_1_sale)
	->with('tarlac_agri_1_sale',$tarlac_agri_1_sale)
	->with('tarlac_mid_1_sale',$tarlac_mid_1_sale)
	->with('tarlac_online_1_sale',$tarlac_online_1_sale)
	->with('tarlac_let_2_sale',$tarlac_let_2_sale)
	->with('tarlac_nle_2_sale',$tarlac_nle_2_sale)
	->with('tarlac_crim_2_sale',$tarlac_crim_2_sale)
	->with('tarlac_civil_2_sale',$tarlac_civil_2_sale)
	->with('tarlac_psyc_2_sale',$tarlac_psyc_2_sale)
	->with('tarlac_nclex_2_sale',$tarlac_nclex_2_sale)
	->with('tarlac_ielts_2_sale',$tarlac_ielts_2_sale)
	->with('tarlac_social_2_sale',$tarlac_social_2_sale)
	->with('tarlac_agri_2_sale',$tarlac_agri_2_sale)
	->with('tarlac_mid_2_sale',$tarlac_mid_1_sale)
	->with('tarlac_online_2_sale',$tarlac_online_1_sale)
	->with('tarlac_let',$tarlac_let)
	->with('tarlac_nle',$tarlac_nle)
	->with('tarlac_crim',$tarlac_crim)
	->with('tarlac_civil',$tarlac_civil)
	->with('tarlac_psyc',$tarlac_psyc)
	->with('tarlac_nclex',$tarlac_nclex)
	->with('tarlac_ielts',$tarlac_ielts)
	->with('tarlac_social',$tarlac_social)
	->with('tarlac_agri',$tarlac_agri)
	->with('tarlac_mid',$tarlac_mid)
	->with('tarlac_online',$tarlac_online)

	->with('vigan_let_1_sale',$vigan_let_1_sale)
	->with('vigan_nle_1_sale',$vigan_nle_1_sale)
	->with('vigan_crim_1_sale',$vigan_crim_1_sale)
	->with('vigan_civil_1_sale',$vigan_civil_1_sale)
	->with('vigan_psyc_1_sale',$vigan_psyc_1_sale)
	->with('vigan_nclex_1_sale',$vigan_nclex_1_sale)
	->with('vigan_ielts_1_sale',$vigan_ielts_1_sale)
	->with('vigan_social_1_sale',$vigan_social_1_sale)
	->with('vigan_agri_1_sale',$vigan_agri_1_sale)
	->with('vigan_mid_1_sale',$vigan_mid_1_sale)
	->with('vigan_online_1_sale',$vigan_online_1_sale)
	->with('vigan_let_2_sale',$vigan_let_2_sale)
	->with('vigan_nle_2_sale',$vigan_nle_2_sale)
	->with('vigan_crim_2_sale',$vigan_crim_2_sale)
	->with('vigan_civil_2_sale',$vigan_civil_2_sale)
	->with('vigan_psyc_2_sale',$vigan_psyc_2_sale)
	->with('vigan_nclex_2_sale',$vigan_nclex_2_sale)
	->with('vigan_ielts_2_sale',$vigan_ielts_2_sale)
	->with('vigan_social_2_sale',$vigan_social_2_sale)
	->with('vigan_agri_2_sale',$vigan_agri_2_sale)
	->with('vigan_mid_2_sale',$vigan_mid_1_sale)
	->with('vigan_online_2_sale',$vigan_online_1_sale)
	->with('vigan_let',$vigan_let)
	->with('vigan_nle',$vigan_nle)
	->with('vigan_crim',$vigan_crim)
	->with('vigan_civil',$vigan_civil)
	->with('vigan_psyc',$vigan_psyc)
	->with('vigan_nclex',$vigan_nclex)
	->with('vigan_ielts',$vigan_ielts)
	->with('vigan_social',$vigan_social)
	->with('vigan_agri',$vigan_agri)
	->with('vigan_mid',$vigan_mid)
	->with('vigan_online',$vigan_online)
	;
	

}

public function total_agri(){
	$program = "Agriculture";
	$abra_a_prog = AbraAgri::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioAgri::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanAgri::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonAgri::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanAgri::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewAgri::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasAgri::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionAgri::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaAgri::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasAgri::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacAgri::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganAgri::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_civil(){
	$program = "Civil Service";
	$abra_a_prog = AbraCivil::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioCivil::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanCivil::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonCivil::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanCivil::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewCivil::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasCivil::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionCivil::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaCivil::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasCivil::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacCivil::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganCivil::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_crim(){
	$program = "Criminology";
	
	$abra_a_prog = AbraCrim::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioCrim::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanCrim::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonCrim::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanCrim::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewCrim::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasCrim::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionCrim::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaCrim::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasCrim::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacCrim::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganCrim::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_ielts(){
	$program = "IELTS";
	
	$abra_a_prog = AbraIelt::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioIelt::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanIelt::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonIelt::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanIelt::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewIelt::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasIelt::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionIelt::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaIelt::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasIelt::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacIelt::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganIelt::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_let(){
	$program = "LET";
	
		
	$abra_a_prog = AbraLet::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioLet::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanLet::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonLet::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanLet::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewLet::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasLet::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionLet::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaLet::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasLet::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacLet::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganLet::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_nclex(){
	$program = "NCLEX";
	
		
	$abra_a_prog = AbraNclex::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioNclex::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanNclex::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonNclex::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanNclex::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewNclex::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasNclex::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionNclex::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaNclex::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasNclex::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacNclex::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganNclex::where('status','=','Enrolled')->get();



	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_nle(){
	$program = "NLE";
	
		
	$abra_a_prog = AbraNle::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioNle::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanNle::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonNle::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanNle::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewNle::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasNle::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionNle::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaNle::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasNle::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacNle::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganNle::where('status','=','Enrolled')->get();



	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_psyc(){
	$program = "Psychometrician";
	
		
	$abra_a_prog = AbraPsyc::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioPsyc::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanPsyc::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonPsyc::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanPsyc::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewPsyc::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasPsyc::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionPsyc::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaPsyc::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasPsyc::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacPsyc::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganPsyc::where('status','=','Enrolled')->get();



	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_social(){
	$program = "Social Work";
	
		
	$abra_a_prog = AbraSocial::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioSocial::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanSocial::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonSocial::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanSocial::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewSocial::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasSocial::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionSocial::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaSocial::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasSocial::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacSocial::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganSocial::where('status','=','Enrolled')->get();



	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_mid(){
	$program = "Midwifery";
	
		
	$abra_a_prog = AbraMid::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioMid::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanMid::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonMid::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanMid::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewMid::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasMid::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionMid::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaMid::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasMid::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacMid::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganMid::where('status','=','Enrolled')->get();



	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_online(){
	$program = "Online Only";
	
		
	$abra_a_prog = AbraOnly::where('status','=','Enrolled')->get();
	$baguio_a_prog = BaguioOnly::where('status','=','Enrolled')->get();
	$calapan_a_prog = CalapanOnly::where('status','=','Enrolled')->get();
	$candon_a_prog = CandonOnly::where('status','=','Enrolled')->get();
	$dagupan_a_prog = DagupanOnly::where('status','=','Enrolled')->get();
	$fairview_a_prog = FairviewOnly::where('status','=','Enrolled')->get();
	$las_pinas_a_prog = LasPinasOnly::where('status','=','Enrolled')->get();
	$launion_a_prog = LaunionOnly::where('status','=','Enrolled')->get();
	$manila_a_prog = ManilaOnly::where('status','=','Enrolled')->get();
	$roxas_a_prog = RoxasOnly::where('status','=','Enrolled')->get();
	$tarlac_a_prog = TarlacOnly::where('status','=','Enrolled')->get();
	$vigan_a_prog = ViganOnly::where('status','=','Enrolled')->get();



	return view ('admin.total-enrollee')
	->with('abra_a_prog',$abra_a_prog)
	->with('baguio_a_prog',$baguio_a_prog)
	->with('calapan_a_prog',$calapan_a_prog)
	->with('candon_a_prog',$candon_a_prog)
	->with('dagupan_a_prog',$dagupan_a_prog)
	->with('fairview_a_prog',$fairview_a_prog)
	->with('las_pinas_a_prog',$las_pinas_a_prog)
	->with('launion_a_prog',$launion_a_prog)
	->with('manila_a_prog',$manila_a_prog)
	->with('roxas_a_prog',$roxas_a_prog)
	->with('tarlac_a_prog',$tarlac_a_prog)
	->with('vigan_a_prog',$vigan_a_prog)
	->with('program',$program);
}

public function total_dropped(){

	$abra_a_let=AbraLet::where('status','=','Dropped')->get();

    $abra_a_nle=AbraNle::where('status','=','Dropped')->get();

    $abra_a_crim=AbraCrim::where('status','=','Dropped')->get();

    $abra_a_civil=AbraCivil::where('status','=','Dropped')->get();

    $abra_a_psyc=AbraPsyc::where('status','=','Dropped')->get();

    $abra_a_nclex=AbraNclex::where('status','=','Dropped')->get();

    $abra_a_ielt=AbraIelt::where('status','=','Dropped')->get();

    $abra_a_social=AbraSocial::where('status','=','Dropped')->get();

    $abra_a_agri=AbraAgri::where('status','=','Dropped')->get();

    $abra_a_mid=AbraMid::where('status','=','Dropped')->get();

	$abra_a_online=AbraOnline::where('status','=','Dropped')->get();
	
	$baguio_a_let=BaguioLet::where('status','=','Dropped')->get();

    $baguio_a_nle=BaguioNle::where('status','=','Dropped')->get();

    $baguio_a_crim=BaguioCrim::where('status','=','Dropped')->get();

    $baguio_a_civil=BaguioCivil::where('status','=','Dropped')->get();

    $baguio_a_psyc=BaguioPsyc::where('status','=','Dropped')->get();

    $baguio_a_nclex=BaguioNclex::where('status','=','Dropped')->get();

    $baguio_a_ielt=BaguioIelt::where('status','=','Dropped')->get();

    $baguio_a_social=BaguioSocial::where('status','=','Dropped')->get();

    $baguio_a_agri=BaguioAgri::where('status','=','Dropped')->get();

    $baguio_a_mid=BaguioMid::where('status','=','Dropped')->get();

	$baguio_a_online=BaguioOnline::where('status','=','Dropped')->get();
	
	$calapan_a_let=CalapanLet::where('status','=','Dropped')->get();

    $calapan_a_nle=CalapanNle::where('status','=','Dropped')->get();

    $calapan_a_crim=CalapanCrim::where('status','=','Dropped')->get();

    $calapan_a_civil=CalapanCivil::where('status','=','Dropped')->get();

    $calapan_a_psyc=CalapanPsyc::where('status','=','Dropped')->get();

    $calapan_a_nclex=CalapanNclex::where('status','=','Dropped')->get();

    $calapan_a_ielt=CalapanIelt::where('status','=','Dropped')->get();

    $calapan_a_social=CalapanSocial::where('status','=','Dropped')->get();

    $calapan_a_agri=CalapanAgri::where('status','=','Dropped')->get();

    $calapan_a_mid=CalapanMid::where('status','=','Dropped')->get();

	$calapan_a_online=CalapanOnline::where('status','=','Dropped')->get();
	
	$candon_a_let=CandonLet::where('status','=','Dropped')->get();

    $candon_a_nle=CandonNle::where('status','=','Dropped')->get();

    $candon_a_crim=CandonCrim::where('status','=','Dropped')->get();

    $candon_a_civil=CandonCivil::where('status','=','Dropped')->get();

    $candon_a_psyc=CandonPsyc::where('status','=','Dropped')->get();

    $candon_a_nclex=CandonNclex::where('status','=','Dropped')->get();

    $candon_a_ielt=CandonIelt::where('status','=','Dropped')->get();

    $candon_a_social=CandonSocial::where('status','=','Dropped')->get();

    $candon_a_agri=CandonAgri::where('status','=','Dropped')->get();

    $candon_a_mid=CandonMid::where('status','=','Dropped')->get();

	$candon_a_online=CandonOnline::where('status','=','Dropped')->get();
	
	$dagupan_a_let=DagupanLet::where('status','=','Dropped')->get();

    $dagupan_a_nle=DagupanNle::where('status','=','Dropped')->get();

    $dagupan_a_crim=DagupanCrim::where('status','=','Dropped')->get();

    $dagupan_a_civil=DagupanCivil::where('status','=','Dropped')->get();

    $dagupan_a_psyc=DagupanPsyc::where('status','=','Dropped')->get();

    $dagupan_a_nclex=DagupanNclex::where('status','=','Dropped')->get();

    $dagupan_a_ielt=DagupanIelt::where('status','=','Dropped')->get();

    $dagupan_a_social=DagupanSocial::where('status','=','Dropped')->get();

    $dagupan_a_agri=DagupanAgri::where('status','=','Dropped')->get();

    $dagupan_a_mid=DagupanMid::where('status','=','Dropped')->get();

	$dagupan_a_online=DagupanOnline::where('status','=','Dropped')->get();
	
	$fairview_a_let=FairviewLet::where('status','=','Dropped')->get();

    $fairview_a_nle=FairviewNle::where('status','=','Dropped')->get();

    $fairview_a_crim=FairviewCrim::where('status','=','Dropped')->get();

    $fairview_a_civil=FairviewCivil::where('status','=','Dropped')->get();

    $fairview_a_psyc=FairviewPsyc::where('status','=','Dropped')->get();

    $fairview_a_nclex=FairviewNclex::where('status','=','Dropped')->get();

    $fairview_a_ielt=FairviewIelt::where('status','=','Dropped')->get();

    $fairview_a_social=FairviewSocial::where('status','=','Dropped')->get();

    $fairview_a_agri=FairviewAgri::where('status','=','Dropped')->get();

    $fairview_a_mid=FairviewMid::where('status','=','Dropped')->get();

	$fairview_a_online=FairviewOnline::where('status','=','Dropped')->get();
	
	$las_pinas_a_let=LasPinasLet::where('status','=','Dropped')->get();

    $las_pinas_a_nle=LasPinasNle::where('status','=','Dropped')->get();

    $las_pinas_a_crim=LasPinasCrim::where('status','=','Dropped')->get();

    $las_pinas_a_civil=LasPinasCivil::where('status','=','Dropped')->get();

    $las_pinas_a_psyc=LasPinasPsyc::where('status','=','Dropped')->get();

    $las_pinas_a_nclex=LasPinasNclex::where('status','=','Dropped')->get();

    $las_pinas_a_ielt=LasPinasIelt::where('status','=','Dropped')->get();

    $las_pinas_a_social=LasPinasSocial::where('status','=','Dropped')->get();

    $las_pinas_a_agri=LasPinasAgri::where('status','=','Dropped')->get();

    $las_pinas_a_mid=LasPinasMid::where('status','=','Dropped')->get();

	$las_pinas_a_online=LasPinasOnline::where('status','=','Dropped')->get();
	
	$launion_a_let=LaunionLet::where('status','=','Dropped')->get();

    $launion_a_nle=LaunionNle::where('status','=','Dropped')->get();

    $launion_a_crim=LaunionCrim::where('status','=','Dropped')->get();

    $launion_a_civil=LaunionCivil::where('status','=','Dropped')->get();

    $launion_a_psyc=LaunionPsyc::where('status','=','Dropped')->get();

    $launion_a_nclex=LaunionNclex::where('status','=','Dropped')->get();

    $launion_a_ielt=LaunionIelt::where('status','=','Dropped')->get();

    $launion_a_social=LaunionSocial::where('status','=','Dropped')->get();

    $launion_a_agri=LaunionAgri::where('status','=','Dropped')->get();

    $launion_a_mid=LaunionMid::where('status','=','Dropped')->get();

	$launion_a_online=LaunionOnline::where('status','=','Dropped')->get();
	
	$manila_a_let=ManilaLet::where('status','=','Dropped')->get();

    $manila_a_nle=ManilaNle::where('status','=','Dropped')->get();

    $manila_a_crim=ManilaCrim::where('status','=','Dropped')->get();

    $manila_a_civil=ManilaCivil::where('status','=','Dropped')->get();

    $manila_a_psyc=ManilaPsyc::where('status','=','Dropped')->get();

    $manila_a_nclex=ManilaNclex::where('status','=','Dropped')->get();

    $manila_a_ielt=ManilaIelt::where('status','=','Dropped')->get();

    $manila_a_social=ManilaSocial::where('status','=','Dropped')->get();

    $manila_a_agri=ManilaAgri::where('status','=','Dropped')->get();

    $manila_a_mid=ManilaMid::where('status','=','Dropped')->get();

	$manila_a_online=ManilaOnline::where('status','=','Dropped')->get();
	
	$roxas_a_let=RoxasLet::where('status','=','Dropped')->get();

    $roxas_a_nle=RoxasNle::where('status','=','Dropped')->get();

    $roxas_a_crim=RoxasCrim::where('status','=','Dropped')->get();

    $roxas_a_civil=RoxasCivil::where('status','=','Dropped')->get();

    $roxas_a_psyc=RoxasPsyc::where('status','=','Dropped')->get();

    $roxas_a_nclex=RoxasNclex::where('status','=','Dropped')->get();

    $roxas_a_ielt=RoxasIelt::where('status','=','Dropped')->get();

    $roxas_a_social=RoxasSocial::where('status','=','Dropped')->get();

    $roxas_a_agri=RoxasAgri::where('status','=','Dropped')->get();

    $roxas_a_mid=RoxasMid::where('status','=','Dropped')->get();

	$roxas_a_online=RoxasOnline::where('status','=','Dropped')->get();
	
	
	
	$tarlac_a_let=TarlacLet::where('status','=','Dropped')->get();

    $tarlac_a_nle=TarlacNle::where('status','=','Dropped')->get();

    $tarlac_a_crim=TarlacCrim::where('status','=','Dropped')->get();

    $tarlac_a_civil=TarlacCivil::where('status','=','Dropped')->get();

    $tarlac_a_psyc=TarlacPsyc::where('status','=','Dropped')->get();

    $tarlac_a_nclex=TarlacNclex::where('status','=','Dropped')->get();

    $tarlac_a_ielt=TarlacIelt::where('status','=','Dropped')->get();

    $tarlac_a_social=TarlacSocial::where('status','=','Dropped')->get();

    $tarlac_a_agri=TarlacAgri::where('status','=','Dropped')->get();

    $tarlac_a_mid=TarlacMid::where('status','=','Dropped')->get();

	$tarlac_a_online=TarlacOnline::where('status','=','Dropped')->get();
	
	$vigan_a_let=ViganLet::where('status','=','Dropped')->get();

    $vigan_a_nle=ViganNle::where('status','=','Dropped')->get();

    $vigan_a_crim=ViganCrim::where('status','=','Dropped')->get();

    $vigan_a_civil=ViganCivil::where('status','=','Dropped')->get();

    $vigan_a_psyc=ViganPsyc::where('status','=','Dropped')->get();

    $vigan_a_nclex=ViganNclex::where('status','=','Dropped')->get();

    $vigan_a_ielt=ViganIelt::where('status','=','Dropped')->get();

    $vigan_a_social=ViganSocial::where('status','=','Dropped')->get();

    $vigan_a_agri=ViganAgri::where('status','=','Dropped')->get();

    $vigan_a_mid=ViganMid::where('status','=','Dropped')->get();

    $vigan_a_online=ViganOnline::where('status','=','Dropped')->get();

    return view ('admin.total-dropped')
    ->with('abra_a_let',$abra_a_let)
    ->with('abra_a_nle',$abra_a_nle)
    ->with('abra_a_crim',$abra_a_crim)
    ->with('abra_a_civil',$abra_a_civil)
    ->with('abra_a_psyc',$abra_a_psyc)
    ->with('abra_a_nclex',$abra_a_nclex)
    ->with('abra_a_ielt',$abra_a_ielt)
    ->with('abra_a_social',$abra_a_social)
    ->with('abra_a_agri',$abra_a_agri)
    ->with('abra_a_mid',$abra_a_mid)
	->with('abra_a_online',$abra_a_online)

	->with('baguio_a_let',$baguio_a_let)
    ->with('baguio_a_nle',$baguio_a_nle)
    ->with('baguio_a_crim',$baguio_a_crim)
    ->with('baguio_a_civil',$baguio_a_civil)
    ->with('baguio_a_psyc',$baguio_a_psyc)
    ->with('baguio_a_nclex',$baguio_a_nclex)
    ->with('baguio_a_ielt',$baguio_a_ielt)
    ->with('baguio_a_social',$baguio_a_social)
    ->with('baguio_a_agri',$baguio_a_agri)
    ->with('baguio_a_mid',$baguio_a_mid)
	->with('baguio_a_online',$baguio_a_online)

	->with('calapan_a_let',$calapan_a_let)
    ->with('calapan_a_nle',$calapan_a_nle)
    ->with('calapan_a_crim',$calapan_a_crim)
    ->with('calapan_a_civil',$calapan_a_civil)
    ->with('calapan_a_psyc',$calapan_a_psyc)
    ->with('calapan_a_nclex',$calapan_a_nclex)
    ->with('calapan_a_ielt',$calapan_a_ielt)
    ->with('calapan_a_social',$calapan_a_social)
    ->with('calapan_a_agri',$calapan_a_agri)
    ->with('calapan_a_mid',$calapan_a_mid)
	->with('calapan_a_online',$calapan_a_online)

	->with('candon_a_let',$candon_a_let)
    ->with('candon_a_nle',$candon_a_nle)
    ->with('candon_a_crim',$candon_a_crim)
    ->with('candon_a_civil',$candon_a_civil)
    ->with('candon_a_psyc',$candon_a_psyc)
    ->with('candon_a_nclex',$candon_a_nclex)
    ->with('candon_a_ielt',$candon_a_ielt)
    ->with('candon_a_social',$candon_a_social)
    ->with('candon_a_agri',$candon_a_agri)
    ->with('candon_a_mid',$candon_a_mid)
	->with('candon_a_online',$candon_a_online)

	->with('dagupan_a_let',$dagupan_a_let)
    ->with('dagupan_a_nle',$dagupan_a_nle)
    ->with('dagupan_a_crim',$dagupan_a_crim)
    ->with('dagupan_a_civil',$dagupan_a_civil)
    ->with('dagupan_a_psyc',$dagupan_a_psyc)
    ->with('dagupan_a_nclex',$dagupan_a_nclex)
    ->with('dagupan_a_ielt',$dagupan_a_ielt)
    ->with('dagupan_a_social',$dagupan_a_social)
    ->with('dagupan_a_agri',$dagupan_a_agri)
    ->with('dagupan_a_mid',$dagupan_a_mid)
	->with('dagupan_a_online',$dagupan_a_online)

	->with('fairview_a_let',$fairview_a_let)
    ->with('fairview_a_nle',$fairview_a_nle)
    ->with('fairview_a_crim',$fairview_a_crim)
    ->with('fairview_a_civil',$fairview_a_civil)
    ->with('fairview_a_psyc',$fairview_a_psyc)
    ->with('fairview_a_nclex',$fairview_a_nclex)
    ->with('fairview_a_ielt',$fairview_a_ielt)
    ->with('fairview_a_social',$fairview_a_social)
    ->with('fairview_a_agri',$fairview_a_agri)
    ->with('fairview_a_mid',$fairview_a_mid)
	->with('fairview_a_online',$fairview_a_online)

	->with('las_pinas_a_let',$las_pinas_a_let)
    ->with('las_pinas_a_nle',$las_pinas_a_nle)
    ->with('las_pinas_a_crim',$las_pinas_a_crim)
    ->with('las_pinas_a_civil',$las_pinas_a_civil)
    ->with('las_pinas_a_psyc',$las_pinas_a_psyc)
    ->with('las_pinas_a_nclex',$las_pinas_a_nclex)
    ->with('las_pinas_a_ielt',$las_pinas_a_ielt)
    ->with('las_pinas_a_social',$las_pinas_a_social)
    ->with('las_pinas_a_agri',$las_pinas_a_agri)
    ->with('las_pinas_a_mid',$las_pinas_a_mid)
	->with('las_pinas_a_online',$las_pinas_a_online)

	->with('launion_a_let',$launion_a_let)
    ->with('launion_a_nle',$launion_a_nle)
    ->with('launion_a_crim',$launion_a_crim)
    ->with('launion_a_civil',$launion_a_civil)
    ->with('launion_a_psyc',$launion_a_psyc)
    ->with('launion_a_nclex',$launion_a_nclex)
    ->with('launion_a_ielt',$launion_a_ielt)
    ->with('launion_a_social',$launion_a_social)
    ->with('launion_a_agri',$launion_a_agri)
    ->with('launion_a_mid',$launion_a_mid)
	->with('launion_a_online',$launion_a_online)

	->with('manila_a_let',$manila_a_let)
    ->with('manila_a_nle',$manila_a_nle)
    ->with('manila_a_crim',$manila_a_crim)
    ->with('manila_a_civil',$manila_a_civil)
    ->with('manila_a_psyc',$manila_a_psyc)
    ->with('manila_a_nclex',$manila_a_nclex)
    ->with('manila_a_ielt',$manila_a_ielt)
    ->with('manila_a_social',$manila_a_social)
    ->with('manila_a_agri',$manila_a_agri)
    ->with('manila_a_mid',$manila_a_mid)
	->with('manila_a_online',$manila_a_online)

	->with('roxas_a_let',$roxas_a_let)
    ->with('roxas_a_nle',$roxas_a_nle)
    ->with('roxas_a_crim',$roxas_a_crim)
    ->with('roxas_a_civil',$roxas_a_civil)
    ->with('roxas_a_psyc',$roxas_a_psyc)
    ->with('roxas_a_nclex',$roxas_a_nclex)
    ->with('roxas_a_ielt',$roxas_a_ielt)
    ->with('roxas_a_social',$roxas_a_social)
    ->with('roxas_a_agri',$roxas_a_agri)
    ->with('roxas_a_mid',$roxas_a_mid)
	->with('roxas_a_online',$roxas_a_online)

	

	->with('tarlac_a_let',$tarlac_a_let)
    ->with('tarlac_a_nle',$tarlac_a_nle)
    ->with('tarlac_a_crim',$tarlac_a_crim)
    ->with('tarlac_a_civil',$tarlac_a_civil)
    ->with('tarlac_a_psyc',$tarlac_a_psyc)
    ->with('tarlac_a_nclex',$tarlac_a_nclex)
    ->with('tarlac_a_ielt',$tarlac_a_ielt)
    ->with('tarlac_a_social',$tarlac_a_social)
    ->with('tarlac_a_agri',$tarlac_a_agri)
    ->with('tarlac_a_mid',$tarlac_a_mid)
	->with('tarlac_a_online',$tarlac_a_online)

	->with('vigan_a_let',$vigan_a_let)
    ->with('vigan_a_nle',$vigan_a_nle)
    ->with('vigan_a_crim',$vigan_a_crim)
    ->with('vigan_a_civil',$vigan_a_civil)
    ->with('vigan_a_psyc',$vigan_a_psyc)
    ->with('vigan_a_nclex',$vigan_a_nclex)
    ->with('vigan_a_ielt',$vigan_a_ielt)
    ->with('vigan_a_social',$vigan_a_social)
    ->with('vigan_a_agri',$vigan_a_agri)
    ->with('vigan_a_mid',$vigan_a_mid)
	->with('vigan_a_online',$vigan_a_online)
	;
}

public function total_scholars(){

	$abra_a_let=AbraLet::where('category','=','Scholar')->get();

    $abra_a_nle=AbraNle::where('category','=','Scholar')->get();

    $abra_a_crim=AbraCrim::where('category','=','Scholar')->get();

    $abra_a_civil=AbraCivil::where('category','=','Scholar')->get();

    $abra_a_psyc=AbraPsyc::where('category','=','Scholar')->get();

    $abra_a_nclex=AbraNclex::where('category','=','Scholar')->get();

    $abra_a_ielt=AbraIelt::where('category','=','Scholar')->get();

    $abra_a_social=AbraSocial::where('category','=','Scholar')->get();

    $abra_a_agri=AbraAgri::where('category','=','Scholar')->get();

    $abra_a_mid=AbraMid::where('category','=','Scholar')->get();

	$abra_a_online=AbraOnline::where('category','=','Scholar')->get();
	
	$baguio_a_let=BaguioLet::where('category','=','Scholar')->get();

    $baguio_a_nle=BaguioNle::where('category','=','Scholar')->get();

    $baguio_a_crim=BaguioCrim::where('category','=','Scholar')->get();

    $baguio_a_civil=BaguioCivil::where('category','=','Scholar')->get();

    $baguio_a_psyc=BaguioPsyc::where('category','=','Scholar')->get();

    $baguio_a_nclex=BaguioNclex::where('category','=','Scholar')->get();

    $baguio_a_ielt=BaguioIelt::where('category','=','Scholar')->get();

    $baguio_a_social=BaguioSocial::where('category','=','Scholar')->get();

    $baguio_a_agri=BaguioAgri::where('category','=','Scholar')->get();

    $baguio_a_mid=BaguioMid::where('category','=','Scholar')->get();

	$baguio_a_online=BaguioOnline::where('category','=','Scholar')->get();
	
	$calapan_a_let=CalapanLet::where('category','=','Scholar')->get();

    $calapan_a_nle=CalapanNle::where('category','=','Scholar')->get();

    $calapan_a_crim=CalapanCrim::where('category','=','Scholar')->get();

    $calapan_a_civil=CalapanCivil::where('category','=','Scholar')->get();

    $calapan_a_psyc=CalapanPsyc::where('category','=','Scholar')->get();

    $calapan_a_nclex=CalapanNclex::where('category','=','Scholar')->get();

    $calapan_a_ielt=CalapanIelt::where('category','=','Scholar')->get();

    $calapan_a_social=CalapanSocial::where('category','=','Scholar')->get();

    $calapan_a_agri=CalapanAgri::where('category','=','Scholar')->get();

    $calapan_a_mid=CalapanMid::where('category','=','Scholar')->get();

	$calapan_a_online=CalapanOnline::where('category','=','Scholar')->get();
	
	$candon_a_let=CandonLet::where('category','=','Scholar')->get();

    $candon_a_nle=CandonNle::where('category','=','Scholar')->get();

    $candon_a_crim=CandonCrim::where('category','=','Scholar')->get();

    $candon_a_civil=CandonCivil::where('category','=','Scholar')->get();

    $candon_a_psyc=CandonPsyc::where('category','=','Scholar')->get();

    $candon_a_nclex=CandonNclex::where('category','=','Scholar')->get();

    $candon_a_ielt=CandonIelt::where('category','=','Scholar')->get();

    $candon_a_social=CandonSocial::where('category','=','Scholar')->get();

    $candon_a_agri=CandonAgri::where('category','=','Scholar')->get();

    $candon_a_mid=CandonMid::where('category','=','Scholar')->get();

	$candon_a_online=CandonOnline::where('category','=','Scholar')->get();
	
	$dagupan_a_let=DagupanLet::where('category','=','Scholar')->get();

    $dagupan_a_nle=DagupanNle::where('category','=','Scholar')->get();

    $dagupan_a_crim=DagupanCrim::where('category','=','Scholar')->get();

    $dagupan_a_civil=DagupanCivil::where('category','=','Scholar')->get();

    $dagupan_a_psyc=DagupanPsyc::where('category','=','Scholar')->get();

    $dagupan_a_nclex=DagupanNclex::where('category','=','Scholar')->get();

    $dagupan_a_ielt=DagupanIelt::where('category','=','Scholar')->get();

    $dagupan_a_social=DagupanSocial::where('category','=','Scholar')->get();

    $dagupan_a_agri=DagupanAgri::where('category','=','Scholar')->get();

    $dagupan_a_mid=DagupanMid::where('category','=','Scholar')->get();

	$dagupan_a_online=DagupanOnline::where('category','=','Scholar')->get();
	
	$fairview_a_let=FairviewLet::where('category','=','Scholar')->get();

    $fairview_a_nle=FairviewNle::where('category','=','Scholar')->get();

    $fairview_a_crim=FairviewCrim::where('category','=','Scholar')->get();

    $fairview_a_civil=FairviewCivil::where('category','=','Scholar')->get();

    $fairview_a_psyc=FairviewPsyc::where('category','=','Scholar')->get();

    $fairview_a_nclex=FairviewNclex::where('category','=','Scholar')->get();

    $fairview_a_ielt=FairviewIelt::where('category','=','Scholar')->get();

    $fairview_a_social=FairviewSocial::where('category','=','Scholar')->get();

    $fairview_a_agri=FairviewAgri::where('category','=','Scholar')->get();

    $fairview_a_mid=FairviewMid::where('category','=','Scholar')->get();

	$fairview_a_online=FairviewOnline::where('category','=','Scholar')->get();
	
	$las_pinas_a_let=LasPinasLet::where('category','=','Scholar')->get();

    $las_pinas_a_nle=LasPinasNle::where('category','=','Scholar')->get();

    $las_pinas_a_crim=LasPinasCrim::where('category','=','Scholar')->get();

    $las_pinas_a_civil=LasPinasCivil::where('category','=','Scholar')->get();

    $las_pinas_a_psyc=LasPinasPsyc::where('category','=','Scholar')->get();

    $las_pinas_a_nclex=LasPinasNclex::where('category','=','Scholar')->get();

    $las_pinas_a_ielt=LasPinasIelt::where('category','=','Scholar')->get();

    $las_pinas_a_social=LasPinasSocial::where('category','=','Scholar')->get();

    $las_pinas_a_agri=LasPinasAgri::where('category','=','Scholar')->get();

    $las_pinas_a_mid=LasPinasMid::where('category','=','Scholar')->get();

	$las_pinas_a_online=LasPinasOnline::where('category','=','Scholar')->get();
	
	$launion_a_let=LaunionLet::where('category','=','Scholar')->get();

    $launion_a_nle=LaunionNle::where('category','=','Scholar')->get();

    $launion_a_crim=LaunionCrim::where('category','=','Scholar')->get();

    $launion_a_civil=LaunionCivil::where('category','=','Scholar')->get();

    $launion_a_psyc=LaunionPsyc::where('category','=','Scholar')->get();

    $launion_a_nclex=LaunionNclex::where('category','=','Scholar')->get();

    $launion_a_ielt=LaunionIelt::where('category','=','Scholar')->get();

    $launion_a_social=LaunionSocial::where('category','=','Scholar')->get();

    $launion_a_agri=LaunionAgri::where('category','=','Scholar')->get();

    $launion_a_mid=LaunionMid::where('category','=','Scholar')->get();

	$launion_a_online=LaunionOnline::where('category','=','Scholar')->get();
	
	$manila_a_let=ManilaLet::where('category','=','Scholar')->get();

    $manila_a_nle=ManilaNle::where('category','=','Scholar')->get();

    $manila_a_crim=ManilaCrim::where('category','=','Scholar')->get();

    $manila_a_civil=ManilaCivil::where('category','=','Scholar')->get();

    $manila_a_psyc=ManilaPsyc::where('category','=','Scholar')->get();

    $manila_a_nclex=ManilaNclex::where('category','=','Scholar')->get();

    $manila_a_ielt=ManilaIelt::where('category','=','Scholar')->get();

    $manila_a_social=ManilaSocial::where('category','=','Scholar')->get();

    $manila_a_agri=ManilaAgri::where('category','=','Scholar')->get();

    $manila_a_mid=ManilaMid::where('category','=','Scholar')->get();

	$manila_a_online=ManilaOnline::where('category','=','Scholar')->get();
	
	$roxas_a_let=RoxasLet::where('category','=','Scholar')->get();

    $roxas_a_nle=RoxasNle::where('category','=','Scholar')->get();

    $roxas_a_crim=RoxasCrim::where('category','=','Scholar')->get();

    $roxas_a_civil=RoxasCivil::where('category','=','Scholar')->get();

    $roxas_a_psyc=RoxasPsyc::where('category','=','Scholar')->get();

    $roxas_a_nclex=RoxasNclex::where('category','=','Scholar')->get();

    $roxas_a_ielt=RoxasIelt::where('category','=','Scholar')->get();

    $roxas_a_social=RoxasSocial::where('category','=','Scholar')->get();

    $roxas_a_agri=RoxasAgri::where('category','=','Scholar')->get();

    $roxas_a_mid=RoxasMid::where('category','=','Scholar')->get();

	$roxas_a_online=RoxasOnline::where('category','=','Scholar')->get();
	
	
	
	$tarlac_a_let=TarlacLet::where('category','=','Scholar')->get();

    $tarlac_a_nle=TarlacNle::where('category','=','Scholar')->get();

    $tarlac_a_crim=TarlacCrim::where('category','=','Scholar')->get();

    $tarlac_a_civil=TarlacCivil::where('category','=','Scholar')->get();

    $tarlac_a_psyc=TarlacPsyc::where('category','=','Scholar')->get();

    $tarlac_a_nclex=TarlacNclex::where('category','=','Scholar')->get();

    $tarlac_a_ielt=TarlacIelt::where('category','=','Scholar')->get();

    $tarlac_a_social=TarlacSocial::where('category','=','Scholar')->get();

    $tarlac_a_agri=TarlacAgri::where('category','=','Scholar')->get();

    $tarlac_a_mid=TarlacMid::where('category','=','Scholar')->get();

	$tarlac_a_online=TarlacOnline::where('category','=','Scholar')->get();
	
	$vigan_a_let=ViganLet::where('category','=','Scholar')->get();

    $vigan_a_nle=ViganNle::where('category','=','Scholar')->get();

    $vigan_a_crim=ViganCrim::where('category','=','Scholar')->get();

    $vigan_a_civil=ViganCivil::where('category','=','Scholar')->get();

    $vigan_a_psyc=ViganPsyc::where('category','=','Scholar')->get();

    $vigan_a_nclex=ViganNclex::where('category','=','Scholar')->get();

    $vigan_a_ielt=ViganIelt::where('category','=','Scholar')->get();

    $vigan_a_social=ViganSocial::where('category','=','Scholar')->get();

    $vigan_a_agri=ViganAgri::where('category','=','Scholar')->get();

    $vigan_a_mid=ViganMid::where('category','=','Scholar')->get();

    $vigan_a_online=ViganOnline::where('category','=','Scholar')->get();


    return view ('admin.total-scholars')
    ->with('abra_a_let',$abra_a_let)
    ->with('abra_a_nle',$abra_a_nle)
    ->with('abra_a_crim',$abra_a_crim)
    ->with('abra_a_civil',$abra_a_civil)
    ->with('abra_a_psyc',$abra_a_psyc)
    ->with('abra_a_nclex',$abra_a_nclex)
    ->with('abra_a_ielt',$abra_a_ielt)
    ->with('abra_a_social',$abra_a_social)
    ->with('abra_a_agri',$abra_a_agri)
    ->with('abra_a_mid',$abra_a_mid)
	->with('abra_a_online',$abra_a_online)

	->with('baguio_a_let',$baguio_a_let)
    ->with('baguio_a_nle',$baguio_a_nle)
    ->with('baguio_a_crim',$baguio_a_crim)
    ->with('baguio_a_civil',$baguio_a_civil)
    ->with('baguio_a_psyc',$baguio_a_psyc)
    ->with('baguio_a_nclex',$baguio_a_nclex)
    ->with('baguio_a_ielt',$baguio_a_ielt)
    ->with('baguio_a_social',$baguio_a_social)
    ->with('baguio_a_agri',$baguio_a_agri)
    ->with('baguio_a_mid',$baguio_a_mid)
	->with('baguio_a_online',$baguio_a_online)

	->with('calapan_a_let',$calapan_a_let)
    ->with('calapan_a_nle',$calapan_a_nle)
    ->with('calapan_a_crim',$calapan_a_crim)
    ->with('calapan_a_civil',$calapan_a_civil)
    ->with('calapan_a_psyc',$calapan_a_psyc)
    ->with('calapan_a_nclex',$calapan_a_nclex)
    ->with('calapan_a_ielt',$calapan_a_ielt)
    ->with('calapan_a_social',$calapan_a_social)
    ->with('calapan_a_agri',$calapan_a_agri)
    ->with('calapan_a_mid',$calapan_a_mid)
	->with('calapan_a_online',$calapan_a_online)

	->with('candon_a_let',$candon_a_let)
    ->with('candon_a_nle',$candon_a_nle)
    ->with('candon_a_crim',$candon_a_crim)
    ->with('candon_a_civil',$candon_a_civil)
    ->with('candon_a_psyc',$candon_a_psyc)
    ->with('candon_a_nclex',$candon_a_nclex)
    ->with('candon_a_ielt',$candon_a_ielt)
    ->with('candon_a_social',$candon_a_social)
    ->with('candon_a_agri',$candon_a_agri)
    ->with('candon_a_mid',$candon_a_mid)
	->with('candon_a_online',$candon_a_online)

	->with('dagupan_a_let',$dagupan_a_let)
    ->with('dagupan_a_nle',$dagupan_a_nle)
    ->with('dagupan_a_crim',$dagupan_a_crim)
    ->with('dagupan_a_civil',$dagupan_a_civil)
    ->with('dagupan_a_psyc',$dagupan_a_psyc)
    ->with('dagupan_a_nclex',$dagupan_a_nclex)
    ->with('dagupan_a_ielt',$dagupan_a_ielt)
    ->with('dagupan_a_social',$dagupan_a_social)
    ->with('dagupan_a_agri',$dagupan_a_agri)
    ->with('dagupan_a_mid',$dagupan_a_mid)
	->with('dagupan_a_online',$dagupan_a_online)

	->with('fairview_a_let',$fairview_a_let)
    ->with('fairview_a_nle',$fairview_a_nle)
    ->with('fairview_a_crim',$fairview_a_crim)
    ->with('fairview_a_civil',$fairview_a_civil)
    ->with('fairview_a_psyc',$fairview_a_psyc)
    ->with('fairview_a_nclex',$fairview_a_nclex)
    ->with('fairview_a_ielt',$fairview_a_ielt)
    ->with('fairview_a_social',$fairview_a_social)
    ->with('fairview_a_agri',$fairview_a_agri)
    ->with('fairview_a_mid',$fairview_a_mid)
	->with('fairview_a_online',$fairview_a_online)

	->with('las_pinas_a_let',$las_pinas_a_let)
    ->with('las_pinas_a_nle',$las_pinas_a_nle)
    ->with('las_pinas_a_crim',$las_pinas_a_crim)
    ->with('las_pinas_a_civil',$las_pinas_a_civil)
    ->with('las_pinas_a_psyc',$las_pinas_a_psyc)
    ->with('las_pinas_a_nclex',$las_pinas_a_nclex)
    ->with('las_pinas_a_ielt',$las_pinas_a_ielt)
    ->with('las_pinas_a_social',$las_pinas_a_social)
    ->with('las_pinas_a_agri',$las_pinas_a_agri)
    ->with('las_pinas_a_mid',$las_pinas_a_mid)
	->with('las_pinas_a_online',$las_pinas_a_online)

	->with('launion_a_let',$launion_a_let)
    ->with('launion_a_nle',$launion_a_nle)
    ->with('launion_a_crim',$launion_a_crim)
    ->with('launion_a_civil',$launion_a_civil)
    ->with('launion_a_psyc',$launion_a_psyc)
    ->with('launion_a_nclex',$launion_a_nclex)
    ->with('launion_a_ielt',$launion_a_ielt)
    ->with('launion_a_social',$launion_a_social)
    ->with('launion_a_agri',$launion_a_agri)
    ->with('launion_a_mid',$launion_a_mid)
	->with('launion_a_online',$launion_a_online)

	->with('manila_a_let',$manila_a_let)
    ->with('manila_a_nle',$manila_a_nle)
    ->with('manila_a_crim',$manila_a_crim)
    ->with('manila_a_civil',$manila_a_civil)
    ->with('manila_a_psyc',$manila_a_psyc)
    ->with('manila_a_nclex',$manila_a_nclex)
    ->with('manila_a_ielt',$manila_a_ielt)
    ->with('manila_a_social',$manila_a_social)
    ->with('manila_a_agri',$manila_a_agri)
    ->with('manila_a_mid',$manila_a_mid)
	->with('manila_a_online',$manila_a_online)

	->with('roxas_a_let',$roxas_a_let)
    ->with('roxas_a_nle',$roxas_a_nle)
    ->with('roxas_a_crim',$roxas_a_crim)
    ->with('roxas_a_civil',$roxas_a_civil)
    ->with('roxas_a_psyc',$roxas_a_psyc)
    ->with('roxas_a_nclex',$roxas_a_nclex)
    ->with('roxas_a_ielt',$roxas_a_ielt)
    ->with('roxas_a_social',$roxas_a_social)
    ->with('roxas_a_agri',$roxas_a_agri)
    ->with('roxas_a_mid',$roxas_a_mid)
	->with('roxas_a_online',$roxas_a_online)

	

	->with('tarlac_a_let',$tarlac_a_let)
    ->with('tarlac_a_nle',$tarlac_a_nle)
    ->with('tarlac_a_crim',$tarlac_a_crim)
    ->with('tarlac_a_civil',$tarlac_a_civil)
    ->with('tarlac_a_psyc',$tarlac_a_psyc)
    ->with('tarlac_a_nclex',$tarlac_a_nclex)
    ->with('tarlac_a_ielt',$tarlac_a_ielt)
    ->with('tarlac_a_social',$tarlac_a_social)
    ->with('tarlac_a_agri',$tarlac_a_agri)
    ->with('tarlac_a_mid',$tarlac_a_mid)
	->with('tarlac_a_online',$tarlac_a_online)

	->with('vigan_a_let',$vigan_a_let)
    ->with('vigan_a_nle',$vigan_a_nle)
    ->with('vigan_a_crim',$vigan_a_crim)
    ->with('vigan_a_civil',$vigan_a_civil)
    ->with('vigan_a_psyc',$vigan_a_psyc)
    ->with('vigan_a_nclex',$vigan_a_nclex)
    ->with('vigan_a_ielt',$vigan_a_ielt)
    ->with('vigan_a_social',$vigan_a_social)
    ->with('vigan_a_agri',$vigan_a_agri)
    ->with('vigan_a_mid',$vigan_a_mid)
	->with('vigan_a_online',$vigan_a_online)
	;
}

public function tuition_fees(){

	$abra_a_tuition = AbraTuition::all();
	$baguio_a_tuition = BaguioTuition::all();
	$calapan_a_tuition = CalapanTuition::all();
	$candon_a_tuition = CandonTuition::all();
	$dagupan_a_tuition = DagupanTuition::all();
	$fairview_a_tuition = FairviewTuition::all();
	$las_pinas_a_tuition = LasPinasTuition::all();
	$launion_a_tuition = LaunionTuition::all();
	$manila_a_tuition = ManilaTuition::all();
	$roxas_a_tuition = RoxasTuition::all();
	$tarlac_a_tuition = TarlacTuition::all();
	$vigan_a_tuition = ViganTuition::all();
	$branch = Branch::all();
	$program = Program::all();
	
	return view ('admin.tuition-fees')
	->with('abra_a_tuition',$abra_a_tuition)
	->with('baguio_a_tuition',$baguio_a_tuition)
	->with('calapan_a_tuition',$calapan_a_tuition)
	->with('candon_a_tuition',$candon_a_tuition)
	->with('dagupan_a_tuition',$dagupan_a_tuition)
	->with('fairview_a_tuition',$fairview_a_tuition)
	->with('las_pinas_a_tuition',$las_pinas_a_tuition)
	->with('launion_a_tuition',$launion_a_tuition)
	->with('manila_a_tuition',$manila_a_tuition)
	->with('roxas_a_tuition',$roxas_a_tuition)
	->with('tarlac_a_tuition',$tarlac_a_tuition)
	->with('vigan_a_tuition',$vigan_a_tuition)
	->with('branch',$branch)
	->with('program',$program);
}
public function insert_tuition(Request $request){

	$input = $request->except(['_token']);
	$branch = $input['branch'];
	

	if($branch == 'Abra'){
		
		$existent1 = AbraTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			AbraTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Baguio'){
		
		$existent1 = BaguioTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			BaguioTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Calapan'){
		
		$existent1 = CalapanTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			CalapanTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Candon'){
		
		$existent1 = CandonTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			CandonTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Dagupan'){
		
		$existent1 = DagupanTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			DagupanTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}

	if($branch == 'Fairview'){
		
		$existent1 = FairviewTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			FairviewTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'LasPinas'){
		
		$existent1 = LasPinasTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			LasPinasTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Launion'){
		
		$existent1 = LaunionTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			LaunionTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Manila'){
		
		$existent1 = ManilaTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			ManilaTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Roxas'){
		
		$existent1 = RoxasTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			RoxasTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Tarlac'){
		
		$existent1 = TarlacTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			TarlacTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
	if($branch == 'Vigan'){
		
		$existent1 = RoxasTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			RoxasTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}
}

public function update_tuition(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Abra'){
		AbraTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Baguio'){
		BaguioTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Calapan'){
		CalapanTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Candon'){
		CandonTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Dagupan'){
		DagupanTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Fairview'){
		FairviewTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'LasPinas'){
		LasPinasTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Launion'){
		LaunionTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Manila'){
		ManilaTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Roxas'){
		RoxasTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Tarlac'){
		TarlacTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
	if($branch == 'Vigan'){
		ViganTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
}

public function delete_tuition(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Abra'){

              AbraTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Baguio'){

              BaguioTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Calapan'){

              CalapanTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Candon'){

              CandonTuition::where('id','=',$input['id'])->delete();
           
    	}
		if($input['branch'] == 'Dagupan'){

              DagupanTuition::where('id','=',$input['id'])->delete();
           
		}
		if($input['branch'] == 'Fairview'){

			FairviewTuition::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'LaPinas'){

			LaPinasTuition::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Launion'){

			LaunionTuition::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Manila'){

			ManilaTuition::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Roxas'){

			RoxasTuition::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Tarlac'){

			TarlacTuition::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Vigan'){

			ViganTuition::where('id','=',$input['id'])->delete();
		 
	  }

              return response()->json([
            'success' => true,
            'message' => '1 tuition has been deleted',
        ]);
      
         }

public function discounts(){
	
	$abra_a_discount = AbraDiscount::all();
	$baguio_a_discount = BaguioDiscount::all();
	$calapan_a_discount = CalapanDiscount::all();
	$candon_a_discount = CandonDiscount::all();
	$dagupan_a_discount = DagupanDiscount::all();
	$fairview_a_discount = FairviewDiscount::all();
	$las_pinas_a_discount = LasPinasDiscount::all();
	$launion_a_discount = LaunionDiscount::all();
	$manila_a_discount = ManilaDiscount::all();
	$roxas_a_discount = RoxasDiscount::all();
	$tarlac_a_discount = TarlacDiscount::all();
	$vigan_a_discount = ViganDiscount::all();
	$branch = Branch::all();
	$program = Program::all();
	return view ('admin.discounts')
	
	
	->with('abra_a_discount',$abra_a_discount)
	->with('baguio_a_discount',$baguio_a_discount)
	->with('calapan_a_discount',$calapan_a_discount)
	->with('candon_a_discount',$candon_a_discount)
	->with('dagupan_a_discount',$dagupan_a_discount)
	->with('fairview_a_discount',$fairview_a_discount)
	->with('las_pinas_a_discount',$las_pinas_a_discount)
	->with('launion_a_discount',$launion_a_discount)
	->with('manila_a_discount',$manila_a_discount)
	->with('roxas_a_discount',$roxas_a_discount)
	->with('tarlac_a_discount',$tarlac_a_discount)
	->with('vigan_a_discount',$vigan_a_discount)
	->with('branch',$branch)
	->with('program',$program);
}

public function insert_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Abra'){
		AbraDiscount::insert($input);
	}
	if($branch == 'Baguio'){
		BaguioDiscount::insert($input);
	}
	if($branch == 'Calapan'){
		CalapanDiscount::insert($input);
	}
	if($branch == 'Candon'){
		CandonDiscount::insert($input);
	}
	if($branch == 'Dagupan'){
		DagupanDiscount::insert($input);
	}
	if($branch == 'Fairview'){
		FairviewDiscount::insert($input);
	}
	if($branch == 'LasPinas'){
		LasPinasDiscount::insert($input);
	}
	if($branch == 'Launion'){
		LaunionDiscount::insert($input);
	}
	if($branch == 'Manila'){
		ManilaDiscount::insert($input);
	}
	if($branch == 'Roxas'){
		RoxasDiscount::insert($input);
	}
	if($branch == 'Tarlac'){
		TarlacDiscount::insert($input);
	}
	if($branch == 'Vigan'){
		ViganDiscount::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New discount has been added',
        ]);

}

public function update_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Abra'){
		AbraDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Baguio'){
		BaguioDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Calapan'){
		CalapanDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Candon'){
		CandonDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Dagupan'){
		DagupanDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Fairview'){
		FairviewDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'LasPinas'){
		LasPinasDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Launion'){
		LaunionDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Manila'){
		ManilaDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Roxas'){
		RoxasDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Tarlac'){
		TarlacDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	if($branch == 'Vigan'){
		ViganDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}

return response()->json([
            'success' => true,
            'message' => '1 discount has been updated',
        ]);

}

public function delete_discount(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Abra'){

			AbraDiscount::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Baguio'){

			BaguioDiscount::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Calapan'){

			CalapanDiscount::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Candon'){

			CandonDiscount::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Dagupan'){

			DagupanDiscount::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Fairview'){

		  FairviewDiscount::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'LaPinas'){

		  LaPinasDiscount::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Launion'){

		  LaunionDiscount::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Manila'){

		  ManilaDiscount::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Roxas'){

		  RoxasDiscount::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Tarlac'){

		  TarlacDiscount::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Vigan'){

		  ViganDiscount::where('id','=',$input['id'])->delete();
	   
	}
		  
		  

       return response()->json([
            'success' => true,
            'message' => '1 discount has been deleted',
        ]);
      
         }
    

public function total_books(){
	$branch = Branch::all();
	$abra_a_book = AbraBooksInventorie::all();
	$abra_a_sale = AbraBooksSale::all();
	
	$baguio_a_book = BaguioBooksInventorie::all();
    $baguio_a_sale = BaguioBooksSale::all();

	$calapan_a_book = CandonBooksInventorie::all();
	$calapan_a_sale = CandonBooksSale::all();
	
	$candon_a_book = CandonBooksInventorie::all();
    $candon_a_sale = CandonBooksSale::all();

	$dagupan_a_book = DagupanBooksInventorie::all();
	$dagupan_a_sale = DagupanBooksSale::all();
	
	$fairview_a_book = FairviewBooksInventorie::all();
	$fairview_a_sale = FairviewBooksSale::all();
	
	$las_pinas_a_book = LasPinasBooksInventorie::all();
    $las_pinas_a_sale = LasPinasBooksSale::all();

	$launion_a_book = LaunionBooksInventorie::all();
	$launion_a_sale = LaunionBooksSale::all();
	
	$manila_a_book = ManilaBooksInventorie::all();
    $manila_a_sale = ManilaBooksSale::all();

	$roxas_a_book = RoxasBooksInventorie::all();
	$roxas_a_sale = RoxasBooksSale::all();
	
	$tarlac_a_book = TarlacBooksInventorie::all();
    $tarlac_a_sale = TarlacBooksSale::all();

	$vigan_a_book = ViganBooksInventorie::all();
	$vigan_a_sale = ViganBooksSale::all();
	

	$program = Program::all();
	return view ('admin.total-books')
	->with('abra_a_book',$abra_a_book)
	->with('abra_a_sale',$abra_a_sale)

	->with('baguio_a_book',$baguio_a_book)
	->with('baguio_a_sale',$baguio_a_sale)

	->with('calapan_a_book',$calapan_a_book)
	->with('calapan_a_sale',$calapan_a_sale)
	
	->with('candon_a_book',$candon_a_book)
	->with('candon_a_sale',$candon_a_sale)

	->with('dagupan_a_book',$dagupan_a_book)
	->with('dagupan_a_sale',$dagupan_a_sale)
	
	->with('fairview_a_book',$fairview_a_book)
	->with('fairview_a_sale',$fairview_a_sale)

	->with('las_pinas_a_book',$las_pinas_a_book)
	->with('las_pinas_a_sale',$las_pinas_a_sale)
	
	->with('launion_a_book',$launion_a_book)
	->with('launion_a_sale',$launion_a_sale)

	->with('manila_a_book',$manila_a_book)
	->with('manila_a_sale',$manila_a_sale)

	->with('roxas_a_book',$roxas_a_book)
	->with('roxas_a_sale',$roxas_a_sale)
	
	->with('tarlac_a_book',$tarlac_a_book)
	->with('tarlac_a_sale',$tarlac_a_sale)

	->with('vigan_a_book',$vigan_a_book)
	->with('vigan_a_sale',$vigan_a_sale)

	->with('branch',$branch)
	->with('program',$program);
}

public function insert_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Abra'){
		AbraBooksInventorie::insert($input);
	}
	if($branch == 'Baguio'){
		BaguioBooksInventorie::insert($input);
	}
	if($branch == 'Calapan'){
		CalapanBooksInventorie::insert($input);
	}
	if($branch == 'Candon'){
		CandonBooksInventorie::insert($input);
	}
	if($branch == 'Dagupan'){
		DagupanBooksInventorie::insert($input);
	}
	if($branch == 'Fairview'){
		FairviewBooksInventorie::insert($input);
	}
	if($branch == 'LasPinas'){
		LasPinasBooksInventorie::insert($input);
	}
	if($branch == 'Launion'){
		LaunionBooksInventorie::insert($input);
	}
	if($branch == 'Manila'){
		ManilaBooksInventorie::insert($input);
	}
	if($branch == 'Roxas'){
		RoxasBooksInventorie::insert($input);
	}
	if($branch == 'Tarlac'){
		TarlacBooksInventorie::insert($input);
	}
	if($branch == 'Vigan'){
		ViganBooksInventorie::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New book has been added',
        ]);

}

public function update_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	
	if($branch == 'Abra'){
		AbraBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Baguio'){
		BaguioBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Calapan'){
		CalapanBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Candon'){
		CandonBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Dagupan'){
		DagupanBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Fairview'){
		FairviewBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'LasPinas'){
		LasPinasBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Launion'){
		LaunionBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Roxas'){
		RoxasBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Tarlac'){
		TarlacooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	
	if($branch == 'Vigan'){
		ViganBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	if($branch == 'Manila'){
		ManilaBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	

return response()->json([
            'success' => true,
            'message' => 'Book inventories has been updated',
        ]);

}

public function delete_book(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Abra'){

			AbraBooksInventorie::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Baguio'){

			BaguioBooksInventorie::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Calapan'){

			CalapanBooksInventorie::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Candon'){

			CandonBooksInventorie::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Dagupan'){

			DagupanBooksInventorie::where('id','=',$input['id'])->delete();
		 
	  }
	  if($input['branch'] == 'Fairview'){

		  FairviewBooksInventorie::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'LaPinas'){

		  LaPinasBooksInventorie::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Launion'){

		  LaunionBooksInventorie::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Manila'){

		  ManilaBooksInventorie::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Roxas'){

		  RoxasBooksInventorie::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Tarlac'){

		  TarlacBooksInventorie::where('id','=',$input['id'])->delete();
	   
	}
	if($input['branch'] == 'Vigan'){

		  ViganBooksInventorie::where('id','=',$input['id'])->delete();
	   
	}
		  
		 


    return response()->json([
            'success' => true,
            'message' => '1 book record has been deleted',
        ]);
}

public function total_expense(){

	$abra_a_sale = AbraExpense::all();
	$baguio_a_sale = BaguioExpense::all();
	$calapan_a_sale = CalapanExpense::all();
	$candon_a_sale = CandonExpense::all();
	$dagupan_a_sale = DagupanExpense::all();
	$fairview_a_sale = FairviewExpense::all();
	$las_pinas_a_sale = LasPinasExpense::all();
	$launion_a_sale = LaunionExpense::all();
	$manila_a_sale = ManilaExpense::all();
	$roxas_a_sale = RoxasExpense::all();
	$tarlac_a_sale = TarlacExpense::all();
	$vigan_a_sale = ViganExpense::all();
	
	
	return view ('admin.total-expense')
	->with('abra_a_sale',$abra_a_sale)
	->with('baguio_a_sale',$baguio_a_sale)
	->with('calapan_a_sale',$calapan_a_sale)
	->with('candon_a_sale',$candon_a_sale)
	->with('dagupan_a_sale',$dagupan_a_sale)
	->with('fairview_a_sale',$fairview_a_sale)
	->with('las_pinas_a_sale',$las_pinas_a_sale)
	->with('launion_a_sale',$launion_a_sale)
	->with('manila_a_sale',$manila_a_sale)
	->with('roxas_a_sale',$roxas_a_sale)
	->with('tarlac_a_sale',$tarlac_a_sale)
	->with('vigan_a_sale',$vigan_a_sale)
	;
	
}

public function user(){
	$branch = Branch::all();
	$user = User::where('show','=','1')->get();
	$role = Role::all();
	return view ('admin.user')->with('role',$role)->with('user',$user)->with('branch',$branch);
}

public function insert_user(Request $request){

$input = $request->except(['_token']);

$validatedData = $request->validate([
	'username' => 'unique:users|required',
	'email' => 'unique:users|required',
]);
$roleID = 0;

if($input['role']=='abra'){
	$roleID = 2;
}
if($input['role']=='baguio'){
	$roleID = 3;
}
if($input['role']=='calapan'){
	$roleID = 4;
}
if($input['role']=='candon'){
	$roleID = 5;
}
if($input['role']=='dagupan'){
	$roleID = 6;
}
if($input['role']=='fairview'){
	$roleID = 7;
}
if($input['role']=='las_pinas'){
	$roleID = 8;
}
if($input['role']=='launion'){
	$roleID = 9;
}
if($input['role']=='manila'){
	$roleID = 10;
}
if($input['role']=='roxas'){
	$roleID = 11;
}
if($input['role']=='tarlac'){
	$roleID = 12;
}
if($input['role']=='vigan'){
	$roleID = 13;
}
if($input['role']=='ra_abra'){
	$roleID = 14;
}
if($input['role']=='ra_baguio'){
	$roleID = 15;
}
if($input['role']=='ra_calapan'){
	$roleID = 16;
}
if($input['role']=='ra_candon'){
	$roleID = 17;
}
if($input['role']=='ra_dagupan'){
	$roleID = 18;
}
if($input['role']=='ra_fairview'){
	$roleID = 19;
}
if($input['role']=='ra_las_pinas'){
	$roleID = 20;
}
if($input['role']=='ra_launion'){
	$roleID = 21;
}
if($input['role']=='ra_manila'){
	$roleID = 22;
}
if($input['role']=='ra_roxas'){
	$roleID = 23;
}
if($input['role']=='ra_tarlac'){
	$roleID = 24;
}
if($input['role']=='ra_vigan'){
	$roleID = 25;
}
if($input['role']=='abra_enrollment'){
	$roleID = 28;
}
if($input['role']=='abra_cashier'){
	$roleID = 29;
}
if($input['role']=='baguio_enrollment'){
	$roleID = 30;
}
if($input['role']=='baguio_cashier'){
	$roleID = 31;
}
if($input['role']=='calapan_enrollment'){
	$roleID = 32;
}
if($input['role']=='calapan_cashier'){
	$roleID = 33;
}
if($input['role']=='candon_enrollment'){
	$roleID = 34;
}
if($input['role']=='candon_cashier'){
	$roleID = 35;
}
if($input['role']=='dagupan_enrollment'){
	$roleID = 36;
}
if($input['role']=='dagupan_cashier'){
	$roleID = 37;
}
if($input['role']=='fairview_enrollment'){
	$roleID = 38;
}
if($input['role']=='fairview_cashier'){
	$roleID = 39;
}
if($input['role']=='las_pinas_enrollment'){
	$roleID = 40;
}
if($input['role']=='las_pinas_cashier'){
	$roleID = 41;
}
if($input['role']=='launion_enrollment'){
	$roleID = 42;
}
if($input['role']=='launion_cashier'){
	$roleID = 43;
}
if($input['role']=='manila_enrollment'){
	$roleID = 44;
}
if($input['role']=='manila_cashier'){
	$roleID = 45;
}
if($input['role']=='roxas_enrollment'){
	$roleID = 46;
}
if($input['role']=='roxas_cashier'){
	$roleID = 47;
}
if($input['role']=='tarlac_enrollment'){
	$roleID = 48;
}
if($input['role']=='tarlac_cashier'){
	$roleID = 49;
}
if($input['role']=='vigan_enrollment'){
	$roleID = 50;
}
if($input['role']=='vigan_cashier'){
	$roleID = 51;
}

$branch = $input['branch'];

	if($branch == 'Main'){
		User::insert([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
			'show'		=> '1',
			'remember_token' => str_random(60),
		]);

		$user_id = User::max('id');

		DB::table('role_user')->insert([

            'user_id' => $user_id,
            'role_id'    => '1',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'New Administrator has been added',
        ]);
	}
	if($branch != 'Admin'){

		
		User::insert([
			'role' => $input['role'],
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
			'show'		=> '1',
			'remember_token' => str_random(60),
		]);

		$user_id = User::max('id');

		DB::table('role_user')->insert([

            'user_id' => $user_id,
            'role_id'    => $roleID,
            ]); 

		return response()->json([
            'success' => true,
			'message' => 'New Member has been added',
			'input'	  =>  $input
        ]);
	}


}
public function update_user(Request $request){

$input = $request->except(['_token']);


$roleID = 0;
if($input['role']=='abra'){
	$roleID = 2;
}
if($input['role']=='baguio'){
	$roleID = 3;
}
if($input['role']=='calapan'){
	$roleID = 4;
}
if($input['role']=='candon'){
	$roleID = 5;
}
if($input['role']=='dagupan'){
	$roleID = 6;
}
if($input['role']=='fairview'){
	$roleID = 7;
}
if($input['role']=='las_pinas'){
	$roleID = 8;
}
if($input['role']=='launion'){
	$roleID = 9;
}
if($input['role']=='manila'){
	$roleID = 10;
}
if($input['role']=='roxas'){
	$roleID = 11;
}
if($input['role']=='tarlac'){
	$roleID = 12;
}
if($input['role']=='vigan'){
	$roleID = 13;
}
if($input['role']=='ra_abra'){
	$roleID = 14;
}
if($input['role']=='ra_baguio'){
	$roleID = 15;
}
if($input['role']=='ra_calapan'){
	$roleID = 16;
}
if($input['role']=='ra_candon'){
	$roleID = 17;
}
if($input['role']=='ra_dagupan'){
	$roleID = 18;
}
if($input['role']=='ra_fairview'){
	$roleID = 19;
}
if($input['role']=='ra_las_pinas'){
	$roleID = 20;
}
if($input['role']=='ra_launion'){
	$roleID = 21;
}
if($input['role']=='ra_manila'){
	$roleID = 22;
}
if($input['role']=='ra_roxas'){
	$roleID = 23;
}
if($input['role']=='ra_tarlac'){
	$roleID = 24;
}
if($input['role']=='ra_vigan'){
	$roleID = 25;
}
if($input['role']=='abra_enrollment'){
	$roleID = 28;
}
if($input['role']=='abra_cashier'){
	$roleID = 29;
}
if($input['role']=='baguio_enrollment'){
	$roleID = 30;
}
if($input['role']=='baguio_cashier'){
	$roleID = 31;
}
if($input['role']=='calapan_enrollment'){
	$roleID = 32;
}
if($input['role']=='calapan_cashier'){
	$roleID = 33;
}
if($input['role']=='candon_enrollment'){
	$roleID = 34;
}
if($input['role']=='candon_cashier'){
	$roleID = 35;
}
if($input['role']=='dagupan_enrollment'){
	$roleID = 36;
}
if($input['role']=='dagupan_cashier'){
	$roleID = 37;
}
if($input['role']=='fairview_enrollment'){
	$roleID = 38;
}
if($input['role']=='fairview_cashier'){
	$roleID = 39;
}
if($input['role']=='las_pinas_enrollment'){
	$roleID = 40;
}
if($input['role']=='las_pinas_cashier'){
	$roleID = 41;
}
if($input['role']=='launion_enrollment'){
	$roleID = 42;
}
if($input['role']=='launion_cashier'){
	$roleID = 43;
}
if($input['role']=='manila_enrollment'){
	$roleID = 44;
}
if($input['role']=='manila_cashier'){
	$roleID = 45;
}
if($input['role']=='roxas_enrollment'){
	$roleID = 46;
}
if($input['role']=='roxas_cashier'){
	$roleID = 47;
}
if($input['role']=='tarlac_enrollment'){
	$roleID = 48;
}
if($input['role']=='tarlac_cashier'){
	$roleID = 49;
}
if($input['role']=='vigan_enrollment'){
	$roleID = 50;
}
if($input['role']=='vigan_cashier'){
	$roleID = 51;
}



$id = $input['id'];
$branch = $input['branch'];

	if($branch == 'Main'){
		if($input['password'] == null){
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}

		else{
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'password' => Hash::make($input['password']),
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}



		DB::table('role_user')->where('user_id','=',$id)->update([

            'user_id' => $id,
            'role_id'    => '1',
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'User has been updated',
        ]);
	}
	if($branch != 'Admin'){
		if($input['password'] == null){
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
		}

		else{
		User::where('id','=',$id)->update([
			'role' => $input['role'],
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch'	=> $input['branch'],
		]);
	}



		DB::table('role_user')->where('user_id','=',$id)->update([

            'user_id' => $id,
            'role_id'    => $roleID,
            ]); 

		return response()->json([
            'success' => true,
            'message' => 'User has been updated',
        ]);
	}
	
}
public function delete_user(Request $request){

	if(isset($request->id)){
              $todo = User::findOrFail($request->id);
              $todo->delete();
              return response()->json([
            'success' => true,
            'message' => 'User has been deleted',
        ]);
        }


}
function checkemail(Request $request)
{
 if($request->get('email'))
 {
  $email = $request->get('email');
  $data = DB::table("users")
   ->where('email', $email)
   ->count();
  if($data > 0)
  {
   echo 'unique';
  }
  else
  {
   echo 'not_unique';
  }
 }
}//end check for email

function checkusername(Request $request)
{
 if($request->get('username'))
 {
  $username = $request->get('username');
  $data = DB::table("users")
   ->where('username', $username)
   ->count();
  if($data > 0)
  {
   echo 'unique';
  }
  else
  {
   echo 'not_unique';
  }
 }
}//e4nd check for username




function abra_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = AbraScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = AbraScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = AbraScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 AbraScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card


function baguio_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = BaguioScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = BaguioScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = BaguioScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 BaguioScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card


function calapan_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = CalapanScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = CalapanScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = CalapanScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 CalapanScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function candon_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = CandonScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = CandonScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = CandonScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 CandonScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function dagupan_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = DagupanScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = DagupanScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = DagupanScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 DagupanScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function fairview_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = FairviewScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = FairviewScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = FairviewScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 FairviewScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card


function las_pinas_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = LasPinasScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = LasPinasScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = LasPinasScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 LasPinasScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card


function launion_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = LaunionScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = LaunionScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = LaunionScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 LaunionScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function manila_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = ManilaScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = ManilaScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = ManilaScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 ManilaScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function roxas_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = RoxasScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = RoxasScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = RoxasScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 RoxasScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function tarlac_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = TarlacScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = TarlacScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = TarlacScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 TarlacScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

function vigan_populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = ViganScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = ViganScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = ViganScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 ViganScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card

public function expense_setup(Request $request){

	$branch = $request->input('branch');
	$Season = $request->input('season');
	$Year = $request->input('year');

	$setup = ExpenseSetup::where('Branch','=',$branch)->count();
	$id = ExpenseSetup::where('Branch','=',$branch)->value('id');

	if ($setup == 0) {
		//new settings
		$Set = New ExpenseSetup;
		$Set->Branch = $branch;
		$Set->Season = $Season;
		$Set->Year = $Year;
		$Set->save();

		return "save";
	} else {
		
		$updset = ExpenseSetup::find($id);
		$updset->Branch = $branch;
		$updset->Season = $Season;
		$updset->Year = $Year;
		$updset->save();
		return "update";
	}
	

}

public function dash_settings($param){

		$branch = Branch::all();
		
		
        
        
    return view('admin.admin-settings')
    ->with('branch',$branch)
    ->with('param',$param);
    
}

public function save_adsettings(Request $request){
    
    $param = $request->input('param');
    $branch = $request->input('branch');
	$Season = $request->input('season');
	$Year = $request->input('year');
    
    
   	$setup = AdminSettings::where('Branch','=',$branch)->where('Account','=',$param)->count();
	$id = AdminSettings::where('Branch','=',$branch)->where('Account','=',$param)->value('id');
    
	if ($setup == 0) {
		//new settings
		$Set = New AdminSettings;
		$Set->Account = $param;
		$Set->Branch = $branch;
		$Set->Season = $Season;
		$Set->Year = $Year;
		$Set->save();

     return Redirect()->back()->with(['message' => 'Setting has been Set']);
        // 	return response()->json([
        //     'success' => true,
        //     'message' => 'New Setting has been set',
        // ]);
		
	} else {
		
		$updset = AdminSettings::find($id);
		$updset->Account = $param;
		$updset->Branch = $branch;
		$updset->Season = $Season;
		$updset->Year = $Year;
		$updset->save();
		
		 return Redirect()->back()->with(['message' => 'Setting has been Updated']);
// 			return response()->json([
//             'success' => true,
//             'message' => 'Setting has been Updated',
//         ]);
		
	}
    

}


public function attendance(){

  //login to api
  $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
  $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
	  'form_params' => [
		  "email"=>"admin@main.cbrc.solutions",
		  "password"=>"main@dmin"
	  ]
  ]);
//insert data to api

  if ($res->getStatusCode() == 200) { // 200 OK
	  $response_data = json_decode($res->getBody()->getContents());
   //save first payment to api   
	  $sendPayment = $client->request('GET', 'https://cbrc.solutions/api/main/attendance-report?token='.$response_data->access_token);
	

  }//end of 200 ok   

	return view('radashboard.attendance')->with($sendPayment->getContents());

}
public function evaluation(){
	return view('radashboard.evaluation');
}

public function filter_exam(){
	$program = Program::all();

	  $sy = date("Y")+1;
      $years = range($sy,2010);

	return view('radashboard.filter_monitoring_exam')
	->with('years',$years)
	->with('program',$program);
}

public function exam_monitoring(Request $request){

	$season = $request->input('season');
	$year = $request->input('year');
	$program = $request->input('program');
	$major = $request->input('major');

	return view('radashboard.exam_monitoring')
	->with('season',$season)
	->with('year',$year)
	->with('program',$program)
	->with('major',$major);

}



public function expense_settings(){
	$branch = Branch::all();
	return view('admin.expense-settings')->with('branch',$branch);
}



public function lecturer(){
	return view('radashboard.lecturer');
}
public function online_completion(){
	return view('radashboard.online_completion');
}
public function result_analysis(){
	return view('radashboard.result_analysis');
	
}



}
