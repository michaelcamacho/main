<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Roxas\RoxasAgri;
use App\Model\Roxas\RoxasBookCash;
use App\Model\Roxas\RoxasBooksInventorie;
use App\Model\Roxas\RoxasBooksSale;
use App\Model\Roxas\RoxasBudget;
use App\Model\Roxas\RoxasCivil;
use App\Model\Roxas\RoxasCrim;
use App\Model\Roxas\RoxasDiscount;
use App\Model\Roxas\RoxasDropped;
use App\Model\Roxas\RoxasExpense;
use App\Model\Roxas\RoxasIelt;
use App\Model\Roxas\RoxasLet;
use App\Model\Roxas\RoxasMid;
use App\Model\Roxas\RoxasNclex;
use App\Model\Roxas\RoxasNle;
use App\Model\Roxas\RoxasOnline;
use App\Model\Roxas\RoxasPsyc;
use App\Model\Roxas\RoxasReceivable;
use App\Model\Roxas\RoxasS1Sale;
use App\Model\Roxas\RoxasS2Sale;
use App\Model\Roxas\RoxasS1Cash;
use App\Model\Roxas\RoxasS2Cash;
use App\Model\Roxas\RoxasScholar;
use App\Model\Roxas\RoxasSocial;
use App\Model\Roxas\RoxasTuition;
use App\Model\Roxas\RoxasPettyCash;
use App\Model\Roxas\RoxasRemit;
use App\Model\Roxas\RoxasReservation;
use App\Model\Roxas\RoxasEmployee;
use App\Model\Roxas\RoxasScoreCards;
use App\Model\Roxas\RoxasBookTransfer;

use App\Model\Roxas\RoxasLecturerAEvaluation;
use App\Model\Roxas\RoxasLecturerBEvaluation;
use App\Model\Roxas\RoxasComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class RoxasCashierController extends Controller
{
    private $branch = "Roxas";

    private $sbranch = "roxas";

public function __construct()
    {

         $this->middleware('role:roxas_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = RoxasBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = RoxasBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = RoxasBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             RoxasBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = RoxasBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        RoxasBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = RoxasBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        RoxasBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('roxas-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('roxas-cashier/book-payment');
        }
}
public function books_table(){

$book = RoxasBooksInventorie::all();
$sale = RoxasBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }