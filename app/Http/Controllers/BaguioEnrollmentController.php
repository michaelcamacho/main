<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Baguio\BaguioAgri;
use App\Model\Baguio\BaguioBookCash;
use App\Model\Baguio\BaguioBooksInventorie;
use App\Model\Baguio\BaguioBooksSale;
use App\Model\Baguio\BaguioBudget;
use App\Model\Baguio\BaguioCivil;
use App\Model\Baguio\BaguioCrim;
use App\Model\Baguio\BaguioDiscount;
use App\Model\Baguio\BaguioDropped;
use App\Model\Baguio\BaguioExpense;
use App\Model\Baguio\BaguioIelt;
use App\Model\Baguio\BaguioLet;
use App\Model\Baguio\BaguioMid;
use App\Model\Baguio\BaguioNclex;
use App\Model\Baguio\BaguioNle;
use App\Model\Baguio\BaguioOnline;
use App\Model\Baguio\BaguioPsyc;
use App\Model\Baguio\BaguioReceivable;
use App\Model\Baguio\BaguioS1Sale;
use App\Model\Baguio\BaguioS2Sale;
use App\Model\Baguio\BaguioS1Cash;
use App\Model\Baguio\BaguioS2Cash;
use App\Model\Baguio\BaguioScholar;
use App\Model\Baguio\BaguioSocial;
use App\Model\Baguio\BaguioTuition;
use App\Model\Baguio\BaguioPettyCash;
use App\Model\Baguio\BaguioRemit;
use App\Model\Baguio\BaguioReservation;
use App\Model\Baguio\BaguioEmployee;
use App\Model\Baguio\BaguioScoreCards;
use App\Model\Baguio\BaguioBookTransfer;

use App\Model\Baguio\BaguioLecturerAEvaluation;
use App\Model\Baguio\BaguioLecturerBEvaluation;
use App\Model\Baguio\BaguioComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class BaguioEnrollmentController extends Controller
{

    private $branch = "Baguio";

    private $sbranch = "baguio";

public function __construct()
    {

         $this->middleware('role:baguio_enrollment');
        
    }
    public function new_payment(){
        $this->populateScoreCard();
        $branch= $this->branch; 
        $date = date('M-d-Y');
        $program = Program::all();
        return view('member.new-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }


// Register New Enrollee

public function add_enrollee(){

    $branch= $this->branch; 

    $program = Program::all();

    return view('member.add-enrollee')->with('branch',$branch)->with('program',$program);
}



public function insert_enrollee(Request $request){

    $input = $request->except(['_token']);
    $branch = $this->branch;
    $course = $input['program'];

    if($course == 'lets'){
        $course = 'LET';
    }
    if($course == 'nles'){
        $course = 'NLE';
    }
    if($course == 'crims'){
        $course = 'Criminology';
    }
    if($course == 'civils'){
        $course = 'Civil Service';
    }

    if($course == 'psycs'){
        $course = 'Psychometrician';
    }
    if($course == 'nclexes'){
        $course = 'NCLEX';
    }
    if($course == 'ielts'){
        $course = 'IELTS';
    }
    if($course == 'socials'){
        $course = 'Social Work';
    }
    if($course == 'agris'){
        $course = 'Agriculture';
    }
    if($course == 'mids'){
        $course = 'Midwifery';
    }
    if($course == 'onlines'){
        $course = 'Online Only';
    }

    $program = $this->sbranch.'_'.$input['program'];

    $lastname = strtoupper($input['last_name']);
    $firstname = strtoupper($input['first_name']);
    $middlename = strtoupper($input['middle_name']);

    $existent = DB::table($program)->where('last_name','=',$lastname)->where('first_name','=',$firstname)->where('middle_name','=',$middlename)->first();

    if($existent != null){
        Alert::error('Failed!', 'This name is already registered.');
        return redirect ('baguio-enrollment/add-enrollee');
    }

    if($existent == null){
    DB::table($program)->insert([

        'cbrc_id'       => $input['cbrc_id'],
        'section'       => $input['section'],
        'last_name'     => strtoupper($input['last_name']),
        'first_name'    => strtoupper($input['first_name']),
        'middle_name'   => strtoupper($input['middle_name']),
        'username'      => $input['username'],
        'password'      => $input['password'],
        'course'        => $course,
        'major'         => $input['major'],
        'program'       => $input['program'],
        'school'        => $input['school'],
        'noa_no'        => $input['noa_no'],
        'take'          => $input['take'],
        'branch'        => $branch,
        'birthdate'     => $input['birthdate'],
        'contact_no'    => $input['contact_no'],
        'email'         => $input['email'],
        'address'       => $input['address'],
        'contact_person'=> $input['contact_person'],
        'contact_details'=> $input['contact_details'],
        'registration'  => 'Walk-in',
        'created_at'    => date('Y-m-d'),
    ]);
    Alert::success('Success!', 'New student has been registered.');
    return redirect ('baguio-enrollment/add-enrollee');

}
}

public function update_enrollee(Request $request){
$input = $request->except(['_token']);
    $branch = $this->branch;
    $program = $input['program'];
    $id = $input['id'];

    if($program == 'LET'){
        $program = 'lets';
    }
    if($program == 'NLE'){
        $program = 'nles';
    }
    if($program == 'Criminology'){
        $program = 'crims';
    }
    if($program == 'Civil Service'){
        $program = 'civils';
    }

    if($program == 'Psychometrician'){
        $program = 'psycs';
    }
    if($program == 'NCLEX'){
        $program = 'nclexes';
    }
    if($program == 'IELTS'){
        $program = 'ielts';
    }
    if($program == 'Social Work'){
        $program = 'socials';
    }
    if($program == 'Agriculture'){
        $program = 'agris';
    }
    if($program == 'Midwifery'){
        $program = 'mids';
    }
    if($program == 'Online Only'){
        $program = 'onlines';
    }

    $program = $this->sbranch.'_'.$program;

    DB::table($program)->where('id','=',$id)->update([

        'cbrc_id'       => $input['cbrc_id'],
        'last_name'     => $input['last_name'],
        'first_name'    => $input['first_name'],
        'middle_name'   => $input['middle_name'],
        'username'      => $input['username'],
        'password'      => $input['password'],
        'major'         => $input['major'],
        'school'        => $input['school'],
        'noa_no'        => $input['noa_no'],
        'take'          => $input['take'],
        'birthdate'     => $input['birthdate'],
        'contact_no'    => $input['contact_no'],
        'email'         => $input['email'],
        'address'       => $input['address'],
        'contact_person'=> $input['contact_person'],
        'contact_details'=> $input['contact_details'],
        'section'        => $input['section'],
    ]);
   
   //login to api
   $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
   $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
       'form_params' => [
           "email"=>"admin@main.cbrc.solutions",
           "password"=>"main@dmin"
       ]
   ]);
//insert data to api

   if ($res->getStatusCode() == 200) { // 200 OK
       $response_data = json_decode($res->getBody()->getContents());
    
       
           //save student info to api
           $studentForApi =  DB::table($program)->where('id','=' , $id)->where('status','=','Enrolled')->first();
           $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
           ,[
           'form_params' => [
               "_method" => "PUT",
               "BranchStdID" => $studentForApi->id,
               "Branch_Name" =>lcfirst($this->sbranch),
               "cbrc_id" =>     $input['cbrc_id'],
               "Lastname" => $input['last_name'],
               "Firstname" =>$input['first_name'],
               "Middlename" =>$input['middle_name'],
               "Birthday" =>$input['birthdate'],
               "Contact_Number" =>$input['contact_no'],
               "Address" =>$input['address'],
               "Email" =>$input['email'],
               "Username" =>$input['username'],
               "Password" =>$input['password'],
               "School" => $input['school'],
               "Program" =>$studentForApi->program,
               "Section" =>$input['section'],
               "Major" =>$input['major'],
               "Take" =>$input['take'],
               "Noa" =>$input['noa_no'],
               "Category" =>$studentForApi->category,
               "Status" =>$studentForApi->status,
               "Contact_Person" =>$input['contact_person'],
               "Contact_Details" =>$input['contact_details'],
               "Facilitation" =>$studentForApi->facilitation,
               "Season" =>$studentForApi->season,
               "Year" =>$studentForApi->year
       
           ]
           ]);
           // return dd($studentForApi);


   }//end of 200 ok

    return response()->json([
        'success' => true,
        'message' => 'Enrollee has been updated',
        // 'response_data1 '=>   $response_data1, 
        'input '=>   $input, 
        // 'responsed '=>   $responsed,
        // 'studentForApi '=>   $studentForApi,
    ]);

}

public function sales_enrollee_table(){

    $sale = BaguioS1Sale::all();

    $sale2 = BaguioS2Sale::all();

    return view ('member.sales-enrollee')->with('sale',$sale)->with('sale2',$sale2);

}


public function fetch_student(){

    $program = Input::get('program');

    if ($program == 'lets' ){

    $student = BaguioLet::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'nles' ){
        $student = BaguioNle::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'crims' ){
        $student = BaguioCrim::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'civils' ){
        $student = BaguioCivil::orderBy('last_name')->get();
    return response()->json($student);
    }

     if ($program == 'psycs' ){
        $student = BaguioPsyc::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'nclexes' ){
        $student = BaguioNclex::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'ielts' ){
        $student = BaguioIelt::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'socials' ){
        $student = BaguioSocial::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'agris' ){
        $student = BaguioAgri::orderBy('last_name')->get();
        return response()->json($student);
    }
    if ($program == 'mids' ){
        $student = BaguioMid::orderBy('last_name')->get();
        return response()->json($student);
    }

    if ($program == 'onlines' ){
        $student = BaguioOnline::orderBy('last_name')->get();
        return response()->json($student);
    }       
}

public function fetch_tuition(){

    $category = Input::get('category');
    $program = Input::get('program');


    if ($program == 'lets' ){

    $tuition = BaguioTuition::where('program','=', 'LET')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'nles' ){
        $tuition = BaguioTuition::where('program','=', 'NLE')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'crims' ){
        $tuition = BaguioTuition::where('program','=', 'Criminology')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'civils' ){
        $tuition = BaguioTuition::where('program','=', 'Civil Service')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

     if ($program == 'psycs' ){
        $tuition = BaguioTuition::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'nclexes' ){
        $tuition = BaguioTuition::where('program','=', 'NCLEX')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'ielts' ){
        $tuition = BaguioTuition::where('program','=', 'IELTS')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'socials' ){
        $tuition = BaguioTuition::where('program','=', 'Social Work')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'agris' ){
        $tuition = BaguioTuition::where('program','=', 'Agriculture')->where('category','=', $category)->get();
        return response()->json($tuition);
    }

    if ($program == 'mids' ){
        $tuition = BaguioTuition::where('program','=', 'Midwifery')->where('category','=', $category)->get();
        return response()->json($tuition);
    }  

    if ($program == 'onlines' ){
        $tuition = BaguioTuition::where('program','=', 'Online Only')->where('category','=', $category)->get();
        return response()->json($tuition);
    }          
}

public function fetch_discount(){

    $program = Input::get('program');
    $category = Input::get('category');


    if ($program == 'lets' ){

    $discount = BaguioDiscount::where('program','=', 'LET')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'nles' ){
        $discount = BaguioDiscount::where('program','=', 'NLE')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'crims' ){
        $discount = BaguioDiscount::where('program','=', 'Criminology')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'civils' ){
        $discount = BaguioDiscount::where('program','=', 'Civil Service')->where('category','=', $category)->get();
    return response()->json($discount);
    }

     if ($program == 'psycs' ){
        $discount = BaguioDiscount::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'nclexes' ){
        $discount = BaguioDiscount::where('program','=', 'NCLEX')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'ielts' ){
        $discount = BaguioDiscount::where('program','=', 'IELTS')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'socials' ){
        $discount = BaguioDiscount::where('program','=', 'Social Work')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'agris' ){
        $discount = BaguioDiscount::where('program','=', 'Agriculture')->where('category','=', $category)->get();
        return response()->json($discount);
    }  

    if ($program == 'mids' ){
        $discount = BaguioDiscount::where('program','=', 'Midwifery')->where('category','=', $category)->get();
        return response()->json($discount);
    } 

    if ($program == 'onlines' ){
        $discount = BaguioDiscount::where('program','=', 'Online Only')->where('category','=', $category)->get();
        return response()->json($discount);
    }       


}
public function add_new_payment(Request $request){

        $input = $request->except(['_token']);
        $season = $input['season'];
        $total_amount = $input['total_amount'];
        $amount_paid = $input['amount_paid'];
        $student = explode('*',$input['name']);
        $balance = $total_amount - $amount_paid;
        $firstPayment =0;        
        
        
        //-----------------for scorecard payment starts here 
        $curr_date = date('M-d-Y');
        
        $program = $this->sbranch.'_'.$input['program'];
        
        $check = DB::table($program)->where('id','=',$student[1])->where('status','=','Enrolled')->first();
        $statusforapi = DB::table($program)->where('id','=',$student[1])->value('status');
        $dis_category = explode(',',$input['discount']);
        $scoreSeason = 0;

        //check what season
        if($season == "Season 1"){
            $scoreSeason = '1';
        }
        if($season == "Season 2"){
            $scoreSeason = '2';
        }//end check what season

        //check if retaker or first timer
        
        $remark = '1stTimer';
        
        if(count($dis_category) == 2){
            if (strpos(strtolower($dis_category[1]), 'bounce') == false || strpos(strtolower($dis_category[1]), 'retaker') == false|| strpos(strtolower($dis_category[1]), 'retake') == false) {
                $remark = 'retake';
            }//end of check if retaker or first timer
        }
        
        //reffer to views/js/payment.blade.php for "student[5]" 
        //skip this if already made any payment   
        if($check == null || $input['tuition_fee'] == null){
        $firstPayment = 1;
        $check_date = BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$curr_date)->first();
           //if let
        if($input['program'] == "lets")
            {
                if($check_date == null)
                {         
                BaguioScoreCards::create([
                    'year' => date('Y',strtotime($curr_date)),
                    'date' => $curr_date,
                    'season' => $scoreSeason,
                    str_replace(' ', '', strtolower($student[5])) => 1,
                ]);
                }
                else// do this if there is existing payment of the current date
                {
                    $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                    $score = $prev[ str_replace(' ', '', strtolower($student[5]))] + 1;
                    
                        BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                            str_replace(' ', '', strtolower($student[5]))  => $score,
                    ]);
                }     
            }//end of if lets


            //if nles
            elseif($input['program'] == "nles")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'nles_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['nles_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'nles_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'nles_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['nles_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'nles_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if nles

            //if crims
            elseif($input['program'] == "crims")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                        'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'crims_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['crims_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'crims_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)), 
                                'date' => $curr_date,
                                'crims_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['crims_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'crims_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if civils

            elseif($input['program'] == "civils")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'civils_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['civils_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'civils_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => date('Y-m-d'),
                                'civils_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['civils_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'civils_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if civils 

            elseif($input['program'] == "psycs")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'psycs_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['psycs_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'psycs_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'psycs_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['psycs_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'psycs_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if psycs

            elseif($input['program'] == "ielts")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'ielts_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['ielts_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'ielts_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'ielts_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['ielts_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'ielts_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if ielts

            elseif($input['program'] == "socials")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'socials_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['socials_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'socials_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'socials_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['socials_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'socials_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if socials

            elseif($input['program'] == "agris")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'agris_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['agris_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'agris_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'agris_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['agris_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'agris_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if agris

           
            
            elseif($input['program'] == "mids")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'mids_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['mids_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'mids_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'mids_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['mids_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'mids_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if mids

            elseif($input['program'] == "onlines")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    BaguioScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'onlines_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['onlines_retakers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'onlines_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         BaguioScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'onlines_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['onlines_1stTimers'] + 1;
                            BaguioScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'onlines_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if onlines
            
            else
            {

            }



//--add facilitation amount to facilitation table
facilitation::create([
'year' => date('Y',strtotime($curr_date)),
'branch' => "Baguio",
'season' => $season,
'facilitation' => $input['facilitation'],
]);
//--end of add facilitation amount to facilitation table


} //end of skip this if already made any payment  

//-----------end of score card update in payment
//-----------end of score card update in payment
//-----------end of score card update in payment




        $reserve = $input['reserve'];

        if($balance < 0 ){
            $balance = 0;
        }
        $discount = explode(',',$input['discount']);
        $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psychometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }
        $discount = explode(',',$input['discount']);

        if($discount[0] == 0) {
            $discount_amount = null;
            $discount_category = null;
        } 
        else {
            
            $discount_amount = $discount[0];
            $discount_category = $discount[1];
        }

        if($input['tuition_fee'] != null){
        if ($season == 'Season 1'){
        DB::table($this->sbranch.'_s1_sales')->insert([
            'date' => $input['date'],
            'student_id' => $student[6],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'season'  => $season,
            'year'  => $input['year'],
            'created_at'    => date('Y-m-d'),
        ]);
        $ini = BaguioS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        $program = $this->sbranch.'_'.$input['program'];

        DB::table($program)->where('id','=',$student[1])->update([

            'category' => $input['category'],
            'status'   => 'Enrolled',
            'facilitation' => $input['facilitation'],
            'year'  => $input['year'],
            'season' => $season
        ]);
        }
        if ($season == 'Season 2'){
        DB::table($this->sbranch.'_s2_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'season' => $season,
            'year'  => $input['year'],
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = BaguioS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        $program = $this->sbranch.'_'.$input['program'];

        DB::table($program)->where('id','=',$student[1])->update([

            'category' => $input['category'],
            'status'   => 'Enrolled',
            'facilitation' => $input['facilitation'],
            'year'  => $input['year'],
            'season' => "Season 1"
        ]);
        }

        Alert::success('Success!', 'Payment has been submitted.');
    }
        if($input['tuition_fee'] == null){
            
            //get year
            $ryear = DB::table($program)->where('id','=',$student[1])->value('year');
            
            if ($input['rseason'] == 'Season 1'){
                
            

        DB::table($this->sbranch.'_s1_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $ryear,
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = BaguioS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        }
        if ($input['rseason'] == 'Season 2'){
        DB::table($this->sbranch.'_s2_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $ryear,
            'season'  => "Season 2",
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = BaguioS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);
        }
        Alert::success('Success!', 'Payment has been submitted.');
        }

       

        if($balance > 0){

            if($input['balance'] == null){
                BaguioReceivable::insert([

                    'enrollee_id' => $student[1],
                    'name'        => $student[0],
                    'program'     => $prog,
                    'contact_no'  => $student[2],
                    'season'      => $season,
                    'balance'     => $balance,
                ]);
            }
            }
            if($input['balance'] != null && $input['tuition_fee'] == null){

                if($amount_paid >= $total_amount){
                    BaguioReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                    }
                if($amount_paid < $total_amount){
                $remaining = $input['balance'];
                $present_balance = $remaining - $amount_paid;
                BaguioReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                    'balance' => $present_balance,
                ]);
                }
            }
            if($input['balance'] != null && $input['tuition_fee'] != null){

                if($amount_paid >= $total_amount){
                    BaguioReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                    }
                else{
                BaguioReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                    'balance' => $balance,
                ]);
                }
            }

           if($input['reserve'] != null){

            BaguioReservation::where('enrollee_id','=',$student[1])->where('program','=',$input['program'])->delete();
           }




           
           if($statusforapi != "Enrolled"){
           
        if($firstPayment == 1){
           //get last inserted payment
           //check what season
        if($season == "Season 1"){
            $lastRec = DB::table($this->sbranch.'_s1_sales')->where('student_id',$student[1])->orderby('id','desc')->first();
    
        }
        if($season == "Season 2"){
            $lastRec = DB::table($this->sbranch.'_s2_sales')->where('student_id',$student[1])->orderby('id','desc')->first();
          
        }//end check what season
           



    //login to api
    $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
    $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
        'form_params' => [
            "email"=>"admin@main.cbrc.solutions",
            "password"=>"main@dmin"
        ]
    ]);
//insert data to api

    if ($res->getStatusCode() == 200) { // 200 OK
        $response_data = json_decode($res->getBody()->getContents());
     //save first payment to api   
        $sendPayment = $client->request('POST', 'https://cbrc.solutions/api/main/payment?token='.$response_data->access_token
        ,[
        'form_params' => [
                    "Branch" => ucwords($this->sbranch),
                    "Season" => $season,
                    "Date" => $lastRec->date,
                    "Name" => $lastRec->name,
                    "Stdid" => $lastRec->student_id,
                    "Program" => $input['program'],
                    "Category" => $lastRec->category,
                    "Discount_category" => $lastRec->discount_category,
                    "Tuition_fee" => $lastRec->tuition_fee,
                    "Facilitation_fee" => $lastRec->facilitation_fee,
                    "year" => $lastRec->year
                    ]
        ]);
        
            //save student info to api
            $studentForApi =  DB::table($program)->where('id','=',$student[1])->where('status','=','Enrolled')->first();
            $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
            ,[
            'form_params' => [
                "BranchStdID" => $studentForApi->id,
                "Branch_Name" =>lcfirst($this->sbranch),
                "cbrc_id" =>$studentForApi->cbrc_id,
                "Lastname" =>$studentForApi->last_name,
                "Firstname" =>$studentForApi->first_name,
                "Middlename" =>$studentForApi->middle_name,
                "Birthday" =>$studentForApi->birthdate,
                "Contact_Number" =>$studentForApi->contact_no,
                "Address" =>$studentForApi->address,
                "Email" =>$studentForApi->email,
                "Username" =>$studentForApi->username,
                "Password" =>$studentForApi->password,
                "School" =>$studentForApi->school,
                "Program" =>$studentForApi->program,
                "Section" =>$studentForApi->section,
                "Major" =>$studentForApi->major,
                "Take" =>$studentForApi->take,
                "Noa" =>$studentForApi->noa_no,
                "Category" =>$studentForApi->category,
                "Status" =>$studentForApi->status,
                "Contact_Person" =>$studentForApi->contact_person,
                "Contact_Details" =>$studentForApi->contact_details,
                "Facilitation" =>$studentForApi->facilitation,
                "Season" =>$studentForApi->season,
                "Year" =>$studentForApi->year
        
            ]
            ]);
    }//end of 200 ok
}//end of firstPayment save to api    

}//end of check if enrolled

           return redirect('/baguio-enrollment/new-payment');
    }

    
public function new_reservation(){
    $branch= $this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.new-reservation')->with('branch',$branch)->with('date',$date)->with('program',$program);
}


public function fetch_balance(){

    $id = Input::get('id');
    $program = Input::get('program');

    if ($program == 'lets' ){
        $program = 'LET';
    }

    if ($program == 'nles' ){
        $program = 'NLE';
    }

    if ($program == 'crims' ){
       $program = 'Criminology';
    }

    if ($program == 'civils' ){
        $program = 'Civil Service';
    }

     if ($program == 'psycs' ){
        $program = 'Psychometrician';
    }

    if ($program == 'nclexes' ){
        $program = 'NCLEX';
    }

    if ($program == 'ielts' ){
        $program = 'IELTS';
    }

    if ($program == 'socials' ){
        $program = 'Social Work';
    }

    if ($program == 'agris' ){
        $program = 'Agriculture';
    } 

    if ($program == 'mids' ){
        $program = 'Midwifery';
    } 

    if ($program == 'onlines' ){
        $program = 'Online Only';
    }

    

    $balance = BaguioReceivable::where('enrollee_id','=', $id)->where('program','=',$program)->get();
    return response()->json($balance);
}

public function fetch_id(){

    $id = Input::get('id');
    $program = Input::get('program');

    if ($program == 'lets' ){

        $student = BaguioLet::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = BaguioNle::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = BaguioCrim::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = BaguioCivil::where('id','=',$id)->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = BaguioPsyc::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = BaguioNclex::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = BaguioIelt::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = BaguioSocial::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = BaguioAgri::where('id','=',$id)->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = BaguioMid::where('id','=',$id)->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = BaguioOnline::where('id','=',$id)->get();
            return response()->json($student);
        }  
}

public function fetch_reserved(){

    $id = Input::get('id');
    $program = Input::get('program');
    

    $fee = BaguioReservation::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        
        return response()->json($fee);

}

public function let_table(){
    $prog = 'LET';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = BaguioLet::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nle_table(){
    $prog = 'NLE';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = BaguioNle::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function crim_table(){
    $prog = 'Criminology';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = BaguioCrim::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function civil_table(){
    $prog = 'Civil Service';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = BaguioCivil::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function psyc_table(){
    $prog = 'Psychometrician';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = BaguioPsyc::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nclex_table(){
    $prog = 'NCLEX';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = BaguioNclex::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function ielts_table(){
    $prog = 'IELTS';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = BaguioIelt::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function social_table(){
    $prog = 'Social Work';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = BaguioSocial::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function agri_table(){
    $prog = 'Agriculture';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = BaguioAgri::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function mid_table(){
    $prog = 'Midwifery';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = BaguioMid::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}

public function online_table(){
    $prog = 'Online Only';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = BaguioOnline::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}

public function insert_reservation(Request $request){

    $input = $request->except(['_token']);
    $details = explode('*',$input['name']);

    $name = $details[0];
    $school = $details[1];
    $email = $details[2];
    $contact_no = $details[3];
    $id = $details[4];

    $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psychometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }

    $existent = BaguioReservation::where('enrollee_id','=',$id)->first();

        if($existent != null){
           
        $old = BaguioReservation::where('enrollee_id','=',$id)->value('reservation_fee');
        
        $new = $old + $input['amount_paid'];

        BaguioReservation::where('enrollee_id','=',$id)->update([
            'reservation_fee' => $new,
        ]);

         $season = $input['season'];
        if ($season == 'Season 1'){

        BaguioS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = BaguioS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        BaguioS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = BaguioS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', '1 student has been updated.');
    return redirect ('baguio-enrollment/new-reservation');
        }

    if($existent == null)
    {

    BaguioReservation::create([
        'enrollee_id'    =>     $id,
        'name'           =>     $name,
        'branch'         =>     $this->branch,
        'program'        =>     $input['program'],
        'prog'           =>     $prog,
        'school'         =>     $school,
        'email'          =>     $email,
        'contact_no'     =>     $contact_no,
        'reservation_fee'=>     $input['amount_paid'],
    ]);

    $season = $input['season'];
    if ($season == 'Season 1'){

        BaguioS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = BaguioS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        BaguioS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = BaguioS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        BaguioS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', 'New student has been reserved.');
    return redirect ('baguio-enrollment/new-reservation');
}
}

public function reservation_table(){

    $reserve = BaguioReservation::all();

    return view ('member.reservation')->with('reserve',$reserve);
}

public function enrolled_table(){

    $let = BaguioLet::where('status','=','Enrolled')->get();
    $nle = BaguioNle::where('status','=','Enrolled')->get();
    $crim = BaguioCrim::where('status','=','Enrolled')->get();
    $civil = BaguioCivil::where('status','=','Enrolled')->get();
    $psyc = BaguioPsyc::where('status','=','Enrolled')->get();
    $nclex = BaguioNclex::where('status','=','Enrolled')->get();
    $ielt = BaguioIelt::where('status','=','Enrolled')->get();
    $social = BaguioSocial::where('status','=','Enrolled')->get();
    $agri = BaguioAgri::where('status','=','Enrolled')->get();
    $mid = BaguioMid::where('category','=','Enrolled')->get();
    $online = BaguioOnline::where('category','=','Enrolled')->get();

    $sale1 = BaguioS1Sale::all();

    $sale2 = BaguioS2Sale::all();
    $tot_amount_paid=0;
    $total_balance=0;
    return view ('member.enrolled')
                ->with('let',$let)
                ->with('nle',$nle)
                ->with('crim',$crim)
                ->with('civil',$civil)
                ->with('psyc',$psyc)
                ->with('nclex',$nclex)
                ->with('ielt',$ielt)
                ->with('social',$social)
                ->with('agri',$agri)
                ->with('mid',$mid)
                ->with('online',$online)
                ->with('tot_amount_paid',$tot_amount_paid)
                ->with('total_balance',$total_balance)
                ->with('sale1',$sale1)
                ->with('sale2',$sale2);

}


function populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = BaguioScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = BaguioScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = BaguioScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 BaguioScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card



}
