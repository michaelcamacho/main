<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Calapan\CalapanAgri;
use App\Model\Calapan\CalapanBookCash;
use App\Model\Calapan\CalapanBooksInventorie;
use App\Model\Calapan\CalapanBooksSale;
use App\Model\Calapan\CalapanBudget;
use App\Model\Calapan\CalapanCivil;
use App\Model\Calapan\CalapanCrim;
use App\Model\Calapan\CalapanDiscount;
use App\Model\Calapan\CalapanDropped;
use App\Model\Calapan\CalapanExpense;
use App\Model\Calapan\CalapanIelt;
use App\Model\Calapan\CalapanLet;
use App\Model\Calapan\CalapanMid;
use App\Model\Calapan\CalapanNclex;
use App\Model\Calapan\CalapanNle;
use App\Model\Calapan\CalapanOnline;
use App\Model\Calapan\CalapanPsyc;
use App\Model\Calapan\CalapanReceivable;
use App\Model\Calapan\CalapanS1Sale;
use App\Model\Calapan\CalapanS2Sale;
use App\Model\Calapan\CalapanS1Cash;
use App\Model\Calapan\CalapanS2Cash;
use App\Model\Calapan\CalapanScholar;
use App\Model\Calapan\CalapanSocial;
use App\Model\Calapan\CalapanTuition;
use App\Model\Calapan\CalapanPettyCash;
use App\Model\Calapan\CalapanRemit;
use App\Model\Calapan\CalapanReservation;
use App\Model\Calapan\CalapanEmployee;
use App\Model\Calapan\CalapanScoreCards;
use App\Model\Calapan\CalapanBookTransfer;

use App\Model\Calapan\CalapanLecturerAEvaluation;
use App\Model\Calapan\CalapanLecturerBEvaluation;
use App\Model\Calapan\CalapanComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class CalapanCashierController extends Controller
{
    private $branch = "Calapan";

    private $sbranch = "calapan";

public function __construct()
    {

         $this->middleware('role:calapan_cashier');
        
    }


public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}
           
    
public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = CalapanBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = CalapanBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = CalapanBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             CalapanBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = CalapanBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        CalapanBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = CalapanBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        CalapanBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ('calapan-cashier/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ('calapan-cashier/book-payment');
        }
}
public function books_table(){

$book = CalapanBooksInventorie::all();
$sale = CalapanBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}

    }