<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Branch;
use App\Model\Calapan\CalapanAgri;
use App\Model\Calapan\CalapanBookCash;
use App\Model\Calapan\CalapanBooksInventorie;
use App\Model\Calapan\CalapanBooksSale;
use App\Model\Calapan\CalapanBudget;
use App\Model\Calapan\CalapanCivil;
use App\Model\Calapan\CalapanCrim;
use App\Model\Calapan\CalapanDiscount;
use App\Model\Calapan\CalapanDropped;
use App\Model\Calapan\CalapanExpense;
use App\Model\Calapan\CalapanIelt;
use App\Model\Calapan\CalapanLet;
use App\Model\Calapan\CalapanMid;
use App\Model\Calapan\CalapanNclex;
use App\Model\Calapan\CalapanNle;
use App\Model\Calapan\CalapanOnline;
use App\Model\Calapan\CalapanPsyc;
use App\Model\Calapan\CalapanReceivable;
use App\Model\Calapan\CalapanS1Sale;
use App\Model\Calapan\CalapanS2Sale;
use App\Model\Calapan\CalapanS1Cash;
use App\Model\Calapan\CalapanS2Cash;
use App\Model\Calapan\CalapanScholar;
use App\Model\Calapan\CalapanSocial;
use App\Model\Calapan\CalapanTuition;
use App\Model\Calapan\CalapanPettyCash;
use App\Model\Calapan\CalapanRemit;
use App\Model\Calapan\CalapanReservation;
use App\Model\Calapan\CalapanEmployee;
use App\Model\Calapan\CalapanScoreCards;
use App\Model\Calapan\CalapanBookTransfer;

use App\Model\Calapan\CalapanLecturerAEvaluation;
use App\Model\Calapan\CalapanLecturerBEvaluation;
use App\Model\Calapan\CalapanComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Auth;
use User;
use DB;
use File;


class CalapanEnrollmentController extends Controller
{

    private $branch = "Calapan";

    private $sbranch = "calapan";

public function __construct()
    {

         $this->middleware('role:calapan_enrollment');
        
    }
    public function new_payment(){
        $this->populateScoreCard();
        $branch= $this->branch; 
        $date = date('M-d-Y');
        $program = Program::all();
        return view('member.new-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }


// Register New Enrollee

public function add_enrollee(){

    $branch= $this->branch; 

    $program = Program::all();

    return view('member.add-enrollee')->with('branch',$branch)->with('program',$program);
}



public function insert_enrollee(Request $request){

    $input = $request->except(['_token']);
    $branch = $this->branch;
    $course = $input['program'];

    if($course == 'lets'){
        $course = 'LET';
    }
    if($course == 'nles'){
        $course = 'NLE';
    }
    if($course == 'crims'){
        $course = 'Criminology';
    }
    if($course == 'civils'){
        $course = 'Civil Service';
    }

    if($course == 'psycs'){
        $course = 'Psychometrician';
    }
    if($course == 'nclexes'){
        $course = 'NCLEX';
    }
    if($course == 'ielts'){
        $course = 'IELTS';
    }
    if($course == 'socials'){
        $course = 'Social Work';
    }
    if($course == 'agris'){
        $course = 'Agriculture';
    }
    if($course == 'mids'){
        $course = 'Midwifery';
    }
    if($course == 'onlines'){
        $course = 'Online Only';
    }

    $program = $this->sbranch.'_'.$input['program'];

    $lastname = strtoupper($input['last_name']);
    $firstname = strtoupper($input['first_name']);
    $middlename = strtoupper($input['middle_name']);

    $existent = DB::table($program)->where('last_name','=',$lastname)->where('first_name','=',$firstname)->where('middle_name','=',$middlename)->first();

    if($existent != null){
        Alert::error('Failed!', 'This name is already registered.');
        return redirect ('calapan-enrollment/add-enrollee');
    }

    if($existent == null){
    DB::table($program)->insert([

        'cbrc_id'       => $input['cbrc_id'],
        'section'       => $input['section'],
        'last_name'     => strtoupper($input['last_name']),
        'first_name'    => strtoupper($input['first_name']),
        'middle_name'   => strtoupper($input['middle_name']),
        'username'      => $input['username'],
        'password'      => $input['password'],
        'course'        => $course,
        'major'         => $input['major'],
        'program'       => $input['program'],
        'school'        => $input['school'],
        'noa_no'        => $input['noa_no'],
        'take'          => $input['take'],
        'branch'        => $branch,
        'birthdate'     => $input['birthdate'],
        'contact_no'    => $input['contact_no'],
        'email'         => $input['email'],
        'address'       => $input['address'],
        'contact_person'=> $input['contact_person'],
        'contact_details'=> $input['contact_details'],
        'registration'  => 'Walk-in',
        'created_at'    => date('Y-m-d'),
    ]);
    Alert::success('Success!', 'New student has been registered.');
    return redirect ('calapan-enrollment/add-enrollee');

}
}

public function update_enrollee(Request $request){
$input = $request->except(['_token']);
    $branch = $this->branch;
    $program = $input['program'];
    $id = $input['id'];

    if($program == 'LET'){
        $program = 'lets';
    }
    if($program == 'NLE'){
        $program = 'nles';
    }
    if($program == 'Criminology'){
        $program = 'crims';
    }
    if($program == 'Civil Service'){
        $program = 'civils';
    }

    if($program == 'Psychometrician'){
        $program = 'psycs';
    }
    if($program == 'NCLEX'){
        $program = 'nclexes';
    }
    if($program == 'IELTS'){
        $program = 'ielts';
    }
    if($program == 'Social Work'){
        $program = 'socials';
    }
    if($program == 'Agriculture'){
        $program = 'agris';
    }
    if($program == 'Midwifery'){
        $program = 'mids';
    }
    if($program == 'Online Only'){
        $program = 'onlines';
    }

    $program = $this->sbranch.'_'.$program;

    DB::table($program)->where('id','=',$id)->update([

        'cbrc_id'       => $input['cbrc_id'],
        'last_name'     => $input['last_name'],
        'first_name'    => $input['first_name'],
        'middle_name'   => $input['middle_name'],
        'username'      => $input['username'],
        'password'      => $input['password'],
        'major'         => $input['major'],
        'school'        => $input['school'],
        'noa_no'        => $input['noa_no'],
        'take'          => $input['take'],
        'birthdate'     => $input['birthdate'],
        'contact_no'    => $input['contact_no'],
        'email'         => $input['email'],
        'address'       => $input['address'],
        'contact_person'=> $input['contact_person'],
        'contact_details'=> $input['contact_details'],
        'section'        => $input['section'],
    ]);
   
   //login to api
   $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
   $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
       'form_params' => [
           "email"=>"admin@main.cbrc.solutions",
           "password"=>"main@dmin"
       ]
   ]);
//insert data to api

   if ($res->getStatusCode() == 200) { // 200 OK
       $response_data = json_decode($res->getBody()->getContents());
    
       
           //save student info to api
           $studentForApi =  DB::table($program)->where('id','=' , $id)->where('status','=','Enrolled')->first();
           $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
           ,[
           'form_params' => [
               "_method" => "PUT",
               "BranchStdID" => $studentForApi->id,
               "Branch_Name" =>lcfirst($this->sbranch),
               "cbrc_id" =>     $input['cbrc_id'],
               "Lastname" => $input['last_name'],
               "Firstname" =>$input['first_name'],
               "Middlename" =>$input['middle_name'],
               "Birthday" =>$input['birthdate'],
               "Contact_Number" =>$input['contact_no'],
               "Address" =>$input['address'],
               "Email" =>$input['email'],
               "Username" =>$input['username'],
               "Password" =>$input['password'],
               "School" => $input['school'],
               "Program" =>$studentForApi->program,
               "Section" =>$input['section'],
               "Major" =>$input['major'],
               "Take" =>$input['take'],
               "Noa" =>$input['noa_no'],
               "Category" =>$studentForApi->category,
               "Status" =>$studentForApi->status,
               "Contact_Person" =>$input['contact_person'],
               "Contact_Details" =>$input['contact_details'],
               "Facilitation" =>$studentForApi->facilitation,
               "Season" =>$studentForApi->season,
               "Year" =>$studentForApi->year
       
           ]
           ]);
           // return dd($studentForApi);


   }//end of 200 ok

    return response()->json([
        'success' => true,
        'message' => 'Enrollee has been updated',
        // 'response_data1 '=>   $response_data1, 
        'input '=>   $input, 
        // 'responsed '=>   $responsed,
        // 'studentForApi '=>   $studentForApi,
    ]);

}

public function sales_enrollee_table(){

    $sale = CalapanS1Sale::all();

    $sale2 = CalapanS2Sale::all();

    return view ('member.sales-enrollee')->with('sale',$sale)->with('sale2',$sale2);

}


public function fetch_student(){

    $program = Input::get('program');

    if ($program == 'lets' ){

    $student = CalapanLet::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'nles' ){
        $student = CalapanNle::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'crims' ){
        $student = CalapanCrim::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'civils' ){
        $student = CalapanCivil::orderBy('last_name')->get();
    return response()->json($student);
    }

     if ($program == 'psycs' ){
        $student = CalapanPsyc::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'nclexes' ){
        $student = CalapanNclex::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'ielts' ){
        $student = CalapanIelt::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'socials' ){
        $student = CalapanSocial::orderBy('last_name')->get();
    return response()->json($student);
    }

    if ($program == 'agris' ){
        $student = CalapanAgri::orderBy('last_name')->get();
        return response()->json($student);
    }
    if ($program == 'mids' ){
        $student = CalapanMid::orderBy('last_name')->get();
        return response()->json($student);
    }

    if ($program == 'onlines' ){
        $student = CalapanOnline::orderBy('last_name')->get();
        return response()->json($student);
    }       
}

public function fetch_tuition(){

    $category = Input::get('category');
    $program = Input::get('program');


    if ($program == 'lets' ){

    $tuition = CalapanTuition::where('program','=', 'LET')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'nles' ){
        $tuition = CalapanTuition::where('program','=', 'NLE')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'crims' ){
        $tuition = CalapanTuition::where('program','=', 'Criminology')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'civils' ){
        $tuition = CalapanTuition::where('program','=', 'Civil Service')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

     if ($program == 'psycs' ){
        $tuition = CalapanTuition::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'nclexes' ){
        $tuition = CalapanTuition::where('program','=', 'NCLEX')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'ielts' ){
        $tuition = CalapanTuition::where('program','=', 'IELTS')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'socials' ){
        $tuition = CalapanTuition::where('program','=', 'Social Work')->where('category','=', $category)->get();
    return response()->json($tuition);
    }

    if ($program == 'agris' ){
        $tuition = CalapanTuition::where('program','=', 'Agriculture')->where('category','=', $category)->get();
        return response()->json($tuition);
    }

    if ($program == 'mids' ){
        $tuition = CalapanTuition::where('program','=', 'Midwifery')->where('category','=', $category)->get();
        return response()->json($tuition);
    }  

    if ($program == 'onlines' ){
        $tuition = CalapanTuition::where('program','=', 'Online Only')->where('category','=', $category)->get();
        return response()->json($tuition);
    }          
}

public function fetch_discount(){

    $program = Input::get('program');
    $category = Input::get('category');


    if ($program == 'lets' ){

    $discount = CalapanDiscount::where('program','=', 'LET')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'nles' ){
        $discount = CalapanDiscount::where('program','=', 'NLE')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'crims' ){
        $discount = CalapanDiscount::where('program','=', 'Criminology')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'civils' ){
        $discount = CalapanDiscount::where('program','=', 'Civil Service')->where('category','=', $category)->get();
    return response()->json($discount);
    }

     if ($program == 'psycs' ){
        $discount = CalapanDiscount::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'nclexes' ){
        $discount = CalapanDiscount::where('program','=', 'NCLEX')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'ielts' ){
        $discount = CalapanDiscount::where('program','=', 'IELTS')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'socials' ){
        $discount = CalapanDiscount::where('program','=', 'Social Work')->where('category','=', $category)->get();
    return response()->json($discount);
    }

    if ($program == 'agris' ){
        $discount = CalapanDiscount::where('program','=', 'Agriculture')->where('category','=', $category)->get();
        return response()->json($discount);
    }  

    if ($program == 'mids' ){
        $discount = CalapanDiscount::where('program','=', 'Midwifery')->where('category','=', $category)->get();
        return response()->json($discount);
    } 

    if ($program == 'onlines' ){
        $discount = CalapanDiscount::where('program','=', 'Online Only')->where('category','=', $category)->get();
        return response()->json($discount);
    }       


}
public function add_new_payment(Request $request){

        $input = $request->except(['_token']);
        $season = $input['season'];
        $total_amount = $input['total_amount'];
        $amount_paid = $input['amount_paid'];
        $student = explode('*',$input['name']);
        $balance = $total_amount - $amount_paid;
        $firstPayment =0;        
        
        
        //-----------------for scorecard payment starts here 
        $curr_date = date('M-d-Y');
        
        $program = $this->sbranch.'_'.$input['program'];
        
        $check = DB::table($program)->where('id','=',$student[1])->where('status','=','Enrolled')->first();
        $statusforapi = DB::table($program)->where('id','=',$student[1])->value('status');
        $dis_category = explode(',',$input['discount']);
        $scoreSeason = 0;

        //check what season
        if($season == "Season 1"){
            $scoreSeason = '1';
        }
        if($season == "Season 2"){
            $scoreSeason = '2';
        }//end check what season

        //check if retaker or first timer
        
        $remark = '1stTimer';
        
        if(count($dis_category) == 2){
            if (strpos(strtolower($dis_category[1]), 'bounce') == false || strpos(strtolower($dis_category[1]), 'retaker') == false|| strpos(strtolower($dis_category[1]), 'retake') == false) {
                $remark = 'retake';
            }//end of check if retaker or first timer
        }
        
        //reffer to views/js/payment.blade.php for "student[5]" 
        //skip this if already made any payment   
        if($check == null || $input['tuition_fee'] == null){
        $firstPayment = 1;
        $check_date = CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$curr_date)->first();
           //if let
        if($input['program'] == "lets")
            {
                if($check_date == null)
                {         
                CalapanScoreCards::create([
                    'year' => date('Y',strtotime($curr_date)),
                    'date' => $curr_date,
                    'season' => $scoreSeason,
                    str_replace(' ', '', strtolower($student[5])) => 1,
                ]);
                }
                else// do this if there is existing payment of the current date
                {
                    $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                    $score = $prev[ str_replace(' ', '', strtolower($student[5]))] + 1;
                    
                        CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                            str_replace(' ', '', strtolower($student[5]))  => $score,
                    ]);
                }     
            }//end of if lets


            //if nles
            elseif($input['program'] == "nles")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'nles_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['nles_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'nles_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'nles_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['nles_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'nles_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if nles

            //if crims
            elseif($input['program'] == "crims")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                        'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'crims_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['crims_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'crims_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)), 
                                'date' => $curr_date,
                                'crims_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['crims_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'crims_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if civils

            elseif($input['program'] == "civils")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'civils_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['civils_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'civils_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => date('Y-m-d'),
                                'civils_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['civils_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'civils_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if civils 

            elseif($input['program'] == "psycs")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'psycs_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['psycs_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'psycs_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'psycs_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['psycs_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'psycs_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if psycs

            elseif($input['program'] == "ielts")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'ielts_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['ielts_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'ielts_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'ielts_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['ielts_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'ielts_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if ielts

            elseif($input['program'] == "socials")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'socials_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['socials_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'socials_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'socials_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['socials_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'socials_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if socials

            elseif($input['program'] == "agris")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'agris_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['agris_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'agris_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'agris_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['agris_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'agris_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if agris

           
            
            elseif($input['program'] == "mids")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'mids_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['mids_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'mids_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'mids_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['mids_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'mids_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if mids

            elseif($input['program'] == "onlines")
            {
                if($remark == "retake")
                {
                    if($check_date == null)
                    {         
                    CalapanScoreCards::create([
                            'year' => date('Y',strtotime($curr_date)),
                            'date' => $curr_date,
                            'onlines_retakers' => 1,
                            'season' => $scoreSeason,
                    ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['onlines_retakers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'onlines_retakers' => $score,
                        ]);
                    }
                }
                else
                {
                    if($check_date == null)
                    {  
                         CalapanScoreCards::create([
                                'year' => date('Y',strtotime($curr_date)),
                                'date' => $curr_date,
                                'onlines_1stTimers' => 1,
                                'season' => $scoreSeason,
                        ]);
                    }
                    else// do this if there is existing payment of the current date
                    {
                        $prev =  CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->first();
                        $score = $prev['onlines_1stTimers'] + 1;
                            CalapanScoreCards::where('season','=',$scoreSeason)->where('date','=',$check_date['date'])->update([
                                'onlines_1stTimers'  => $score,
                        ]);
                    }
                }
            }//end of if onlines
            
            else
            {

            }



//--add facilitation amount to facilitation table
facilitation::create([
'year' => date('Y',strtotime($curr_date)),
'branch' => "Calapan",
'season' => $season,
'facilitation' => $input['facilitation'],
]);
//--end of add facilitation amount to facilitation table


} //end of skip this if already made any payment  

//-----------end of score card update in payment
//-----------end of score card update in payment
//-----------end of score card update in payment




        $reserve = $input['reserve'];

        if($balance < 0 ){
            $balance = 0;
        }
        $discount = explode(',',$input['discount']);
        $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psychometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }
        $discount = explode(',',$input['discount']);

        if($discount[0] == 0) {
            $discount_amount = null;
            $discount_category = null;
        } 
        else {
            
            $discount_amount = $discount[0];
            $discount_category = $discount[1];
        }

        if($input['tuition_fee'] != null){
        if ($season == 'Season 1'){
        DB::table($this->sbranch.'_s1_sales')->insert([
            'date' => $input['date'],
            'student_id' => $student[6],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'season'  => $season,
            'year'  => $input['year'],
            'created_at'    => date('Y-m-d'),
        ]);
        $ini = CalapanS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        $program = $this->sbranch.'_'.$input['program'];

        DB::table($program)->where('id','=',$student[1])->update([

            'category' => $input['category'],
            'status'   => 'Enrolled',
            'facilitation' => $input['facilitation'],
            'year'  => $input['year'],
            'season' => $season
        ]);
        }
        if ($season == 'Season 2'){
        DB::table($this->sbranch.'_s2_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'season' => $season,
            'year'  => $input['year'],
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = CalapanS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        $program = $this->sbranch.'_'.$input['program'];

        DB::table($program)->where('id','=',$student[1])->update([

            'category' => $input['category'],
            'status'   => 'Enrolled',
            'facilitation' => $input['facilitation'],
            'year'  => $input['year'],
            'season' => "Season 1"
        ]);
        }

        Alert::success('Success!', 'Payment has been submitted.');
    }
        if($input['tuition_fee'] == null){
            
            //get year
            $ryear = DB::table($program)->where('id','=',$student[1])->value('year');
            
            if ($input['rseason'] == 'Season 1'){
                
            

        DB::table($this->sbranch.'_s1_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $ryear,
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = CalapanS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        }
        if ($input['rseason'] == 'Season 2'){
        DB::table($this->sbranch.'_s2_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $ryear,
            'season'  => "Season 2",
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = CalapanS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);
        }
        Alert::success('Success!', 'Payment has been submitted.');
        }

       

        if($balance > 0){

            if($input['balance'] == null){
                CalapanReceivable::insert([

                    'enrollee_id' => $student[1],
                    'name'        => $student[0],
                    'program'     => $prog,
                    'contact_no'  => $student[2],
                    'season'      => $season,
                    'balance'     => $balance,
                ]);
            }
            }
            if($input['balance'] != null && $input['tuition_fee'] == null){

                if($amount_paid >= $total_amount){
                    CalapanReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                    }
                if($amount_paid < $total_amount){
                $remaining = $input['balance'];
                $present_balance = $remaining - $amount_paid;
                CalapanReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                    'balance' => $present_balance,
                ]);
                }
            }
            if($input['balance'] != null && $input['tuition_fee'] != null){

                if($amount_paid >= $total_amount){
                    CalapanReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                    }
                else{
                CalapanReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                    'balance' => $balance,
                ]);
                }
            }

           if($input['reserve'] != null){

            CalapanReservation::where('enrollee_id','=',$student[1])->where('program','=',$input['program'])->delete();
           }




           
           if($statusforapi != "Enrolled"){
           
        if($firstPayment == 1){
           //get last inserted payment
           //check what season
        if($season == "Season 1"){
            $lastRec = DB::table($this->sbranch.'_s1_sales')->where('student_id',$student[1])->orderby('id','desc')->first();
    
        }
        if($season == "Season 2"){
            $lastRec = DB::table($this->sbranch.'_s2_sales')->where('student_id',$student[1])->orderby('id','desc')->first();
          
        }//end check what season
           



    //login to api
    $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
    $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
        'form_params' => [
            "email"=>"admin@main.cbrc.solutions",
            "password"=>"main@dmin"
        ]
    ]);
//insert data to api

    if ($res->getStatusCode() == 200) { // 200 OK
        $response_data = json_decode($res->getBody()->getContents());
     //save first payment to api   
        $sendPayment = $client->request('POST', 'https://cbrc.solutions/api/main/payment?token='.$response_data->access_token
        ,[
        'form_params' => [
                    "Branch" => ucwords($this->sbranch),
                    "Season" => $season,
                    "Date" => $lastRec->date,
                    "Name" => $lastRec->name,
                    "Stdid" => $lastRec->student_id,
                    "Program" => $input['program'],
                    "Category" => $lastRec->category,
                    "Discount_category" => $lastRec->discount_category,
                    "Tuition_fee" => $lastRec->tuition_fee,
                    "Facilitation_fee" => $lastRec->facilitation_fee,
                    "year" => $lastRec->year
                    ]
        ]);
        
            //save student info to api
            $studentForApi =  DB::table($program)->where('id','=',$student[1])->where('status','=','Enrolled')->first();
            $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
            ,[
            'form_params' => [
                "BranchStdID" => $studentForApi->id,
                "Branch_Name" =>lcfirst($this->sbranch),
                "cbrc_id" =>$studentForApi->cbrc_id,
                "Lastname" =>$studentForApi->last_name,
                "Firstname" =>$studentForApi->first_name,
                "Middlename" =>$studentForApi->middle_name,
                "Birthday" =>$studentForApi->birthdate,
                "Contact_Number" =>$studentForApi->contact_no,
                "Address" =>$studentForApi->address,
                "Email" =>$studentForApi->email,
                "Username" =>$studentForApi->username,
                "Password" =>$studentForApi->password,
                "School" =>$studentForApi->school,
                "Program" =>$studentForApi->program,
                "Section" =>$studentForApi->section,
                "Major" =>$studentForApi->major,
                "Take" =>$studentForApi->take,
                "Noa" =>$studentForApi->noa_no,
                "Category" =>$studentForApi->category,
                "Status" =>$studentForApi->status,
                "Contact_Person" =>$studentForApi->contact_person,
                "Contact_Details" =>$studentForApi->contact_details,
                "Facilitation" =>$studentForApi->facilitation,
                "Season" =>$studentForApi->season,
                "Year" =>$studentForApi->year
        
            ]
            ]);
    }//end of 200 ok
}//end of firstPayment save to api    

}//end of check if enrolled

           return redirect('/calapan-enrollment/new-payment');
    }

    
public function new_reservation(){
    $branch= $this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.new-reservation')->with('branch',$branch)->with('date',$date)->with('program',$program);
}


public function fetch_balance(){

    $id = Input::get('id');
    $program = Input::get('program');

    if ($program == 'lets' ){
        $program = 'LET';
    }

    if ($program == 'nles' ){
        $program = 'NLE';
    }

    if ($program == 'crims' ){
       $program = 'Criminology';
    }

    if ($program == 'civils' ){
        $program = 'Civil Service';
    }

     if ($program == 'psycs' ){
        $program = 'Psychometrician';
    }

    if ($program == 'nclexes' ){
        $program = 'NCLEX';
    }

    if ($program == 'ielts' ){
        $program = 'IELTS';
    }

    if ($program == 'socials' ){
        $program = 'Social Work';
    }

    if ($program == 'agris' ){
        $program = 'Agriculture';
    } 

    if ($program == 'mids' ){
        $program = 'Midwifery';
    } 

    if ($program == 'onlines' ){
        $program = 'Online Only';
    }

    

    $balance = CalapanReceivable::where('enrollee_id','=', $id)->where('program','=',$program)->get();
    return response()->json($balance);
}

public function fetch_id(){

    $id = Input::get('id');
    $program = Input::get('program');

    if ($program == 'lets' ){

        $student = CalapanLet::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = CalapanNle::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = CalapanCrim::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = CalapanCivil::where('id','=',$id)->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = CalapanPsyc::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = CalapanNclex::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = CalapanIelt::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = CalapanSocial::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = CalapanAgri::where('id','=',$id)->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = CalapanMid::where('id','=',$id)->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = CalapanOnline::where('id','=',$id)->get();
            return response()->json($student);
        }  
}

public function fetch_reserved(){

    $id = Input::get('id');
    $program = Input::get('program');
    

    $fee = CalapanReservation::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        
        return response()->json($fee);

}

public function let_table(){
    $prog = 'LET';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = CalapanLet::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nle_table(){
    $prog = 'NLE';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = CalapanNle::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function crim_table(){
    $prog = 'Criminology';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = CalapanCrim::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function civil_table(){
    $prog = 'Civil Service';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = CalapanCivil::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function psyc_table(){
    $prog = 'Psychometrician';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = CalapanPsyc::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nclex_table(){
    $prog = 'NCLEX';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = CalapanNclex::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function ielts_table(){
    $prog = 'IELTS';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = CalapanIelt::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function social_table(){
    $prog = 'Social Work';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = CalapanSocial::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function agri_table(){
    $prog = 'Agriculture';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = CalapanAgri::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function mid_table(){
    $prog = 'Midwifery';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = CalapanMid::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}

public function online_table(){
    $prog = 'Online Only';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = CalapanOnline::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}

public function insert_reservation(Request $request){

    $input = $request->except(['_token']);
    $details = explode('*',$input['name']);

    $name = $details[0];
    $school = $details[1];
    $email = $details[2];
    $contact_no = $details[3];
    $id = $details[4];

    $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psychometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }

    $existent = CalapanReservation::where('enrollee_id','=',$id)->first();

        if($existent != null){
           
        $old = CalapanReservation::where('enrollee_id','=',$id)->value('reservation_fee');
        
        $new = $old + $input['amount_paid'];

        CalapanReservation::where('enrollee_id','=',$id)->update([
            'reservation_fee' => $new,
        ]);

         $season = $input['season'];
        if ($season == 'Season 1'){

        CalapanS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = CalapanS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        CalapanS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = CalapanS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', '1 student has been updated.');
    return redirect ('calapan-enrollment/new-reservation');
        }

    if($existent == null)
    {

    CalapanReservation::create([
        'enrollee_id'    =>     $id,
        'name'           =>     $name,
        'branch'         =>     $this->branch,
        'program'        =>     $input['program'],
        'prog'           =>     $prog,
        'school'         =>     $school,
        'email'          =>     $email,
        'contact_no'     =>     $contact_no,
        'reservation_fee'=>     $input['amount_paid'],
    ]);

    $season = $input['season'];
    if ($season == 'Season 1'){

        CalapanS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = CalapanS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        CalapanS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = CalapanS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        CalapanS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', 'New student has been reserved.');
    return redirect ('calapan-enrollment/new-reservation');
}
}

public function reservation_table(){

    $reserve = CalapanReservation::all();

    return view ('member.reservation')->with('reserve',$reserve);
}

public function enrolled_table(){

    $let = CalapanLet::where('status','=','Enrolled')->get();
    $nle = CalapanNle::where('status','=','Enrolled')->get();
    $crim = CalapanCrim::where('status','=','Enrolled')->get();
    $civil = CalapanCivil::where('status','=','Enrolled')->get();
    $psyc = CalapanPsyc::where('status','=','Enrolled')->get();
    $nclex = CalapanNclex::where('status','=','Enrolled')->get();
    $ielt = CalapanIelt::where('status','=','Enrolled')->get();
    $social = CalapanSocial::where('status','=','Enrolled')->get();
    $agri = CalapanAgri::where('status','=','Enrolled')->get();
    $mid = CalapanMid::where('category','=','Enrolled')->get();
    $online = CalapanOnline::where('category','=','Enrolled')->get();

    $sale1 = CalapanS1Sale::all();

    $sale2 = CalapanS2Sale::all();
    $tot_amount_paid=0;
    $total_balance=0;
    return view ('member.enrolled')
                ->with('let',$let)
                ->with('nle',$nle)
                ->with('crim',$crim)
                ->with('civil',$civil)
                ->with('psyc',$psyc)
                ->with('nclex',$nclex)
                ->with('ielt',$ielt)
                ->with('social',$social)
                ->with('agri',$agri)
                ->with('mid',$mid)
                ->with('online',$online)
                ->with('tot_amount_paid',$tot_amount_paid)
                ->with('total_balance',$total_balance)
                ->with('sale1',$sale1)
                ->with('sale2',$sale2);

}


function populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = CalapanScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = CalapanScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = CalapanScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 CalapanScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card



}
