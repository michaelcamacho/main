<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class facilitation extends Model
{
    //
    protected $fillable = [
        'facilitation',
        'season',
        'year'
    ];
}
