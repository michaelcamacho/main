<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
