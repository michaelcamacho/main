<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
