<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
