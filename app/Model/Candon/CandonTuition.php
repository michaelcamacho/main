<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
