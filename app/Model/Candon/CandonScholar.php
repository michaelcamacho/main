<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
