<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
