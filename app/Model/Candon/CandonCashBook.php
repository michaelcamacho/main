<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
