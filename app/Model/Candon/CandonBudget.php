<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
