<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
