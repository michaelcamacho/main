<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
