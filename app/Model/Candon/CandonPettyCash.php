<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
