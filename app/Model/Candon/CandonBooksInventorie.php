<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
