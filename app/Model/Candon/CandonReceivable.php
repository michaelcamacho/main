<?php

namespace App\Model\Candon;

use Illuminate\Database\Eloquent\Model;

class CandonReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
