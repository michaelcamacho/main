<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
