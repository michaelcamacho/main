<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioComment extends Model
{
    protected $fillable = [
    	'name',
		'lecturer',
		'like',
		'dislike',
		'comment',
		'branch',
		'program',
		'section',
		'class',
		'subject',
		'date',
    ];
}
