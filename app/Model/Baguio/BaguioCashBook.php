<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
