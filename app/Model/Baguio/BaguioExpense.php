<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
