<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
