<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
