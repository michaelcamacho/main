<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
