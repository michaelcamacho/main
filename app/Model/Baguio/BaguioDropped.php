<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
