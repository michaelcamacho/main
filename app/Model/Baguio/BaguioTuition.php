<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
