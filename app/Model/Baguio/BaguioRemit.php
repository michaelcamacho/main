<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
