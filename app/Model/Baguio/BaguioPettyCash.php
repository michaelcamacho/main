<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
