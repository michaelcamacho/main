<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
