<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
