<?php

namespace App\Model\Baguio;

use Illuminate\Database\Eloquent\Model;

class BaguioReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
