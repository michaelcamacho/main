<?php

namespace App\Model\LasPinas;

use Illuminate\Database\Eloquent\Model;

class LasPinasExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
