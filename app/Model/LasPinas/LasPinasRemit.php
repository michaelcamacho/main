<?php

namespace App\Model\LasPinas;

use Illuminate\Database\Eloquent\Model;

class LasPinasRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
