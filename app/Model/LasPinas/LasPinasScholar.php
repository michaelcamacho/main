<?php

namespace App\Model\LasPinas;

use Illuminate\Database\Eloquent\Model;

class LasPinasScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
