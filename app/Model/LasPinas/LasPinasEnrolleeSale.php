<?php

namespace App\Model\LasPinas;

use Illuminate\Database\Eloquent\Model;

class LasPinasEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
