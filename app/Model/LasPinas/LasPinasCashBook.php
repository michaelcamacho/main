<?php

namespace App\Model\LasPinas;

use Illuminate\Database\Eloquent\Model;

class LasPinasCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
