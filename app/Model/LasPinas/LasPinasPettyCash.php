<?php

namespace App\Model\LasPinas;

use Illuminate\Database\Eloquent\Model;

class LasPinasPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
