<?php

namespace App\Model\LasPinas;

use Illuminate\Database\Eloquent\Model;

class LasPinasReservation extends Model
{
	protected $fillable = [
    		'enrollee_id',
            'name',
            'branch',
            'program',
            'category',
            'discount_amount',
            'discount_category',
            'prog',
            'school',
            'email',
            'contact_no',
            'reservation_fee',
        ];
}
