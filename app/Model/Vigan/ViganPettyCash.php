<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
