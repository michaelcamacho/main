<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganS2Sale extends Model
{
    protected $fillable = [
    	'date',
        'name',
        'program',
        'category',
        'discount_category',
        'tuition_fee',
        'facilitation_fee',
        'discount',
        'amount_paid',
        'balance',
        'year',

    ];
}
