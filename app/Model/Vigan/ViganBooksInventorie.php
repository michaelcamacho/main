<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
