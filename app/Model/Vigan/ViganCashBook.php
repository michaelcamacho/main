<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
