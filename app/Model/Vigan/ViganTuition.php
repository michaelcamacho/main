<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
