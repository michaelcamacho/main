<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
