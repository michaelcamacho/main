<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
