<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
