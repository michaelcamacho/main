<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganCrim extends Model
{
    protected $fillable = [
        'cbrc_id',
        'last_name',
        'first_name',
        'middle_name',
        'username',
        'password',
        'course',
        'major',
        'program',
        'section',
        'id_pic',
        'school',
        'noa_no',
        'category',
        'status',
        'take',
        'branch',
        'birthdate',
        'contact_no',
        'email',
        'address',
        'contact_person',
        'contact_details',
        'facilitation',
        'registration',
    ];
}
