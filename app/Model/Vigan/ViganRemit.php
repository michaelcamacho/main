<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
