<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
