<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
