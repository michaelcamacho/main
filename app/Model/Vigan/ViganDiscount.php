<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
