<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
