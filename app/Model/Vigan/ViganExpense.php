<?php

namespace App\Model\Vigan;

use Illuminate\Database\Eloquent\Model;

class ViganExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
