<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
