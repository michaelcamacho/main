<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
