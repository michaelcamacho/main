<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
