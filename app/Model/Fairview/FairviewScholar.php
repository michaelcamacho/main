<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
