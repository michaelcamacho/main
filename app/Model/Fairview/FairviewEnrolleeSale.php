<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
