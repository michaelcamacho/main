<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
