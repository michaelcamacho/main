<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
