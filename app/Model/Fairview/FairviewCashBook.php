<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
