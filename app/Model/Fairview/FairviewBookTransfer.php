<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewBookTransfer extends Model
{
    protected $fillable = [
        
        'book_transId',
        'book_program',
        'book_major',
        'book_author',
        'book_title',
        'book_quantity',
        'book_stockInitial',
        'book_stockFinal'
    ];
}
