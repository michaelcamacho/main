<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
