<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
