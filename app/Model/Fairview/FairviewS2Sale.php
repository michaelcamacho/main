<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewS2Sale extends Model
{
    protected $fillable = [
    	'date',
        'name',
        'program',
        'category',
        'discount_category',
        'tuition_fee',
        'facilitation_fee',
        'discount',
        'amount_paid',
        'balance',
        'year',

    ];
}
