<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
