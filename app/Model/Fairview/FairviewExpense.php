<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
