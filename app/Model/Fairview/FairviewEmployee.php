<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewEmployee extends Model
{

  protected $guarded = [];
  
    protected $fillable = [
    	'id',
    	'employee_no',
    	'last_name',
    	'first_name',
    	'middle_name',
    	'birthdate',
    	'gender',
    	'status',
    	'address',
    	'email',
    	'contact_no',
        'contact_person',
        'contact_details',
    	'position',
    	'employment_status',
    	'rate',
    	'date_hired',
    	'sss',
    	'phil_health',
    	'pag_ibig',
    	'tin',
        'cover_image'
    ];
}
