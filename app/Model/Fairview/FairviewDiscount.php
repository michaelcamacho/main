<?php

namespace App\Model\Fairview;

use Illuminate\Database\Eloquent\Model;

class FairviewDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
