<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
