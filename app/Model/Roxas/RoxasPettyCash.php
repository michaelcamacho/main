<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
