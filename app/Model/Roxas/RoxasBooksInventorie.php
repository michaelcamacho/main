<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
