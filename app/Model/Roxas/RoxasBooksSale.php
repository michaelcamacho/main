<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
