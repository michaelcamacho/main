<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
