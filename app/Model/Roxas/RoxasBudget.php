<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
