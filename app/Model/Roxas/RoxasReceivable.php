<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
