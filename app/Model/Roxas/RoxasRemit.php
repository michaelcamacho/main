<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
