<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
