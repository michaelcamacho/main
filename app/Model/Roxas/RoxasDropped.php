<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
