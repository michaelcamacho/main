<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
