<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
