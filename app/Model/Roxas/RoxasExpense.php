<?php

namespace App\Model\Roxas;

use Illuminate\Database\Eloquent\Model;

class RoxasExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
