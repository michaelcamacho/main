<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
