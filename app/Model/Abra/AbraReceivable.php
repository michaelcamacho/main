<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
