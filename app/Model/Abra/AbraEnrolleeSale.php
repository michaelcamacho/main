<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
