<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
