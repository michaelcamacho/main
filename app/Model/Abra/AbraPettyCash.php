<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
