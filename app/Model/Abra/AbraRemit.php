<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
