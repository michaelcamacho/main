<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraReservation extends Model
{
	protected $fillable = [
    		'enrollee_id',
            'name',
            'branch',
            'program',
            'category',
            'discount_amount',
            'discount_category',
            'prog',
            'school',
            'email',
            'contact_no',
            'reservation_fee',
        ];
}
