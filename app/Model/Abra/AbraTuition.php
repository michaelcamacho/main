<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
