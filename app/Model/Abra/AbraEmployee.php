<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraEmployee extends Model
{

  protected $guarded = [];
  
    protected $fillable = [
    	'id',
    	'employee_no',
    	'last_name',
    	'first_name',
    	'middle_name',
    	'birthdate',
    	'gender',
    	'status',
    	'address',
    	'email',
    	'contact_no',
        'contact_person',
        'contact_details',
    	'position',
    	'employment_status',
    	'rate',
    	'date_hired',
    	'sss',
    	'phil_health',
    	'pag_ibig',
    	'tin',
        'cover_image'
    ];
}
