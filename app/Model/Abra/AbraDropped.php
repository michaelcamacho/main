<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
