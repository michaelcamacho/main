<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
