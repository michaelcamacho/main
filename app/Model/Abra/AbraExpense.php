<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
