<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
