<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
