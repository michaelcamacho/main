<?php

namespace App\Model\Abra;

use Illuminate\Database\Eloquent\Model;

class AbraScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
