<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
