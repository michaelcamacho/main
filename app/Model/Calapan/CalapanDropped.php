<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
