<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
