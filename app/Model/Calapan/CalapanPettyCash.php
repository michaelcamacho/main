<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
