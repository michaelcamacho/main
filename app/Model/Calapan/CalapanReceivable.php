<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
