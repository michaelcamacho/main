<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
