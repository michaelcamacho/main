<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
