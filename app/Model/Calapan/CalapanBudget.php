<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
