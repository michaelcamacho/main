<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
