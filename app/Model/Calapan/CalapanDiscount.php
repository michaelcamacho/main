<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
