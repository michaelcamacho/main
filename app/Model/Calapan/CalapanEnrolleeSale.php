<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
