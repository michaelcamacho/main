<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
