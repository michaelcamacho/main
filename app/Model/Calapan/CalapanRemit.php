<?php

namespace App\Model\Calapan;

use Illuminate\Database\Eloquent\Model;

class CalapanRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
