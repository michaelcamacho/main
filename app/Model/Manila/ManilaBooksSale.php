<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
