<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
