<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
