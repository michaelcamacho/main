<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaComment extends Model
{
    protected $fillable = [
    	'name',
		'lecturer',
		'like',
		'dislike',
		'comment',
		'branch',
		'program',
		'section',
		'class',
		'subject',
		'date',
    ];
}
