<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
