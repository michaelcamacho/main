<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
