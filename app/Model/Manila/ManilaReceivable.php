<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
