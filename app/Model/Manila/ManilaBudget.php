<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
