<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaReservation extends Model
{
	protected $fillable = [
    		'enrollee_id',
            'name',
            'branch',
            'program',
            'category',
            'discount_amount',
            'discount_category',
            'prog',
            'school',
            'email',
            'contact_no',
            'reservation_fee',
        ];
}
