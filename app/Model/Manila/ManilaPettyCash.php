<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
