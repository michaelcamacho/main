<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
