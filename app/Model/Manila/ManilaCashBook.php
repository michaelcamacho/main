<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
