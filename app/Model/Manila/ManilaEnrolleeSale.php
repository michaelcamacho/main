<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
