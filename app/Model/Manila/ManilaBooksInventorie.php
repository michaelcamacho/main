<?php

namespace App\Model\Manila;

use Illuminate\Database\Eloquent\Model;

class ManilaBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
