<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
