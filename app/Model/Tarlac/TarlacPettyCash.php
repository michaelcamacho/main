<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
