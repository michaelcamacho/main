<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
