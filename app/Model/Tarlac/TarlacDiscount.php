<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
