<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacS1Sale extends Model
{
    protected $fillable = [
    	'date',
        'name',
        'program',
        'category',
        'discount_category',
        'tuition_fee',
        'facilitation_fee',
        'discount',
        'amount_paid',
        'balance',
        'year',

    ];
}
