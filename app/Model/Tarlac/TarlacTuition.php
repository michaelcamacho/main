<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
