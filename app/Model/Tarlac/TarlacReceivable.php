<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
