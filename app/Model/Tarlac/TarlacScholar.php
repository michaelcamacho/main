<?php

namespace App\Model\Tarlac;

use Illuminate\Database\Eloquent\Model;

class TarlacScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
