<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
