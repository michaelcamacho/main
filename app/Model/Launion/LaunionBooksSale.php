<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
