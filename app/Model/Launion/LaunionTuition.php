<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionTuition extends Model
{
    protected $fillable = [
        'program',
        'branch',
        'category',
        'tuition_fee',
        'season',
        'year',
        'facilitation_fee',

    ];
}
