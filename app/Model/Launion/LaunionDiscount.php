<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
