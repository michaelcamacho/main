<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
