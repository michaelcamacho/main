<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
