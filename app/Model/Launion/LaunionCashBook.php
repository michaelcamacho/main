<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
