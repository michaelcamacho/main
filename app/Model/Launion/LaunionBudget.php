<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
