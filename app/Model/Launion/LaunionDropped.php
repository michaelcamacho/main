<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionDropped extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'branch',
        'program',
        'school',
        'email',
        'contact_no',

    ];

}
