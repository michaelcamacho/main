<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionReceivable extends Model
{
    protected $fillable = [
        'name',
        'program',
        'contact_no',
        'season',
        'balance',

    ];
}
