<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
