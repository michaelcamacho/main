<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionExpense extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'program',
        'category',
        'sub_category',
        'amount',
        'remarks',
        'author',

    ];
}
