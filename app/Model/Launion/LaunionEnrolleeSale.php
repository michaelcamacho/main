<?php

namespace App\Model\Launion;

use Illuminate\Database\Eloquent\Model;

class LaunionEnrolleeSale extends Model
{
    protected $fillable = [
    	'cash_sales'
    ];
}
