<?php

namespace App\Model\Dagupan;

use Illuminate\Database\Eloquent\Model;

class DagupanRemit extends Model
{
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
