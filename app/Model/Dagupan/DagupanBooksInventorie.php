<?php

namespace App\Model\Dagupan;

use Illuminate\Database\Eloquent\Model;

class DagupanBooksInventorie extends Model
{
    protected $fillable = [
    	'branch',
        'program',
        'book_title',
        'price',
        'available',
        
    ];
}
