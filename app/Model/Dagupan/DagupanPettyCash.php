<?php

namespace App\Model\Dagupan;

use Illuminate\Database\Eloquent\Model;

class DagupanPettyCash extends Model
{
    protected $fillable = [
    	'petty_cash'
];
}
