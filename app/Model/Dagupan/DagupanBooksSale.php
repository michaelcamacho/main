<?php

namespace App\Model\Dagupan;

use Illuminate\Database\Eloquent\Model;

class DagupanBooksSale extends Model
{
    protected $fillable = [
        'date',
        'branch',
        'name',
        'program',
        'book_title',
        'amount',
        
    ];
}
