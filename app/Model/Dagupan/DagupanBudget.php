<?php

namespace App\Model\Dagupan;

use Illuminate\Database\Eloquent\Model;

class DagupanBudget extends Model
{
    protected $fillable = [
        'date',
        'amount',
        'category',
        'remarks',
        
    ];
}
