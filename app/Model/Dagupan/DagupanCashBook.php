<?php

namespace App\Model\Dagupan;

use Illuminate\Database\Eloquent\Model;

class DagupanCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
