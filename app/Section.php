<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
	protected $fillable = [
    'id',
    'branch',
	'program',
	'aka_class',
	'aka_subject',
	'section',
	];
}
