<?php


Route::get('/', function () {
    return view('auth.login');
});

/* START DAET */
/* App */
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/user/activation/{token}', 'Auth\RegisterController@userActivation');

Route::get('cbrc/online-registration','RegistrationController@index');

Route::post('new-register','RegistrationController@register');

Route::get('thank-you','RegistrationController@thank_you');

Route::get('/json-major','MainController@fetch_major');


/* Admin */
Route::get('/dashboard','AdminController@dashboard')->middleware('auth');
Route::get('/total-program','AdminController@total_program')->middleware('auth');
Route::get('/total-agri','AdminController@total_agri')->middleware('auth');
Route::get('/total-civil','AdminController@total_civil')->middleware('auth');
Route::get('/total-crim','AdminController@total_crim')->middleware('auth');
Route::get('/total-ielts','AdminController@total_ielts')->middleware('auth');
Route::get('/total-let','AdminController@total_let')->middleware('auth');
Route::get('/total-nclex','AdminController@total_nclex')->middleware('auth');
Route::get('/total-nle','AdminController@total_nle')->middleware('auth');
Route::get('/total-psyc','AdminController@total_psyc')->middleware('auth');
Route::get('/total-mid','AdminController@total_mid')->middleware('auth');
Route::get('/total-online','AdminController@total_online')->middleware('auth');
Route::get('/total-social','AdminController@total_social')->middleware('auth');
Route::get('/total-dropped','AdminController@total_dropped')->middleware('auth');
Route::get('/total-scholars','AdminController@total_scholars')->middleware('auth');
Route::get('/tuition-fees','AdminController@tuition_fees')->middleware('auth');
Route::get('/discounts','AdminController@discounts')->middleware('auth');
Route::get('/total-books','AdminController@total_books')->middleware('auth');
Route::get('/total-expense','AdminController@total_expense')->middleware('auth');
Route::get('/users','AdminController@user')->middleware('auth');
Route::get('/abra/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/baguio/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/calapan/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/candon/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/dagupan/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/fairview/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/las_pinas/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/launion/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/manila/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/roxas/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/tarlac/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/vigan/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/expense-settings','AdminController@expense_settings')->middleware('auth');
Route::post('/setup-expense','AdminController@expense_setup')->middleware('auth');



// ======================= RA dashbaord Routes ===========================

Route::get('/radashboard/attendance', 'AdminController@attendance');
Route::get('/radashboard/evaluation', 'AdminController@evaluation');
Route::get('/radashboard/lecturer', 'AdminController@lecturer');
Route::get('/radashboard/result-analysis', 'AdminController@result_analysis');



/* Add Data */
Route::post('/insert-tuition','AdminController@insert_tuition')->middleware('auth');
Route::post('/insert-discount','AdminController@insert_discount')->middleware('auth');
Route::post('/insert-book','AdminController@insert_book')->middleware('auth');
Route::post('/insert-user','AdminController@insert_user')->middleware('auth');
Route::post('/save-adsettings','AdminController@save_adsettings')->middleware('auth');

/* Update Data */
Route::post('/update-tuition','AdminController@update_tuition')->middleware('auth');
Route::post('/update-discount','AdminController@update_discount')->middleware('auth');
Route::post('/update-book','AdminController@update_book')->middleware('auth');
Route::post('/update-user','AdminController@update_user')->middleware('auth');

/* Delete Data */
Route::post('/delete-tuition','AdminController@delete_tuition')->middleware('auth');
Route::post('/delete-discount','AdminController@delete_discount')->middleware('auth');
Route::post('/delete-book','AdminController@delete_book')->middleware('auth');
Route::delete('/delete-user','AdminController@delete_user')->middleware('auth');




/* Member */


//==========================================Abra====================================//
/* abra */
Route::get('/abra/dashboard','AbraController@index')->middleware('auth');
Route::get('/abra/add-enrollee','AbraController@add_enrollee')->middleware('auth');
Route::get('/abra/new-payment','AbraController@new_payment')->middleware('auth');
Route::get('/abra/new-reservation','AbraController@new_reservation')->middleware('auth');
Route::get('/abra/add-expense','AbraController@add_expense')->middleware('auth');
Route::get('/abra/add-budget','AbraController@add_budget')->middleware('auth');
Route::get('/abra/book-payment','AbraController@book_payment')->middleware('auth');
Route::get('/abra/new-remit', 'AbraController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/abra/json-student','AbraController@fetch_student')->middleware('auth');
Route::get('/abra/json-tuition','AbraController@fetch_tuition')->middleware('auth');
Route::get('/abra/json-discount','AbraController@fetch_discount')->middleware('auth');
Route::get('/abra/json-balance','AbraController@fetch_balance')->middleware('auth');
Route::get('/abra/json-expense','AbraController@fetch_expense')->middleware('auth');
Route::get('/abra/json-book','AbraController@fetch_book')->middleware('auth');
Route::get('/abra/json-price','AbraController@fetch_book_price')->middleware('auth');
Route::get('/abra/json-reserved','AbraController@fetch_reserved')->middleware('auth');
Route::get('/abra/json-id','AbraController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/abra/insert-enrollee','AbraController@insert_enrollee')->middleware('auth');
Route::post('/abra/insert-new-payment','AbraController@add_new_payment')->middleware('auth');
Route::post('/abra/insert-new-expense','AbraController@add_new_expense')->middleware('auth');
Route::post('/abra/insert-new-budget','AbraController@insert_new_budget')->middleware('auth');
Route::post('/abra/insert-book-payment', 'AbraController@insert_book_payment')->middleware('auth');
Route::post('/abra/insert-book-transfer', 'AbraController@insert_book_transfer')->middleware('auth');
Route::post('/abra/drop-student', 'AbraController_@drop_student')->middleware('auth');
Route::post('/abra/insert-remit', 'AbraController@insert_remit')->middleware('auth');
Route::post('/abra/insert-reservation','AbraController@insert_reservation')->middleware('auth');
Route::post('/abra/insert-employee','AbraController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/abra/update-enrollee','AbraController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/abra/delete-enrollee','AbraController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/abra/let','AbraController@let_table')->middleware('auth');
Route::get('/abra/nle','AbraController@nle_table')->middleware('auth');
Route::get('/abra/crim','AbraController@crim_table')->middleware('auth');
Route::get('/abra/civil','AbraController@civil_table')->middleware('auth');
Route::get('/abra/psyc','AbraController@psyc_table')->middleware('auth');
Route::get('/abra/nclex','AbraController@nclex_table')->middleware('auth');
Route::get('/abra/ielts','AbraController@ielts_table')->middleware('auth');
Route::get('/abra/social','AbraController@social_table')->middleware('auth');
Route::get('/abra/agri','AbraController@agri_table')->middleware('auth');
Route::get('/abra/mid','AbraController@mid_table')->middleware('auth');
Route::get('/abra/online','AbraController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/abra/tuition','AbraController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/abra/scholar','AbraController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/abra/enrolled','AbraController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/abra/dropped','AbraController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/abra/sales-enrollee','AbraController@sales_enrollee_table')->middleware('auth');
Route::get('/abra/sales-program','AbraController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/abra/receivable-enrollee','AbraController@receivable_enrollee_table')->middleware('auth');
Route::get('/abra/receivable-program','AbraController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/abra/expense','AbraController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/abra/books','AbraController@books_table')->middleware('auth');
Route::get('/abra/remit', 'AbraController@remit')->middleware('auth');
Route::get('/abra/reservation', 'AbraController@reservation_table')->middleware('auth');

Route::post('abra/clear-enrollee','AbraController@clear_enrollee')->middleware('auth');
Route::post('abra/clear-sale-season1','AbraController@clear_sale_season1')->middleware('auth');
Route::post('abra/clear-sale-season2','AbraController@clear_sale_season2')->middleware('auth');
Route::post('abra/clear-receivable','AbraController@clear_receivable')->middleware('auth');
Route::post('abra/clear-expense','AbraController@clear_expense')->middleware('auth');
Route::post('abra/clear-book','AbraController@clear_book')->middleware('auth');
Route::post('abra/clear-reservation','AbraController@clear_reservation')->middleware('auth');

/* Abra Lecturer */
Route::get('abra/lecturer-evaluation','AbraController@lec_eval')->middleware('auth');
Route::get('abra/add-lecturer','AbraController@add_lec')->middleware('auth');
Route::get('abra/evaluate-lecturer','AbraController@eval_lec')->middleware('auth');
Route::get('abra/json-class','AbraController@fetch_class')->middleware('auth');
Route::get('abra/json-subject','AbraController@fetch_subject')->middleware('auth');
Route::get('abra/json-section','AbraController@fetch_section')->middleware('auth');
Route::get('abra/json-lecturer','AbraController@fetch_lecturer')->middleware('auth');
Route::get('abra/json-lecturerb','AbraController@fetch_lecturerb')->middleware('auth');

Route::post('abra/insert-lecturer','AbraController@insert_lecturer')->middleware('auth');
Route::post('abra/insert-evaluation','AbraController@insert_eval')->middleware('auth');

Route::post('abra/clear-lecturers','AbraController@clear_lecturers')->middleware('auth');


/* Reports Abra */

Route::get('abra/today','AbraController@today')->middleware('auth');
Route::get('abra/yesterday','AbraController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Abra */

Route::get('abra/budget','AbraController@budget_record')->middleware('auth');
Route::post('abra/clear-budget','AbraController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('abra/delete-sale/s1/{id}','AbraController@delete_sale1');
Route::get('abra/delete-sale/s2/{id}','AbraController@delete_sale2');
/* End Delete */

/** csv */
Route::post('abra/csv-enrollee','AbraController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/abra/add-employee','AbraController@add_employee')->middleware('auth');
Route::get('/abra/employee-record','AbraController@employee_record')->middleware('auth');
Route::post('/abra/update-employee','AbraController@update_employee')->middleware('auth');
Route::get('/abra/delete-employee/{id}','AbraController@delete_employee')->middleware('auth');
Route::get('/taskHistory','AbraController@taskHistory')->middleware('auth');

// Member
Route::get('/abra/view-employee','AbraController@view_employee')->middleware('auth');

// student id
Route::get('/abra/student-id','AbraController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\AbraController@bulletin');
Route::get('/fetch-branches/{bn}','AbraController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('abra/financial-report','AbraController@financialreport')->middleware('auth');

/** scorecard */
Route::get('abra/scorecard-season/{season}','AbraController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/abra/bookTransferRecord','AbraController@bookTransfer_table')->middleware('auth');
Route::get('/abra/book-transfer','AbraController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'AbraController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'AbraController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','AbraController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/abra/delete-expense/{id}','AbraController@delete_expense')->middleware('auth');


//=============Abra Enrollment =========

/* Data fetch */
Route::get('/abra-enrollment/json-student','AbraEnrollmentController@fetch_student')->middleware('auth');
Route::get('/abra-enrollment/json-tuition','AbraEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/abra-enrollment/json-discount','AbraEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/abra-enrollment/json-balance','AbraEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/abra-enrollment/json-id','AbraEnrollmentController@fetch_id')->middleware('auth');
Route::get('/abra-enrollment/json-reserved','AbraEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/abra-enrollment/drop-student', 'AbraEnrollmentController_@drop_student')->middleware('auth');
Route::post('/abra-enrollment/insert-enrollee','AbraEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/abra-enrollment/add-enrollee','AbraEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/abra-enrollment/insert-enrollee','AbraEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/abra-enrollment/insert-new-payment','AbraEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/abra-enrollment/update-enrollee','AbraEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','AbraEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/abra-enrollment/new-payment','AbraEnrollmentController@new_payment')->middleware('auth');
Route::get('/abra-enrollment/enrolled','AbraEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/abra-enrollment/sales-enrollee','AbraEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/abra-enrollment/dropped','AbraEnrollmentController@dropped_table')->middleware('auth');

Route::post('/abra-enrollment/insert-reservation','AbraEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/abra-enrollment/new-reservation','AbraEnrollmentController@new_reservation')->middleware('auth');
Route::get('/abra-enrollment/reservation', 'AbraEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/abra-enrollment/let','AbraEnrollmentController@let_table')->middleware('auth');
Route::get('/abra-enrollment/nle','AbraEnrollmentController@nle_table')->middleware('auth');
Route::get('/abra-enrollment/crim','AbraEnrollmentController@crim_table')->middleware('auth');
Route::get('/abra-enrollment/civil','AbraEnrollmentController@civil_table')->middleware('auth');
Route::get('/abra-enrollment/psyc','AbraEnrollmentController@psyc_table')->middleware('auth');
Route::get('/abra-enrollment/nclex','AbraEnrollmentController@nclex_table')->middleware('auth');
Route::get('/abra-enrollment/ielts','AbraEnrollmentController@ielts_table')->middleware('auth');
Route::get('/abra-enrollment/social','AbraEnrollmentController@social_table')->middleware('auth');
Route::get('/abra-enrollment/agri','AbraEnrollmentController@agri_table')->middleware('auth');
Route::get('/abra-enrollment/mid','AbraEnrollmentController@mid_table')->middleware('auth');
Route::get('/abra-enrollment/online','AbraEnrollmentController@online_table')->middleware('auth');

//============Abra Cashier books=================
Route::get('/abra-cashier/books','AbraCashierController@books_table')->middleware('auth');

Route::post('/abra-cashier/insert-book-payment', 'AbraCashierController@insert_book_payment')->middleware('auth');
Route::get('/abra-cashier/book-payment','AbraCashierController@book_payment')->middleware('auth');

Route::get('/abra-cashier/json-book','AbraCashierController@fetch_book')->middleware('auth');
Route::get('/abra-cashier/json-price','AbraCashierController@fetch_book_price')->middleware('auth');

//==========================================Abra====================================//



//==========================================Baguio====================================//
/* baguio */
Route::get('/baguio/dashboard','BaguioController@index')->middleware('auth');
Route::get('/baguio/add-enrollee','BaguioController@add_enrollee')->middleware('auth');
Route::get('/baguio/new-payment','BaguioController@new_payment')->middleware('auth');
Route::get('/baguio/new-reservation','BaguioController@new_reservation')->middleware('auth');
Route::get('/baguio/add-expense','BaguioController@add_expense')->middleware('auth');
Route::get('/baguio/add-budget','BaguioController@add_budget')->middleware('auth');
Route::get('/baguio/book-payment','BaguioController@book_payment')->middleware('auth');
Route::get('/baguio/new-remit', 'BaguioController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/baguio/json-student','BaguioController@fetch_student')->middleware('auth');
Route::get('/baguio/json-tuition','BaguioController@fetch_tuition')->middleware('auth');
Route::get('/baguio/json-discount','BaguioController@fetch_discount')->middleware('auth');
Route::get('/baguio/json-balance','BaguioController@fetch_balance')->middleware('auth');
Route::get('/baguio/json-expense','BaguioController@fetch_expense')->middleware('auth');
Route::get('/baguio/json-book','BaguioController@fetch_book')->middleware('auth');
Route::get('/baguio/json-price','BaguioController@fetch_book_price')->middleware('auth');
Route::get('/baguio/json-reserved','BaguioController@fetch_reserved')->middleware('auth');
Route::get('/baguio/json-id','BaguioController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/baguio/insert-enrollee','BaguioController@insert_enrollee')->middleware('auth');
Route::post('/baguio/insert-new-payment','BaguioController@add_new_payment')->middleware('auth');
Route::post('/baguio/insert-new-expense','BaguioController@add_new_expense')->middleware('auth');
Route::post('/baguio/insert-new-budget','BaguioController@insert_new_budget')->middleware('auth');
Route::post('/baguio/insert-book-payment', 'BaguioController@insert_book_payment')->middleware('auth');
Route::post('/baguio/insert-book-transfer', 'BaguioController@insert_book_transfer')->middleware('auth');
Route::post('/baguio/drop-student', 'BaguioController_@drop_student')->middleware('auth');
Route::post('/baguio/insert-remit', 'BaguioController@insert_remit')->middleware('auth');
Route::post('/baguio/insert-reservation','BaguioController@insert_reservation')->middleware('auth');
Route::post('/baguio/insert-employee','BaguioController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/baguio/update-enrollee','BaguioController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/baguio/delete-enrollee','BaguioController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/baguio/let','BaguioController@let_table')->middleware('auth');
Route::get('/baguio/nle','BaguioController@nle_table')->middleware('auth');
Route::get('/baguio/crim','BaguioController@crim_table')->middleware('auth');
Route::get('/baguio/civil','BaguioController@civil_table')->middleware('auth');
Route::get('/baguio/psyc','BaguioController@psyc_table')->middleware('auth');
Route::get('/baguio/nclex','BaguioController@nclex_table')->middleware('auth');
Route::get('/baguio/ielts','BaguioController@ielts_table')->middleware('auth');
Route::get('/baguio/social','BaguioController@social_table')->middleware('auth');
Route::get('/baguio/agri','BaguioController@agri_table')->middleware('auth');
Route::get('/baguio/mid','BaguioController@mid_table')->middleware('auth');
Route::get('/baguio/online','BaguioController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/baguio/tuition','BaguioController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/baguio/scholar','BaguioController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/baguio/enrolled','BaguioController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/baguio/dropped','BaguioController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/baguio/sales-enrollee','BaguioController@sales_enrollee_table')->middleware('auth');
Route::get('/baguio/sales-program','BaguioController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/baguio/receivable-enrollee','BaguioController@receivable_enrollee_table')->middleware('auth');
Route::get('/baguio/receivable-program','BaguioController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/baguio/expense','BaguioController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/baguio/books','BaguioController@books_table')->middleware('auth');
Route::get('/baguio/remit', 'BaguioController@remit')->middleware('auth');
Route::get('/baguio/reservation', 'BaguioController@reservation_table')->middleware('auth');

Route::post('baguio/clear-enrollee','BaguioController@clear_enrollee')->middleware('auth');
Route::post('baguio/clear-sale-season1','BaguioController@clear_sale_season1')->middleware('auth');
Route::post('baguio/clear-sale-season2','BaguioController@clear_sale_season2')->middleware('auth');
Route::post('baguio/clear-receivable','BaguioController@clear_receivable')->middleware('auth');
Route::post('baguio/clear-expense','BaguioController@clear_expense')->middleware('auth');
Route::post('baguio/clear-book','BaguioController@clear_book')->middleware('auth');
Route::post('baguio/clear-reservation','BaguioController@clear_reservation')->middleware('auth');

/* Abra Lecturer */
Route::get('baguio/lecturer-evaluation','BaguioController@lec_eval')->middleware('auth');
Route::get('baguio/add-lecturer','BaguioController@add_lec')->middleware('auth');
Route::get('baguio/evaluate-lecturer','BaguioController@eval_lec')->middleware('auth');
Route::get('baguio/json-class','BaguioController@fetch_class')->middleware('auth');
Route::get('baguio/json-subject','BaguioController@fetch_subject')->middleware('auth');
Route::get('baguio/json-section','BaguioController@fetch_section')->middleware('auth');
Route::get('baguio/json-lecturer','BaguioController@fetch_lecturer')->middleware('auth');
Route::get('baguio/json-lecturerb','BaguioController@fetch_lecturerb')->middleware('auth');

Route::post('baguio/insert-lecturer','BaguioController@insert_lecturer')->middleware('auth');
Route::post('baguio/insert-evaluation','BaguioController@insert_eval')->middleware('auth');

Route::post('baguio/clear-lecturers','BaguioController@clear_lecturers')->middleware('auth');


/* Reports Baguio */

Route::get('baguio/today','BaguioController@today')->middleware('auth');
Route::get('baguio/yesterday','BaguioController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Baguio */

Route::get('baguio/budget','BaguioController@budget_record')->middleware('auth');
Route::post('baguio/clear-budget','BaguioController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('baguio/delete-sale/s1/{id}','BaguioController@delete_sale1');
Route::get('baguio/delete-sale/s2/{id}','BaguioController@delete_sale2');
/* End Delete */

/** csv */
Route::post('baguio/csv-enrollee','BaguioController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/baguio/add-employee','BaguioController@add_employee')->middleware('auth');
Route::get('/baguio/employee-record','BaguioController@employee_record')->middleware('auth');
Route::post('/baguio/update-employee','BaguioController@update_employee')->middleware('auth');
Route::get('/baguio/delete-employee/{id}','BaguioController@delete_employee')->middleware('auth');
Route::get('/taskHistory','BaguioController@taskHistory')->middleware('auth');

// Member
Route::get('/baguio/view-employee','BaguioController@view_employee')->middleware('auth');

// student id
Route::get('/baguio/student-id','BaguioController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\BaguioController@bulletin');
Route::get('/fetch-branches/{bn}','BaguioController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('baguio/financial-report','BaguioController@financialreport')->middleware('auth');

/** scorecard */
Route::get('baguio/scorecard-season/{season}','BaguioController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/baguio/bookTransferRecord','BaguioController@bookTransfer_table')->middleware('auth');
Route::get('/baguio/book-transfer','BaguioController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'BaguioController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'BaguioController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','BaguioController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/baguio/delete-expense/{id}','BaguioController@delete_expense')->middleware('auth');


//=============Baguio Enrollment =========

/* Data fetch */
Route::get('/baguio-enrollment/json-student','BaguioEnrollmentController@fetch_student')->middleware('auth');
Route::get('/baguio-enrollment/json-tuition','BaguioEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/baguio-enrollment/json-discount','BaguioEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/baguio-enrollment/json-balance','BaguioEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/baguio-enrollment/json-id','BaguioEnrollmentController@fetch_id')->middleware('auth');
Route::get('/baguio-enrollment/json-reserved','BaguioEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/baguio-enrollment/drop-student', 'BaguioEnrollmentController_@drop_student')->middleware('auth');
Route::post('/baguio-enrollment/insert-enrollee','BaguioEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/baguio-enrollment/add-enrollee','BaguioEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/baguio-enrollment/insert-enrollee','BaguioEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/baguio-enrollment/insert-new-payment','BaguioEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/baguio-enrollment/update-enrollee','BaguioEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','BaguioEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/baguio-enrollment/new-payment','BaguioEnrollmentController@new_payment')->middleware('auth');
Route::get('/baguio-enrollment/enrolled','BaguioEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/baguio-enrollment/sales-enrollee','BaguioEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/baguio-enrollment/dropped','BaguioEnrollmentController@dropped_table')->middleware('auth');

Route::post('/baguio-enrollment/insert-reservation','BaguioEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/baguio-enrollment/new-reservation','BaguioEnrollmentController@new_reservation')->middleware('auth');
Route::get('/baguio-enrollment/reservation', 'BaguioEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/baguio-enrollment/let','BaguioEnrollmentController@let_table')->middleware('auth');
Route::get('/baguio-enrollment/nle','BaguioEnrollmentController@nle_table')->middleware('auth');
Route::get('/baguio-enrollment/crim','BaguioEnrollmentController@crim_table')->middleware('auth');
Route::get('/baguio-enrollment/civil','BaguioEnrollmentController@civil_table')->middleware('auth');
Route::get('/baguio-enrollment/psyc','BaguioEnrollmentController@psyc_table')->middleware('auth');
Route::get('/baguio-enrollment/nclex','BaguioEnrollmentController@nclex_table')->middleware('auth');
Route::get('/baguio-enrollment/ielts','BaguioEnrollmentController@ielts_table')->middleware('auth');
Route::get('/baguio-enrollment/social','BaguioEnrollmentController@social_table')->middleware('auth');
Route::get('/baguio-enrollment/agri','BaguioEnrollmentController@agri_table')->middleware('auth');
Route::get('/baguio-enrollment/mid','BaguioEnrollmentController@mid_table')->middleware('auth');
Route::get('/baguio-enrollment/online','BaguioEnrollmentController@online_table')->middleware('auth');

//============Baguio Cashier books=================
Route::get('/baguio-cashier/books','BaguioCashierController@books_table')->middleware('auth');

Route::post('/baguio-cashier/insert-book-payment', 'BaguioCashierController@insert_book_payment')->middleware('auth');
Route::get('/baguio-cashier/book-payment','BaguioCashierController@book_payment')->middleware('auth');

Route::get('/baguio-cashier/json-book','BaguioCashierController@fetch_book')->middleware('auth');
Route::get('/baguio-cashier/json-price','BaguioCashierController@fetch_book_price')->middleware('auth');

//==========================================Baguio====================================//



//==========================================Calapan====================================//
/* calapan */
Route::get('/calapan/dashboard','CalapanController@index')->middleware('auth');
Route::get('/calapan/add-enrollee','CalapanController@add_enrollee')->middleware('auth');
Route::get('/calapan/new-payment','CalapanController@new_payment')->middleware('auth');
Route::get('/calapan/new-reservation','CalapanController@new_reservation')->middleware('auth');
Route::get('/calapan/add-expense','CalapanController@add_expense')->middleware('auth');
Route::get('/calapan/add-budget','CalapanController@add_budget')->middleware('auth');
Route::get('/calapan/book-payment','CalapanController@book_payment')->middleware('auth');
Route::get('/calapan/new-remit', 'CalapanController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/calapan/json-student','CalapanController@fetch_student')->middleware('auth');
Route::get('/calapan/json-tuition','CalapanController@fetch_tuition')->middleware('auth');
Route::get('/calapan/json-discount','CalapanController@fetch_discount')->middleware('auth');
Route::get('/calapan/json-balance','CalapanController@fetch_balance')->middleware('auth');
Route::get('/calapan/json-expense','CalapanController@fetch_expense')->middleware('auth');
Route::get('/calapan/json-book','CalapanController@fetch_book')->middleware('auth');
Route::get('/calapan/json-price','CalapanController@fetch_book_price')->middleware('auth');
Route::get('/calapan/json-reserved','CalapanController@fetch_reserved')->middleware('auth');
Route::get('/calapan/json-id','CalapanController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/calapan/insert-enrollee','CalapanController@insert_enrollee')->middleware('auth');
Route::post('/calapan/insert-new-payment','CalapanController@add_new_payment')->middleware('auth');
Route::post('/calapan/insert-new-expense','CalapanController@add_new_expense')->middleware('auth');
Route::post('/calapan/insert-new-budget','CalapanController@insert_new_budget')->middleware('auth');
Route::post('/calapan/insert-book-payment', 'CalapanController@insert_book_payment')->middleware('auth');
Route::post('/calapan/insert-book-transfer', 'CalapanController@insert_book_transfer')->middleware('auth');
Route::post('/calapan/drop-student', 'CalapanController_@drop_student')->middleware('auth');
Route::post('/calapan/insert-remit', 'CalapanController@insert_remit')->middleware('auth');
Route::post('/calapan/insert-reservation','CalapanController@insert_reservation')->middleware('auth');
Route::post('/calapan/insert-employee','CalapanController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/calapan/update-enrollee','CalapanController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/calapan/delete-enrollee','CalapanController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/calapan/let','CalapanController@let_table')->middleware('auth');
Route::get('/calapan/nle','CalapanController@nle_table')->middleware('auth');
Route::get('/calapan/crim','CalapanController@crim_table')->middleware('auth');
Route::get('/calapan/civil','CalapanController@civil_table')->middleware('auth');
Route::get('/calapan/psyc','CalapanController@psyc_table')->middleware('auth');
Route::get('/calapan/nclex','CalapanController@nclex_table')->middleware('auth');
Route::get('/calapan/ielts','CalapanController@ielts_table')->middleware('auth');
Route::get('/calapan/social','CalapanController@social_table')->middleware('auth');
Route::get('/calapan/agri','CalapanController@agri_table')->middleware('auth');
Route::get('/calapan/mid','CalapanController@mid_table')->middleware('auth');
Route::get('/calapan/online','CalapanController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/calapan/tuition','CalapanController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/calapan/scholar','CalapanController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/calapan/enrolled','CalapanController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/calapan/dropped','CalapanController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/calapan/sales-enrollee','CalapanController@sales_enrollee_table')->middleware('auth');
Route::get('/calapan/sales-program','CalapanController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/calapan/receivable-enrollee','CalapanController@receivable_enrollee_table')->middleware('auth');
Route::get('/calapan/receivable-program','CalapanController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/calapan/expense','CalapanController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/calapan/books','CalapanController@books_table')->middleware('auth');
Route::get('/calapan/remit', 'CalapanController@remit')->middleware('auth');
Route::get('/calapan/reservation', 'CalapanController@reservation_table')->middleware('auth');

Route::post('calapan/clear-enrollee','CalapanController@clear_enrollee')->middleware('auth');
Route::post('calapan/clear-sale-season1','CalapanController@clear_sale_season1')->middleware('auth');
Route::post('calapan/clear-sale-season2','CalapanController@clear_sale_season2')->middleware('auth');
Route::post('calapan/clear-receivable','CalapanController@clear_receivable')->middleware('auth');
Route::post('calapan/clear-expense','CalapanController@clear_expense')->middleware('auth');
Route::post('calapan/clear-book','CalapanController@clear_book')->middleware('auth');
Route::post('calapan/clear-reservation','CalapanController@clear_reservation')->middleware('auth');

/* Abra Lecturer */
Route::get('calapan/lecturer-evaluation','CalapanController@lec_eval')->middleware('auth');
Route::get('calapan/add-lecturer','CalapanController@add_lec')->middleware('auth');
Route::get('calapan/evaluate-lecturer','CalapanController@eval_lec')->middleware('auth');
Route::get('calapan/json-class','CalapanController@fetch_class')->middleware('auth');
Route::get('calapan/json-subject','CalapanController@fetch_subject')->middleware('auth');
Route::get('calapan/json-section','CalapanController@fetch_section')->middleware('auth');
Route::get('calapan/json-lecturer','CalapanController@fetch_lecturer')->middleware('auth');
Route::get('calapan/json-lecturerb','CalapanController@fetch_lecturerb')->middleware('auth');

Route::post('calapan/insert-lecturer','CalapanController@insert_lecturer')->middleware('auth');
Route::post('calapan/insert-evaluation','CalapanController@insert_eval')->middleware('auth');

Route::post('calapan/clear-lecturers','CalapanController@clear_lecturers')->middleware('auth');


/* Reports Calapan */

Route::get('calapan/today','CalapanController@today')->middleware('auth');
Route::get('calapan/yesterday','CalapanController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Calapan */

Route::get('calapan/budget','CalapanController@budget_record')->middleware('auth');
Route::post('calapan/clear-budget','CalapanController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('calapan/delete-sale/s1/{id}','CalapanController@delete_sale1');
Route::get('calapan/delete-sale/s2/{id}','CalapanController@delete_sale2');
/* End Delete */

/** csv */
Route::post('calapan/csv-enrollee','CalapanController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/calapan/add-employee','CalapanController@add_employee')->middleware('auth');
Route::get('/calapan/employee-record','CalapanController@employee_record')->middleware('auth');
Route::post('/calapan/update-employee','CalapanController@update_employee')->middleware('auth');
Route::get('/calapan/delete-employee/{id}','CalapanController@delete_employee')->middleware('auth');
Route::get('/taskHistory','CalapanController@taskHistory')->middleware('auth');

// Member
Route::get('/calapan/view-employee','CalapanController@view_employee')->middleware('auth');

// student id
Route::get('/calapan/student-id','CalapanController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\CalapanController@bulletin');
Route::get('/fetch-branches/{bn}','CalapanController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('calapan/financial-report','CalapanController@financialreport')->middleware('auth');

/** scorecard */
Route::get('calapan/scorecard-season/{season}','CalapanController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/calapan/bookTransferRecord','CalapanController@bookTransfer_table')->middleware('auth');
Route::get('/calapan/book-transfer','CalapanController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'CalapanController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'CalapanController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','CalapanController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/calapan/delete-expense/{id}','CalapanController@delete_expense')->middleware('auth');


//=============Calapan Enrollment =========

/* Data fetch */
Route::get('/calapan-enrollment/json-student','CalapanEnrollmentController@fetch_student')->middleware('auth');
Route::get('/calapan-enrollment/json-tuition','CalapanEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/calapan-enrollment/json-discount','CalapanEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/calapan-enrollment/json-balance','CalapanEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/calapan-enrollment/json-id','CalapanEnrollmentController@fetch_id')->middleware('auth');
Route::get('/calapan-enrollment/json-reserved','CalapanEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/calapan-enrollment/drop-student', 'CalapanEnrollmentController_@drop_student')->middleware('auth');
Route::post('/calapan-enrollment/insert-enrollee','CalapanEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/calapan-enrollment/add-enrollee','CalapanEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/calapan-enrollment/insert-enrollee','CalapanEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/calapan-enrollment/insert-new-payment','CalapanEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/calapan-enrollment/update-enrollee','CalapanEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','CalapanEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/calapan-enrollment/new-payment','CalapanEnrollmentController@new_payment')->middleware('auth');
Route::get('/calapan-enrollment/enrolled','CalapanEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/calapan-enrollment/sales-enrollee','CalapanEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/calapan-enrollment/dropped','CalapanEnrollmentController@dropped_table')->middleware('auth');

Route::post('/calapan-enrollment/insert-reservation','CalapanEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/calapan-enrollment/new-reservation','CalapanEnrollmentController@new_reservation')->middleware('auth');
Route::get('/calapan-enrollment/reservation', 'CalapanEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/calapan-enrollment/let','CalapanEnrollmentController@let_table')->middleware('auth');
Route::get('/calapan-enrollment/nle','CalapanEnrollmentController@nle_table')->middleware('auth');
Route::get('/calapan-enrollment/crim','CalapanEnrollmentController@crim_table')->middleware('auth');
Route::get('/calapan-enrollment/civil','CalapanEnrollmentController@civil_table')->middleware('auth');
Route::get('/calapan-enrollment/psyc','CalapanEnrollmentController@psyc_table')->middleware('auth');
Route::get('/calapan-enrollment/nclex','CalapanEnrollmentController@nclex_table')->middleware('auth');
Route::get('/calapan-enrollment/ielts','CalapanEnrollmentController@ielts_table')->middleware('auth');
Route::get('/calapan-enrollment/social','CalapanEnrollmentController@social_table')->middleware('auth');
Route::get('/calapan-enrollment/agri','CalapanEnrollmentController@agri_table')->middleware('auth');
Route::get('/calapan-enrollment/mid','CalapanEnrollmentController@mid_table')->middleware('auth');
Route::get('/calapan-enrollment/online','CalapanEnrollmentController@online_table')->middleware('auth');

//============Calapan Cashier books=================
Route::get('/calapan-cashier/books','CalapanCashierController@books_table')->middleware('auth');

Route::post('/calapan-cashier/insert-book-payment', 'CalapanCashierController@insert_book_payment')->middleware('auth');
Route::get('/calapan-cashier/book-payment','CalapanCashierController@book_payment')->middleware('auth');

Route::get('/calapan-cashier/json-book','CalapanCashierController@fetch_book')->middleware('auth');
Route::get('/calapan-cashier/json-price','CalapanCashierController@fetch_book_price')->middleware('auth');

//==========================================Calapan====================================//



//==========================================Candon====================================//
/* candon */
Route::get('/candon/dashboard','CandonController@index')->middleware('auth');
Route::get('/candon/add-enrollee','CandonController@add_enrollee')->middleware('auth');
Route::get('/candon/new-payment','CandonController@new_payment')->middleware('auth');
Route::get('/candon/new-reservation','CandonController@new_reservation')->middleware('auth');
Route::get('/candon/add-expense','CandonController@add_expense')->middleware('auth');
Route::get('/candon/add-budget','CandonController@add_budget')->middleware('auth');
Route::get('/candon/book-payment','CandonController@book_payment')->middleware('auth');
Route::get('/candon/new-remit', 'CandonController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/candon/json-student','CandonController@fetch_student')->middleware('auth');
Route::get('/candon/json-tuition','CandonController@fetch_tuition')->middleware('auth');
Route::get('/candon/json-discount','CandonController@fetch_discount')->middleware('auth');
Route::get('/candon/json-balance','CandonController@fetch_balance')->middleware('auth');
Route::get('/candon/json-expense','CandonController@fetch_expense')->middleware('auth');
Route::get('/candon/json-book','CandonController@fetch_book')->middleware('auth');
Route::get('/candon/json-price','CandonController@fetch_book_price')->middleware('auth');
Route::get('/candon/json-reserved','CandonController@fetch_reserved')->middleware('auth');
Route::get('/candon/json-id','CandonController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/candon/insert-enrollee','CandonController@insert_enrollee')->middleware('auth');
Route::post('/candon/insert-new-payment','CandonController@add_new_payment')->middleware('auth');
Route::post('/candon/insert-new-expense','CandonController@add_new_expense')->middleware('auth');
Route::post('/candon/insert-new-budget','CandonController@insert_new_budget')->middleware('auth');
Route::post('/candon/insert-book-payment', 'CandonController@insert_book_payment')->middleware('auth');
Route::post('/candon/insert-book-transfer', 'CandonController@insert_book_transfer')->middleware('auth');
Route::post('/candon/drop-student', 'CandonController_@drop_student')->middleware('auth');
Route::post('/candon/insert-remit', 'CandonController@insert_remit')->middleware('auth');
Route::post('/candon/insert-reservation','CandonController@insert_reservation')->middleware('auth');
Route::post('/candon/insert-employee','CandonController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/candon/update-enrollee','CandonController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/candon/delete-enrollee','CandonController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/candon/let','CandonController@let_table')->middleware('auth');
Route::get('/candon/nle','CandonController@nle_table')->middleware('auth');
Route::get('/candon/crim','CandonController@crim_table')->middleware('auth');
Route::get('/candon/civil','CandonController@civil_table')->middleware('auth');
Route::get('/candon/psyc','CandonController@psyc_table')->middleware('auth');
Route::get('/candon/nclex','CandonController@nclex_table')->middleware('auth');
Route::get('/candon/ielts','CandonController@ielts_table')->middleware('auth');
Route::get('/candon/social','CandonController@social_table')->middleware('auth');
Route::get('/candon/agri','CandonController@agri_table')->middleware('auth');
Route::get('/candon/mid','CandonController@mid_table')->middleware('auth');
Route::get('/candon/online','CandonController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/candon/tuition','CandonController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/candon/scholar','CandonController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/candon/enrolled','CandonController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/candon/dropped','CandonController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/candon/sales-enrollee','CandonController@sales_enrollee_table')->middleware('auth');
Route::get('/candon/sales-program','CandonController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/candon/receivable-enrollee','CandonController@receivable_enrollee_table')->middleware('auth');
Route::get('/candon/receivable-program','CandonController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/candon/expense','CandonController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/candon/books','CandonController@books_table')->middleware('auth');
Route::get('/candon/remit', 'CandonController@remit')->middleware('auth');
Route::get('/candon/reservation', 'CandonController@reservation_table')->middleware('auth');

Route::post('candon/clear-enrollee','CandonController@clear_enrollee')->middleware('auth');
Route::post('candon/clear-sale-season1','CandonController@clear_sale_season1')->middleware('auth');
Route::post('candon/clear-sale-season2','CandonController@clear_sale_season2')->middleware('auth');
Route::post('candon/clear-receivable','CandonController@clear_receivable')->middleware('auth');
Route::post('candon/clear-expense','CandonController@clear_expense')->middleware('auth');
Route::post('candon/clear-book','CandonController@clear_book')->middleware('auth');
Route::post('candon/clear-reservation','CandonController@clear_reservation')->middleware('auth');

/* Abra Lecturer */
Route::get('candon/lecturer-evaluation','CandonController@lec_eval')->middleware('auth');
Route::get('candon/add-lecturer','CandonController@add_lec')->middleware('auth');
Route::get('candon/evaluate-lecturer','CandonController@eval_lec')->middleware('auth');
Route::get('candon/json-class','CandonController@fetch_class')->middleware('auth');
Route::get('candon/json-subject','CandonController@fetch_subject')->middleware('auth');
Route::get('candon/json-section','CandonController@fetch_section')->middleware('auth');
Route::get('candon/json-lecturer','CandonController@fetch_lecturer')->middleware('auth');
Route::get('candon/json-lecturerb','CandonController@fetch_lecturerb')->middleware('auth');

Route::post('candon/insert-lecturer','CandonController@insert_lecturer')->middleware('auth');
Route::post('candon/insert-evaluation','CandonController@insert_eval')->middleware('auth');

Route::post('candon/clear-lecturers','CandonController@clear_lecturers')->middleware('auth');


/* Reports Candon */

Route::get('candon/today','CandonController@today')->middleware('auth');
Route::get('candon/yesterday','CandonController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Candon */

Route::get('candon/budget','CandonController@budget_record')->middleware('auth');
Route::post('candon/clear-budget','CandonController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('candon/delete-sale/s1/{id}','CandonController@delete_sale1');
Route::get('candon/delete-sale/s2/{id}','CandonController@delete_sale2');
/* End Delete */

/** csv */
Route::post('candon/csv-enrollee','CandonController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/candon/add-employee','CandonController@add_employee')->middleware('auth');
Route::get('/candon/employee-record','CandonController@employee_record')->middleware('auth');
Route::post('/candon/update-employee','CandonController@update_employee')->middleware('auth');
Route::get('/candon/delete-employee/{id}','CandonController@delete_employee')->middleware('auth');
Route::get('/taskHistory','CandonController@taskHistory')->middleware('auth');

// Member
Route::get('/candon/view-employee','CandonController@view_employee')->middleware('auth');

// student id
Route::get('/candon/student-id','CandonController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\CandonController@bulletin');
Route::get('/fetch-branches/{bn}','CandonController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('candon/financial-report','CandonController@financialreport')->middleware('auth');

/** scorecard */
Route::get('candon/scorecard-season/{season}','CandonController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/candon/bookTransferRecord','CandonController@bookTransfer_table')->middleware('auth');
Route::get('/candon/book-transfer','CandonController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'CandonController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'CandonController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','CandonController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/candon/delete-expense/{id}','CandonController@delete_expense')->middleware('auth');


//=============Candon Enrollment =========

/* Data fetch */
Route::get('/candon-enrollment/json-student','CandonEnrollmentController@fetch_student')->middleware('auth');
Route::get('/candon-enrollment/json-tuition','CandonEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/candon-enrollment/json-discount','CandonEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/candon-enrollment/json-balance','CandonEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/candon-enrollment/json-id','CandonEnrollmentController@fetch_id')->middleware('auth');
Route::get('/candon-enrollment/json-reserved','CandonEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/candon-enrollment/drop-student', 'CandonEnrollmentController_@drop_student')->middleware('auth');
Route::post('/candon-enrollment/insert-enrollee','CandonEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/candon-enrollment/add-enrollee','CandonEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/candon-enrollment/insert-enrollee','CandonEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/candon-enrollment/insert-new-payment','CandonEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/candon-enrollment/update-enrollee','CandonEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','CandonEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/candon-enrollment/new-payment','CandonEnrollmentController@new_payment')->middleware('auth');
Route::get('/candon-enrollment/enrolled','CandonEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/candon-enrollment/sales-enrollee','CandonEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/candon-enrollment/dropped','CandonEnrollmentController@dropped_table')->middleware('auth');

Route::post('/candon-enrollment/insert-reservation','CandonEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/candon-enrollment/new-reservation','CandonEnrollmentController@new_reservation')->middleware('auth');
Route::get('/candon-enrollment/reservation', 'CandonEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/candon-enrollment/let','CandonEnrollmentController@let_table')->middleware('auth');
Route::get('/candon-enrollment/nle','CandonEnrollmentController@nle_table')->middleware('auth');
Route::get('/candon-enrollment/crim','CandonEnrollmentController@crim_table')->middleware('auth');
Route::get('/candon-enrollment/civil','CandonEnrollmentController@civil_table')->middleware('auth');
Route::get('/candon-enrollment/psyc','CandonEnrollmentController@psyc_table')->middleware('auth');
Route::get('/candon-enrollment/nclex','CandonEnrollmentController@nclex_table')->middleware('auth');
Route::get('/candon-enrollment/ielts','CandonEnrollmentController@ielts_table')->middleware('auth');
Route::get('/candon-enrollment/social','CandonEnrollmentController@social_table')->middleware('auth');
Route::get('/candon-enrollment/agri','CandonEnrollmentController@agri_table')->middleware('auth');
Route::get('/candon-enrollment/mid','CandonEnrollmentController@mid_table')->middleware('auth');
Route::get('/candon-enrollment/online','CandonEnrollmentController@online_table')->middleware('auth');

//============Candon Cashier books=================
Route::get('/candon-cashier/books','CandonCashierController@books_table')->middleware('auth');

Route::post('/candon-cashier/insert-book-payment', 'CandonCashierController@insert_book_payment')->middleware('auth');
Route::get('/candon-cashier/book-payment','CandonCashierController@book_payment')->middleware('auth');

Route::get('/candon-cashier/json-book','CandonCashierController@fetch_book')->middleware('auth');
Route::get('/candon-cashier/json-price','CandonCashierController@fetch_book_price')->middleware('auth');

//==========================================Candon====================================//



//==========================================Dagupan====================================//
/* dagupan */
Route::get('/dagupan/dashboard','DagupanController@index')->middleware('auth');
Route::get('/dagupan/add-enrollee','DagupanController@add_enrollee')->middleware('auth');
Route::get('/dagupan/new-payment','DagupanController@new_payment')->middleware('auth');
Route::get('/dagupan/new-reservation','DagupanController@new_reservation')->middleware('auth');
Route::get('/dagupan/add-expense','DagupanController@add_expense')->middleware('auth');
Route::get('/dagupan/add-budget','DagupanController@add_budget')->middleware('auth');
Route::get('/dagupan/book-payment','DagupanController@book_payment')->middleware('auth');
Route::get('/dagupan/new-remit', 'DagupanController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/dagupan/json-student','DagupanController@fetch_student')->middleware('auth');
Route::get('/dagupan/json-tuition','DagupanController@fetch_tuition')->middleware('auth');
Route::get('/dagupan/json-discount','DagupanController@fetch_discount')->middleware('auth');
Route::get('/dagupan/json-balance','DagupanController@fetch_balance')->middleware('auth');
Route::get('/dagupan/json-expense','DagupanController@fetch_expense')->middleware('auth');
Route::get('/dagupan/json-book','DagupanController@fetch_book')->middleware('auth');
Route::get('/dagupan/json-price','DagupanController@fetch_book_price')->middleware('auth');
Route::get('/dagupan/json-reserved','DagupanController@fetch_reserved')->middleware('auth');
Route::get('/dagupan/json-id','DagupanController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/dagupan/insert-enrollee','DagupanController@insert_enrollee')->middleware('auth');
Route::post('/dagupan/insert-new-payment','DagupanController@add_new_payment')->middleware('auth');
Route::post('/dagupan/insert-new-expense','DagupanController@add_new_expense')->middleware('auth');
Route::post('/dagupan/insert-new-budget','DagupanController@insert_new_budget')->middleware('auth');
Route::post('/dagupan/insert-book-payment', 'DagupanController@insert_book_payment')->middleware('auth');
Route::post('/dagupan/insert-book-transfer', 'DagupanController@insert_book_transfer')->middleware('auth');
Route::post('/dagupan/drop-student', 'DagupanController_@drop_student')->middleware('auth');
Route::post('/dagupan/insert-remit', 'DagupanController@insert_remit')->middleware('auth');
Route::post('/dagupan/insert-reservation','DagupanController@insert_reservation')->middleware('auth');
Route::post('/dagupan/insert-employee','DagupanController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/dagupan/update-enrollee','DagupanController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/dagupan/delete-enrollee','DagupanController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/dagupan/let','DagupanController@let_table')->middleware('auth');
Route::get('/dagupan/nle','DagupanController@nle_table')->middleware('auth');
Route::get('/dagupan/crim','DagupanController@crim_table')->middleware('auth');
Route::get('/dagupan/civil','DagupanController@civil_table')->middleware('auth');
Route::get('/dagupan/psyc','DagupanController@psyc_table')->middleware('auth');
Route::get('/dagupan/nclex','DagupanController@nclex_table')->middleware('auth');
Route::get('/dagupan/ielts','DagupanController@ielts_table')->middleware('auth');
Route::get('/dagupan/social','DagupanController@social_table')->middleware('auth');
Route::get('/dagupan/agri','DagupanController@agri_table')->middleware('auth');
Route::get('/dagupan/mid','DagupanController@mid_table')->middleware('auth');
Route::get('/dagupan/online','DagupanController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/dagupan/tuition','DagupanController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/dagupan/scholar','DagupanController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/dagupan/enrolled','DagupanController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/dagupan/dropped','DagupanController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/dagupan/sales-enrollee','DagupanController@sales_enrollee_table')->middleware('auth');
Route::get('/dagupan/sales-program','DagupanController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/dagupan/receivable-enrollee','DagupanController@receivable_enrollee_table')->middleware('auth');
Route::get('/dagupan/receivable-program','DagupanController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/dagupan/expense','DagupanController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/dagupan/books','DagupanController@books_table')->middleware('auth');
Route::get('/dagupan/remit', 'DagupanController@remit')->middleware('auth');
Route::get('/dagupan/reservation', 'DagupanController@reservation_table')->middleware('auth');

Route::post('dagupan/clear-enrollee','DagupanController@clear_enrollee')->middleware('auth');
Route::post('dagupan/clear-sale-season1','DagupanController@clear_sale_season1')->middleware('auth');
Route::post('dagupan/clear-sale-season2','DagupanController@clear_sale_season2')->middleware('auth');
Route::post('dagupan/clear-receivable','DagupanController@clear_receivable')->middleware('auth');
Route::post('dagupan/clear-expense','DagupanController@clear_expense')->middleware('auth');
Route::post('dagupan/clear-book','DagupanController@clear_book')->middleware('auth');
Route::post('dagupan/clear-reservation','DagupanController@clear_reservation')->middleware('auth');

/* Abra Lecturer */
Route::get('dagupan/lecturer-evaluation','DagupanController@lec_eval')->middleware('auth');
Route::get('dagupan/add-lecturer','DagupanController@add_lec')->middleware('auth');
Route::get('dagupan/evaluate-lecturer','DagupanController@eval_lec')->middleware('auth');
Route::get('dagupan/json-class','DagupanController@fetch_class')->middleware('auth');
Route::get('dagupan/json-subject','DagupanController@fetch_subject')->middleware('auth');
Route::get('dagupan/json-section','DagupanController@fetch_section')->middleware('auth');
Route::get('dagupan/json-lecturer','DagupanController@fetch_lecturer')->middleware('auth');
Route::get('dagupan/json-lecturerb','DagupanController@fetch_lecturerb')->middleware('auth');

Route::post('dagupan/insert-lecturer','DagupanController@insert_lecturer')->middleware('auth');
Route::post('dagupan/insert-evaluation','DagupanController@insert_eval')->middleware('auth');

Route::post('dagupan/clear-lecturers','DagupanController@clear_lecturers')->middleware('auth');


/* Reports Dagupan */

Route::get('dagupan/today','DagupanController@today')->middleware('auth');
Route::get('dagupan/yesterday','DagupanController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Dagupan */

Route::get('dagupan/budget','DagupanController@budget_record')->middleware('auth');
Route::post('dagupan/clear-budget','DagupanController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('dagupan/delete-sale/s1/{id}','DagupanController@delete_sale1');
Route::get('dagupan/delete-sale/s2/{id}','DagupanController@delete_sale2');
/* End Delete */

/** csv */
Route::post('dagupan/csv-enrollee','DagupanController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/dagupan/add-employee','DagupanController@add_employee')->middleware('auth');
Route::get('/dagupan/employee-record','DagupanController@employee_record')->middleware('auth');
Route::post('/dagupan/update-employee','DagupanController@update_employee')->middleware('auth');
Route::get('/dagupan/delete-employee/{id}','DagupanController@delete_employee')->middleware('auth');
Route::get('/taskHistory','DagupanController@taskHistory')->middleware('auth');

// Member
Route::get('/dagupan/view-employee','DagupanController@view_employee')->middleware('auth');

// student id
Route::get('/dagupan/student-id','DagupanController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\DagupanController@bulletin');
Route::get('/fetch-branches/{bn}','DagupanController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('dagupan/financial-report','DagupanController@financialreport')->middleware('auth');

/** scorecard */
Route::get('dagupan/scorecard-season/{season}','DagupanController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/dagupan/bookTransferRecord','DagupanController@bookTransfer_table')->middleware('auth');
Route::get('/dagupan/book-transfer','DagupanController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'DagupanController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'DagupanController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','DagupanController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/dagupan/delete-expense/{id}','DagupanController@delete_expense')->middleware('auth');


//=============Dagupan Enrollment =========

/* Data fetch */
Route::get('/dagupan-enrollment/json-student','DagupanEnrollmentController@fetch_student')->middleware('auth');
Route::get('/dagupan-enrollment/json-tuition','DagupanEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/dagupan-enrollment/json-discount','DagupanEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/dagupan-enrollment/json-balance','DagupanEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/dagupan-enrollment/json-id','DagupanEnrollmentController@fetch_id')->middleware('auth');
Route::get('/dagupan-enrollment/json-reserved','DagupanEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/dagupan-enrollment/drop-student', 'DagupanEnrollmentController_@drop_student')->middleware('auth');
Route::post('/dagupan-enrollment/insert-enrollee','DagupanEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/dagupan-enrollment/add-enrollee','DagupanEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/dagupan-enrollment/insert-enrollee','DagupanEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/dagupan-enrollment/insert-new-payment','DagupanEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/dagupan-enrollment/update-enrollee','DagupanEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','DagupanEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/dagupan-enrollment/new-payment','DagupanEnrollmentController@new_payment')->middleware('auth');
Route::get('/dagupan-enrollment/enrolled','DagupanEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/dagupan-enrollment/sales-enrollee','DagupanEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/dagupan-enrollment/dropped','DagupanEnrollmentController@dropped_table')->middleware('auth');

Route::post('/dagupan-enrollment/insert-reservation','DagupanEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/dagupan-enrollment/new-reservation','DagupanEnrollmentController@new_reservation')->middleware('auth');
Route::get('/dagupan-enrollment/reservation', 'DagupanEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/dagupan-enrollment/let','DagupanEnrollmentController@let_table')->middleware('auth');
Route::get('/dagupan-enrollment/nle','DagupanEnrollmentController@nle_table')->middleware('auth');
Route::get('/dagupan-enrollment/crim','DagupanEnrollmentController@crim_table')->middleware('auth');
Route::get('/dagupan-enrollment/civil','DagupanEnrollmentController@civil_table')->middleware('auth');
Route::get('/dagupan-enrollment/psyc','DagupanEnrollmentController@psyc_table')->middleware('auth');
Route::get('/dagupan-enrollment/nclex','DagupanEnrollmentController@nclex_table')->middleware('auth');
Route::get('/dagupan-enrollment/ielts','DagupanEnrollmentController@ielts_table')->middleware('auth');
Route::get('/dagupan-enrollment/social','DagupanEnrollmentController@social_table')->middleware('auth');
Route::get('/dagupan-enrollment/agri','DagupanEnrollmentController@agri_table')->middleware('auth');
Route::get('/dagupan-enrollment/mid','DagupanEnrollmentController@mid_table')->middleware('auth');
Route::get('/dagupan-enrollment/online','DagupanEnrollmentController@online_table')->middleware('auth');

//============Dagupan Cashier books=================
Route::get('/dagupan-cashier/books','DagupanCashierController@books_table')->middleware('auth');

Route::post('/dagupan-cashier/insert-book-payment', 'DagupanCashierController@insert_book_payment')->middleware('auth');
Route::get('/dagupan-cashier/book-payment','DagupanCashierController@book_payment')->middleware('auth');

Route::get('/dagupan-cashier/json-book','DagupanCashierController@fetch_book')->middleware('auth');
Route::get('/dagupan-cashier/json-price','DagupanCashierController@fetch_book_price')->middleware('auth');

//==========================================Dagupan====================================//



/* Member */


//==========================================Fairview====================================//
/* fairview */
Route::get('/fairview/dashboard','FairviewController@index')->middleware('auth');
Route::get('/fairview/add-enrollee','FairviewController@add_enrollee')->middleware('auth');
Route::get('/fairview/new-payment','FairviewController@new_payment')->middleware('auth');
Route::get('/fairview/new-reservation','FairviewController@new_reservation')->middleware('auth');
Route::get('/fairview/add-expense','FairviewController@add_expense')->middleware('auth');
Route::get('/fairview/add-budget','FairviewController@add_budget')->middleware('auth');
Route::get('/fairview/book-payment','FairviewController@book_payment')->middleware('auth');
Route::get('/fairview/new-remit', 'FairviewController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/fairview/json-student','FairviewController@fetch_student')->middleware('auth');
Route::get('/fairview/json-tuition','FairviewController@fetch_tuition')->middleware('auth');
Route::get('/fairview/json-discount','FairviewController@fetch_discount')->middleware('auth');
Route::get('/fairview/json-balance','FairviewController@fetch_balance')->middleware('auth');
Route::get('/fairview/json-expense','FairviewController@fetch_expense')->middleware('auth');
Route::get('/fairview/json-book','FairviewController@fetch_book')->middleware('auth');
Route::get('/fairview/json-price','FairviewController@fetch_book_price')->middleware('auth');
Route::get('/fairview/json-reserved','FairviewController@fetch_reserved')->middleware('auth');
Route::get('/fairview/json-id','FairviewController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/fairview/insert-enrollee','FairviewController@insert_enrollee')->middleware('auth');
Route::post('/fairview/insert-new-payment','FairviewController@add_new_payment')->middleware('auth');
Route::post('/fairview/insert-new-expense','FairviewController@add_new_expense')->middleware('auth');
Route::post('/fairview/insert-new-budget','FairviewController@insert_new_budget')->middleware('auth');
Route::post('/fairview/insert-book-payment', 'FairviewController@insert_book_payment')->middleware('auth');
Route::post('/fairview/insert-book-transfer', 'FairviewController@insert_book_transfer')->middleware('auth');
Route::post('/fairview/drop-student', 'FairviewController_@drop_student')->middleware('auth');
Route::post('/fairview/insert-remit', 'FairviewController@insert_remit')->middleware('auth');
Route::post('/fairview/insert-reservation','FairviewController@insert_reservation')->middleware('auth');
Route::post('/fairview/insert-employee','FairviewController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/fairview/update-enrollee','FairviewController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/fairview/delete-enrollee','FairviewController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/fairview/let','FairviewController@let_table')->middleware('auth');
Route::get('/fairview/nle','FairviewController@nle_table')->middleware('auth');
Route::get('/fairview/crim','FairviewController@crim_table')->middleware('auth');
Route::get('/fairview/civil','FairviewController@civil_table')->middleware('auth');
Route::get('/fairview/psyc','FairviewController@psyc_table')->middleware('auth');
Route::get('/fairview/nclex','FairviewController@nclex_table')->middleware('auth');
Route::get('/fairview/ielts','FairviewController@ielts_table')->middleware('auth');
Route::get('/fairview/social','FairviewController@social_table')->middleware('auth');
Route::get('/fairview/agri','FairviewController@agri_table')->middleware('auth');
Route::get('/fairview/mid','FairviewController@mid_table')->middleware('auth');
Route::get('/fairview/online','FairviewController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/fairview/tuition','FairviewController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/fairview/scholar','FairviewController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/fairview/enrolled','FairviewController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/fairview/dropped','FairviewController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/fairview/sales-enrollee','FairviewController@sales_enrollee_table')->middleware('auth');
Route::get('/fairview/sales-program','FairviewController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/fairview/receivable-enrollee','FairviewController@receivable_enrollee_table')->middleware('auth');
Route::get('/fairview/receivable-program','FairviewController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/fairview/expense','FairviewController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/fairview/books','FairviewController@books_table')->middleware('auth');
Route::get('/fairview/remit', 'FairviewController@remit')->middleware('auth');
Route::get('/fairview/reservation', 'FairviewController@reservation_table')->middleware('auth');

Route::post('fairview/clear-enrollee','FairviewController@clear_enrollee')->middleware('auth');
Route::post('fairview/clear-sale-season1','FairviewController@clear_sale_season1')->middleware('auth');
Route::post('fairview/clear-sale-season2','FairviewController@clear_sale_season2')->middleware('auth');
Route::post('fairview/clear-receivable','FairviewController@clear_receivable')->middleware('auth');
Route::post('fairview/clear-expense','FairviewController@clear_expense')->middleware('auth');
Route::post('fairview/clear-book','FairviewController@clear_book')->middleware('auth');
Route::post('fairview/clear-reservation','FairviewController@clear_reservation')->middleware('auth');

/* Fairview Lecturer */
Route::get('fairview/lecturer-evaluation','FairviewController@lec_eval')->middleware('auth');
Route::get('fairview/add-lecturer','FairviewController@add_lec')->middleware('auth');
Route::get('fairview/evaluate-lecturer','FairviewController@eval_lec')->middleware('auth');
Route::get('fairview/json-class','FairviewController@fetch_class')->middleware('auth');
Route::get('fairview/json-subject','FairviewController@fetch_subject')->middleware('auth');
Route::get('fairview/json-section','FairviewController@fetch_section')->middleware('auth');
Route::get('fairview/json-lecturer','FairviewController@fetch_lecturer')->middleware('auth');
Route::get('fairview/json-lecturerb','FairviewController@fetch_lecturerb')->middleware('auth');

Route::post('fairview/insert-lecturer','FairviewController@insert_lecturer')->middleware('auth');
Route::post('fairview/insert-evaluation','FairviewController@insert_eval')->middleware('auth');

Route::post('fairview/clear-lecturers','FairviewController@clear_lecturers')->middleware('auth');


/* Reports Fairview */

Route::get('fairview/today','FairviewController@today')->middleware('auth');
Route::get('fairview/yesterday','FairviewController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Fairview */

Route::get('fairview/budget','FairviewController@budget_record')->middleware('auth');
Route::post('fairview/clear-budget','FairviewController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('fairview/delete-sale/s1/{id}','FairviewController@delete_sale1');
Route::get('fairview/delete-sale/s2/{id}','FairviewController@delete_sale2');
/* End Delete */

/** csv */
Route::post('fairview/csv-enrollee','FairviewController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/fairview/add-employee','FairviewController@add_employee')->middleware('auth');
Route::get('/fairview/employee-record','FairviewController@employee_record')->middleware('auth');
Route::post('/fairview/update-employee','FairviewController@update_employee')->middleware('auth');
Route::get('/fairview/delete-employee/{id}','FairviewController@delete_employee')->middleware('auth');
Route::get('/taskHistory','FairviewController@taskHistory')->middleware('auth');

// Member
Route::get('/fairview/view-employee','FairviewController@view_employee')->middleware('auth');

// student id
Route::get('/fairview/student-id','FairviewController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\FairviewController@bulletin');
Route::get('/fetch-branches/{bn}','FairviewController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('fairview/financial-report','FairviewController@financialreport')->middleware('auth');

/** scorecard */
Route::get('fairview/scorecard-season/{season}','FairviewController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/fairview/bookTransferRecord','FairviewController@bookTransfer_table')->middleware('auth');
Route::get('/fairview/book-transfer','FairviewController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'FairviewController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'FairviewController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','FairviewController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/fairview/delete-expense/{id}','FairviewController@delete_expense')->middleware('auth');


//=============Fairview Enrollment =========

/* Data fetch */
Route::get('/fairview-enrollment/json-student','FairviewEnrollmentController@fetch_student')->middleware('auth');
Route::get('/fairview-enrollment/json-tuition','FairviewEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/fairview-enrollment/json-discount','FairviewEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/fairview-enrollment/json-balance','FairviewEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/fairview-enrollment/json-id','FairviewEnrollmentController@fetch_id')->middleware('auth');
Route::get('/fairview-enrollment/json-reserved','FairviewEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/fairview-enrollment/drop-student', 'FairviewEnrollmentController_@drop_student')->middleware('auth');
Route::post('/fairview-enrollment/insert-enrollee','FairviewEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/fairview-enrollment/add-enrollee','FairviewEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/fairview-enrollment/insert-enrollee','FairviewEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/fairview-enrollment/insert-new-payment','FairviewEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/fairview-enrollment/update-enrollee','FairviewEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','FairviewEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/fairview-enrollment/new-payment','FairviewEnrollmentController@new_payment')->middleware('auth');
Route::get('/fairview-enrollment/enrolled','FairviewEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/fairview-enrollment/sales-enrollee','FairviewEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/fairview-enrollment/dropped','FairviewEnrollmentController@dropped_table')->middleware('auth');

Route::post('/fairview-enrollment/insert-reservation','FairviewEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/fairview-enrollment/new-reservation','FairviewEnrollmentController@new_reservation')->middleware('auth');
Route::get('/fairview-enrollment/reservation', 'FairviewEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/fairview-enrollment/let','FairviewEnrollmentController@let_table')->middleware('auth');
Route::get('/fairview-enrollment/nle','FairviewEnrollmentController@nle_table')->middleware('auth');
Route::get('/fairview-enrollment/crim','FairviewEnrollmentController@crim_table')->middleware('auth');
Route::get('/fairview-enrollment/civil','FairviewEnrollmentController@civil_table')->middleware('auth');
Route::get('/fairview-enrollment/psyc','FairviewEnrollmentController@psyc_table')->middleware('auth');
Route::get('/fairview-enrollment/nclex','FairviewEnrollmentController@nclex_table')->middleware('auth');
Route::get('/fairview-enrollment/ielts','FairviewEnrollmentController@ielts_table')->middleware('auth');
Route::get('/fairview-enrollment/social','FairviewEnrollmentController@social_table')->middleware('auth');
Route::get('/fairview-enrollment/agri','FairviewEnrollmentController@agri_table')->middleware('auth');
Route::get('/fairview-enrollment/mid','FairviewEnrollmentController@mid_table')->middleware('auth');
Route::get('/fairview-enrollment/online','FairviewEnrollmentController@online_table')->middleware('auth');

//============Fairview Cashier books=================
Route::get('/fairview-cashier/books','FairviewCashierController@books_table')->middleware('auth');

Route::post('/fairview-cashier/insert-book-payment', 'FairviewCashierController@insert_book_payment')->middleware('auth');
Route::get('/fairview-cashier/book-payment','FairviewCashierController@book_payment')->middleware('auth');

Route::get('/fairview-cashier/json-book','FairviewCashierController@fetch_book')->middleware('auth');
Route::get('/fairview-cashier/json-price','FairviewCashierController@fetch_book_price')->middleware('auth');

//==========================================Fairview====================================//



//==========================================LasPinas====================================//
/* las_pinas */
Route::get('/las_pinas/dashboard','LasPinasController@index')->middleware('auth');
Route::get('/las_pinas/add-enrollee','LasPinasController@add_enrollee')->middleware('auth');
Route::get('/las_pinas/new-payment','LasPinasController@new_payment')->middleware('auth');
Route::get('/las_pinas/new-reservation','LasPinasController@new_reservation')->middleware('auth');
Route::get('/las_pinas/add-expense','LasPinasController@add_expense')->middleware('auth');
Route::get('/las_pinas/add-budget','LasPinasController@add_budget')->middleware('auth');
Route::get('/las_pinas/book-payment','LasPinasController@book_payment')->middleware('auth');
Route::get('/las_pinas/new-remit', 'LasPinasController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/las_pinas/json-student','LasPinasController@fetch_student')->middleware('auth');
Route::get('/las_pinas/json-tuition','LasPinasController@fetch_tuition')->middleware('auth');
Route::get('/las_pinas/json-discount','LasPinasController@fetch_discount')->middleware('auth');
Route::get('/las_pinas/json-balance','LasPinasController@fetch_balance')->middleware('auth');
Route::get('/las_pinas/json-expense','LasPinasController@fetch_expense')->middleware('auth');
Route::get('/las_pinas/json-book','LasPinasController@fetch_book')->middleware('auth');
Route::get('/las_pinas/json-price','LasPinasController@fetch_book_price')->middleware('auth');
Route::get('/las_pinas/json-reserved','LasPinasController@fetch_reserved')->middleware('auth');
Route::get('/las_pinas/json-id','LasPinasController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/las_pinas/insert-enrollee','LasPinasController@insert_enrollee')->middleware('auth');
Route::post('/las_pinas/insert-new-payment','LasPinasController@add_new_payment')->middleware('auth');
Route::post('/las_pinas/insert-new-expense','LasPinasController@add_new_expense')->middleware('auth');
Route::post('/las_pinas/insert-new-budget','LasPinasController@insert_new_budget')->middleware('auth');
Route::post('/las_pinas/insert-book-payment', 'LasPinasController@insert_book_payment')->middleware('auth');
Route::post('/las_pinas/insert-book-transfer', 'LasPinasController@insert_book_transfer')->middleware('auth');
Route::post('/las_pinas/drop-student', 'LasPinasController_@drop_student')->middleware('auth');
Route::post('/las_pinas/insert-remit', 'LasPinasController@insert_remit')->middleware('auth');
Route::post('/las_pinas/insert-reservation','LasPinasController@insert_reservation')->middleware('auth');
Route::post('/las_pinas/insert-employee','LasPinasController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/las_pinas/update-enrollee','LasPinasController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/las_pinas/delete-enrollee','LasPinasController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/las_pinas/let','LasPinasController@let_table')->middleware('auth');
Route::get('/las_pinas/nle','LasPinasController@nle_table')->middleware('auth');
Route::get('/las_pinas/crim','LasPinasController@crim_table')->middleware('auth');
Route::get('/las_pinas/civil','LasPinasController@civil_table')->middleware('auth');
Route::get('/las_pinas/psyc','LasPinasController@psyc_table')->middleware('auth');
Route::get('/las_pinas/nclex','LasPinasController@nclex_table')->middleware('auth');
Route::get('/las_pinas/ielts','LasPinasController@ielts_table')->middleware('auth');
Route::get('/las_pinas/social','LasPinasController@social_table')->middleware('auth');
Route::get('/las_pinas/agri','LasPinasController@agri_table')->middleware('auth');
Route::get('/las_pinas/mid','LasPinasController@mid_table')->middleware('auth');
Route::get('/las_pinas/online','LasPinasController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/las_pinas/tuition','LasPinasController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/las_pinas/scholar','LasPinasController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/las_pinas/enrolled','LasPinasController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/las_pinas/dropped','LasPinasController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/las_pinas/sales-enrollee','LasPinasController@sales_enrollee_table')->middleware('auth');
Route::get('/las_pinas/sales-program','LasPinasController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/las_pinas/receivable-enrollee','LasPinasController@receivable_enrollee_table')->middleware('auth');
Route::get('/las_pinas/receivable-program','LasPinasController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/las_pinas/expense','LasPinasController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/las_pinas/books','LasPinasController@books_table')->middleware('auth');
Route::get('/las_pinas/remit', 'LasPinasController@remit')->middleware('auth');
Route::get('/las_pinas/reservation', 'LasPinasController@reservation_table')->middleware('auth');

Route::post('las_pinas/clear-enrollee','LasPinasController@clear_enrollee')->middleware('auth');
Route::post('las_pinas/clear-sale-season1','LasPinasController@clear_sale_season1')->middleware('auth');
Route::post('las_pinas/clear-sale-season2','LasPinasController@clear_sale_season2')->middleware('auth');
Route::post('las_pinas/clear-receivable','LasPinasController@clear_receivable')->middleware('auth');
Route::post('las_pinas/clear-expense','LasPinasController@clear_expense')->middleware('auth');
Route::post('las_pinas/clear-book','LasPinasController@clear_book')->middleware('auth');
Route::post('las_pinas/clear-reservation','LasPinasController@clear_reservation')->middleware('auth');

/* Fairview Lecturer */
Route::get('las_pinas/lecturer-evaluation','LasPinasController@lec_eval')->middleware('auth');
Route::get('las_pinas/add-lecturer','LasPinasController@add_lec')->middleware('auth');
Route::get('las_pinas/evaluate-lecturer','LasPinasController@eval_lec')->middleware('auth');
Route::get('las_pinas/json-class','LasPinasController@fetch_class')->middleware('auth');
Route::get('las_pinas/json-subject','LasPinasController@fetch_subject')->middleware('auth');
Route::get('las_pinas/json-section','LasPinasController@fetch_section')->middleware('auth');
Route::get('las_pinas/json-lecturer','LasPinasController@fetch_lecturer')->middleware('auth');
Route::get('las_pinas/json-lecturerb','LasPinasController@fetch_lecturerb')->middleware('auth');

Route::post('las_pinas/insert-lecturer','LasPinasController@insert_lecturer')->middleware('auth');
Route::post('las_pinas/insert-evaluation','LasPinasController@insert_eval')->middleware('auth');

Route::post('las_pinas/clear-lecturers','LasPinasController@clear_lecturers')->middleware('auth');


/* Reports LasPinas */

Route::get('las_pinas/today','LasPinasController@today')->middleware('auth');
Route::get('las_pinas/yesterday','LasPinasController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record LasPinas */

Route::get('las_pinas/budget','LasPinasController@budget_record')->middleware('auth');
Route::post('las_pinas/clear-budget','LasPinasController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('las_pinas/delete-sale/s1/{id}','LasPinasController@delete_sale1');
Route::get('las_pinas/delete-sale/s2/{id}','LasPinasController@delete_sale2');
/* End Delete */

/** csv */
Route::post('las_pinas/csv-enrollee','LasPinasController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/las_pinas/add-employee','LasPinasController@add_employee')->middleware('auth');
Route::get('/las_pinas/employee-record','LasPinasController@employee_record')->middleware('auth');
Route::post('/las_pinas/update-employee','LasPinasController@update_employee')->middleware('auth');
Route::get('/las_pinas/delete-employee/{id}','LasPinasController@delete_employee')->middleware('auth');
Route::get('/taskHistory','LasPinasController@taskHistory')->middleware('auth');

// Member
Route::get('/las_pinas/view-employee','LasPinasController@view_employee')->middleware('auth');

// student id
Route::get('/las_pinas/student-id','LasPinasController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\LasPinasController@bulletin');
Route::get('/fetch-branches/{bn}','LasPinasController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('las_pinas/financial-report','LasPinasController@financialreport')->middleware('auth');

/** scorecard */
Route::get('las_pinas/scorecard-season/{season}','LasPinasController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/las_pinas/bookTransferRecord','LasPinasController@bookTransfer_table')->middleware('auth');
Route::get('/las_pinas/book-transfer','LasPinasController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'LasPinasController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'LasPinasController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','LasPinasController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/las_pinas/delete-expense/{id}','LasPinasController@delete_expense')->middleware('auth');


//=============LasPinas Enrollment =========

/* Data fetch */
Route::get('/las_pinas-enrollment/json-student','LasPinasEnrollmentController@fetch_student')->middleware('auth');
Route::get('/las_pinas-enrollment/json-tuition','LasPinasEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/las_pinas-enrollment/json-discount','LasPinasEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/las_pinas-enrollment/json-balance','LasPinasEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/las_pinas-enrollment/json-id','LasPinasEnrollmentController@fetch_id')->middleware('auth');
Route::get('/las_pinas-enrollment/json-reserved','LasPinasEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/las_pinas-enrollment/drop-student', 'LasPinasEnrollmentController_@drop_student')->middleware('auth');
Route::post('/las_pinas-enrollment/insert-enrollee','LasPinasEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/las_pinas-enrollment/add-enrollee','LasPinasEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/las_pinas-enrollment/insert-enrollee','LasPinasEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/las_pinas-enrollment/insert-new-payment','LasPinasEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/las_pinas-enrollment/update-enrollee','LasPinasEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','LasPinasEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/las_pinas-enrollment/new-payment','LasPinasEnrollmentController@new_payment')->middleware('auth');
Route::get('/las_pinas-enrollment/enrolled','LasPinasEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/las_pinas-enrollment/sales-enrollee','LasPinasEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/las_pinas-enrollment/dropped','LasPinasEnrollmentController@dropped_table')->middleware('auth');

Route::post('/las_pinas-enrollment/insert-reservation','LasPinasEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/las_pinas-enrollment/new-reservation','LasPinasEnrollmentController@new_reservation')->middleware('auth');
Route::get('/las_pinas-enrollment/reservation', 'LasPinasEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/las_pinas-enrollment/let','LasPinasEnrollmentController@let_table')->middleware('auth');
Route::get('/las_pinas-enrollment/nle','LasPinasEnrollmentController@nle_table')->middleware('auth');
Route::get('/las_pinas-enrollment/crim','LasPinasEnrollmentController@crim_table')->middleware('auth');
Route::get('/las_pinas-enrollment/civil','LasPinasEnrollmentController@civil_table')->middleware('auth');
Route::get('/las_pinas-enrollment/psyc','LasPinasEnrollmentController@psyc_table')->middleware('auth');
Route::get('/las_pinas-enrollment/nclex','LasPinasEnrollmentController@nclex_table')->middleware('auth');
Route::get('/las_pinas-enrollment/ielts','LasPinasEnrollmentController@ielts_table')->middleware('auth');
Route::get('/las_pinas-enrollment/social','LasPinasEnrollmentController@social_table')->middleware('auth');
Route::get('/las_pinas-enrollment/agri','LasPinasEnrollmentController@agri_table')->middleware('auth');
Route::get('/las_pinas-enrollment/mid','LasPinasEnrollmentController@mid_table')->middleware('auth');
Route::get('/las_pinas-enrollment/online','LasPinasEnrollmentController@online_table')->middleware('auth');

//============LasPinas Cashier books=================
Route::get('/las_pinas-cashier/books','LasPinasCashierController@books_table')->middleware('auth');

Route::post('/las_pinas-cashier/insert-book-payment', 'LasPinasCashierController@insert_book_payment')->middleware('auth');
Route::get('/las_pinas-cashier/book-payment','LasPinasCashierController@book_payment')->middleware('auth');

Route::get('/las_pinas-cashier/json-book','LasPinasCashierController@fetch_book')->middleware('auth');
Route::get('/las_pinas-cashier/json-price','LasPinasCashierController@fetch_book_price')->middleware('auth');

//==========================================LasPinas====================================//



//==========================================Launion====================================//
/* launion */
Route::get('/launion/dashboard','LaunionController@index')->middleware('auth');
Route::get('/launion/add-enrollee','LaunionController@add_enrollee')->middleware('auth');
Route::get('/launion/new-payment','LaunionController@new_payment')->middleware('auth');
Route::get('/launion/new-reservation','LaunionController@new_reservation')->middleware('auth');
Route::get('/launion/add-expense','LaunionController@add_expense')->middleware('auth');
Route::get('/launion/add-budget','LaunionController@add_budget')->middleware('auth');
Route::get('/launion/book-payment','LaunionController@book_payment')->middleware('auth');
Route::get('/launion/new-remit', 'LaunionController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/launion/json-student','LaunionController@fetch_student')->middleware('auth');
Route::get('/launion/json-tuition','LaunionController@fetch_tuition')->middleware('auth');
Route::get('/launion/json-discount','LaunionController@fetch_discount')->middleware('auth');
Route::get('/launion/json-balance','LaunionController@fetch_balance')->middleware('auth');
Route::get('/launion/json-expense','LaunionController@fetch_expense')->middleware('auth');
Route::get('/launion/json-book','LaunionController@fetch_book')->middleware('auth');
Route::get('/launion/json-price','LaunionController@fetch_book_price')->middleware('auth');
Route::get('/launion/json-reserved','LaunionController@fetch_reserved')->middleware('auth');
Route::get('/launion/json-id','LaunionController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/launion/insert-enrollee','LaunionController@insert_enrollee')->middleware('auth');
Route::post('/launion/insert-new-payment','LaunionController@add_new_payment')->middleware('auth');
Route::post('/launion/insert-new-expense','LaunionController@add_new_expense')->middleware('auth');
Route::post('/launion/insert-new-budget','LaunionController@insert_new_budget')->middleware('auth');
Route::post('/launion/insert-book-payment', 'LaunionController@insert_book_payment')->middleware('auth');
Route::post('/launion/insert-book-transfer', 'LaunionController@insert_book_transfer')->middleware('auth');
Route::post('/launion/drop-student', 'LaunionController_@drop_student')->middleware('auth');
Route::post('/launion/insert-remit', 'LaunionController@insert_remit')->middleware('auth');
Route::post('/launion/insert-reservation','LaunionController@insert_reservation')->middleware('auth');
Route::post('/launion/insert-employee','LaunionController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/launion/update-enrollee','LaunionController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/launion/delete-enrollee','LaunionController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/launion/let','LaunionController@let_table')->middleware('auth');
Route::get('/launion/nle','LaunionController@nle_table')->middleware('auth');
Route::get('/launion/crim','LaunionController@crim_table')->middleware('auth');
Route::get('/launion/civil','LaunionController@civil_table')->middleware('auth');
Route::get('/launion/psyc','LaunionController@psyc_table')->middleware('auth');
Route::get('/launion/nclex','LaunionController@nclex_table')->middleware('auth');
Route::get('/launion/ielts','LaunionController@ielts_table')->middleware('auth');
Route::get('/launion/social','LaunionController@social_table')->middleware('auth');
Route::get('/launion/agri','LaunionController@agri_table')->middleware('auth');
Route::get('/launion/mid','LaunionController@mid_table')->middleware('auth');
Route::get('/launion/online','LaunionController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/launion/tuition','LaunionController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/launion/scholar','LaunionController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/launion/enrolled','LaunionController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/launion/dropped','LaunionController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/launion/sales-enrollee','LaunionController@sales_enrollee_table')->middleware('auth');
Route::get('/launion/sales-program','LaunionController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/launion/receivable-enrollee','LaunionController@receivable_enrollee_table')->middleware('auth');
Route::get('/launion/receivable-program','LaunionController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/launion/expense','LaunionController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/launion/books','LaunionController@books_table')->middleware('auth');
Route::get('/launion/remit', 'LaunionController@remit')->middleware('auth');
Route::get('/launion/reservation', 'LaunionController@reservation_table')->middleware('auth');

Route::post('launion/clear-enrollee','LaunionController@clear_enrollee')->middleware('auth');
Route::post('launion/clear-sale-season1','LaunionController@clear_sale_season1')->middleware('auth');
Route::post('launion/clear-sale-season2','LaunionController@clear_sale_season2')->middleware('auth');
Route::post('launion/clear-receivable','LaunionController@clear_receivable')->middleware('auth');
Route::post('launion/clear-expense','LaunionController@clear_expense')->middleware('auth');
Route::post('launion/clear-book','LaunionController@clear_book')->middleware('auth');
Route::post('launion/clear-reservation','LaunionController@clear_reservation')->middleware('auth');

/* Fairview Lecturer */
Route::get('launion/lecturer-evaluation','LaunionController@lec_eval')->middleware('auth');
Route::get('launion/add-lecturer','LaunionController@add_lec')->middleware('auth');
Route::get('launion/evaluate-lecturer','LaunionController@eval_lec')->middleware('auth');
Route::get('launion/json-class','LaunionController@fetch_class')->middleware('auth');
Route::get('launion/json-subject','LaunionController@fetch_subject')->middleware('auth');
Route::get('launion/json-section','LaunionController@fetch_section')->middleware('auth');
Route::get('launion/json-lecturer','LaunionController@fetch_lecturer')->middleware('auth');
Route::get('launion/json-lecturerb','LaunionController@fetch_lecturerb')->middleware('auth');

Route::post('launion/insert-lecturer','LaunionController@insert_lecturer')->middleware('auth');
Route::post('launion/insert-evaluation','LaunionController@insert_eval')->middleware('auth');

Route::post('launion/clear-lecturers','LaunionController@clear_lecturers')->middleware('auth');


/* Reports Launion */

Route::get('launion/today','LaunionController@today')->middleware('auth');
Route::get('launion/yesterday','LaunionController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Launion */

Route::get('launion/budget','LaunionController@budget_record')->middleware('auth');
Route::post('launion/clear-budget','LaunionController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('launion/delete-sale/s1/{id}','LaunionController@delete_sale1');
Route::get('launion/delete-sale/s2/{id}','LaunionController@delete_sale2');
/* End Delete */

/** csv */
Route::post('launion/csv-enrollee','LaunionController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/launion/add-employee','LaunionController@add_employee')->middleware('auth');
Route::get('/launion/employee-record','LaunionController@employee_record')->middleware('auth');
Route::post('/launion/update-employee','LaunionController@update_employee')->middleware('auth');
Route::get('/launion/delete-employee/{id}','LaunionController@delete_employee')->middleware('auth');
Route::get('/taskHistory','LaunionController@taskHistory')->middleware('auth');

// Member
Route::get('/launion/view-employee','LaunionController@view_employee')->middleware('auth');

// student id
Route::get('/launion/student-id','LaunionController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\LaunionController@bulletin');
Route::get('/fetch-branches/{bn}','LaunionController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('launion/financial-report','LaunionController@financialreport')->middleware('auth');

/** scorecard */
Route::get('launion/scorecard-season/{season}','LaunionController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/launion/bookTransferRecord','LaunionController@bookTransfer_table')->middleware('auth');
Route::get('/launion/book-transfer','LaunionController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'LaunionController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'LaunionController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','LaunionController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/launion/delete-expense/{id}','LaunionController@delete_expense')->middleware('auth');


//=============Launion Enrollment =========

/* Data fetch */
Route::get('/launion-enrollment/json-student','LaunionEnrollmentController@fetch_student')->middleware('auth');
Route::get('/launion-enrollment/json-tuition','LaunionEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/launion-enrollment/json-discount','LaunionEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/launion-enrollment/json-balance','LaunionEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/launion-enrollment/json-id','LaunionEnrollmentController@fetch_id')->middleware('auth');
Route::get('/launion-enrollment/json-reserved','LaunionEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/launion-enrollment/drop-student', 'LaunionEnrollmentController_@drop_student')->middleware('auth');
Route::post('/launion-enrollment/insert-enrollee','LaunionEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/launion-enrollment/add-enrollee','LaunionEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/launion-enrollment/insert-enrollee','LaunionEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/launion-enrollment/insert-new-payment','LaunionEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/launion-enrollment/update-enrollee','LaunionEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','LaunionEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/launion-enrollment/new-payment','LaunionEnrollmentController@new_payment')->middleware('auth');
Route::get('/launion-enrollment/enrolled','LaunionEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/launion-enrollment/sales-enrollee','LaunionEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/launion-enrollment/dropped','LaunionEnrollmentController@dropped_table')->middleware('auth');

Route::post('/launion-enrollment/insert-reservation','LaunionEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/launion-enrollment/new-reservation','LaunionEnrollmentController@new_reservation')->middleware('auth');
Route::get('/launion-enrollment/reservation', 'LaunionEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/launion-enrollment/let','LaunionEnrollmentController@let_table')->middleware('auth');
Route::get('/launion-enrollment/nle','LaunionEnrollmentController@nle_table')->middleware('auth');
Route::get('/launion-enrollment/crim','LaunionEnrollmentController@crim_table')->middleware('auth');
Route::get('/launion-enrollment/civil','LaunionEnrollmentController@civil_table')->middleware('auth');
Route::get('/launion-enrollment/psyc','LaunionEnrollmentController@psyc_table')->middleware('auth');
Route::get('/launion-enrollment/nclex','LaunionEnrollmentController@nclex_table')->middleware('auth');
Route::get('/launion-enrollment/ielts','LaunionEnrollmentController@ielts_table')->middleware('auth');
Route::get('/launion-enrollment/social','LaunionEnrollmentController@social_table')->middleware('auth');
Route::get('/launion-enrollment/agri','LaunionEnrollmentController@agri_table')->middleware('auth');
Route::get('/launion-enrollment/mid','LaunionEnrollmentController@mid_table')->middleware('auth');
Route::get('/launion-enrollment/online','LaunionEnrollmentController@online_table')->middleware('auth');

//============Launion Cashier books=================
Route::get('/launion-cashier/books','LaunionCashierController@books_table')->middleware('auth');

Route::post('/launion-cashier/insert-book-payment', 'LaunionCashierController@insert_book_payment')->middleware('auth');
Route::get('/launion-cashier/book-payment','LaunionCashierController@book_payment')->middleware('auth');

Route::get('/launion-cashier/json-book','LaunionCashierController@fetch_book')->middleware('auth');
Route::get('/launion-cashier/json-price','LaunionCashierController@fetch_book_price')->middleware('auth');

//==========================================Launion====================================//



//==========================================Manila====================================//
/* manila */
Route::get('/manila/dashboard','ManilaController@index')->middleware('auth');
Route::get('/manila/add-enrollee','ManilaController@add_enrollee')->middleware('auth');
Route::get('/manila/new-payment','ManilaController@new_payment')->middleware('auth');
Route::get('/manila/new-reservation','ManilaController@new_reservation')->middleware('auth');
Route::get('/manila/add-expense','ManilaController@add_expense')->middleware('auth');
Route::get('/manila/add-budget','ManilaController@add_budget')->middleware('auth');
Route::get('/manila/book-payment','ManilaController@book_payment')->middleware('auth');
Route::get('/manila/new-remit', 'ManilaController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/manila/json-student','ManilaController@fetch_student')->middleware('auth');
Route::get('/manila/json-tuition','ManilaController@fetch_tuition')->middleware('auth');
Route::get('/manila/json-discount','ManilaController@fetch_discount')->middleware('auth');
Route::get('/manila/json-balance','ManilaController@fetch_balance')->middleware('auth');
Route::get('/manila/json-expense','ManilaController@fetch_expense')->middleware('auth');
Route::get('/manila/json-book','ManilaController@fetch_book')->middleware('auth');
Route::get('/manila/json-price','ManilaController@fetch_book_price')->middleware('auth');
Route::get('/manila/json-reserved','ManilaController@fetch_reserved')->middleware('auth');
Route::get('/manila/json-id','ManilaController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/manila/insert-enrollee','ManilaController@insert_enrollee')->middleware('auth');
Route::post('/manila/insert-new-payment','ManilaController@add_new_payment')->middleware('auth');
Route::post('/manila/insert-new-expense','ManilaController@add_new_expense')->middleware('auth');
Route::post('/manila/insert-new-budget','ManilaController@insert_new_budget')->middleware('auth');
Route::post('/manila/insert-book-payment', 'ManilaController@insert_book_payment')->middleware('auth');
Route::post('/manila/insert-book-transfer', 'ManilaController@insert_book_transfer')->middleware('auth');
Route::post('/manila/drop-student', 'ManilaController_@drop_student')->middleware('auth');
Route::post('/manila/insert-remit', 'ManilaController@insert_remit')->middleware('auth');
Route::post('/manila/insert-reservation','ManilaController@insert_reservation')->middleware('auth');
Route::post('/manila/insert-employee','ManilaController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/manila/update-enrollee','ManilaController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/manila/delete-enrollee','ManilaController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/manila/let','ManilaController@let_table')->middleware('auth');
Route::get('/manila/nle','ManilaController@nle_table')->middleware('auth');
Route::get('/manila/crim','ManilaController@crim_table')->middleware('auth');
Route::get('/manila/civil','ManilaController@civil_table')->middleware('auth');
Route::get('/manila/psyc','ManilaController@psyc_table')->middleware('auth');
Route::get('/manila/nclex','ManilaController@nclex_table')->middleware('auth');
Route::get('/manila/ielts','ManilaController@ielts_table')->middleware('auth');
Route::get('/manila/social','ManilaController@social_table')->middleware('auth');
Route::get('/manila/agri','ManilaController@agri_table')->middleware('auth');
Route::get('/manila/mid','ManilaController@mid_table')->middleware('auth');
Route::get('/manila/online','ManilaController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/manila/tuition','ManilaController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/manila/scholar','ManilaController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/manila/enrolled','ManilaController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/manila/dropped','ManilaController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/manila/sales-enrollee','ManilaController@sales_enrollee_table')->middleware('auth');
Route::get('/manila/sales-program','ManilaController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/manila/receivable-enrollee','ManilaController@receivable_enrollee_table')->middleware('auth');
Route::get('/manila/receivable-program','ManilaController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/manila/expense','ManilaController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/manila/books','ManilaController@books_table')->middleware('auth');
Route::get('/manila/remit', 'ManilaController@remit')->middleware('auth');
Route::get('/manila/reservation', 'ManilaController@reservation_table')->middleware('auth');

Route::post('manila/clear-enrollee','ManilaController@clear_enrollee')->middleware('auth');
Route::post('manila/clear-sale-season1','ManilaController@clear_sale_season1')->middleware('auth');
Route::post('manila/clear-sale-season2','ManilaController@clear_sale_season2')->middleware('auth');
Route::post('manila/clear-receivable','ManilaController@clear_receivable')->middleware('auth');
Route::post('manila/clear-expense','ManilaController@clear_expense')->middleware('auth');
Route::post('manila/clear-book','ManilaController@clear_book')->middleware('auth');
Route::post('manila/clear-reservation','ManilaController@clear_reservation')->middleware('auth');

/* Fairview Lecturer */
Route::get('manila/lecturer-evaluation','ManilaController@lec_eval')->middleware('auth');
Route::get('manila/add-lecturer','ManilaController@add_lec')->middleware('auth');
Route::get('manila/evaluate-lecturer','ManilaController@eval_lec')->middleware('auth');
Route::get('manila/json-class','ManilaController@fetch_class')->middleware('auth');
Route::get('manila/json-subject','ManilaController@fetch_subject')->middleware('auth');
Route::get('manila/json-section','ManilaController@fetch_section')->middleware('auth');
Route::get('manila/json-lecturer','ManilaController@fetch_lecturer')->middleware('auth');
Route::get('manila/json-lecturerb','ManilaController@fetch_lecturerb')->middleware('auth');

Route::post('manila/insert-lecturer','ManilaController@insert_lecturer')->middleware('auth');
Route::post('manila/insert-evaluation','ManilaController@insert_eval')->middleware('auth');

Route::post('manila/clear-lecturers','ManilaController@clear_lecturers')->middleware('auth');


/* Reports Manila */

Route::get('manila/today','ManilaController@today')->middleware('auth');
Route::get('manila/yesterday','ManilaController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Manila */

Route::get('manila/budget','ManilaController@budget_record')->middleware('auth');
Route::post('manila/clear-budget','ManilaController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('manila/delete-sale/s1/{id}','ManilaController@delete_sale1');
Route::get('manila/delete-sale/s2/{id}','ManilaController@delete_sale2');
/* End Delete */

/** csv */
Route::post('manila/csv-enrollee','ManilaController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/manila/add-employee','ManilaController@add_employee')->middleware('auth');
Route::get('/manila/employee-record','ManilaController@employee_record')->middleware('auth');
Route::post('/manila/update-employee','ManilaController@update_employee')->middleware('auth');
Route::get('/manila/delete-employee/{id}','ManilaController@delete_employee')->middleware('auth');
Route::get('/taskHistory','ManilaController@taskHistory')->middleware('auth');

// Member
Route::get('/manila/view-employee','ManilaController@view_employee')->middleware('auth');

// student id
Route::get('/manila/student-id','ManilaController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\ManilaController@bulletin');
Route::get('/fetch-branches/{bn}','ManilaController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('manila/financial-report','ManilaController@financialreport')->middleware('auth');

/** scorecard */
Route::get('manila/scorecard-season/{season}','ManilaController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/manila/bookTransferRecord','ManilaController@bookTransfer_table')->middleware('auth');
Route::get('/manila/book-transfer','ManilaController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'ManilaController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'ManilaController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','ManilaController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/manila/delete-expense/{id}','ManilaController@delete_expense')->middleware('auth');


//=============Manila Enrollment =========

/* Data fetch */
Route::get('/manila-enrollment/json-student','ManilaEnrollmentController@fetch_student')->middleware('auth');
Route::get('/manila-enrollment/json-tuition','ManilaEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/manila-enrollment/json-discount','ManilaEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/manila-enrollment/json-balance','ManilaEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/manila-enrollment/json-id','ManilaEnrollmentController@fetch_id')->middleware('auth');
Route::get('/manila-enrollment/json-reserved','ManilaEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/manila-enrollment/drop-student', 'ManilaEnrollmentController_@drop_student')->middleware('auth');
Route::post('/manila-enrollment/insert-enrollee','ManilaEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/manila-enrollment/add-enrollee','ManilaEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/manila-enrollment/insert-enrollee','ManilaEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/manila-enrollment/insert-new-payment','ManilaEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/manila-enrollment/update-enrollee','ManilaEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','ManilaEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/manila-enrollment/new-payment','ManilaEnrollmentController@new_payment')->middleware('auth');
Route::get('/manila-enrollment/enrolled','ManilaEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/manila-enrollment/sales-enrollee','ManilaEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/manila-enrollment/dropped','ManilaEnrollmentController@dropped_table')->middleware('auth');

Route::post('/manila-enrollment/insert-reservation','ManilaEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/manila-enrollment/new-reservation','ManilaEnrollmentController@new_reservation')->middleware('auth');
Route::get('/manila-enrollment/reservation', 'ManilaEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/manila-enrollment/let','ManilaEnrollmentController@let_table')->middleware('auth');
Route::get('/manila-enrollment/nle','ManilaEnrollmentController@nle_table')->middleware('auth');
Route::get('/manila-enrollment/crim','ManilaEnrollmentController@crim_table')->middleware('auth');
Route::get('/manila-enrollment/civil','ManilaEnrollmentController@civil_table')->middleware('auth');
Route::get('/manila-enrollment/psyc','ManilaEnrollmentController@psyc_table')->middleware('auth');
Route::get('/manila-enrollment/nclex','ManilaEnrollmentController@nclex_table')->middleware('auth');
Route::get('/manila-enrollment/ielts','ManilaEnrollmentController@ielts_table')->middleware('auth');
Route::get('/manila-enrollment/social','ManilaEnrollmentController@social_table')->middleware('auth');
Route::get('/manila-enrollment/agri','ManilaEnrollmentController@agri_table')->middleware('auth');
Route::get('/manila-enrollment/mid','ManilaEnrollmentController@mid_table')->middleware('auth');
Route::get('/manila-enrollment/online','ManilaEnrollmentController@online_table')->middleware('auth');

//============Manila Cashier books=================
Route::get('/manila-cashier/books','ManilaCashierController@books_table')->middleware('auth');

Route::post('/manila-cashier/insert-book-payment', 'ManilaCashierController@insert_book_payment')->middleware('auth');
Route::get('/manila-cashier/book-payment','ManilaCashierController@book_payment')->middleware('auth');

Route::get('/manila-cashier/json-book','ManilaCashierController@fetch_book')->middleware('auth');
Route::get('/manila-cashier/json-price','ManilaCashierController@fetch_book_price')->middleware('auth');

//==========================================Manila====================================//



//==========================================Roxas====================================//
/* roxas */
Route::get('/roxas/dashboard','RoxasController@index')->middleware('auth');
Route::get('/roxas/add-enrollee','RoxasController@add_enrollee')->middleware('auth');
Route::get('/roxas/new-payment','RoxasController@new_payment')->middleware('auth');
Route::get('/roxas/new-reservation','RoxasController@new_reservation')->middleware('auth');
Route::get('/roxas/add-expense','RoxasController@add_expense')->middleware('auth');
Route::get('/roxas/add-budget','RoxasController@add_budget')->middleware('auth');
Route::get('/roxas/book-payment','RoxasController@book_payment')->middleware('auth');
Route::get('/roxas/new-remit', 'RoxasController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/roxas/json-student','RoxasController@fetch_student')->middleware('auth');
Route::get('/roxas/json-tuition','RoxasController@fetch_tuition')->middleware('auth');
Route::get('/roxas/json-discount','RoxasController@fetch_discount')->middleware('auth');
Route::get('/roxas/json-balance','RoxasController@fetch_balance')->middleware('auth');
Route::get('/roxas/json-expense','RoxasController@fetch_expense')->middleware('auth');
Route::get('/roxas/json-book','RoxasController@fetch_book')->middleware('auth');
Route::get('/roxas/json-price','RoxasController@fetch_book_price')->middleware('auth');
Route::get('/roxas/json-reserved','RoxasController@fetch_reserved')->middleware('auth');
Route::get('/roxas/json-id','RoxasController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/roxas/insert-enrollee','RoxasController@insert_enrollee')->middleware('auth');
Route::post('/roxas/insert-new-payment','RoxasController@add_new_payment')->middleware('auth');
Route::post('/roxas/insert-new-expense','RoxasController@add_new_expense')->middleware('auth');
Route::post('/roxas/insert-new-budget','RoxasController@insert_new_budget')->middleware('auth');
Route::post('/roxas/insert-book-payment', 'RoxasController@insert_book_payment')->middleware('auth');
Route::post('/roxas/insert-book-transfer', 'RoxasController@insert_book_transfer')->middleware('auth');
Route::post('/roxas/drop-student', 'RoxasController_@drop_student')->middleware('auth');
Route::post('/roxas/insert-remit', 'RoxasController@insert_remit')->middleware('auth');
Route::post('/roxas/insert-reservation','RoxasController@insert_reservation')->middleware('auth');
Route::post('/roxas/insert-employee','RoxasController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/roxas/update-enrollee','RoxasController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/roxas/delete-enrollee','RoxasController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/roxas/let','RoxasController@let_table')->middleware('auth');
Route::get('/roxas/nle','RoxasController@nle_table')->middleware('auth');
Route::get('/roxas/crim','RoxasController@crim_table')->middleware('auth');
Route::get('/roxas/civil','RoxasController@civil_table')->middleware('auth');
Route::get('/roxas/psyc','RoxasController@psyc_table')->middleware('auth');
Route::get('/roxas/nclex','RoxasController@nclex_table')->middleware('auth');
Route::get('/roxas/ielts','RoxasController@ielts_table')->middleware('auth');
Route::get('/roxas/social','RoxasController@social_table')->middleware('auth');
Route::get('/roxas/agri','RoxasController@agri_table')->middleware('auth');
Route::get('/roxas/mid','RoxasController@mid_table')->middleware('auth');
Route::get('/roxas/online','RoxasController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/roxas/tuition','RoxasController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/roxas/scholar','RoxasController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/roxas/enrolled','RoxasController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/roxas/dropped','RoxasController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/roxas/sales-enrollee','RoxasController@sales_enrollee_table')->middleware('auth');
Route::get('/roxas/sales-program','RoxasController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/roxas/receivable-enrollee','RoxasController@receivable_enrollee_table')->middleware('auth');
Route::get('/roxas/receivable-program','RoxasController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/roxas/expense','RoxasController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/roxas/books','RoxasController@books_table')->middleware('auth');
Route::get('/roxas/remit', 'RoxasController@remit')->middleware('auth');
Route::get('/roxas/reservation', 'RoxasController@reservation_table')->middleware('auth');

Route::post('roxas/clear-enrollee','RoxasController@clear_enrollee')->middleware('auth');
Route::post('roxas/clear-sale-season1','RoxasController@clear_sale_season1')->middleware('auth');
Route::post('roxas/clear-sale-season2','RoxasController@clear_sale_season2')->middleware('auth');
Route::post('roxas/clear-receivable','RoxasController@clear_receivable')->middleware('auth');
Route::post('roxas/clear-expense','RoxasController@clear_expense')->middleware('auth');
Route::post('roxas/clear-book','RoxasController@clear_book')->middleware('auth');
Route::post('roxas/clear-reservation','RoxasController@clear_reservation')->middleware('auth');

/* Fairview Lecturer */
Route::get('roxas/lecturer-evaluation','RoxasController@lec_eval')->middleware('auth');
Route::get('roxas/add-lecturer','RoxasController@add_lec')->middleware('auth');
Route::get('roxas/evaluate-lecturer','RoxasController@eval_lec')->middleware('auth');
Route::get('roxas/json-class','RoxasController@fetch_class')->middleware('auth');
Route::get('roxas/json-subject','RoxasController@fetch_subject')->middleware('auth');
Route::get('roxas/json-section','RoxasController@fetch_section')->middleware('auth');
Route::get('roxas/json-lecturer','RoxasController@fetch_lecturer')->middleware('auth');
Route::get('roxas/json-lecturerb','RoxasController@fetch_lecturerb')->middleware('auth');

Route::post('roxas/insert-lecturer','RoxasController@insert_lecturer')->middleware('auth');
Route::post('roxas/insert-evaluation','RoxasController@insert_eval')->middleware('auth');

Route::post('roxas/clear-lecturers','RoxasController@clear_lecturers')->middleware('auth');


/* Reports Roxas */

Route::get('roxas/today','RoxasController@today')->middleware('auth');
Route::get('roxas/yesterday','RoxasController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Roxas */

Route::get('roxas/budget','RoxasController@budget_record')->middleware('auth');
Route::post('roxas/clear-budget','RoxasController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('roxas/delete-sale/s1/{id}','RoxasController@delete_sale1');
Route::get('roxas/delete-sale/s2/{id}','RoxasController@delete_sale2');
/* End Delete */

/** csv */
Route::post('roxas/csv-enrollee','RoxasController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/roxas/add-employee','RoxasController@add_employee')->middleware('auth');
Route::get('/roxas/employee-record','RoxasController@employee_record')->middleware('auth');
Route::post('/roxas/update-employee','RoxasController@update_employee')->middleware('auth');
Route::get('/roxas/delete-employee/{id}','RoxasController@delete_employee')->middleware('auth');
Route::get('/taskHistory','RoxasController@taskHistory')->middleware('auth');

// Member
Route::get('/roxas/view-employee','RoxasController@view_employee')->middleware('auth');

// student id
Route::get('/roxas/student-id','RoxasController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\RoxasController@bulletin');
Route::get('/fetch-branches/{bn}','RoxasController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('roxas/financial-report','RoxasController@financialreport')->middleware('auth');

/** scorecard */
Route::get('roxas/scorecard-season/{season}','RoxasController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/roxas/bookTransferRecord','RoxasController@bookTransfer_table')->middleware('auth');
Route::get('/roxas/book-transfer','RoxasController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'RoxasController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'RoxasController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','RoxasController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/roxas/delete-expense/{id}','RoxasController@delete_expense')->middleware('auth');


//=============Roxas Enrollment =========

/* Data fetch */
Route::get('/roxas-enrollment/json-student','RoxasEnrollmentController@fetch_student')->middleware('auth');
Route::get('/roxas-enrollment/json-tuition','RoxasEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/roxas-enrollment/json-discount','RoxasEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/roxas-enrollment/json-balance','RoxasEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/roxas-enrollment/json-id','RoxasEnrollmentController@fetch_id')->middleware('auth');
Route::get('/roxas-enrollment/json-reserved','RoxasEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/roxas-enrollment/drop-student', 'RoxasEnrollmentController_@drop_student')->middleware('auth');
Route::post('/roxas-enrollment/insert-enrollee','RoxasEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/roxas-enrollment/add-enrollee','RoxasEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/roxas-enrollment/insert-enrollee','RoxasEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/roxas-enrollment/insert-new-payment','RoxasEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/roxas-enrollment/update-enrollee','RoxasEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','RoxasEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/roxas-enrollment/new-payment','RoxasEnrollmentController@new_payment')->middleware('auth');
Route::get('/roxas-enrollment/enrolled','RoxasEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/roxas-enrollment/sales-enrollee','RoxasEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/roxas-enrollment/dropped','RoxasEnrollmentController@dropped_table')->middleware('auth');

Route::post('/roxas-enrollment/insert-reservation','RoxasEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/roxas-enrollment/new-reservation','RoxasEnrollmentController@new_reservation')->middleware('auth');
Route::get('/roxas-enrollment/reservation', 'RoxasEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/roxas-enrollment/let','RoxasEnrollmentController@let_table')->middleware('auth');
Route::get('/roxas-enrollment/nle','RoxasEnrollmentController@nle_table')->middleware('auth');
Route::get('/roxas-enrollment/crim','RoxasEnrollmentController@crim_table')->middleware('auth');
Route::get('/roxas-enrollment/civil','RoxasEnrollmentController@civil_table')->middleware('auth');
Route::get('/roxas-enrollment/psyc','RoxasEnrollmentController@psyc_table')->middleware('auth');
Route::get('/roxas-enrollment/nclex','RoxasEnrollmentController@nclex_table')->middleware('auth');
Route::get('/roxas-enrollment/ielts','RoxasEnrollmentController@ielts_table')->middleware('auth');
Route::get('/roxas-enrollment/social','RoxasEnrollmentController@social_table')->middleware('auth');
Route::get('/roxas-enrollment/agri','RoxasEnrollmentController@agri_table')->middleware('auth');
Route::get('/roxas-enrollment/mid','RoxasEnrollmentController@mid_table')->middleware('auth');
Route::get('/roxas-enrollment/online','RoxasEnrollmentController@online_table')->middleware('auth');

//============Roxas Cashier books=================
Route::get('/roxas-cashier/books','RoxasCashierController@books_table')->middleware('auth');

Route::post('/roxas-cashier/insert-book-payment', 'RoxasCashierController@insert_book_payment')->middleware('auth');
Route::get('/roxas-cashier/book-payment','RoxasCashierController@book_payment')->middleware('auth');

Route::get('/roxas-cashier/json-book','RoxasCashierController@fetch_book')->middleware('auth');
Route::get('/roxas-cashier/json-price','RoxasCashierController@fetch_book_price')->middleware('auth');

//==========================================Roxas====================================//






//==========================================Tarlac====================================//
/* tarlac */
Route::get('/tarlac/dashboard','TarlacController@index')->middleware('auth');
Route::get('/tarlac/add-enrollee','TarlacController@add_enrollee')->middleware('auth');
Route::get('/tarlac/new-payment','TarlacController@new_payment')->middleware('auth');
Route::get('/tarlac/new-reservation','TarlacController@new_reservation')->middleware('auth');
Route::get('/tarlac/add-expense','TarlacController@add_expense')->middleware('auth');
Route::get('/tarlac/add-budget','TarlacController@add_budget')->middleware('auth');
Route::get('/tarlac/book-payment','TarlacController@book_payment')->middleware('auth');
Route::get('/tarlac/new-remit', 'TarlacController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/tarlac/json-student','TarlacController@fetch_student')->middleware('auth');
Route::get('/tarlac/json-tuition','TarlacController@fetch_tuition')->middleware('auth');
Route::get('/tarlac/json-discount','TarlacController@fetch_discount')->middleware('auth');
Route::get('/tarlac/json-balance','TarlacController@fetch_balance')->middleware('auth');
Route::get('/tarlac/json-expense','TarlacController@fetch_expense')->middleware('auth');
Route::get('/tarlac/json-book','TarlacController@fetch_book')->middleware('auth');
Route::get('/tarlac/json-price','TarlacController@fetch_book_price')->middleware('auth');
Route::get('/tarlac/json-reserved','TarlacController@fetch_reserved')->middleware('auth');
Route::get('/tarlac/json-id','TarlacController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/tarlac/insert-enrollee','TarlacController@insert_enrollee')->middleware('auth');
Route::post('/tarlac/insert-new-payment','TarlacController@add_new_payment')->middleware('auth');
Route::post('/tarlac/insert-new-expense','TarlacController@add_new_expense')->middleware('auth');
Route::post('/tarlac/insert-new-budget','TarlacController@insert_new_budget')->middleware('auth');
Route::post('/tarlac/insert-book-payment', 'TarlacController@insert_book_payment')->middleware('auth');
Route::post('/tarlac/insert-book-transfer', 'TarlacController@insert_book_transfer')->middleware('auth');
Route::post('/tarlac/drop-student', 'TarlacController_@drop_student')->middleware('auth');
Route::post('/tarlac/insert-remit', 'TarlacController@insert_remit')->middleware('auth');
Route::post('/tarlac/insert-reservation','TarlacController@insert_reservation')->middleware('auth');
Route::post('/tarlac/insert-employee','TarlacController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/tarlac/update-enrollee','TarlacController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/tarlac/delete-enrollee','TarlacController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/tarlac/let','TarlacController@let_table')->middleware('auth');
Route::get('/tarlac/nle','TarlacController@nle_table')->middleware('auth');
Route::get('/tarlac/crim','TarlacController@crim_table')->middleware('auth');
Route::get('/tarlac/civil','TarlacController@civil_table')->middleware('auth');
Route::get('/tarlac/psyc','TarlacController@psyc_table')->middleware('auth');
Route::get('/tarlac/nclex','TarlacController@nclex_table')->middleware('auth');
Route::get('/tarlac/ielts','TarlacController@ielts_table')->middleware('auth');
Route::get('/tarlac/social','TarlacController@social_table')->middleware('auth');
Route::get('/tarlac/agri','TarlacController@agri_table')->middleware('auth');
Route::get('/tarlac/mid','TarlacController@mid_table')->middleware('auth');
Route::get('/tarlac/online','TarlacController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/tarlac/tuition','TarlacController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/tarlac/scholar','TarlacController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/tarlac/enrolled','TarlacController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/tarlac/dropped','TarlacController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/tarlac/sales-enrollee','TarlacController@sales_enrollee_table')->middleware('auth');
Route::get('/tarlac/sales-program','TarlacController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/tarlac/receivable-enrollee','TarlacController@receivable_enrollee_table')->middleware('auth');
Route::get('/tarlac/receivable-program','TarlacController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/tarlac/expense','TarlacController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/tarlac/books','TarlacController@books_table')->middleware('auth');
Route::get('/tarlac/remit', 'TarlacController@remit')->middleware('auth');
Route::get('/tarlac/reservation', 'TarlacController@reservation_table')->middleware('auth');

Route::post('tarlac/clear-enrollee','TarlacController@clear_enrollee')->middleware('auth');
Route::post('tarlac/clear-sale-season1','TarlacController@clear_sale_season1')->middleware('auth');
Route::post('tarlac/clear-sale-season2','TarlacController@clear_sale_season2')->middleware('auth');
Route::post('tarlac/clear-receivable','TarlacController@clear_receivable')->middleware('auth');
Route::post('tarlac/clear-expense','TarlacController@clear_expense')->middleware('auth');
Route::post('tarlac/clear-book','TarlacController@clear_book')->middleware('auth');
Route::post('tarlac/clear-reservation','TarlacController@clear_reservation')->middleware('auth');

/* Fairview Lecturer */
Route::get('tarlac/lecturer-evaluation','TarlacController@lec_eval')->middleware('auth');
Route::get('tarlac/add-lecturer','TarlacController@add_lec')->middleware('auth');
Route::get('tarlac/evaluate-lecturer','TarlacController@eval_lec')->middleware('auth');
Route::get('tarlac/json-class','TarlacController@fetch_class')->middleware('auth');
Route::get('tarlac/json-subject','TarlacController@fetch_subject')->middleware('auth');
Route::get('tarlac/json-section','TarlacController@fetch_section')->middleware('auth');
Route::get('tarlac/json-lecturer','TarlacController@fetch_lecturer')->middleware('auth');
Route::get('tarlac/json-lecturerb','TarlacController@fetch_lecturerb')->middleware('auth');

Route::post('tarlac/insert-lecturer','TarlacController@insert_lecturer')->middleware('auth');
Route::post('tarlac/insert-evaluation','TarlacController@insert_eval')->middleware('auth');

Route::post('tarlac/clear-lecturers','TarlacController@clear_lecturers')->middleware('auth');


/* Reports Tarlac */

Route::get('tarlac/today','TarlacController@today')->middleware('auth');
Route::get('tarlac/yesterday','TarlacController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Tarlac */

Route::get('tarlac/budget','TarlacController@budget_record')->middleware('auth');
Route::post('tarlac/clear-budget','TarlacController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('tarlac/delete-sale/s1/{id}','TarlacController@delete_sale1');
Route::get('tarlac/delete-sale/s2/{id}','TarlacController@delete_sale2');
/* End Delete */

/** csv */
Route::post('tarlac/csv-enrollee','TarlacController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/tarlac/add-employee','TarlacController@add_employee')->middleware('auth');
Route::get('/tarlac/employee-record','TarlacController@employee_record')->middleware('auth');
Route::post('/tarlac/update-employee','TarlacController@update_employee')->middleware('auth');
Route::get('/tarlac/delete-employee/{id}','TarlacController@delete_employee')->middleware('auth');
Route::get('/taskHistory','TarlacController@taskHistory')->middleware('auth');

// Member
Route::get('/tarlac/view-employee','TarlacController@view_employee')->middleware('auth');

// student id
Route::get('/tarlac/student-id','TarlacController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\TarlacController@bulletin');
Route::get('/fetch-branches/{bn}','TarlacController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('tarlac/financial-report','TarlacController@financialreport')->middleware('auth');

/** scorecard */
Route::get('tarlac/scorecard-season/{season}','TarlacController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/tarlac/bookTransferRecord','TarlacController@bookTransfer_table')->middleware('auth');
Route::get('/tarlac/book-transfer','TarlacController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'TarlacController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'TarlacController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','TarlacController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/tarlac/delete-expense/{id}','TarlacController@delete_expense')->middleware('auth');


//=============Tarlac Enrollment =========

/* Data fetch */
Route::get('/tarlac-enrollment/json-student','TarlacEnrollmentController@fetch_student')->middleware('auth');
Route::get('/tarlac-enrollment/json-tuition','TarlacEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/tarlac-enrollment/json-discount','TarlacEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/tarlac-enrollment/json-balance','TarlacEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/tarlac-enrollment/json-id','TarlacEnrollmentController@fetch_id')->middleware('auth');
Route::get('/tarlac-enrollment/json-reserved','TarlacEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/tarlac-enrollment/drop-student', 'TarlacEnrollmentController_@drop_student')->middleware('auth');
Route::post('/tarlac-enrollment/insert-enrollee','TarlacEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/tarlac-enrollment/add-enrollee','TarlacEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/tarlac-enrollment/insert-enrollee','TarlacEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/tarlac-enrollment/insert-new-payment','TarlacEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/tarlac-enrollment/update-enrollee','TarlacEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','TarlacEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/tarlac-enrollment/new-payment','TarlacEnrollmentController@new_payment')->middleware('auth');
Route::get('/tarlac-enrollment/enrolled','TarlacEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/tarlac-enrollment/sales-enrollee','TarlacEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/tarlac-enrollment/dropped','TarlacEnrollmentController@dropped_table')->middleware('auth');

Route::post('/tarlac-enrollment/insert-reservation','TarlacEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/tarlac-enrollment/new-reservation','TarlacEnrollmentController@new_reservation')->middleware('auth');
Route::get('/tarlac-enrollment/reservation', 'TarlacEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/tarlac-enrollment/let','TarlacEnrollmentController@let_table')->middleware('auth');
Route::get('/tarlac-enrollment/nle','TarlacEnrollmentController@nle_table')->middleware('auth');
Route::get('/tarlac-enrollment/crim','TarlacEnrollmentController@crim_table')->middleware('auth');
Route::get('/tarlac-enrollment/civil','TarlacEnrollmentController@civil_table')->middleware('auth');
Route::get('/tarlac-enrollment/psyc','TarlacEnrollmentController@psyc_table')->middleware('auth');
Route::get('/tarlac-enrollment/nclex','TarlacEnrollmentController@nclex_table')->middleware('auth');
Route::get('/tarlac-enrollment/ielts','TarlacEnrollmentController@ielts_table')->middleware('auth');
Route::get('/tarlac-enrollment/social','TarlacEnrollmentController@social_table')->middleware('auth');
Route::get('/tarlac-enrollment/agri','TarlacEnrollmentController@agri_table')->middleware('auth');
Route::get('/tarlac-enrollment/mid','TarlacEnrollmentController@mid_table')->middleware('auth');
Route::get('/tarlac-enrollment/online','TarlacEnrollmentController@online_table')->middleware('auth');

//============Tarlac Cashier books=================
Route::get('/tarlac-cashier/books','TarlacCashierController@books_table')->middleware('auth');

Route::post('/tarlac-cashier/insert-book-payment', 'TarlacCashierController@insert_book_payment')->middleware('auth');
Route::get('/tarlac-cashier/book-payment','TarlacCashierController@book_payment')->middleware('auth');

Route::get('/tarlac-cashier/json-book','TarlacCashierController@fetch_book')->middleware('auth');
Route::get('/tarlac-cashier/json-price','TarlacCashierController@fetch_book_price')->middleware('auth');

//==========================================Tarlac====================================//



//==========================================Vigan====================================//
/* vigan */
Route::get('/vigan/dashboard','ViganController@index')->middleware('auth');
Route::get('/vigan/add-enrollee','ViganController@add_enrollee')->middleware('auth');
Route::get('/vigan/new-payment','ViganController@new_payment')->middleware('auth');
Route::get('/vigan/new-reservation','ViganController@new_reservation')->middleware('auth');
Route::get('/vigan/add-expense','ViganController@add_expense')->middleware('auth');
Route::get('/vigan/add-budget','ViganController@add_budget')->middleware('auth');
Route::get('/vigan/book-payment','ViganController@book_payment')->middleware('auth');
Route::get('/vigan/new-remit', 'ViganController@new_remit')->middleware('auth');



/* Data fetch */
Route::get('/vigan/json-student','ViganController@fetch_student')->middleware('auth');
Route::get('/vigan/json-tuition','ViganController@fetch_tuition')->middleware('auth');
Route::get('/vigan/json-discount','ViganController@fetch_discount')->middleware('auth');
Route::get('/vigan/json-balance','ViganController@fetch_balance')->middleware('auth');
Route::get('/vigan/json-expense','ViganController@fetch_expense')->middleware('auth');
Route::get('/vigan/json-book','ViganController@fetch_book')->middleware('auth');
Route::get('/vigan/json-price','ViganController@fetch_book_price')->middleware('auth');
Route::get('/vigan/json-reserved','ViganController@fetch_reserved')->middleware('auth');
Route::get('/vigan/json-id','ViganController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/vigan/insert-enrollee','ViganController@insert_enrollee')->middleware('auth');
Route::post('/vigan/insert-new-payment','ViganController@add_new_payment')->middleware('auth');
Route::post('/vigan/insert-new-expense','ViganController@add_new_expense')->middleware('auth');
Route::post('/vigan/insert-new-budget','ViganController@insert_new_budget')->middleware('auth');
Route::post('/vigan/insert-book-payment', 'ViganController@insert_book_payment')->middleware('auth');
Route::post('/vigan/insert-book-transfer', 'ViganController@insert_book_transfer')->middleware('auth');
Route::post('/vigan/drop-student', 'ViganController_@drop_student')->middleware('auth');
Route::post('/vigan/insert-remit', 'ViganController@insert_remit')->middleware('auth');
Route::post('/vigan/insert-reservation','ViganController@insert_reservation')->middleware('auth');
Route::post('/vigan/insert-employee','ViganController@insert_employee')->middleware('auth');


/* Update Data */
Route::post('/vigan/update-enrollee','ViganController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/vigan/delete-enrollee','ViganController@delete_enrollee')->middleware('auth');
/* Tables */

	/* Enrollee tables */
Route::get('/vigan/let','ViganController@let_table')->middleware('auth');
Route::get('/vigan/nle','ViganController@nle_table')->middleware('auth');
Route::get('/vigan/crim','ViganController@crim_table')->middleware('auth');
Route::get('/vigan/civil','ViganController@civil_table')->middleware('auth');
Route::get('/vigan/psyc','ViganController@psyc_table')->middleware('auth');
Route::get('/vigan/nclex','ViganController@nclex_table')->middleware('auth');
Route::get('/vigan/ielts','ViganController@ielts_table')->middleware('auth');
Route::get('/vigan/social','ViganController@social_table')->middleware('auth');
Route::get('/vigan/agri','ViganController@agri_table')->middleware('auth');
Route::get('/vigan/mid','ViganController@mid_table')->middleware('auth');
Route::get('/vigan/online','ViganController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/vigan/tuition','ViganController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/vigan/scholar','ViganController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/vigan/enrolled','ViganController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/vigan/dropped','ViganController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/vigan/sales-enrollee','ViganController@sales_enrollee_table')->middleware('auth');
Route::get('/vigan/sales-program','ViganController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/vigan/receivable-enrollee','ViganController@receivable_enrollee_table')->middleware('auth');
Route::get('/vigan/receivable-program','ViganController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/vigan/expense','ViganController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/vigan/books','ViganController@books_table')->middleware('auth');
Route::get('/vigan/remit', 'ViganController@remit')->middleware('auth');
Route::get('/vigan/reservation', 'ViganController@reservation_table')->middleware('auth');

Route::post('vigan/clear-enrollee','ViganController@clear_enrollee')->middleware('auth');
Route::post('vigan/clear-sale-season1','ViganController@clear_sale_season1')->middleware('auth');
Route::post('vigan/clear-sale-season2','ViganController@clear_sale_season2')->middleware('auth');
Route::post('vigan/clear-receivable','ViganController@clear_receivable')->middleware('auth');
Route::post('vigan/clear-expense','ViganController@clear_expense')->middleware('auth');
Route::post('vigan/clear-book','ViganController@clear_book')->middleware('auth');
Route::post('vigan/clear-reservation','ViganController@clear_reservation')->middleware('auth');

/* Fairview Lecturer */
Route::get('vigan/lecturer-evaluation','ViganController@lec_eval')->middleware('auth');
Route::get('vigan/add-lecturer','ViganController@add_lec')->middleware('auth');
Route::get('vigan/evaluate-lecturer','ViganController@eval_lec')->middleware('auth');
Route::get('vigan/json-class','ViganController@fetch_class')->middleware('auth');
Route::get('vigan/json-subject','ViganController@fetch_subject')->middleware('auth');
Route::get('vigan/json-section','ViganController@fetch_section')->middleware('auth');
Route::get('vigan/json-lecturer','ViganController@fetch_lecturer')->middleware('auth');
Route::get('vigan/json-lecturerb','ViganController@fetch_lecturerb')->middleware('auth');

Route::post('vigan/insert-lecturer','ViganController@insert_lecturer')->middleware('auth');
Route::post('vigan/insert-evaluation','ViganController@insert_eval')->middleware('auth');

Route::post('vigan/clear-lecturers','ViganController@clear_lecturers')->middleware('auth');


/* Reports Vigan */

Route::get('vigan/today','ViganController@today')->middleware('auth');
Route::get('vigan/yesterday','ViganController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Vigan */

Route::get('vigan/budget','ViganController@budget_record')->middleware('auth');
Route::post('vigan/clear-budget','ViganController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('vigan/delete-sale/s1/{id}','ViganController@delete_sale1');
Route::get('vigan/delete-sale/s2/{id}','ViganController@delete_sale2');
/* End Delete */

/** csv */
Route::post('vigan/csv-enrollee','ViganController@csv_enrollee')->middleware('auth');

/* employee */

Route::get('/vigan/add-employee','ViganController@add_employee')->middleware('auth');
Route::get('/vigan/employee-record','ViganController@employee_record')->middleware('auth');
Route::post('/vigan/update-employee','ViganController@update_employee')->middleware('auth');
Route::get('/vigan/delete-employee/{id}','ViganController@delete_employee')->middleware('auth');
Route::get('/taskHistory','ViganController@taskHistory')->middleware('auth');

// Member
Route::get('/vigan/view-employee','ViganController@view_employee')->middleware('auth');

// student id
Route::get('/vigan/student-id','ViganController@student_id')->middleware('auth');

// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\ViganController@bulletin');
Route::get('/fetch-branches/{bn}','ViganController@fetch_branches')->middleware('auth');

// financialreport 
Route::get('vigan/financial-report','ViganController@financialreport')->middleware('auth');

/** scorecard */
Route::get('vigan/scorecard-season/{season}','ViganController@scorecard')->middleware('auth');

/* check username and email if exist*/
Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

Route::get('/vigan/bookTransferRecord','ViganController@bookTransfer_table')->middleware('auth');
Route::get('/vigan/book-transfer','ViganController@book_transfer')->middleware('auth');


Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


Route::get('/radashboard/viewLecturerReports', 'ViganController@lecturerView');
Route::get('/radashboard/onlineCompletion', 'ViganController@onlineCompletion');
Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');


Route::get('/fetch-book-list/{id}','ViganController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/vigan/delete-expense/{id}','ViganController@delete_expense')->middleware('auth');


//=============Vigan Enrollment =========

/* Data fetch */
Route::get('/vigan-enrollment/json-student','ViganEnrollmentController@fetch_student')->middleware('auth');
Route::get('/vigan-enrollment/json-tuition','ViganEnrollmentController@fetch_tuition')->middleware('auth');
Route::get('/vigan-enrollment/json-discount','ViganEnrollmentController@fetch_discount')->middleware('auth');
Route::get('/vigan-enrollment/json-balance','ViganEnrollmentController@fetch_balance')->middleware('auth');
Route::get('/vigan-enrollment/json-id','ViganEnrollmentController@fetch_id')->middleware('auth');
Route::get('/vigan-enrollment/json-reserved','ViganEnrollmentController@fetch_reserved')->middleware('auth');

/* Add Data */
Route::post('/vigan-enrollment/drop-student', 'ViganEnrollmentController_@drop_student')->middleware('auth');
Route::post('/vigan-enrollment/insert-enrollee','ViganEnrollmentController@insert_enrollee')->middleware('auth');

Route::get('/vigan-enrollment/add-enrollee','ViganEnrollmentController@add_enrollee')->middleware('auth');
Route::post('/vigan-enrollment/insert-enrollee','ViganEnrollmentController@insert_enrollee')->middleware('auth');
Route::post('/vigan-enrollment/insert-new-payment','ViganEnrollmentController@add_new_payment')->middleware('auth');
Route::post('/vigan-enrollment/update-enrollee','ViganEnrollmentController@update_enrollee')->middleware('auth');
Route::post('enrollment-csv-enrollee','ViganEnrollmentController@csv_enrollee')->middleware('auth');

Route::get('/vigan-enrollment/new-payment','ViganEnrollmentController@new_payment')->middleware('auth');
Route::get('/vigan-enrollment/enrolled','ViganEnrollmentController@enrolled_table')->middleware('auth');
Route::get('/vigan-enrollment/sales-enrollee','ViganEnrollmentController@sales_enrollee_table')->middleware('auth');
Route::get('/vigan-enrollment/dropped','ViganEnrollmentController@dropped_table')->middleware('auth');

Route::post('/vigan-enrollment/insert-reservation','ViganEnrollmentController@insert_reservation')->middleware('auth');
Route::get('/vigan-enrollment/new-reservation','ViganEnrollmentController@new_reservation')->middleware('auth');
Route::get('/vigan-enrollment/reservation', 'ViganEnrollmentController@reservation_table')->middleware('auth');


/* Enrollee tables */
Route::get('/vigan-enrollment/let','ViganEnrollmentController@let_table')->middleware('auth');
Route::get('/vigan-enrollment/nle','ViganEnrollmentController@nle_table')->middleware('auth');
Route::get('/vigan-enrollment/crim','ViganEnrollmentController@crim_table')->middleware('auth');
Route::get('/vigan-enrollment/civil','ViganEnrollmentController@civil_table')->middleware('auth');
Route::get('/vigan-enrollment/psyc','ViganEnrollmentController@psyc_table')->middleware('auth');
Route::get('/vigan-enrollment/nclex','ViganEnrollmentController@nclex_table')->middleware('auth');
Route::get('/vigan-enrollment/ielts','ViganEnrollmentController@ielts_table')->middleware('auth');
Route::get('/vigan-enrollment/social','ViganEnrollmentController@social_table')->middleware('auth');
Route::get('/vigan-enrollment/agri','ViganEnrollmentController@agri_table')->middleware('auth');
Route::get('/vigan-enrollment/mid','ViganEnrollmentController@mid_table')->middleware('auth');
Route::get('/vigan-enrollment/online','ViganEnrollmentController@online_table')->middleware('auth');

//============Vigan Cashier books=================
Route::get('/vigan-cashier/books','ViganCashierController@books_table')->middleware('auth');

Route::post('/vigan-cashier/insert-book-payment', 'ViganCashierController@insert_book_payment')->middleware('auth');
Route::get('/vigan-cashier/book-payment','ViganCashierController@book_payment')->middleware('auth');

Route::get('/vigan-cashier/json-book','ViganCashierController@fetch_book')->middleware('auth');
Route::get('/vigan-cashier/json-price','ViganCashierController@fetch_book_price')->middleware('auth');

//==========================================Vigan====================================//







Route::get('500', function()
{
	abort(500);
});

