<?php


Route::get('/', function () {
    return view('auth.login');
});

/* START DAET */
/* App */
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/user/activation/{token}', 'Auth\RegisterController@userActivation');

Route::get('cbrc/online-registration','RegistrationController@index');

Route::post('new-register','RegistrationController@register');

Route::get('thank-you','RegistrationController@thank_you');

Route::get('/json-major','MainController@fetch_major');


/* Admin */
Route::get('/dashboard','AdminController@dashboard')->middleware('auth');
Route::get('/total-program','AdminController@total_program')->middleware('auth');
Route::get('/total-agri','AdminController@total_agri')->middleware('auth');
Route::get('/total-civil','AdminController@total_civil')->middleware('auth');
Route::get('/total-crim','AdminController@total_crim')->middleware('auth');
Route::get('/total-ielts','AdminController@total_ielts')->middleware('auth');
Route::get('/total-let','AdminController@total_let')->middleware('auth');
Route::get('/total-nclex','AdminController@total_nclex')->middleware('auth');
Route::get('/total-nle','AdminController@total_nle')->middleware('auth');
Route::get('/total-psyc','AdminController@total_psyc')->middleware('auth');
Route::get('/total-mid','AdminController@total_mid')->middleware('auth');
Route::get('/total-online','AdminController@total_online')->middleware('auth');
Route::get('/total-social','AdminController@total_social')->middleware('auth');
Route::get('/total-dropped','AdminController@total_dropped')->middleware('auth');
Route::get('/total-scholars','AdminController@total_scholars')->middleware('auth');
Route::get('/tuition-fees','AdminController@tuition_fees')->middleware('auth');
Route::get('/discounts','AdminController@discounts')->middleware('auth');
Route::get('/total-books','AdminController@total_books')->middleware('auth');
Route::get('/total-expense','AdminController@total_expense')->middleware('auth');
Route::get('/users','AdminController@user')->middleware('auth');


/* Add Data */
Route::post('/insert-tuition','AdminController@insert_tuition')->middleware('auth');
Route::post('/insert-discount','AdminController@insert_discount')->middleware('auth');
Route::post('/insert-book','AdminController@insert_book')->middleware('auth');
Route::post('/insert-user','AdminController@insert_user')->middleware('auth');

/* Update Data */
Route::post('/update-tuition','AdminController@update_tuition')->middleware('auth');
Route::post('/update-discount','AdminController@update_discount')->middleware('auth');
Route::post('/update-book','AdminController@update_book')->middleware('auth');
Route::post('/update-user','AdminController@update_user')->middleware('auth');

/* Delete Data */
Route::post('/delete-tuition','AdminController@delete_tuition')->middleware('auth');
Route::post('/delete-discount','AdminController@delete_discount')->middleware('auth');
Route::post('/delete-book','AdminController@delete_book')->middleware('auth');
Route::delete('/delete-user','AdminController@delete_user')->middleware('auth');

/* Member */
/* Novaliches */
Route::get('/novaliches/dashboard','NovalichesController@index')->middleware('auth');
Route::get('/novaliches/add-enrollee','NovalichesController@add_enrollee')->middleware('auth');
Route::get('/novaliches/new-payment','NovalichesController@new_payment')->middleware('auth');
Route::get('/novaliches/new-reservation','NovalichesController@new_reservation')->middleware('auth');
Route::get('/novaliches/add-expense','NovalichesController@add_expense')->middleware('auth');
Route::get('/novaliches/add-budget','NovalichesController@add_budget')->middleware('auth');
Route::get('/novaliches/book-payment','NovalichesController@book_payment')->middleware('auth');
Route::get('/novaliches/new-remit', 'NovalichesController@new_remit')->middleware('auth');


/* Data fetch */
Route::get('/novaliches/json-student','NovalichesController@fetch_student')->middleware('auth');
Route::get('/novaliches/json-tuition','NovalichesController@fetch_tuition')->middleware('auth');
Route::get('/novaliches/json-discount','NovalichesController@fetch_discount')->middleware('auth');
Route::get('/novaliches/json-balance','NovalichesController@fetch_balance')->middleware('auth');
Route::get('/novaliches/json-expense','NovalichesController@fetch_expense')->middleware('auth');
Route::get('/novaliches/json-book','NovalichesController@fetch_book')->middleware('auth');
Route::get('/novaliches/json-price','NovalichesController@fetch_book_price')->middleware('auth');
Route::get('/novaliches/json-reserved','NovalichesController@fetch_reserved')->middleware('auth');
Route::get('/novaliches/json-id','NovalichesController@fetch_id')->middleware('auth');

/* Add Data */
Route::post('/novaliches/insert-enrollee','NovalichesController@insert_enrollee')->middleware('auth');
Route::post('/novaliches/insert-new-payment','NovalichesController@add_new_payment')->middleware('auth');
Route::post('/novaliches/insert-new-expense','NovalichesController@add_new_expense')->middleware('auth');
Route::post('/novaliches/insert-new-budget','NovalichesController@insert_new_budget')->middleware('auth');
Route::post('/novaliches/insert-book-payment', 'NovalichesController@insert_book_payment')->middleware('auth');
Route::post('/novaliches/drop-student', 'NovalichesController@drop_student')->middleware('auth');
Route::post('/novaliches/insert-remit', 'NovalichesController@insert_remit')->middleware('auth');
Route::post('/novaliches/insert-reservation','NovalichesController@insert_reservation')->middleware('auth');


/* Update Data */
Route::post('/novaliches/update-enrollee','NovalichesController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/novaliches/delete-enrollee','NovalichesController@delete_enrollee')->middleware('auth');

/* Tables */

	/* Enrollee tables */
Route::get('/novaliches/let','NovalichesController@let_table')->middleware('auth');
Route::get('/novaliches/nle','NovalichesController@nle_table')->middleware('auth');
Route::get('/novaliches/crim','NovalichesController@crim_table')->middleware('auth');
Route::get('/novaliches/civil','NovalichesController@civil_table')->middleware('auth');
Route::get('/novaliches/psyc','NovalichesController@psyc_table')->middleware('auth');
Route::get('/novaliches/nclex','NovalichesController@nclex_table')->middleware('auth');
Route::get('/novaliches/ielts','NovalichesController@ielts_table')->middleware('auth');
Route::get('/novaliches/social','NovalichesController@social_table')->middleware('auth');
Route::get('/novaliches/agri','NovalichesController@agri_table')->middleware('auth');
Route::get('/novaliches/mid','NovalichesController@mid_table')->middleware('auth');
Route::get('/novaliches/online','NovalichesController@online_table')->middleware('auth');
	/* Tuition and Discounts table */
Route::get('/novaliches/tuition','NovalichesController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/novaliches/scholar','NovalichesController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/novaliches/enrolled','NovalichesController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/novaliches/dropped','NovalichesController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/novaliches/sales-enrollee','NovalichesController@sales_enrollee_table')->middleware('auth');
Route::get('/novaliches/sales-program','NovalichesController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/novaliches/receivable-enrollee','NovalichesController@receivable_enrollee_table')->middleware('auth');
Route::get('/novaliches/receivable-program','NovalichesController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/novaliches/expense','NovalichesController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/novaliches/books','NovalichesController@books_table')->middleware('auth');
Route::get('/novaliches/remit', 'NovalichesController@remit')->middleware('auth');
Route::get('/novaliches/reservation', 'NovalichesController@reservation_table')->middleware('auth');

Route::post('novaliches/clear-enrollee','NovalichesController@clear_enrollee')->middleware('auth');
Route::post('novaliches/clear-sale-season1','NovalichesController@clear_sale_season1')->middleware('auth');
Route::post('novaliches/clear-sale-season2','NovalichesController@clear_sale_season2')->middleware('auth');
Route::post('novaliches/clear-receivable','NovalichesController@clear_receivable')->middleware('auth');
Route::post('novaliches/clear-expense','NovalichesController@clear_expense')->middleware('auth');
Route::post('novaliches/clear-book','NovalichesController@clear_book')->middleware('auth');
Route::post('novaliches/clear-reservation','NovalichesController@clear_reservation')->middleware('auth');

/* Daet Lecturer */
Route::get('novaliches/lecturer-evaluation','NovalichesController@lec_eval')->middleware('auth');
Route::get('novaliches/add-lecturer','NovalichesController@add_lec')->middleware('auth');
Route::get('novaliches/evaluate-lecturer','NovalichesController@eval_lec')->middleware('auth');

Route::get('novaliches/json-class','NovalichesController@fetch_class')->middleware('auth');
Route::get('novaliches/json-subject','NovalichesController@fetch_subject')->middleware('auth');
Route::get('novaliches/json-section','NovalichesController@fetch_section')->middleware('auth');
Route::get('novaliches/json-lecturer','NovalichesController@fetch_lecturer')->middleware('auth');
Route::get('novaliches/json-lecturerb','NovalichesController@fetch_lecturerb')->middleware('auth');

Route::post('novaliches/insert-lecturer','NovalichesController@insert_lecturer')->middleware('auth');
Route::post('novaliches/insert-evaluation','NovalichesController@insert_eval')->middleware('auth');

Route::post('novaliches/clear-lecturers','NovalichesController@clear_lecturers')->middleware('auth');


/* Reports Novaliches */

Route::get('novaliches/today','NovalichesController@today')->middleware('auth');
Route::get('novaliches/yesterday','NovalichesController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Novaliches */

Route::get('novaliches/budget','NovalichesController@budget_record')->middleware('auth');
Route::post('novaliches/clear-budget','NovalichesController@clear_budget')->middleware('auth');
/* End Budger Record */

/* Delete Sales */
Route::get('novaliches/delete-sale/s1/{id}','NovalichesController@delete_sale1');
Route::get('novaliches/delete-sale/s2/{id}','NovalichesController@delete_sale2');
/* End Delete */

/* CSV Upload */

Route::post('novaliches/csv-enrollee','NovalichesController@csv_enrollee')->middleware('auth');

/* End CSV Upload */

/* END Novaliches */
/* Member */

/* Errors */

Route::get('500', function()
{
    abort(500);
});

