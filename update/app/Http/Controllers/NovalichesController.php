<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Model\Novaliches\NovalichesAgri;
use App\Model\Novaliches\NovalichesBookCash;
use App\Model\Novaliches\NovalichesBooksInventorie;
use App\Model\Novaliches\NovalichesBooksSale;
use App\Model\Novaliches\NovalichesBudget;
use App\Model\Novaliches\NovalichesCivil;
use App\Model\Novaliches\NovalichesCrim;
use App\Model\Novaliches\NovalichesDiscount;
use App\Model\Novaliches\NovalichesDropped;
use App\Model\Novaliches\NovalichesExpense;
use App\Model\Novaliches\NovalichesIelt;
use App\Model\Novaliches\NovalichesLet;
use App\Model\Novaliches\NovalichesMid;
use App\Model\Novaliches\NovalichesNclex;
use App\Model\Novaliches\NovalichesNle;
use App\Model\Novaliches\NovalichesOnline;
use App\Model\Novaliches\NovalichesPsyc;
use App\Model\Novaliches\NovalichesReceivable;
use App\Model\Novaliches\NovalichesS1Sale;
use App\Model\Novaliches\NovalichesS2Sale;
use App\Model\Novaliches\NovalichesS1Cash;
use App\Model\Novaliches\NovalichesS2Cash;
use App\Model\Novaliches\NovalichesScholar;
use App\Model\Novaliches\NovalichesSocial;
use App\Model\Novaliches\NovalichesTuition;
use App\Model\Novaliches\NovalichesPettyCash;
use App\Model\Novaliches\NovalichesRemit;
use App\Model\Novaliches\NovalichesReservation;

use App\Model\Novaliches\NovalichesLecturerAEvaluation;
use App\Model\Novaliches\NovalichesLecturerBEvaluation;
use App\Model\Novaliches\NovalichesComment;

use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use Auth;
use User;
use DB;




class NovalichesController extends Controller
{
	private $branch = "Novaliches";

    private $sbranch = "novaliches";

public function __construct()
    {

         $this->middleware('role:novaliches|admin');
        
    }

public function index(){

        $pettycash = NovalichesPettyCash::where('id','=','1')->value('petty_cash');

        $s1_sale = NovalichesS1Sale::sum('amount_paid');

        $s2_sale = NovalichesS2Sale::sum('amount_paid');

        $s1_cash = NovalichesS1Cash::where('id','=','1')->value('cash');

        $s2_cash = NovalichesS2Cash::where('id','=','1')->value('cash');

        $receivable = NovalichesReceivable::sum('balance');

        $expense = NovalichesExpense::sum('amount');

        $enrollee = NovalichesLet::where('status','=','Enrolled')->count()+ NovalichesNle::where('status','=','Enrolled')->count() + NovalichesCrim::where('status','=','Enrolled')->count() +NovalichesCivil::where('status','=','Enrolled')->count()+ NovalichesPsyc::where('status','=','Enrolled')->count();NovalichesNclex::where('status','=','Enrolled')->count()+ NovalichesIelt::where('status','=','Enrolled')->count()+ NovalichesSocial::where('status','=','Enrolled')->count()+ NovalichesAgri::where('status','=','Enrolled')->count()+ NovalichesMid::where('status','=','Enrolled')->count()+ NovalichesOnline::where('status','=','Enrolled')->count();

        $unpaid = NovalichesReceivable::count();
        
        $s1_dis = NovalichesS1Sale::sum('discount');

        $s2_dis = NovalichesS2Sale::sum('discount');

        $discount = $s1_dis + $s2_dis;

        $book = NovalichesBooksSale::sum('amount');

        $book_cash = NovalichesBookCash::where('id','=','1')->value('cash');

    	return view ('member.member-dashboard')
        ->with('pettycash',$pettycash)
        ->with('s1_sale',$s1_sale)
        ->with('s2_sale',$s2_sale)
        ->with('s1_cash',$s1_cash)
        ->with('s2_cash',$s2_cash)
        ->with('receivable', $receivable)
        ->with('expense',$expense)
        ->with('enrollee',$enrollee)
        ->with('unpaid',$unpaid)
        ->with('discount',$discount)
        ->with('book',$book)
        ->with('book_cash',$book_cash);
    }

// Register New Enrollee

public function add_enrollee(){

        $branch= $this->branch; 

        $program = Program::all();

        return view('member.add-enrollee')->with('branch',$branch)->with('program',$program);
    }


public function insert_enrollee(Request $request){

    	$input = $request->except(['_token']);
        $branch = $this->branch;
        $course = $input['program'];

        if($course == 'lets'){
            $course = 'LET';
        }
        if($course == 'nles'){
            $course = 'NLE';
        }
        if($course == 'crims'){
            $course = 'Criminology';
        }
        if($course == 'civils'){
            $course = 'Civil Service';
        }

        if($course == 'psycs'){
            $course = 'Psycometrician';
        }
        if($course == 'nclexes'){
            $course = 'NCLEX';
        }
        if($course == 'ielts'){
            $course = 'IELTS';
        }
        if($course == 'socials'){
            $course = 'Social Work';
        }
        if($course == 'agris'){
            $course = 'Agriculture';
        }
        if($course == 'mids'){
            $course = 'Midwifery';
        }
        if($course == 'onlines'){
            $course = 'Online Only';
        }

    	$program = $this->sbranch.'_'.$input['program'];

        $lastname = strtoupper($input['last_name']);
        $firstname = strtoupper($input['first_name']);
        $middlename = strtoupper($input['middle_name']);

        $existent = DB::table($program)->where('last_name','=',$lastname)->where('first_name','=',$firstname)->where('middle_name','=',$middlename)->first();

        if($existent != null){
            Alert::error('Failed!', 'This name is already registered.');
            return redirect ($this->sbranch.'/add-enrollee');
        }

        if($existent == null){
 		DB::table($program)->insert([

            'cbrc_id'       => $input['cbrc_id'],
            'last_name'     => strtoupper($input['last_name']),
            'first_name'    => strtoupper($input['first_name']),
            'middle_name'   => strtoupper($input['middle_name']),
            'username'      => $input['username'],
            'password'      => $input['password'],
            'course'        => $course,
            'major'         => $input['major'],
            'program'       => $input['program'],
            'school'        => $input['school'],
            'noa_no'        => $input['noa_no'],
            'take'          => $input['take'],
            'branch'        => $branch,
            'birthdate'     => $input['birthdate'],
            'contact_no'    => $input['contact_no'],
            'email'         => $input['email'],
            'address'       => $input['address'],
            'contact_person'=> $input['contact_person'],
            'contact_details'=> $input['contact_details'],
            'registration'  => 'Walk-in',
            'created_at'    => date('Y-m-d'),
        ]);
        Alert::success('Success!', 'New student has been registered.');
 		return redirect ($this->sbranch.'/add-enrollee');

    }
    }

public function update_enrollee(Request $request){
    $input = $request->except(['_token']);
        $branch = $this->branch;
        $program = $input['program'];
        $id = $input['id'];

        if($program == 'LET'){
            $program = 'lets';
        }
        if($program == 'NLE'){
            $program = 'nles';
        }
        if($program == 'Criminology'){
            $program = 'crims';
        }
        if($program == 'Civil Service'){
            $program = 'civils';
        }

        if($program == 'Psycometrician'){
            $program = 'psycs';
        }
        if($program == 'NCLEX'){
            $program = 'nclexes';
        }
        if($program == 'IELTS'){
            $program = 'ielts';
        }
        if($program == 'Social Work'){
            $program = 'socials';
        }
        if($program == 'Agriculture'){
            $program = 'agris';
        }
        if($program == 'Midwifery'){
            $program = 'mids';
        }
        if($program == 'Online Only'){
            $program = 'onlines';
        }

        $program = $this->sbranch.'_'.$program;

        DB::table($program)->where('id','=',$id)->update([

            'cbrc_id'       => $input['cbrc_id'],
            'last_name'     => $input['last_name'],
            'first_name'    => $input['first_name'],
            'middle_name'   => $input['middle_name'],
            'username'      => $input['username'],
            'password'      => $input['password'],
            'major'         => $input['major'],
            'school'        => $input['school'],
            'noa_no'        => $input['noa_no'],
            'take'          => $input['take'],
            'birthdate'     => $input['birthdate'],
            'contact_no'    => $input['contact_no'],
            'email'         => $input['email'],
            'address'       => $input['address'],
            'contact_person'=> $input['contact_person'],
            'contact_details'=> $input['contact_details'],
            'section'   => $input['section'],
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Enrollee has been updated',
        ]);

    }

    public function delete_enrollee(Request $request){
    $input = $request->except(['_token']);
        $branch = $this->branch;
        $program = $input['program'];
        $id = $input['id'];

        if($program == 'LET'){
            $program = 'lets';
        }
        if($program == 'NLE'){
            $program = 'nles';
        }
        if($program == 'Criminology'){
            $program = 'crims';
        }
        if($program == 'Civil Service'){
            $program = 'civils';
        }

        if($program == 'Psycometrician'){
            $program = 'psycs';
        }
        if($program == 'NCLEX'){
            $program = 'nclexes';
        }
        if($program == 'IELTS'){
            $program = 'ielts';
        }
        if($program == 'Social Work'){
            $program = 'socials';
        }
        if($program == 'Agriculture'){
            $program = 'agris';
        }
        if($program == 'Midwifery'){
            $program = 'mids';
        }
        if($program == 'Online Only'){
            $program = 'onlines';
        }

        $program = $this->sbranch.'_'.$program;

        DB::table($program)->where('id','=',$id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Enrollee has been updated',
        ]);

    }

// Add New Payment

public function new_payment(){

        $branch= $this->branch; 
        $date = date('M-d-Y');
        $program = Program::all();
        return view('member.new-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }

public function fetch_student(){

        $program = Input::get('program');

        if ($program == 'lets' ){

        $student = NovalichesLet::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = NovalichesNle::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = NovalichesCrim::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = NovalichesCivil::orderBy('last_name')->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = NovalichesPsyc::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = NovalichesNclex::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = NovalichesIelt::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = NovalichesSocial::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = NovalichesAgri::orderBy('last_name')->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = NovalichesMid::orderBy('last_name')->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = NovalichesOnline::orderBy('last_name')->get();
            return response()->json($student);
        }       
    }

public function fetch_tuition(){

        $category = Input::get('category');
        $program = Input::get('program');


        if ($program == 'lets' ){

        $tuition = NovalichesTuition::where('program','=', 'LET')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'nles' ){
            $tuition = NovalichesTuition::where('program','=', 'NLE')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'crims' ){
            $tuition = NovalichesTuition::where('program','=', 'Criminology')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'civils' ){
            $tuition = NovalichesTuition::where('program','=', 'Civil Service')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

         if ($program == 'psycs' ){
            $tuition = NovalichesTuition::where('program','=', 'Psycometrician')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'nclexes' ){
            $tuition = NovalichesTuition::where('program','=', 'NCLEX')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'ielts' ){
            $tuition = NovalichesTuition::where('program','=', 'IELTS')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'socials' ){
            $tuition = NovalichesTuition::where('program','=', 'Social Work')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'agris' ){
            $tuition = NovalichesTuition::where('program','=', 'Agriculture')->where('category','=', $category)->get();
            return response()->json($tuition);
        }

        if ($program == 'mids' ){
            $tuition = NovalichesTuition::where('program','=', 'Midwifery')->where('category','=', $category)->get();
            return response()->json($tuition);
        }  

        if ($program == 'onlines' ){
            $tuition = NovalichesTuition::where('program','=', 'Online Only')->where('category','=', $category)->get();
            return response()->json($tuition);
        }          
    }

public function fetch_discount(){

        $program = Input::get('program');
        $category = Input::get('category');


        if ($program == 'lets' ){

        $discount = NovalichesDiscount::where('program','=', 'LET')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'nles' ){
            $discount = NovalichesDiscount::where('program','=', 'NLE')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'crims' ){
            $discount = NovalichesDiscount::where('program','=', 'Criminology')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'civils' ){
            $discount = NovalichesDiscount::where('program','=', 'Civil Service')->where('category','=', $category)->get();
        return response()->json($discount);
        }

         if ($program == 'psycs' ){
            $discount = NovalichesDiscount::where('program','=', 'Psycometrician')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'nclexes' ){
            $discount = NovalichesDiscount::where('program','=', 'NCLEX')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'ielts' ){
            $discount = NovalichesDiscount::where('program','=', 'IELTS')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'socials' ){
            $discount = NovalichesDiscount::where('program','=', 'Social Work')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'agris' ){
            $discount = NovalichesDiscount::where('program','=', 'Agriculture')->where('category','=', $category)->get();
            return response()->json($discount);
        }  

        if ($program == 'mids' ){
            $discount = NovalichesDiscount::where('program','=', 'Midwifery')->where('category','=', $category)->get();
            return response()->json($discount);
        } 

        if ($program == 'onlines' ){
            $discount = NovalichesDiscount::where('program','=', 'Online Only')->where('category','=', $category)->get();
            return response()->json($discount);
        }       


    }

public function add_new_payment(Request $request){

        $input = $request->except(['_token']);
        $season = $input['season'];
        $total_amount = $input['total_amount'];
        $amount_paid = $input['amount_paid'];
        $student = explode('*',$input['name']);
        $balance = $total_amount - $amount_paid;

        $reserve = $input['reserve'];

        if($balance < 0 ){
            $balance = 0;
        }
        $discount = explode(',',$input['discount']);
        $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psycometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }
        $discount = explode(',',$input['discount']);

        if($discount[0] == 0) {
            $discount_amount = null;
            $discount_category = null;
        } 
        else {
            
            $discount_amount = $discount[0];
            $discount_category = $discount[1];
        }

        if($input['tuition_fee'] != null){
        if ($season == 'Season 1'){
        DB::table($this->sbranch.'_s1_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $input['year'],
            'created_at'    => date('Y-m-d'),
        ]);
        $ini = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        $program = $this->sbranch.'_'.$input['program'];

        DB::table($program)->where('id','=',$student[1])->update([

            'category' => $input['category'],
            'status'   => 'Enrolled',
            'facilitation' => $input['facilitation'],
            'section'   => $input['section'],
        ]);
        }
        if ($season == 'Season 2'){
        DB::table($this->sbranch.'_s2_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $input['year'],
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = NovalichesS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        $program = $this->sbranch.'_'.$input['program'];

        DB::table($program)->where('id','=',$student[1])->update([

            'category' => $input['category'],
            'status'   => 'Enrolled',
            'facilitation' => $input['facilitation'],
            'section'   => $input['section'],
        ]);
        }

        Alert::success('Success!', 'Payment has been submitted.');
    }
        if($input['tuition_fee'] == null){
            if ($input['rseason'] == 'Season 1'){
        DB::table($this->sbranch.'_s1_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $input['year'],
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        }
        if ($input['rseason'] == 'Season 2'){
        DB::table($this->sbranch.'_s2_sales')->insert([
            'date' => $input['date'],
            'name' => $student[0],
            'program'=> $prog,
            'category' => $input['category'],
            'discount_category' => $discount_category,
            'tuition_fee' =>$input['tuition_fee'],
            'facilitation_fee' =>$input['facilitation'],
            'discount' => $discount_amount,
            'amount_paid' =>$input['amount_paid'],
            'balance' => $balance,
            'year'  => $input['year'],
             'created_at'    => date('Y-m-d'),
        ]);

        $ini = NovalichesS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);
        }
        Alert::success('Success!', 'Payment has been submitted.');
        }

       

        if($balance > 0){

            if($input['balance'] == null){
                NovalichesReceivable::insert([

                    'enrollee_id' => $student[1],
                    'name'        => $student[0],
                    'program'     => $prog,
                    'contact_no'  => $student[2],
                    'season'      => $season,
                    'balance'     => $balance,
                ]);
            }
            }
            if($input['balance'] != null && $input['tuition_fee'] == null){

                if($amount_paid >= $total_amount){
                    NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                    }
                if($amount_paid < $total_amount){
                $remaining = $input['balance'];
                $present_balance = $remaining - $amount_paid;
                NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                    'balance' => $present_balance,
                ]);
                }
            }
            if($input['balance'] != null && $input['tuition_fee'] != null){

                if($amount_paid >= $total_amount){
                    NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                    }
                else{
                NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                    'balance' => $balance,
                ]);
                }
            }

           if($input['reserve'] != null){

            NovalichesReservation::where('enrollee_id','=',$student[1])->where('program','=',$input['program'])->delete();
           }
           
        return redirect ($this->sbranch.'/new-payment');
    }

public function add_expense(){
        $branch=$this->branch; 
        $program = Program::all();
        $date = date('M-d-Y');
        return view('member.add-expense')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }

public function fetch_expense(){
        $category = Input::get('category');
        $sub_category = Expense::where('category','=', $category)->get();
        return response()->json($sub_category);
    }

public function fetch_id(){

    $id = Input::get('id');
    $program = Input::get('program');

    if ($program == 'lets' ){

        $student = NovalichesLet::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = NovalichesNle::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = NovalichesCrim::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = NovalichesCivil::where('id','=',$id)->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = NovalichesPsyc::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = NovalichesNclex::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = NovalichesIelt::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = NovalichesSocial::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = NovalichesAgri::where('id','=',$id)->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = NovalichesMid::where('id','=',$id)->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = NovalichesOnline::where('id','=',$id)->get();
            return response()->json($student);
        }  
}

public function fetch_balance(){

        $id = Input::get('id');
        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psycometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 

        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 

        if ($program == 'onlines' ){
            $program = 'Online Only';
        }

        

        $balance = NovalichesReceivable::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        return response()->json($balance);
    }

public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psycometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = NovalichesBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psycometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}

public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);
 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psycometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value) {

             NovalichesBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $input['amount_paid'],
             ]);

        
        $avai = NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);

        $book_cash = NovalichesBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        NovalichesBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            
        }
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ($this->sbranch.'/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ($this->sbranch.'/book-payment');
        }

        
}
public function add_new_expense(Request $request){

        $input = $request->except(['_token']);
        $author = Auth::user()->name;
        
        NovalichesExpense::insert([
            'date'=>$input['date'],
            'branch'=>$this->branch,
            'program'=>$input['program'],
            'category'=>$input['category'],
            'sub_category'=>$input['sub_category'],
            'amount'=>$input['amount'],
            'remarks'=>$input['remarks'],
            'author' => $author,
            ]);
        $pettycash = NovalichesPettyCash::where('id','=','1')->value('petty_cash');

        $updated_pettycash = $pettycash - $input['amount'];

        NovalichesPettyCash::where('id','=','1')->update([
            'petty_cash' => $updated_pettycash,
        ]);
        Alert::success('Success!', 'New expense has been added.');
        return redirect ($this->sbranch.'/add-expense');
    }

public function add_budget(){
    $branch=$this->branch;
    $date = date('M-d-Y');
    return view('member.add-budget')->with('branch',$branch)->with('date',$date);
}

public function insert_new_budget(Request $request){
    $input = $request->except(['_token']);
    NovalichesBudget::insert($input);

    $initial = NovalichesPettyCash::where('id','=','1')->value('petty_cash');

    $amount = $input['amount'];

    $total = $initial + $amount;

    NovalichesPettyCash::where('id','=','1')->update([
        'petty_cash' => $total,
    ]);
    Alert::success('Success!', 'New budget has been added.');
    return redirect ($this->sbranch.'/add-budget');
}

public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}

public function let_table(){
    $prog = 'LET';
    $branch=$this->branch; 
    $enrollee = NovalichesLet::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function nle_table(){
    $prog = 'NLE';
    $branch=$this->branch; 
    $enrollee = NovalichesNle::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function crim_table(){
    $prog = 'Criminology';
    $branch=$this->branch; 
    $enrollee = NovalichesCrim::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function civil_table(){
    $prog = 'Civil Service';
    $branch=$this->branch; 
    $enrollee = NovalichesCivil::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function psyc_table(){
    $prog = 'Psycometrician';
    $branch=$this->branch; 
    $enrollee = NovalichesPsyc::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function nclex_table(){
    $prog = 'NCLEX';
    $branch=$this->branch; 
    $enrollee = NovalichesNclex::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function ielts_table(){
    $prog = 'IELTS';
    $branch=$this->branch; 
    $enrollee = NovalichesIelt::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function social_table(){
    $prog = 'Social Work';
    $branch=$this->branch; 
    $enrollee = NovalichesSocial::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function agri_table(){
    $prog = 'Agriculture';
    $branch=$this->branch; 
    $enrollee = NovalichesAgri::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function mid_table(){
    $prog = 'Midwifery';
    $branch=$this->branch; 
    $enrollee = NovalichesMid::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}

public function online_table(){
    $prog = 'Online Only';
    $branch=$this->branch; 
    $enrollee = NovalichesOnline::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('prog',$prog);
}
public function scholar_table(){

    $let = NovalichesLet::where('category','=','Scholar')->get();
    $nle = NovalichesNle::where('category','=','Scholar')->get();
    $crim = NovalichesCrim::where('category','=','Scholar')->get();
    $civil = NovalichesCivil::where('category','=','Scholar')->get();
    $psyc = NovalichesPsyc::where('category','=','Scholar')->get();
    $nclex = NovalichesNclex::where('category','=','Scholar')->get();
    $ielt = NovalichesIelt::where('category','=','Scholar')->get();
    $social = NovalichesSocial::where('category','=','Scholar')->get();
    $agri = NovalichesAgri::where('category','=','Scholar')->get();
    $mid = NovalichesMid::where('category','=','Scholar')->get();
    $online = NovalichesOnline::where('category','=','Scholar')->get();

    return view ('member.scholar')->with('let',$let)->with('nle',$nle)->with('crim',$crim)->with('civil',$civil)->with('psyc',$psyc)->with('nclex',$nclex)->with('ielt',$ielt)->with('social',$social)->with('agri',$agri)->with('mid',$mid)->with('online',$online);

}
public function enrolled_table(){

    $let = NovalichesLet::where('status','=','Enrolled')->get();
    $nle = NovalichesNle::where('status','=','Enrolled')->get();
    $crim = NovalichesCrim::where('status','=','Enrolled')->get();
    $civil = NovalichesCivil::where('status','=','Enrolled')->get();
    $psyc = NovalichesPsyc::where('status','=','Enrolled')->get();
    $nclex = NovalichesNclex::where('status','=','Enrolled')->get();
    $ielt = NovalichesIelt::where('status','=','Enrolled')->get();
    $social = NovalichesSocial::where('status','=','Enrolled')->get();
    $agri = NovalichesAgri::where('status','=','Enrolled')->get();
    $mid = NovalichesMid::where('category','=','Enrolled')->get();
    $online = NovalichesOnline::where('category','=','Enrolled')->get();


    return view ('member.enrolled')->with('let',$let)->with('nle',$nle)->with('crim',$crim)->with('civil',$civil)->with('psyc',$psyc)->with('nclex',$nclex)->with('ielt',$ielt)->with('social',$social)->with('agri',$agri)->with('mid',$mid)->with('online',$online);

}
public function dropped_table(){

    $let = NovalichesLet::where('status','=','Dropped')->get();
    $nle = NovalichesNle::where('status','=','Dropped')->get();
    $crim = NovalichesCrim::where('status','=','Dropped')->get();
    $civil = NovalichesCivil::where('status','=','Dropped')->get();
    $psyc = NovalichesPsyc::where('status','=','Dropped')->get();
    $nclex = NovalichesNclex::where('status','=','Dropped')->get();
    $ielt = NovalichesIelt::where('status','=','Dropped')->get();
    $social = NovalichesSocial::where('status','=','Dropped')->get();
    $agri = NovalichesAgri::where('status','=','Dropped')->get();
    $mid = NovalichesMid::where('category','=','Dropped')->get();
    $online = NovalichesOnline::where('category','=','Dropped')->get();

    return view ('member.dropped')->with('let',$let)->with('nle',$nle)->with('crim',$crim)->with('civil',$civil)->with('psyc',$psyc)->with('nclex',$nclex)->with('ielt',$ielt)->with('social',$social)->with('agri',$agri)->with('mid',$mid)->with('online',$online);

}

public function drop_student(Request $request){
    $input = $request->except(['_token']);

    $id = $input['id'];
    $program = $input['program'];

    if($program == 'lets'){

        NovalichesLet::where('id','=',$id)->update([
            'status' => 'Dropped',
        ]);

        return response()->json([
            'success' => true,
            'message' => '1 Student has beed dropped.',
        ]);
    }


}
public function tuition_table(){

    $tuition = NovalichesTuition::where('branch','=',$this->branch)->get();

    $discount = NovalichesDiscount::where('branch','=',$this->branch)->get();

    return view ('member.tuition')->with('tuition',$tuition)->with('discount',$discount);

}

public function expense_table(){

    $expense = NovalichesExpense::all();

    return view ('member.expense')->with('expense',$expense);

}
public function sales_enrollee_table(){

    $sale = NovalichesS1Sale::all();

    $sale2 = NovalichesS2Sale::all();

    return view ('member.sales-enrollee')->with('sale',$sale)->with('sale2',$sale2);

}

public function sales_program_table(){

    $let_1_sale=NovalichesS1Sale::where('program','=','LET')->sum('amount_paid');
    $nle_1_sale=NovalichesS1Sale::where('program','=','NLE')->sum('amount_paid');
    $crim_1_sale=NovalichesS1Sale::where('program','=','Criminology')->sum('amount_paid');
    $civil_1_sale=NovalichesS1Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psyc_1_sale=NovalichesS1Sale::where('program','=','Psycometrician')->sum('amount_paid');
    $nclex_1_sale=NovalichesS1Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ielts_1_sale=NovalichesS1Sale::where('program','=','IELTS')->sum('amount_paid');
    $social_1_sale=NovalichesS1Sale::where('program','=','Social')->sum('amount_paid');
    $agri_1_sale=NovalichesS1Sale::where('program','=','Agriculture')->sum('amount_paid');
    $mid_1_sale=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');
    $online_1_sale=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');

    $let_2_sale=NovalichesS2Sale::where('program','=','LET')->sum('amount_paid');
    $nle_2_sale=NovalichesS2Sale::where('program','=','NLE')->sum('amount_paid');
    $crim_2_sale=NovalichesS2Sale::where('program','=','Criminology')->sum('amount_paid');
    $civil_2_sale=NovalichesS2Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psyc_2_sale=NovalichesS2Sale::where('program','=','Psycometrician')->sum('amount_paid');
    $nclex_2_sale=NovalichesS2Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ielts_2_sale=NovalichesS2Sale::where('program','=','IELTS')->sum('amount_paid');
    $social_2_sale=NovalichesS2Sale::where('program','=','Social')->sum('amount_paid');
    $agri_2_sale=NovalichesS2Sale::where('program','=','Agriculture')->sum('amount_paid');
    $mid_2_sale=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');
    $online_2_sale=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');

    $let =  NovalichesLet::where('status','=','Enrolled')->count();
    $nle =  NovalichesNle::where('status','=','Enrolled')->count();
    $crim = NovalichesCrim::where('status','=','Enrolled')->count();
    $civil= NovalichesCivil::where('status','=','Enrolled')->count();
    $psyc = NovalichesPsyc::where('status','=','Enrolled')->count();
    $nclex = NovalichesNclex::where('status','=','Enrolled')->count();
    $ielts = NovalichesIelt::where('status','=','Enrolled')->count();
    $social = NovalichesSocial::where('status','=','Enrolled')->count();
    $agri = NovalichesAgri::where('status','=','Enrolled')->count();
    $mid = NovalichesMid::where('status','=','Enrolled')->count();
    $online = NovalichesOnline::where('status','=','Enrolled')->count();

   

    return view ('member.sales-program')
    ->with('let_1_sale',$let_1_sale)
    ->with('nle_1_sale',$nle_1_sale)
    ->with('crim_1_sale',$crim_1_sale)
    ->with('civil_1_sale',$civil_1_sale)
    ->with('psyc_1_sale',$psyc_1_sale)
    ->with('nclex_1_sale',$nclex_1_sale)
    ->with('ielts_1_sale',$ielts_1_sale)
    ->with('social_1_sale',$social_1_sale)
    ->with('agri_1_sale',$agri_1_sale)
    ->with('mid_1_sale',$mid_1_sale)
    ->with('online_1_sale',$online_1_sale)
    ->with('let_2_sale',$let_2_sale)
    ->with('nle_2_sale',$nle_2_sale)
    ->with('crim_2_sale',$crim_2_sale)
    ->with('civil_2_sale',$civil_2_sale)
    ->with('psyc_2_sale',$psyc_2_sale)
    ->with('nclex_2_sale',$nclex_2_sale)
    ->with('ielts_2_sale',$ielts_2_sale)
    ->with('social_2_sale',$social_2_sale)
    ->with('agri_2_sale',$agri_2_sale)
    ->with('mid_2_sale',$mid_2_sale)
    ->with('online_2_sale',$online_2_sale)
    ->with('let',$let)
    ->with('nle',$nle)
    ->with('crim',$crim)
    ->with('civil',$civil)
    ->with('psyc',$psyc)
    ->with('nclex',$nclex)
    ->with('ielts',$ielts)
    ->with('social',$social)
    ->with('agri',$agri)
    ->with('mid',$mid)
    ->with('online',$online);

}

public function receivable_enrollee_table(){

    $receivable = NovalichesReceivable::all();
    return view ('member.receivable-enrollee')->with('receivable',$receivable);

}
public function receivable_program_table(){

    $let_receivable=NovalichesReceivable::where('program','=','LET')->sum('balance');
    $nle_receivable=NovalichesReceivable::where('program','=','NLE')->sum('balance');
    $crim_receivable=NovalichesReceivable::where('program','=','Criminology')->sum('balance');
    $civil_receivable=NovalichesReceivable::where('program','=','Civil Service')->sum('balance');
    $psyc_receivable=NovalichesReceivable::where('program','=','Psycometrician')->sum('balance');
    $nclex_receivable=NovalichesReceivable::where('program','=','NCLEX')->sum('balance');
    $ielts_receivable=NovalichesReceivable::where('program','=','IELTS')->sum('balance');
    $social_receivable=NovalichesReceivable::where('program','=','Social')->sum('balance');
    $agri_receivable=NovalichesReceivable::where('program','=','Agriculture')->sum('balance');
    $mid_receivable=NovalichesReceivable::where('program','=','Midwifery')->sum('balance');
    $online_receivable=NovalichesReceivable::where('program','=','Online Only')->sum('balance');

    $let=NovalichesReceivable::where('program','=','LET')->count();
    $nle=NovalichesReceivable::where('program','=','NLE')->count();
    $crim=NovalichesReceivable::where('program','=','Criminology')->count();
    $civil=NovalichesReceivable::where('program','=','Civil Service')->count();
    $psyc=NovalichesReceivable::where('program','=','Psycometrician')->count();
    $nclex=NovalichesReceivable::where('program','=','NCLEX')->count();
    $ielts=NovalichesReceivable::where('program','=','IELTS')->count();
    $social=NovalichesReceivable::where('program','=','Social')->count();
    $agri=NovalichesReceivable::where('program','=','Agriculture')->count();
    $mid=NovalichesReceivable::where('program','=','Midwifery')->count();
    $online=NovalichesReceivable::where('program','=','Online Only')->count();

    return view ('member.receivable-program')
    ->with('let_receivable',$let_receivable)
    ->with('nle_receivable',$nle_receivable)
    ->with('crim_receivable',$crim_receivable)
    ->with('civil_receivable',$civil_receivable)
    ->with('psyc_receivable',$psyc_receivable)
    ->with('nclex_receivable',$nclex_receivable)
    ->with('ielts_receivable',$ielts_receivable)
    ->with('social_receivable',$social_receivable)
    ->with('agri_receivable',$agri_receivable)
    ->with('mid_receivable',$mid_receivable)
    ->with('online_receivable',$online_receivable)
    ->with('let',$let)
    ->with('nle',$nle)
    ->with('crim',$crim)
    ->with('civil',$civil)
    ->with('psyc',$psyc)
    ->with('nclex',$nclex)
    ->with('ielts',$ielts)
    ->with('social',$social)
    ->with('agri',$agri)
    ->with('mid',$mid)
    ->with('online',$online);
}

public function books_table(){

$book = NovalichesBooksInventorie::all();
$sale = NovalichesBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}
public function new_remit(){
    $date = date('M-d-Y');
    $branch = $this->branch;
    return view ('member.new-remit')->with('branch',$branch)->with('date',$date);
}

public function insert_remit(Request $request){

    $input = $request->except(['_token']);
    $author = Auth::user()->name;
    

    if($input['category'] == 'Sales' && $input['season'] == 'Season 1'){
        
        NovalichesRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'season'    => $input['season'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);


        $available = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }

        else{
            NovalichesS1Cash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from season 1 has been remitted.');
    }

    if($input['category'] == 'Sales' && $input['season'] == 'Season 2'){

        NovalichesRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'season'    => $input['season'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);

        $available = NovalichesS2Cash::where('id','=','1')->value('cash');
        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }

        else{
            NovalichesS2Cash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from season 2 has been remitted.');
    }

    if($input['category'] == 'Books'){

        NovalichesRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);

        $available = NovalichesBookCash::where('id','=','1')->value('cash');
        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }

        else{
            NovalichesBookCash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from books has been remitted.');
    }

    return redirect ($this->sbranch.'/new-remit');

}


public function remit(){

    $remit = NovalichesRemit::all();

    return view ('member.remit')->with('remit',$remit);
}

public function clear_enrollee(Request $request){

    $input = $request->except(['_token']);
    $program = $input['program'];

    DB::table($this->sbranch.'_'.$program)->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}
public function clear_sale_season1(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_s1_sales')->truncate();

    NovalichesS1Cash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_sale_season2(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_s2_sales')->truncate();

    NovalichesS2Cash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_receivable(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_receivables')->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_expense(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_expenses')->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_book(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_books_sales')->truncate();

    NovalichesBookCash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function new_reservation(){
    $branch= $this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view ('member.new-reservation')->with('branch',$branch)->with('date',$date)->with('program',$program);
}

public function insert_reservation(Request $request){

    $input = $request->except(['_token']);
    $details = explode('*',$input['name']);

    $name = $details[0];
    $school = $details[1];
    $email = $details[2];
    $contact_no = $details[3];
    $id = $details[4];

    $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psycometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }

    $existent = NovalichesReservation::where('enrollee_id','=',$id)->first();

        if($existent != null){
           
        $old = NovalichesReservation::where('enrollee_id','=',$id)->value('reservation_fee');
        
        $new = $old + $input['amount_paid'];

        NovalichesReservation::where('enrollee_id','=',$id)->update([
            'reservation_fee' => $new,
        ]);

         $season = $input['season'];
        if ($season == 'Season 1'){

        NovalichesS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        NovalichesS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', '1 student has been updated.');
    return redirect ($this->sbranch.'/new-reservation');
        }

    if($existent == null)
    {

    NovalichesReservation::create([
        'enrollee_id'    =>     $id,
        'name'           =>     $name,
        'branch'         =>     $this->branch,
        'program'        =>     $input['program'],
        'prog'           =>     $prog,
        'school'         =>     $school,
        'email'          =>     $email,
        'contact_no'     =>     $contact_no,
        'reservation_fee'=>     $input['amount_paid'],
    ]);

    $season = $input['season'];
    if ($season == 'Season 1'){

        NovalichesS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        NovalichesS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', 'New student has been reserved.');
    return redirect ($this->sbranch.'/new-reservation');
}
}

public function reservation_table(){

    $reserve = NovalichesReservation::all();

    return view ('member.reservation')->with('reserve',$reserve);
}

public function fetch_reserved(){

    $id = Input::get('id');
    $program = Input::get('program');
    

    $fee = NovalichesReservation::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        
        return response()->json($fee);

}

public function clear_reservation(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_reservations')->truncate();


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}


/* Lecturer Evaluation */

public function add_lec(){

    $program = Program::all();

    return view ('member.add-lecturer')->with('program',$program);
}

public function fetch_class(){
    $program = Input::get('program');

        $class = DB::table('classes')->where('program','=',$program)->get();

        return response()->json($class);
        
}

public function fetch_subject(){
    $program = Input::get('program');
    $class = Input::get('class');

        $subject = DB::table('subjects')->where('program','=',$program)->where('class','=',$class)->get();

        return response()->json($subject);
        
}

public function fetch_section(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');

        $section = Section::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('branch','=',$this->branch)->get();

        return response()->json($section);
        
}

public function fetch_lecturer(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');
    $section = Input::get('section');

        $lecturer = NovalichesLecturerAEvaluation::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('section','=',$section)->get();

        return response()->json($lecturer);
        
}

public function fetch_lecturerb(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');
    $section = Input::get('section');

        $lecturer = NovalichesLecturerBEvaluation::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('section','=',$section)->get();

        return response()->json($lecturer);
        
}

public function insert_lecturer(Request $request){

    $input = $request->except(['_token']);

    $class   = explode('*',$input['class']);
    $subject = explode('*',$input['subject']);

    $existent1 = NovalichesLecturerAEvaluation::where('lecturer','=',strtoupper($input['lecturer']))->where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->first();

    $existent2 = NovalichesLecturerBEvaluation::where('lecturer','=',strtoupper($input['lecturer']))
        ->where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->first();

    if($existent1 != null && $existent2 != null){

        Alert::error('Failed!', 'This lecturer was already assigned.');
        return redirect ($this->sbranch.'/add-lecturer');

    }
    if($existent1 == null && $existent2 == null){
    NovalichesLecturerAEvaluation::create([
        'date'      => $input['date'],
        'lecturer'  => strtoupper($input['lecturer']),
        'branch'    => $this->branch,
        'program'   => $input['program'],
        'section'   => $input['section'],
        'class'     => $class[1],
        'aka_class' => $class[0],
        'subject'   => $subject[1],
        'aka_subject'=>$subject[0],
        'review_ambassador'=>strtoupper($input['ambassador']),
        'excellentA'    => 0,
        'goodA'         => 0,
        'fairA'         => 0,
        'poorA'         => 0,
        'verypoorA'     => 0,
        'excellentB'    => 0,
        'goodB'         => 0,
        'fairB'         => 0,
        'poorB'         => 0,
        'verypoorB'     => 0,
        'excellentC'    => 0,
        'goodC'         => 0,
        'fairC'         => 0,
        'poorC'         => 0,
        'verypoorC'     => 0,
        'excellentD'    => 0,
        'goodD'         => 0,
        'fairD'         => 0,
        'poorD'         => 0,
        'verypoorD'     => 0,
        'excellentE'    => 0,
        'goodE'         => 0,
        'fairE'         => 0,
        'poorE'         => 0,
        'verypoorE'     => 0,
        'excellentF'    => 0,
        'goodF'         => 0,
        'fairF'         => 0,
        'poorF'         => 0,
        'verypoorF'     => 0,
        'excellentG'    => 0,
        'goodG'         => 0,
        'fairG'         => 0,
        'poorG'         => 0,
        'verypoorG'     => 0,
    ]);

    NovalichesLecturerBEvaluation::create([
        'date'      => $input['date'],
        'lecturer'  => strtoupper($input['lecturer']),
        'branch'    => $this->branch,
        'program'   => $input['program'],
        'section'   => $input['section'],
        'class'     => $class[1],
        'aka_class' => $class[0],
        'subject'   => $subject[1],
        'aka_subject'=>$subject[0],
        'review_ambassador'=>strtoupper($input['ambassador']),
        'excellentH'    => 0,
        'goodH'         => 0,
        'fairH'         => 0,
        'poorH'         => 0,
        'verypoorH'     => 0,
        'excellentI'    => 0,
        'goodI'         => 0,
        'fairI'         => 0,
        'poorI'         => 0,
        'verypoorI'     => 0,
        'excellentJ'    => 0,
        'goodJ'         => 0,
        'fairJ'         => 0,
        'poorJ'         => 0,
        'verypoorJ'     => 0,
        'excellentK'    => 0,
        'goodK'         => 0,
        'fairK'         => 0,
        'poorK'         => 0,
        'verypoorK'     => 0,
        'excellentL'    => 0,
        'goodL'         => 0,
        'fairL'         => 0,
        'poorL'         => 0,
        'verypoorL'     => 0,
        'excellentM'    => 0,
        'goodM'         => 0,
        'fairM'         => 0,
        'poorM'         => 0,
        'verypoorM'     => 0,
        'excellentN'    => 0,
        'goodN'         => 0,
        'fairN'         => 0,
        'poorN'         => 0,
        'verypoorN'     => 0,
        'excellentO'    => 0,
        'goodO'         => 0,
        'fairO'         => 0,
        'poorO'         => 0,
        'verypoorO'     => 0,
        'excellentP'    => 0,
        'goodP'         => 0,
        'fairP'         => 0,
        'poorP'         => 0,
        'verypoorP'     => 0,
        'excellentQ'    => 0,
        'goodQ'         => 0,
        'fairQ'         => 0,
        'poorQ'         => 0,
        'verypoorQ'     => 0,
    ]);

    $validate = Section::where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->where('branch','=',$this->branch)
        ->first();
    if($validate == null){
        Section::create([
            'branch'        =>  $this->branch,
            'program'       =>  $input['program'],
            'aka_class'     =>  $class[0],
            'aka_subject'   =>  $subject[0],
            'section'       =>  $input['section'],
        ]);
    }
    Alert::success('Success!', 'New lecturer has been added.');
    return redirect ($this->sbranch.'/add-lecturer');
}
}

public function insert_eval(Request $request){

    $input = $request->except(['_token']); 
    
    $id = $input['id'];
    $qA = $input['qA'];
    $qB = $input['qB'];
    $qC = $input['qC'];
    $qD = $input['qD'];
    $qE = $input['qE'];
    $qF = $input['qF'];
    $qG = $input['qG'];
    $qH = $input['qH'];
    $qI = $input['qI'];
    $qJ = $input['qJ'];
    $qK = $input['qK'];
    $qL = $input['qL'];
    $qM = $input['qM'];
    $qN = $input['qN'];
    $qO = $input['qO'];
    $qP = $input['qP'];
    $qQ = $input['qQ'];

    $evalA = NovalichesLecturerAEvaluation::find($id);
    $evalA->$qA += 1;
    $evalA->$qB += 1;
    $evalA->$qC += 1;
    $evalA->$qD += 1;
    $evalA->$qE += 1;
    $evalA->$qF += 1;
    $evalA->$qG += 1;
    $evalA->save();

    $evalB = NovalichesLecturerBEvaluation::find($id);
    $evalB->$qH += 1;
    $evalB->$qI += 1;
    $evalB->$qJ += 1;
    $evalB->$qK += 1;
    $evalB->$qL += 1;
    $evalB->$qM += 1;
    $evalB->$qN += 1;
    $evalB->$qO += 1;
    $evalB->$qP += 1;
    $evalB->$qQ += 1;
    $evalB->save();

    NovalichesComment::create([
        'name'      =>  $input['student'],
        'lecturer'  =>  $input['lecturer'],
        'like'      =>  $input['like'],
        'dislike'   =>  $input['dislike'],
        'comment'   =>  $input['comment'],
        'branch'    =>  $this->branch,
        'program'   =>  $input['program'],
        'section'   =>  $input['section'],
        'class'     =>  $input['class'],
        'subject'   =>  $input['subject'],
        'date'      =>  $input['date'],
    ]);

    return response()->json([
            'success' => true,
            'message' => '1 has been lecturer has been evaluated',
        ]);

}

public function eval_lec(){

    $program = Program::all();

    return view ('member.evaluate-lecturer')->with('program',$program);;
}

public function lec_eval(){

    $program = Program::all();
    $comment = NovalichesComment::all();
    return view ('member.lecturer-evaluation')->with('program',$program)->with('comment',$comment);
}

public function clear_lecturers(Request $request){

    $input = $request->except(['_token']);

    NovalichesLecturerAEvaluation::truncate();
    NovalichesLecturerBEvaluation::truncate();
    NovalichesComment::truncate();
    Section::where('branch','=','Novaliches')->delete();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function today(){

    $branch= $this->branch; 
    $date = date('M-d-Y');

    $let_sale = NovalichesS1Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid');

    $let_book = NovalichesBooksSale::where('program','=','LET')->where('date','=',$date)->sum('amount');

    $nle_sale = NovalichesS1Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid');

    $nle_book = NovalichesBooksSale::where('program','=','NLE')->where('date','=',$date)->sum('amount');

    $crim_sale = NovalichesS1Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid');

    $crim_book = NovalichesBooksSale::where('program','=','Criminology')->where('date','=',$date)->sum('amount');

    $civil_sale = NovalichesS1Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid');

    $civil_book = NovalichesBooksSale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount');

    $psyc_sale = NovalichesS1Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid');

    $psyc_book = NovalichesBooksSale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount');

    $nclex_sale = NovalichesS1Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid');

    $nclex_book = NovalichesBooksSale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount');

    $ielts_sale = NovalichesS1Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid');

    $ielts_book = NovalichesBooksSale::where('program','=','IELTS')->where('date','=',$date)->sum('amount');

    $social_sale = NovalichesS1Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid');

    $social_book = NovalichesBooksSale::where('program','=','Social Work')->where('date','=',$date)->sum('amount');

    $agri_sale = NovalichesS1Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid');

    $agri_book = NovalichesBooksSale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount');

    $mid_sale = NovalichesS1Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid');

    $mid_book = NovalichesBooksSale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount');

    $online_sale = NovalichesS1Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid');

    $online_book = NovalichesBooksSale::where('program','=','Online Only')->where('date','=',$date)->sum('amount');

    return view ('member.today-report')->with('branch',$branch)->with('date',$date)
    ->with('let_sale',$let_sale)->with('let_book',$let_book)
    ->with('nle_sale',$nle_sale)->with('nle_book',$nle_book)
    ->with('crim_sale',$crim_sale)->with('crim_book',$crim_book)
    ->with('civil_sale',$civil_sale)->with('civil_book',$civil_book)
    ->with('psyc_sale',$psyc_sale)->with('psyc_book',$psyc_book)
    ->with('nclex_sale',$nclex_sale)->with('nclex_book',$nclex_book)
    ->with('ielts_sale',$ielts_sale)->with('ielts_book',$ielts_book)
    ->with('social_sale',$social_sale)->with('social_book',$social_book)
    ->with('agri_sale',$agri_sale)->with('agri_book',$agri_book)
    ->with('mid_sale',$mid_sale)->with('mid_book',$mid_book)
    ->with('online_sale',$online_sale)->with('online_book',$online_book);

}

public function yesterday(){
 
 $branch= $this->branch; 
    $date = date('M-d-Y',strtotime("-1 days"));


    $let_sale = NovalichesS1Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid');

    $let_book = NovalichesBooksSale::where('program','=','LET')->where('date','=',$date)->sum('amount');

    $nle_sale = NovalichesS1Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid');

    $nle_book = NovalichesBooksSale::where('program','=','NLE')->where('date','=',$date)->sum('amount');

    $crim_sale = NovalichesS1Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid');

    $crim_book = NovalichesBooksSale::where('program','=','Criminology')->where('date','=',$date)->sum('amount');

    $civil_sale = NovalichesS1Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid');

    $civil_book = NovalichesBooksSale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount');

    $psyc_sale = NovalichesS1Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid');

    $psyc_book = NovalichesBooksSale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount');

    $nclex_sale = NovalichesS1Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid');

    $nclex_book = NovalichesBooksSale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount');

    $ielts_sale = NovalichesS1Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid');

    $ielts_book = NovalichesBooksSale::where('program','=','IELTS')->where('date','=',$date)->sum('amount');

    $social_sale = NovalichesS1Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid');

    $social_book = NovalichesBooksSale::where('program','=','Social Work')->where('date','=',$date)->sum('amount');

    $agri_sale = NovalichesS1Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid');

    $agri_book = NovalichesBooksSale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount');

    $mid_sale = NovalichesS1Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid');

    $mid_book = NovalichesBooksSale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount');

    $online_sale = NovalichesS1Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid');

    $online_book = NovalichesBooksSale::where('program','=','Online Only')->where('date','=',$date)->sum('amount');

    return view ('member.yesterday-report')->with('branch',$branch)->with('date',$date)
    ->with('let_sale',$let_sale)->with('let_book',$let_book)
    ->with('nle_sale',$nle_sale)->with('nle_book',$nle_book)
    ->with('crim_sale',$crim_sale)->with('crim_book',$crim_book)
    ->with('civil_sale',$civil_sale)->with('civil_book',$civil_book)
    ->with('psyc_sale',$psyc_sale)->with('psyc_book',$psyc_book)
    ->with('nclex_sale',$nclex_sale)->with('nclex_book',$nclex_book)
    ->with('ielts_sale',$ielts_sale)->with('ielts_book',$ielts_book)
    ->with('social_sale',$social_sale)->with('social_book',$social_book)
    ->with('agri_sale',$agri_sale)->with('agri_book',$agri_book)
    ->with('mid_sale',$mid_sale)->with('mid_book',$mid_book)
    ->with('online_sale',$online_sale)->with('online_book',$online_book);

}

public function budget_record(){

    $branch= $this->branch; 
    $budget = NovalichesBudget::all();

    return view ('member.budget')->with('budget',$budget)->with('branch',$branch);
}

public function clear_budget(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_budgets')->truncate();


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function delete_sale1($id){

        $val = NovalichesS1Sale::where('id','=',$id)->value('amount_paid');
        $ini = NovalichesS1Cash::where('id','=','1')->value('cash');
        $bal = NovalichesS1Sale::where('id','=',$id)->value('balance');
        $name = NovalichesS1Sale::where('id','=',$id)->value('name');
        
        if ($bal != null){

            $receivable = NovalichesReceivable::where('name','=',$name)->where('season','=','Season 1')->value('balance');
            
            $new_bal = $receivable + $val;

            NovalichesReceivable::where('name','=',$name)->where('season','=','Season 1')->update([
            'balance' => $new_bal,
            ]);
             
        }
        else{
            $reservation = NovalichesReservation::where('name','=',$name)->value('reservation_fee');

            $new_bal = $reservation - $val;

            $reservation = NovalichesReservation::where('name','=',$name)->update([
                'reservation_fee' => $new_bal,
            ]);
        }

        if($val <= $ini){

        $sale = NovalichesS1Sale::findorfail($id);
        $sale->delete();

        $total = $ini - $val;

        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        

        Alert::success('Success!', '1 sales record has been deleted.');
        }
        else{
        Alert::error('Failed!', 'Insufficient Cash.');

        }
        return redirect ($this->sbranch.'/sales-enrollee');
}

public function delete_sale2($id){
        $val = NovalichesS2Sale::where('id','=',$id)->value('amount_paid');
        $ini = NovalichesS2Cash::where('id','=','1')->value('cash');
        $bal = NovalichesS2Sale::where('id','=',$id)->value('balance');
        $name = NovalichesS2Sale::where('id','=',$id)->value('name');
        
        if ($bal != null){

            $receivable = NovalichesReceivable::where('name','=',$name)->where('season','=','Season 2')->value('balance');
            
            $new_bal = $receivable + $val;

            NovalichesReceivable::where('name','=',$name)->where('season','=','Season 2')->update([
            'balance' => $new_bal,
            ]);
             
        }
        else{
            $reservation = NovalichesReservation::where('name','=',$name)->value('reservation_fee');

            $new_bal = $reservation - $val;

            $reservation = NovalichesReservation::where('name','=',$name)->update([
                'reservation_fee' => $new_bal,
            ]);
        }

        if($val <= $ini){

        $sale = NovalichesS2Sale::findorfail($id);
        $sale->delete();

        $total = $ini - $val;

        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

        Alert::success('Success!', '1 sales record has been deleted.');
        }
        else{
        Alert::error('Failed!', 'Insufficient Cash.');

        }
        return redirect ($this->sbranch.'/sales-enrollee');
    
}
public function csv_enrollee(Request $request){

            $input = $request->except(['_token']);
            $program = $input['program'];
            $prog = "";
            if($program == 'LET'){
            $prog = 'let';
            $program = 'lets';
            $db = 'NovalichesLet';
            }
            if($program == 'NLE'){
                $prog = 'nle';
                $program = 'nles';
                $db = 'NovalichesNle';
            }
            if($program == 'Criminology'){
                $prog = 'crim';
                $program = 'crims';
                $db = 'NovalichesCrim';
            }
            if($program == 'Civil Service'){
                $prog = 'civil';
                $program = 'civils';
                $db = 'NovalichesCivil';
            }

            if($program == 'Psycometrician'){
                $prog = 'psyc';
                $program = 'psycs';
                $db = 'NovalichesPsyc';
            }
            if($program == 'NCLEX'){
                $prog = 'nclex';
                $program = 'nclexes';
                $db = 'NovalichesPsyc';
            }
            if($program == 'IELTS'){
                $prog = 'ielt';
                $program = 'ielts';
                $db = 'NovalichesIelt';
            }
            if($program == 'Social Work'){
                $prog = 'social';
                $program = 'socials';
                $db = 'NovalichesSocial';
            }
            if($program == 'Agriculture'){
                $prog = 'agri';
                $program = 'agris';
                $db = 'NovalichesAgri';
            }
            if($program == 'Midwifery'){
                $prog = 'mid';
                $program = 'mids';
                $db = 'NovalichesMid';
            }
            if($program == 'Online Only'){
                $prog = 'online';
                $program = 'onlines';
                $db = 'NovalichesOnline';
            }



            $input['csv_enrollee'] = null;
            $csv="";
            $file = "";
            if($request->hasFile('csv'))
            {
            $extension = $request->csv;
            $extension = $request->csv->getClientOriginalExtension(); // getting excel extension
            $uploader = Auth::user()->branch;
            $input['csv'] = 'uploads/csv/'.''.$uploader;
            $csv = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
            $request->csv->move($input['csv'], $csv); 

            $file = $input['csv']."/".$csv;
             }
            else{
                $file = Auth::user()->csv;
            }

            if ($input['program'] == 'LET'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesLet ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'NLE'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesNle ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }


        if ($input['program'] == 'Criminology'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesCrim ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Civil Service'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesCivil ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Psycometrician'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesPsyc ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'NCLEX'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesNclex ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'IELTS'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesIelt ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Social Work'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesSocial ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Agriculture'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesAgri ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Midwifery'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesMid ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Online Only'){
            if (($handle = fopen ( 'uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesOnline ();
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                fclose ( $handle );
            }
        }

            
            Alert::success('Success!', 'Your file has been successfully imported.');

            return redirect ($this->sbranch.'/'.$prog); 
}
}
