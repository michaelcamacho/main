<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesCashBook extends Model
{
    protected $fillable = [
    	'book_cash_sales'
    ];
}
