<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesDiscount extends Model
{
    protected $fillable = [
        'branch',
        'program',
        'category',
        'discount_category',
        'discount_amount',

    ];

}
