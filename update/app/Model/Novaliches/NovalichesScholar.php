<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesScholar extends Model
{
    protected $fillable = [
        'cbrc_id',
        'enrollee_id',
        'name',
        'program',
        'school',
        'branch',
        'contact_no',

    ];

}
