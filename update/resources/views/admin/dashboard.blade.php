@extends('main')
@section('title')
DASHBOARD
@stop
@section('main-content')
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Summary</h6>
                        {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index-2.html">Dashboard</a>
                            </li>
                            {{--<li class="breadcrumb-item active">Home</li>--}}
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->



            <div class="container-fluid">
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                    </div>

                    <div class="widget-holder col-lg-4">
    
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col-md-6 widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Sales for Season 1</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                        <span class="counter">
                                        {{$total_s1 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{ url('/total-program')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Sales for Season 2</p>
                                        <span class="counter-title-a d-block">
                                            Php
                                        <span class="counter">
                                        {{$total_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{ url('/total-program')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View List</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse bg-primary">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Enrollees</p>
                                        <span class="counter-title-a d-block">
                                            
                                            <span class="counter">
                                            {{$total_enrollee or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Records</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Expenses</p>
                                        <span class="counter-title-a d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('total-expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Receivables</p>
                                        <span class="counter-title-a d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_receivable or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Discounts</p>
                                        <span class="counter-title-a d-block">
                                        Php 
                                        <span class="counter">
                                        {{$discount or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    
                 
                  
                    <!-- /.widget-holder -->
                </div>
                <!-- /.widget-list -->
            </div>


             {{--<div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Branch Sales Season 1</h6>
                        <p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>
                    </div>
                </div>
            </div>--}}

            {{--<div class="container-fluid">
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                    </div>

                    <div class="widget-holder col-lg-4">
    
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Daet Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                        <span class="counter">
                                        {{$total_daet_s1 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('daet/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Legaspi Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                        <span class="counter">
                                        {{$total_legaspi_s1 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('legaspi/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Masbate Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                            <span class="counter">
                                            {{$total_masbate_s1 or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <a href="{{url('masbate/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Naga Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_naga_s1 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('naga/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Sorsogon Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_sorsogon_s1 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('sorsogon/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>


                </div>
                <!-- /.widget-list -->
            </div>--}}

            {{--<div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Branch Sales Season 2</h6>
                        <p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>
                    </div>
                </div>
            </div>--}}

            {{--<div class="container-fluid">
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                    </div>

                    <div class="widget-holder col-lg-4">
    
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Daet Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                        <span class="counter">
                                        {{$total_daet_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('daet/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Legaspi Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                        <span class="counter">
                                        {{$total_legaspi_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('legaspi/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Masbate Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                            <span class="counter">
                                            {{$total_masbate_s2 or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <a href="{{url('masbate/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Naga Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_naga_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('naga/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Sorsogon Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_sorsogon_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('sorsogon/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>


                </div>
                <!-- /.widget-list -->
            </div>--}}

            {{--<div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Branch Enrollees</h6>
                        <p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>
                    </div>
                </div>
            </div>--}}

            {{--<div class="container-fluid">
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                    </div>

                    <div class="widget-holder col-lg-4">
    
                    </div>
                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Daet Enrollees</p>
                                        <span class="counter-title d-block">
                                             
                                        <span class="counter">
                                        {{$total_daet or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Legaspi Enrollees</p>
                                        <span class="counter-title d-block"><span class="counter">
                                        {{$total_legaspi or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Masbate Enrollees</p>
                                        <span class="counter-title d-block">
                                            
                                            <span class="counter">
                                            {{$total_masbate or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Naga Enrollees</p>
                                        <span class="counter-title d-block">
                                        
                                        <span class="counter">
                                        {{$total_naga or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Sorsogon Enrollees</p>
                                        <span class="counter-title d-block">
                                        
                                        <span class="counter">
                                        {{$total_sorsogon or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                </div>
                <!-- /.widget-list -->
            </div>--}}

            {{--<div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Branch Expenses</h6>
                        <p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>
                    </div>
                </div>
            </div>--}}

            {{--<div class="container-fluid">
                <div class="widget-list row">
                  
                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Daet Expenses</p>
                                        <span class="counter-title d-block">
                                            Php 
                                        <span class="counter">
                                        {{$daet_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('daet/expense')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Legaspi Expenses</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$legaspi_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('legaspi/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Masbate Expenses</p>
                                        <span class="counter-title d-block">
                                            Php 
                                            <span class="counter">
                                            {{$masbate_expense or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <a href="{{url('masbate/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Naga Expenses</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$naga_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('naga/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Sorsogon Expenses</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$sorsogon_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('sorsogon/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                </div>
                <!-- /.widget-list -->
            </div>--}}
            <!-- /.container-fluid -->
        </main>


@stop
@section('js')
<script type="text/javascript">
    $('.admin-dashboard').addClass('active');
</script>
@stop
