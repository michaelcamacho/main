@extends('main')
@section('title')
 Total {{$program}} Enrollee
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total {{$program}} Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total {{$program}} Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="total-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Branch</th>
                            <th>Contact #</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>School</th>
                            <th>Birthdate</th>                           
                            <th>Program</th>
                            <th>Section</th>
                            <th>Major</th>
                            <th>Take</th>
                            <th>NOA #</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Emer. Contact</th>
                            <th>Emer. #</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($a_prog))
                            @foreach($a_prog as $a_progs)
                        <tr>
                            <td>{{$a_progs->last_name}}, {{$a_progs->first_name}} {{$a_progs->middle_name}}</td>
                            <td>{{$a_progs->cbrc_id}}</td>
                            <td>{{$a_progs->branch}}</td>
                            <td>{{$a_progs->contact_no}}</td>
                            <td>{{$a_progs->email}}</td>
                            <td>{{$a_progs->address}}</td>
                            <td>{{$a_progs->school}}</td>
                            <td>{{$a_progs->birthdate}}</td>
                            <td>{{$a_progs->course}}</td>
                            <td>{{$a_progs->section}}</td>
                            <td>{{$a_progs->major}}</td>
                            <td>{{$a_progs->take}}</td>
                            <td>{{$a_progs->noa_no}}</td>                            
                            <td>{{$a_progs->username}}</td>
                            <td>{{$a_progs->password}}</td>
                            <td>{{$a_progs->contact_person}}</td>
                            <td>{{$a_progs->contact_details}}</td>
                        </tr>
                       @endforeach
                            @endif

                        
                    </tbody>           
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
  $('.ad-enrollee').addClass('active');
  $('.ad-enrollee').addClass('collapse in');
});

</script>
@stop