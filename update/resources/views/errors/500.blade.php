@extends('errors.main')
@section('title')
500
@stop
@section('main-content')

    <div id="wrapper" class="wrapper">
        <div class="content-wrapper">
            <main class="main-wrapper">
                <div class="page-title">
                    <h1 class="color-white">500</h1>
                </div>
                <h3 class="mr-b-5 color-white">Internal server error!</h3>
                <p class="mr-b-30 color-white fs-18 fw-200 heading-font-family">Please close this window and restart a new session or contact your service provider. <br/>Sorry for this inconvenience</p>
                <a href="/" class="btn btn-outline-white btn-rounded btn-block fw-700 text-uppercase">Go Back Home</a>
            </main>
        </div>
        <!-- .content-wrapper -->
    </div>

@endsection
