@extends('main')
@section('title')
NEW BOOK PAYMENT
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
    .btn-danger{
        border-radius: 20px;
        font-size: 12px;
        margin-bottom: 9px;
    }
    .btn-danger:hover{
        background-color: #cc0303;
    }
</style>
<link href="{{ asset('css/select.css') }}" rel="stylesheet" />
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">New Book Payment</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">New Book Payment</h5>
                                   <br/>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-book-payment'}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="branch" id="branch" value="{{Auth::user()->branch}}">
                                                    <input type="hidden" id="receipt" value=""> 
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" name="date" id="date" readonly>
                                                    <label>Date</label>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="program" id="program">
                                                        <option value="0" disable="true" selected="true">--- Select Program ---</option>
                                                        @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group input-has-value">
                                                   <input type="text" class="form-control js-example-basic-single" name="name" id="student" required>
                                                        
                                                    <label>Student Name</label>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                     <select class="form-control" name="book" type="text" id="book">
                                                        <option value="" disable="true" selected="true">--- Select Book ---</option>
                                                       
                                                    </select>
                                                    <label>Book Title</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group price" id="book-title">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group price" id="price">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="form-group" id="remove">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group total-amount price" id="total-amount">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" id="amount_paid" name="amount_paid" type="number" min="0" step="0.01" required>
                                                    <label>Amount Paid</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary print" type="submit" id="add" disabled>Add Book Payment</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
{{--<script src="{{ asset('/js/book-fetch.js') }}"></script>--}}
@include('js.book-payment')
<script src="{{ asset('../js/print-book-payment.js') }}"></script>
@stop