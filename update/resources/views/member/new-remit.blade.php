@extends('main')
@section('title')
REMIT
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Cash Remit</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Cash Remit</h5>
                                   <br/>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-remit'}}">
										@csrf
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" id="date" name="date" readonly>
                                                    <label>Date</label>
                                                </div>
                                            </div>
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="category" id="category" required>
                                                        <option value="">--- Select Cash Category ---</option>
                                                      <option value="Sales">Enrollee Sales</option>
                                                      <option value="Books">Book Sales</option>
                                                  </select>
                                                    <label>Cash Category</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="season" value="0"
                                                    id="season1" disabled>
                                                       <select class="form-control" name="season" id="season2" disabled required>
                                                        <option value="">--- Select Season ---</option>
                                                      <option value="Season 1">Season 1</option>
                                                      <option value="Season 2">Season 2</option>
                                                  </select>
                                                    <label>Season</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>

                                       
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                     <input class="form-control" name="amount" type="number" id="amount" required>
                                                       
                                                    <label>Amount</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" name="remarks" id="remarks">
                                                    <label>Remarks</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" type="submit" id="button-prevent">Remit...</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('.remit').addClass('active');
});
$('#category').on('change',function(e){
    console.log(e);

    var category = e.target.value;

    if (category == 'Sales'){

        $("#season2").removeAttr('disabled');
        $("#season1").prop('disabled', true);
    }

    if (category == 'Books'){

        $("#season2").prop('disabled', true);
        $("#season1").removeAttr('disabled');
    }


});
</script>
@stop