@extends('main')
@section('title')
ADD ENROLLEE
@stop
@section('css')
<style type="text/css">
	.date[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
}
	.date[type=date]:focus::-webkit-datetime-edit {
    color: #495057 !important;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Add New Enrollee</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Personal Information</h5>
                                   <br/>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-enrollee'}}">
										@csrf
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="cbrc_id" type="text">
                                                    <label>CBRC ID</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="last_name" type="text" required>
                                                    <label>Last Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="first_name" type="text" required>
                                                    <label>First Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="middle_name" type="text" required>
                                                    <label>Middle Name  &nbsp&nbsp(NM for nomiddle name)</label>
                                                </div>
                                            </div>
                                        </div>

                                       

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                	<input class="form-control date" name="birthdate" type="date" required>
                                                    <label>Birth Date</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="contact_no" type="text" required>
                                                    <label>Contact Number</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="address" type="text" required>
                                                    <label>Address</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="email" type="email" required>
                                                    <label>Email</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="username" type="text">
                                                    <label>User Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="password" type="text">
                                                    <label>Password</label>
                                                </div>
                                            </div>
                                        </div>
                                        <br/><br/>
                                        <h5 class="box-title mr-b-0"> Academic Background</h5>
                                        <br/><br/>

                                         <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="school" type="text" required>
                                                    <label>School</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="program" id="program" required>
		                                            	<option value="" >----</option>
		                                                @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
		                                            </select>
		                                            <label>Program</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="major" id="major">
                                                        <option value="">----</option>
                                                    </select>
                                                    <label>Major</label>
                                                </div>
                                            </div>
                                        </div>

                                        

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                     <input class="form-control" name="take" type="text">
                                                    <label>Take</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="noa_no" type="text">
                                                    <label>NOA #</label> 
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                 <div class="form-group">
		                                            
		                                        </div>
                                            </div>
                                        </div>

                                        <br/><br/>
                                        <h5 class="box-title mr-b-0"> Contact Person In Case Of Emergency</h5>
                                        <br/><br/>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="contact_person" required>
                                                    <label>Contact Person Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="contact_details" required>
                                                    <label>Contact Person Number</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                 <div class="form-group">
		                                            
		                                        </div>
                                            </div>
                                        </div>
                                       
                                       <br/>
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" id="button-prevent" type="submit">Add New Student</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('.add-enrollee').addClass('active');
});

$('#program').on('change', function(e){
            console.log(e);

            var program = e.target.value;

            $.get('/json-major?program=' + program,function(data){
                console.log(data);
                
                 $('#major').empty();
                 $('#major').append('<option value="" disable="true" selected="true">----</option>');
                 
                 $.each(data, function(index, majorObj){
                    $('#major').append('<option value="'+ majorObj.aka +'">'+ majorObj.major + '</option>');
                })
            });
        });
 

</script>
@stop