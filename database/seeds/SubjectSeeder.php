<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'The Teaching Profession (Ethico-Legal-Anthropological)',
            'aka'   =>  'let_subj_a',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'The Teaching Profession (Social-Philosophical-Historical)',
            'aka'     => 'let_subj_b',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'Principles and Methods of Teaching',
            'aka'   => 'let_subj_c'
        ]);
 
        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'Assessment of Learning',
            'aka'   => 'let_subj_d',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'Curriculum Development',
             'aka'   => 'let_subj_e',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'Social Dimensions of Education',
             'aka'   => 'let_subj_f',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'Educational Technology',
             'aka'   => 'let_subj_g',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'Child Adolescent Development',
             'aka'   => 'let_subj_h',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'prof_ed',
            'subject' => 'Facilitating Learning',
             'aka'   => 'let_subj_i',
        ]);


        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'gen_ed',
            'subject' => 'English',
             'aka'   => 'let_subj_j',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'gen_ed',
            'subject' => 'Mathematics',
             'aka'   => 'let_subj_k',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'gen_ed',
            'subject' => 'Filipino',
             'aka'   => 'let_subj_l',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'gen_ed',
            'subject' => 'Physical Science',
             'aka'   => 'let_subj_m',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'gen_ed',
            'subject' => 'Biological Science',
             'aka'   => 'let_subj_n',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'gen_ed',
            'subject' => 'Social Science',
             'aka'   => 'let_subj_o',
        ]);

       DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'English',
             'aka'   => 'let_subj_p',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'Mathematics',
             'aka'   => 'let_subj_q',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'Filipino',
             'aka'   => 'let_subj_r',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'Physical Science',
             'aka'   => 'let_subj_s',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'Biological Science',
             'aka'   => 'let_subj_t',
        ]);

        DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'Social Science',
             'aka'   => 'let_subj_u',
        ]);

       DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'TLE',
             'aka'   => 'let_subj_v',
        ]);

       DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'MAPEH',
             'aka'   => 'let_subj_w',
        ]);

       DB::table('subjects')->insert([
        	'program' => 'lets',
            'class' => 'majorship',
            'subject' => 'Values Education',
             'aka'   => 'let_subj_x',
        ]);

    }
}
