<?php

use Illuminate\Database\Seeder;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
        	'program_name' => 'LET',
            'aka' => 'lets',
        ]);

        DB::table('programs')->insert([
        	'program_name' => 'NLE',
            'aka' => 'nles',
        ]);

        DB::table('programs')->insert([
        	'program_name' => 'Criminology',
            'aka' => 'crims',
        ]);

        DB::table('programs')->insert([
        	'program_name' => 'Civil Service',
            'aka' => 'civils',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'Psychometrician',
            'aka' => 'psycs',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'NCLEX',
            'aka' => 'nclexes',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'IELTS',
            'aka' => 'ielts',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'Social Work',
            'aka' => 'socials',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'Agriculture',
            'aka' => 'agris',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'Midwifery',
            'aka' => 'mids',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'Online Only',
            'aka' => 'onlines',
        ]);
    }
}
