<?php

use Illuminate\Database\Seeder;

class ExpenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expenses')->insert([
        	'category' => 'Salary',
            'sub_category' => '',
        ]);
       	DB::table('expenses')->insert([
        	'category' => 'Tax',
            'sub_category' => '',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Insurance',
            'sub_category' => '',
        ]);
        DB::table('expenses')->insert([
            'category' => 'Loans',
            'sub_category' => '',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Allowance',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Gasoline',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Gifts',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Meals',
        ]);
        
        DB::table('expenses')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Professional Fee',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Transportation',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Posters | Signage | Flyers',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Rent',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Electricity',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Water',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Internet',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Venue',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Sound System',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Telephone',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Transportation',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Load',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Office Supply',
        ]);
        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Gasoline',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Meals',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Waybill',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Maintenance',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Daily Expense',
            'sub_category' => 'Allowance',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Daily Expense',
            'sub_category' => 'Grocery',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Others',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Professional Fee',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Allowance',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Reimbursement',
        ]);

        DB::table('expenses')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Tax',
        ]);
        DB::table('expenses')->insert([
            'category' => 'Lecturer',
            'sub_category' => 'Transportation',
        ]);
        DB::table('expenses')->insert([
            'category' => 'Lecturer',
            'sub_category' => 'Accommodation',
        ]);
        DB::table('expenses')->insert([
            'category' => 'Contribution',
            'sub_category' => 'SSS',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Contribution',
            'sub_category' => 'Pag-ibig',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Contribution',
            'sub_category' => 'PhilHealth',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Investment',
            'sub_category' => 'Franchise Fee',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Investment',
            'sub_category' => 'Office Equipment',
        ]);

        DB::table('expenses')->insert([
            'category' => 'Investment',
            'sub_category' => 'Furniture & Fixture',
        ]);

        DB::table('novaliches_petty_cashes')->insert([
            'petty_cash' => '0',
            
        ]);
        

        DB::table('novaliches_enrollee_sales')->insert([
            'cash_sales' => '0',
            
        ]);
       

        DB::table('novaliches_cash_books')->insert([
            'book_cash_sales' => '0',
            
        ]);
        

        DB::table('novaliches_s1_cashes')->insert([
            'cash' => '0',
            
        ]);

        DB::table('novaliches_s2_cashes')->insert([
            'cash' => '0',
            
        ]);
        

        DB::table('novaliches_book_cashes')->insert([
            'cash' => '0',
            
        ]);
    }
}
