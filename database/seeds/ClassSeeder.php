<?php

use Illuminate\Database\Seeder;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert([
        	'program' => 'lets',
            'class' => 'Professional Education',
            'aka'   => 'prof_ed',
        ]);

        DB::table('classes')->insert([
        	'program' => 'lets',
            'class' => 'General Education',
            'aka'   => 'gen_ed',
        ]);


        DB::table('classes')->insert([
        	'program' => 'lets',
            'class' => 'Majorship',
            'aka'   =>  'majorship',
        ]);
    }
}
