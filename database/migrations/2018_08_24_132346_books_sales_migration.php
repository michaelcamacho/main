<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BooksSalesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('novaliches_books_sales', function (Blueprint $table) {
            $table->increments('id');

            $table->string('date')->nullable();
            $table->string('branch')->nullable();
            $table->string('name')->nullable();
            $table->string('program')->nullable();
            $table->string('book_title')->nullable();
            $table->float('amount')->nullable();

            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_books_sales');

    }
}
