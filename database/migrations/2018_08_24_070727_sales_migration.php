<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaliches_s1_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id')->nullable();
            $table->string('date')->nullable();
            $table->string('name')->nullable();
            $table->string('program')->nullable();
            $table->string('category')->nullable();
            $table->string('discount_category')->nullable();
            $table->float('tuition_fee')->nullable();
            $table->float('facilitation_fee')->nullable();
            $table->float('discount')->nullable();
            $table->float('amount_paid')->nullable();
            $table->float('balance')->nullable();
            $table->string('season')->nullable();
            $table->string('year')->nullable();
            $table->timestamps();

        });

     

        Schema::create('novaliches_s2_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id')->nullable();
            $table->string('date')->nullable();
            $table->string('name')->nullable();
            $table->string('program')->nullable();
            $table->string('category')->nullable();
            $table->string('discount_category')->nullable();
            $table->float('tuition_fee')->nullable();
            $table->float('facilitation_fee')->nullable();
            $table->float('discount')->nullable();
            $table->float('amount_paid')->nullable();
            $table->float('balance')->nullable();
            $table->string('season')->nullable();
            $table->string('year')->nullable();
            $table->timestamps();

        });

         


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_s1_sales');

        Schema::dropIfExists('novaliches_s2_sales');

        


    }
}
