<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CashMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('novaliches_s1_cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->float('cash')->nullable();
            $table->timestamps();
        });

         Schema::create('novaliches_s2_cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->float('cash')->nullable();
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_s1_cashes');
        Schema::dropIfExists('novaliches_s2_cashes');
    }
}
