 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceivablesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('novaliches_receivables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('enrollee_id')->nullable();
            $table->string('name')->nullable();
            $table->string('program')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('season')->nullable();
            $table->float('balance')->nullable();

            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_receivables');

    }
}
