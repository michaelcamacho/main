<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CivilMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

          Schema::create('novaliches_civils', function (Blueprint $table) {
            $table->increments('id');

            $table->string('cbrc_id')->nullable();
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('username')->nullable();
            $table->string('password',60)->nullable();
            $table->string('course')->nullable();
            $table->string('major')->nullable();
            $table->string('program')->nullable();
            $table->string('section')->nullable();
            $table->string('id_pic')->nullable();
            $table->string('school')->nullable();
            $table->string('noa_no')->nullable();
            $table->string('category')->nullable();
            $table->string('status')->nullable();
            $table->string('take')->nullable();
            $table->string('branch')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('contact_details')->nullable();
            $table->string('facilitation')->nullable();
            $table->string('registration')->nullable();
            $table->string('season')->nullable();
            $table->string('year')->nullable();
            $table->timestamps();

        });

       

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_civils');


    }
}
