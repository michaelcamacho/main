<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrolleeSalesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaliches_enrollee_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->float('cash_sales')->nullable();
            $table->timestamps();

        });


        Schema::create('novaliches_cash_books', function (Blueprint $table) {
            $table->increments('id');
            $table->float('book_cash_sales')->nullable();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_enrollee_sales'); 
        Schema::dropIfExists('novaliches_cash_books'); 
 
    }
}
