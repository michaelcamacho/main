<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaliches_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('lecturer')->nullable();
            $table->string('like',4000)->nullable();
            $table->string('dislike',4000)->nullable();
            $table->string('comment',4000)->nullable();
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('section')->nullable();
            $table->string('class')->nullable();
            $table->string('subject')->nullable();
            $table->string('date')->nullable();
            $table->timestamps();

        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_comments');
    }
}
