<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BudgetsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('novaliches_budgets', function (Blueprint $table) {
            $table->increments('id');

            $table->string('date')->nullable();
            $table->float('amount')->nullable();
            $table->string('category')->nullable();
            $table->string('remarks')->nullable();

            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_budgets');


    }
}
