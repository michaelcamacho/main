<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookTranferTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_tranfer_trans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('book_date');
            $table->string('book_remarks');
            $table->string('book_branchSender');
            $table->string('book_branchReciever');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_tranfer_trans');
    }
}
