<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DroppedsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('novaliches_droppeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('enrollee_id')->nullable();
            $table->string('cbrc_id')->nullable();
            $table->string('name')->nullable();
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('school')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('season')->nullable();
            $table->string('year')->nullable();

            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_droppeds');


    }
}
