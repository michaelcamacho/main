<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PettyCashMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaliches_petty_cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->float('petty_cash')->nullable();
            $table->timestamps();

        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_petty_cashes'); 
    }
}
