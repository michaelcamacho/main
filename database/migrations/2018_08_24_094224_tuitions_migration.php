<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TuitionsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('novaliches_tuitions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('category')->nullable();
            $table->float('tuition_fee')->nullable();
            $table->float('facilitation_fee')->nullable();
            $table->string('season')->nullable();
            $table->string('year')->nullable();
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('novaliches_tuitions');
       


    }
}
