<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpensesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('novaliches_expenses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('date')->nullable();
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('category')->nullable();
            $table->string('sub_category')->nullable();
            $table->float('amount')->nullable();
            $table->string('remarks')->nullable();
            $table->string('author')->nullable();
            $table->string('Year')->nullable();
            $table->string('Season')->nullable();
            $table->timestamps();

        });

        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaliches_expenses');

        
        
    }
}
