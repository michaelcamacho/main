<script type="text/javascript">
    $(document).ready(function() {
  $('.new-payment').addClass('active');
});

    // In your Javascript (external .js resource or <script> tag)


 $('#program').on('change', function(e){
            console.log(e);

            var program = e.target.value;
            var branch = $('#branch').val().toLowerCase();

            $.get({{Auth::user()->position}}'json-student?program=' + program,function(data){
                console.log(data);

                 $('#student').empty();
                 $('#student').append('<option value="" disable="true" selected="true">--- Select Student ---</option>');

                 
                 $('#facilitation').empty();
                 $('#facilitation').append('<option value="0" disable="true" selected="true">--- Select Facilitation Fee ---</option>');

                 $('#discount').empty();
                 $('#discount').append('<option value="0" disable="true" selected="true">--- Select Discount ---</option>');

                 $('#category').prop('selectedIndex',0);
                 $('#year').val('');
                 $('#season').val('');
                 $('#rseason').val('');
                 $('#facilitation').val('');
                 $('#tuition').val('');
                 $('#total_amount').val('');
                 $('#balance').val('');
                 $('#id').val('');
                 $('#reserve').val('');

                  $.each(data, function(index, studentObj){
                    $('#student').append('<option value="'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'*'+ studentObj.id+'*'+ studentObj.contact_no +'*'+ studentObj.cbrc_id +'*'+ studentObj.school+'*' +studentObj.major +'*'+ studentObj.id +'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
                });
                 
            });
        });

        $('#student').on('change', function(e){
            console.log(e);

            var name = e.target.value;
            var program = $("#program").val();
            var ids = name.split("*");

            var id = ids[1];

            $.get({{Auth::user()->position}}'json-id?id=' + id + '&program=' + program,function(data){
                console.log(data);


                 $('#id').val('');
                
                  $.each(data, function(index, tuitionObj){
                    $('#id').val(tuitionObj.id);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#hd_branch').val(tuitionObj.branch);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#hd_id').val(tuitionObj.id);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_cbrc_id').val(tuitionObj.cbrc_id);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_last_name').val(tuitionObj.last_name);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_first_name').val(tuitionObj.first_name);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_middle_name').val(tuitionObj.middle_name);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_birthdate').val(tuitionObj.birthdate);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_contact_no').val(tuitionObj.contact_no);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_email').val(tuitionObj.email);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_address').val(tuitionObj.address);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_username').val(tuitionObj.username);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_password').val(tuitionObj.password);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_program').val(tuitionObj.program);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_major').val(tuitionObj.major);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_take').val(tuitionObj.take);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_school').val(tuitionObj.school);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_noa_no').val(tuitionObj.noa_no);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_contact_person').val(tuitionObj.contact_person);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#hd_contact_details').val(tuitionObj.contact_details);
                });

                $.each(data, function(index, tuitionObj){
                    $('#dr').val(moment(tuitionObj.created_at, "YYYY-MM-DD").format("MMM-D-YYYY"));
                 });   
                    
            });

        });


        $('#student').on('change', function(e){
            console.log(e);

            var name = e.target.value;
            var ids = name.split("*");
            var id = ids[1];
            
            var branch = $('#branch').val().toLowerCase();
            var program = $("#program").val();

            

            $.get({{Auth::user()->position}}'json-balance?id=' + id + '&program=' + program,function(data){
                console.log(data);
                $("#total_amount").val('');
                 $('#balance').val('');

                 
                  $.each(data, function(index, tuitionObj){
                    $('#balance').val(tuitionObj.balance);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#rseason').val(tuitionObj.season);
                })

                
            });
        });


        $('#student').on('change', function(e){
            console.log(e);

            var name = e.target.value;
            var ids = name.split("*");
            var id = ids[1];
            
            var branch = $('#branch').val();
            var program = $("#program").val();

            $('#category').prop('selectedIndex',0);
            $('#discount').prop('selectedIndex',0);

            $.get({{Auth::user()->position}}'json-reserved?id=' + id + '&program=' + program,function(data){
                console.log(data);
                
                 $('#reserve').val('');
                 
                //d2

                var value ="";
                var category = "";
              
                

                  $.each(data, function(index, tuitionObj){
                    $('#reserve').val(tuitionObj.reservation_fee);
                    
                    value = tuitionObj.discount_amount +','+ tuitionObj.discount_category;
                    if (value != null) {
             
                    $('#discount').append('<option value="'+ tuitionObj.discount_amount +','+ tuitionObj.discount_category +'">'+ tuitionObj.discount_amount+' ---- '+ tuitionObj.discount_category +'</option>');
                    $('#discount').val(value);
                    }
                    
                    
                    category = tuitionObj.category;

                    if(category != null){
                        $('#category').val(category);
                    }
                 });
                 
                
                
               if(value != null){

                $.get({{Auth::user()->position}}'json-tuition?program=' + program + '&category=' + category,function(data){
                console.log(data);

                 $('#facilitation').val('');
                 $('#season').val('');
                 $('#tuition').val('');
                 $('#year').val('');
                 $('#total_amount').val('');

                  $.each(data, function(index, tuitionObj){
                    $('#season').val(tuitionObj.season);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#year').val(tuitionObj.year);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#tuition').val(tuitionObj.tuition_fee);
                    
                });
                  $.each(data, function(index, tuitionObj){
                    $('#facilitation').val(tuitionObj.facilitation_fee);
                })
            });
            $('.val').addClass("input-has-value");
          

               }
               else{
                $('.val').removeClass("input-has-value");
                $('#discount').prop('selectedIndex',0);
                  $('#discount').append('<option value="0" disable="true" selected="true">--- Select Discount ---</option>');
                  $('#category').prop('selectedIndex',0);

               }

               
            });

    
        });


         $('#category').on('change', function(e){
            console.log(e);

            var category = e.target.value;
            var branch = $('#branch').val().toLowerCase();
            var program = $( "#program" ).val();

            $.get({{Auth::user()->position}}'json-tuition?program=' + program + '&category=' + category,function(data){
                console.log(data);

                 $('#facilitation').val('');
                 $('#season').val('');
                 $('#tuition').val('');
                 $('#year').val('');
                 $('#total_amount').val('');
               

                

                  $.each(data, function(index, tuitionObj){
                    $('#season').val(tuitionObj.season);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#year').val(tuitionObj.year);
                });

                  $.each(data, function(index, tuitionObj){
                    $('#tuition').val(tuitionObj.tuition_fee);
                });
                  $.each(data, function(index, tuitionObj){
                    $('#facilitation').val(tuitionObj.facilitation_fee);
                })
            });
        });

          $('#category').on('change', function(e){
            console.log(e);

            var category = e.target.value;
            var branch = $('#branch').val().toLowerCase();
            var program = $( "#program" ).val();


            $.get({{Auth::user()->position}}'json-discount?program=' + program + '&category=' + category,function(data){
                console.log(data);

                 $('#total_amount').val('');

              $('#discount').val('0');
                 $('#discount').append('<option value="0" disable="true" selected="true">--- Select Discount  ---</option>');
                
                 $.each(data, function(index, studentObj){
                    $('#discount').append('<option value="'+ studentObj.discount_amount +','+ studentObj.discount_category +'">'+ studentObj.discount_amount+' ---- '+ studentObj.discount_category +'</option>');
                })
            });
        });

$('#category').on('change', function(){


            var category = $("#category").val();

            if(category != null){
              $(".val").addClass("input-has-value");
            }

            else{
              $(".val").removeClass("input-has-value");
            }
        });

$('#student').on('change', function(){


              $(".balance").addClass("input-has-value");
            
              

        });


$('#compute').on('click', function(){

            var tuition = parseFloat($("#tuition").val());
            var facilitation = parseFloat($("#facilitation").val());
            var discount = parseFloat($("#discount").val());
            var balance = parseFloat($("#balance").val());
            var reserve = parseFloat($("#reserve").val());
            
            if (isNaN(tuition) === true && isNaN(facilitation) === true && isNaN(balance) === false){

                if (isNaN(reserve) === false){
                    $("#total_amount").val(balance - reserve);
                }
                if (isNaN(reserve) === true){
                $("#total_amount").val(balance);
                }

                $(".total").addClass("input-has-value");
            }
            
            if (isNaN(tuition) === false && isNaN(facilitation) === false && isNaN(balance) === true) {
                total_amount = tuition + facilitation - discount;
                if (isNaN(reserve) === false){
                    $("#total_amount").val(total_amount - reserve);
                }
                if (isNaN(reserve) === true){
                $("#total_amount").val(total_amount);
                }

                $(".total").addClass("input-has-value");
              }

            if (isNaN(tuition) === false && isNaN(facilitation) === false && isNaN(balance) === false){
                total_amount = tuition + facilitation + balance - discount;
                if (isNaN(reserve) === false){
                    $("#total_amount").val(total_amount - reserve);
                }
                if (isNaN(reserve) === true){
                $("#total_amount").val(total_amount);
                }
                $(".total").addClass("input-has-value");
            }

            if (isNaN(tuition) === true && isNaN(facilitation) === true && isNaN(balance) === true){
                $("#total_amount").val('');
            }
      });
            
$(".readonly").keydown(function(e){
        e.preventDefault();
    });

$('#discount').on('click', function(){
    $('#total_amount').val('');
    });

</script>