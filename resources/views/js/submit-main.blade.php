<script type="text/javascript">
$('#main').submit(function(event){
    
  event.preventDefault();
  event.stopPropagation();
  
          
        
               var formData = new FormData();
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('branch', $('#hd_branch').val());
                formData.append('id', $('#hd_id').val());
                formData.append('cbrc_id', $('#hd_cbrc_id').val());
                formData.append('last_name', $('#hd_last_name').val());
                formData.append('first_name', $('#hd_first_name').val());
                formData.append('middle_name', $('#hd_middle_name').val());
                formData.append('birthdate', $('#bhd_irthdate').val());
                formData.append('contact_no', $('#hd_contact_no').val());
                formData.append('address', $('#hd_address').val());
                formData.append('email', $('#hd_email').val());
                formData.append('username', $('#hd_username').val());
                formData.append('password', $('#hd_password').val());
                formData.append('school', $('#hd_school').val());
                formData.append('program', $('#hd_program').val());
                formData.append('major', $('#hd_major').val());
                formData.append('take', $('#hd_take').val());
                formData.append('noa_no', $('#hd_noa_no').val());
                formData.append('category', $('#category').val());
                formData.append('status', 'Enrolled');
                formData.append('contact_person', $('#hd_contact_person').val());
                formData.append('contact_details', $('#hd_contact_details').val());
                formData.append('facilitation', $('#facilitation').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                  url: "",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                           
                 
                   
        });
});//
</script>