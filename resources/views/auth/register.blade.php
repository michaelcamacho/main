@extends('layouts.app')
@section('title')
Register
@stop
@section('main-content')
<div class="auth-2-outer row align-items-center h-p100 m-0">
        <div class="auth-2">
          <div class="auth-logo font-size-40">
            <a href="../index.html" class="text-white"><b>CBRC - MAIN</b></a>
          </div>
          <!-- /.login-logo -->
          <div class="auth-body">
            <p class="auth-msg">Register a new Membership</p>

            <form class="form-element" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf
              <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
                 @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                <span class="ion ion-email form-control-feedback "></span>
              </div>

              <div class="form-group has-feedback">
                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required placeholder="Username">
                 @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                <span class="ion ion-email form-control-feedback "></span>
              </div>

              <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                <span class="ion ion-locked form-control-feedback "></span>
              </div>
              <div class="form-group has-feedback">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Retype password">
                <span class="ion ion-log-in form-control-feedback "></span>
              </div>
              <div class="row">
                <div class="col-12">
                  <div class="checkbox">
                    <input type="checkbox" id="basic_checkbox_1" >
                    <label for="basic_checkbox_1">I agree to the <a href="#" class="text-danger"><b>Terms</b></a></label>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-12 text-center">
                  <button type="submit" class="btn btn-block mt-10 btn-success">SIGN UP</button>
                </div>
                <!-- /.col -->
              </div>
            </form>

            <div class="text-center text-white">
              {{--<p class="mt-50">- Sign With -</p>--}}
              <p class="gap-items-2 mb-20" style="margin-top: 70px;">
                  <a class="btn btn-social-icon btn-outline btn-white" href="https://www.facebook.com/RRpomeranian/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                  <a class="btn btn-social-icon btn-outline btn-white" href="https://www.instagram.com/rrpomeranian/?hl=en" target="_blank"><i class="fab fa-instagram"></i></a>
                </p>    
            </div>
            <!-- /.social-auth-links -->

            <div class="margin-top-30 text-center">
                <p>Already have an account? <a href="{{ url('/login') }}" class="text-info m-l-5">Sign In</a></p>
            </div>

          </div>
        </div>
    
    </div>
    @stop