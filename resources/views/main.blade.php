<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from kineticpro.dharansh.in/default/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Aug 2018 15:06:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pace.css') }}">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title') | CBRC - MAIN</title>
    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/select2/4.0.5/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/jqvmap/1.5.1/jqvmap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/nav.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/public.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/owl.theme.default.css') }}" rel="stylesheet" type="text/css">
     <!-- SweetAlert2  -->
      <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
      <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">

      <link href="{{ asset('css/styles.css')}} " rel="stylesheet" />
      <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css"> 

<!-- FontAwesome-->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<!-- End FontAwesome -->


      <meta name="author" content="cipherfusionphilippines.com" />
        <link rel="canonical" href="https://www.cipherfusionphilippines.com/" />
        <meta property="fb:admins" content="100001809531980">
        <meta property="fb:app_id" content="206704316521457">
        <meta property="og:image" content="https://cbrc-bicol.com/img/fblogo.png"/>
        <meta property="og:locale" content="en_PH" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="CBRC Main" />
        <meta property="og:description" content="The BIGGEST and LEADING Brand in TEST PREPARATION" />
        <meta property="og:url" content="https://cbrc-bicol.com" />
        <meta property="og:site_name" content="CBRC Main" />
        <!-- Mobile Specific Metas
                ================================================== -->
                <meta name="viewport" content="user-scalable=yes, initial-scale=1, maximum-scale=1, width=device-width">

                <meta name="application-name" content="CBRC Main"/>
                        <meta name="msapplication-square70x70logo" content="/img/small.jpg"/>
                        <meta name="msapplication-square150x150logo" content="/img/medium.jpg"/>
                        <meta name="msapplication-wide310x150logo" content="/img/wide.jpg"/>
                        <meta name="msapplication-square310x310logo" content="/img/large.jpg"/>
                        <meta name="msapplication-TileColor" content="#000000"/>
                <script type="text/javascript">
                    cdn_url = 'https://cbrc-bicol.com/';
                </script>


        <script src="{{ asset('js/select2.min.js') }}"></script>
    @yield('css')
    <!-- Head Libs -->
    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
    <script data-pace-options='{ "ajax": false, "selectors": [ "img" ]}' src="{{ asset('assets/js/pace.min.js') }}"></script>

</head>

<body class="sidebar-light sidebar-expand navbar-brand-dark" id="data">

        <a id="fbmessenger" href="https://www.facebook.com/messages/t/cipherfusion" target="_blank"><i class="fab fa-facebook-square"></i> Chat Now</a>

        <a href="fb-messenger://user/1060720417298686" id="fbmessenger-mobile"><i class="fab fa-facebook-messenger"></i> Chat Now</a>
    
<div id="wrapper" class="wrapper">

@include('template.header')

<div class="content-wrapper">

@role('admin')
@include('template.nav')
@endrole

@role('abra')
@include('template.abra-nav')
@endrole

@role('abra_cashier')
@include('template.abra-cashier-nav')
@endrole

@role('abra_enrollment')
@include('template.abra-enrollment-nav')
@endrole

@role('baguio')
@include('template.baguio-nav')
@endrole

@role('baguio_cashier')
@include('template.baguio-cashier-nav')
@endrole

@role('baguio_enrollment')
@include('template.baguio-enrollment-nav')
@endrole

@role('calapan')
@include('template.calapan-nav')
@endrole

@role('calapan_cashier')
@include('template.calapan-cashier-nav')
@endrole

@role('calapan_enrollment')
@include('template.calapan-enrollment-nav')
@endrole

@role('candon')
@include('template.candon-nav')
@endrole

@role('candon_cashier')
@include('template.candon-cashier-nav')
@endrole

@role('candon_enrollment')
@include('template.candon-enrollment-nav')
@endrole

@role('dagupan')
@include('template.dagupan-nav')
@endrole

@role('dagupan_cashier')
@include('template.dagupan-cashier-nav')
@endrole

@role('dagupan_enrollment')
@include('template.dagupan-enrollment-nav')
@endrole

@role('fairview')
@include('template.fairview-nav')
@endrole

@role('fairview_cashier')
@include('template.fairview-cashier-nav')
@endrole

@role('fairview_enrollment')
@include('template.fairview-enrollment-nav')
@endrole

@role('las_pinas')
@include('template.las_pinas-nav')
@endrole

@role('las_pinas_cashier')
@include('template.las_pinas-cashier-nav')
@endrole

@role('las_pinas_enrollment')
@include('template.las_pinas-enrollment-nav')
@endrole

@role('launion')
@include('template.launion-nav')
@endrole

@role('launion_cashier')
@include('template.launion-cashier-nav')
@endrole

@role('launion_enrollment')
@include('template.launion-enrollment-nav')
@endrole

@role('manila')
@include('template.manila-nav')
@endrole

@role('manila_cashier')
@include('template.manila-cashier-nav')
@endrole

@role('manila_enrollment')
@include('template.manila-enrollment-nav')
@endrole

@role('roxas')
@include('template.roxas-nav')
@endrole

@role('roxas_cashier')
@include('template.roxas-cashier-nav')
@endrole

@role('roxas_enrollment')
@include('template.roxas-enrollment-nav')
@endrole

@role('tarlac')
@include('template.tarlac-nav')
@endrole

@role('tarlac_cashier')
@include('template.tarlac-cashier-nav')
@endrole

@role('tarlac_enrollment')
@include('template.tarlac-enrollment-nav')
@endrole

@role('vigan')
@include('template.vigan-nav')
@endrole

@role('vigan_cashier')
@include('template.vigan-cashier-nav')
@endrole

@role('vigan_enrollment')
@include('template.vigan-enrollment-nav')
@endrole


@include('sweet::alert')

@yield('main-content')
</div>

@include('template.footer')

</div>
    <script src="{{ asset('assets/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>       
    <script src="{{ asset('assets/ajax/libs/popper.js/1.14.3/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/metisMenu/2.7.9/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.js"></script>
    <script src="{{asset('assets/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js')}}"></script>
    <script src="{{ asset('assets/ajax/libs/countup.js/1.9.2/countUp.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/Chart.js/2.7.2/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/select2/4.0.5/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jqvmap/1.5.1/jquery.vmap.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jqvmap/1.5.1/maps/jquery.vmap.usa.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/template.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.js') }}"></script>

    


    @yield('js')
    <script type="text/javascript">
        $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
    </script>

    
</body>

</html>