@extends('main')
@section('title')
 Attendance Monitoring
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
     <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="cont">
                                       <form class="form-control">
                                    @csrf
                                    <table class="table table-striped table-bordered"  id="myTable" cellspacing="0">
                                        <div class="form-group">
                                             <p style="margin-bottom: 0px;"><b>Branch :<b></p>
                                            <p style="margin-bottom: 0px;">CBRC Student Attendance Reports</p>               
                                            <p>Season :</p>               
                                                </div> 
                                                <input type="hidden" value="" name="excat" id="cat"/>   
                                                <input type="hidden" value="" name="excatid" id="excatid" />          
                                                <thead>
                                                <tr>
                                                    <th class="text-center">ID Number</th>
                                                    <th class="text-center">Student Name</th>
                                                    <th class="text-center">School</th> 
                                                    <th class="text-center">Present Attend</th>
                                                    <th class="text-center">Total Days</th>
                                                    <th class="text-center">Terminal</th>
                                                    
                                                </tr>       
                                            </thead> 
                                              <tbody id="stdlist"> 
                                                                                    
                                                      <tr> 
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                           
                                                        </tr>
                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>           

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</main>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
@stop