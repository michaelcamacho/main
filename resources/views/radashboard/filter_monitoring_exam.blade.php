@extends('main')
@section('title')
 Exam Monitoring
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">  
      <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-7">
                        <div class="card m-b-30">
                           <div class="card-body">
                             <div class="form-group">
                                <h4 style="margin-bottom: 0px;"> CBRC Exam Monitoring</h4>
                                <p style="color:red;margin-top: -10px;">*Please Select Category</p>
                            <form action="/radashboard/exam-monitoring" method="POST">    
                             @csrf     
                                <label>Select Season</label>
                                 <select id="cs" name ="season"  class="form-control seasonSearch" required>
                                   <option value="0" selected="true" disabled = "true"> --- Select Season ---</option>           
                                   <option value="Season 1">Season 1</option>  
                                   <option value="Season 2">Season 2</option>
                                </select> 
                                <label>Select Year</label>
                                 <select id="cs" name ="year"  class="form-control seasonSearch" required>
                                   <option value="0" selected="true" disabled = "true"> --- Select Year ---</option>           
                                   @foreach($years as $years)
                                   <option>
                                      {{$years}} 
                                   </option>  
                                   @endforeach
                                </select> 
                                <label>Select Program</label>
                                 <select id="cs" name ="program"  class="form-control programFilter" required>
                                   <option value="0" selected="true" disabled = "true"> --- Select Program ---</option>           
                                   @foreach($program as $program)
                                   <option>
                                       {{$program->program_name}}
                                   </option>  
                                   @endforeach
                                </select> 
                                <label>Select Major</label>
                                 <select id="cs" name ="major"  class="form-control majorFilter">
                                   <option value="0" selected="true" disabled = "true"> ---- Select Major ---</option>           
                                   <option value="" id="BEED" style="display:none"> BEED </option>
                                   <option value="" id="BSED" style="display:none"> BSED</option>  
                                
                                </select> 
                                <button type="submit" class="btn btn-success btn-md btn-block sendFilter" style="margin-top: 10px;">Proceed</button>
                            </form>
                            </div>  
                        </div>        
                    </div>  
               </div>
            </div>
        </div>
    </div>
</main>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

    $('.programFilter').on('change', function() {
          var major =  this.value;
          if (major == "LET") {
            $('#BEED').show();
            $('#BSED').show(); 
          }
          else if(major != "LET"){
            $('.majorFilter').val('---- Select Major ---');
             $('#BEED').hide();
            $('#BSED').hide(); 
          }
        });
</script>



@stop