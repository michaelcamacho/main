@extends('main')
@section('title')
 Result Analysis
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
     <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="cont">
                                       <form class="form-control" action="/exam-result" method="POST">
                                    @csrf
                                    <table class="table table-striped table-bordered"  id="myTable" cellspacing="0">
                                                
                                                <thead>
                                                <tr>
                                                    <th class="text-center">CBRC ID</th>
                                                    <th class="text-center">Name</th>
                                                    <Th class="text-center">School</Th> 
                                                    <th class="text-center"># of Take</th>
                                                    <th class="text-center">RAW SCORE</th>
                                                    <th class="text-center">Percent</th>
                                                    <th class="text-center">Quartile</th>
                                                </tr>       
                                            </thead> 
                                              <tbody id="stdlist"> 
                                                                                    
                                                      <tr>      
                                                        <input type="hidden" name="std[]" value="" />
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                            <td class="text-center"></td>
                                                        </tr>
                                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>           

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</main>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
@stop