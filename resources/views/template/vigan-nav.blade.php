  <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
            <!-- User Details -->
            <div class="side-user">
                <figure class="side-user-bg" style="background-image: url(assets/demo/user-image-cropped.jpg)">
                    <img src="assets/demo/user-image-cropped.jpg" alt="" class="d-none">
                </figure>
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <span class="avatar-text">{{substr(Auth::user()->name,0,1)}}</span>
                        <figure class="avatar-img thumb-sm mr-b-0 d-none">
                            <img src="assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                        </figure>
                    </div>
                    <!-- /.d-inline-block -->
                    <div class="lh-14 mr-t-5 sidebar-collapse-hidden">
                        <h6 class="hide-menu side-user-heading">{{ Auth::user()->name }}</h6><small class="hide-menu">{{ Auth::user()->email}}</small>
                    </div>
                </div>
                <!-- /.col-sm-12 -->
            </div>
            <!-- /.side-user -->
            <!-- Sidebar Menu -->
            <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                  
                                     <li class="current-page dashboard"><a href="{{ url('/vigan/dashboard')}}"><i class="list-icon material-icons">home</i> <span class="hide-menu">Dashboard</span></a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/vigan/add-enrollee')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Add Enrollee </span></a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/vigan/new-payment')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">New Payment </span></a></li>

                                    <li class="current-page new-reservation"><a href="{{ url('/vigan/new-reservation')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">New Reservation </span></a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/vigan/add-expense')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Add Expense </span></a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/vigan/add-budget')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Add Budget </span></li>

                                    <li class="current-page book-payment"><a href="{{ url('/vigan/book-payment')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Book Payment </span></a></li>

                                    <li class="current-page remit"><a href="{{ url('/vigan/new-remit')}}"><i class="list-icon material-icons">add</i> <span class="hide-menu">Cash Remit </span></a></li>

                                    <li class="menu-item-has-children reports"><a href="javascript:void(0);">
                                        <i class="list-icon material-icons">apps</i> <span class="hide-menu">Reports</span></a>  
                                        <ul class="list-unstyled sub-menu reports">
                                      
                
                                           <li><a href="{{ url('/vigan/today')}}">Today Transaction</a>
                                            </li>
                                            
                                            <li><a href="{{ url('/vigan/yesterday')}}">Yesterday Transaction</a>
                                            </li>
                                             <li class="item-has-children scorecard"><a href="javascript:void(0);">
                                                    <span class="hide-menu">Score Card</span></a>
                                                    <ul class="list-unstyled sub-menu scorecard">
                                                        <li>
                                                            <a href="{{ url('/vigan/scorecard-season/1')}}">Season 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ url('/vigan/scorecard-season/2')}}">Season 2</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="{{ url('/vigan/financial-report')}}">Financial Report</a>
                                                </li>
                                               
                                          
                                        </ul>
                                    </li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons">folder</i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/vigan/let')}}">LET</a></li>
                                            <li><a href="{{ url('/vigan/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/vigan/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/vigan/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/vigan/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/vigan/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/vigan/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/vigan/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/vigan/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/vigan/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/vigan/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/vigan/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/vigan/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/vigan/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/vigan/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/vigan/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/vigan/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/vigan/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/vigan/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/vigan/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/vigan/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/vigan/books')}}">Books</a></li>
                                            <li><a href="{{ url('/vigan/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/vigan/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                <li>
                                    <li class="menu-item-has-children hris"><a href="javascript:void(0);"><i class="list-icon material-icons">folder</i> <span class="hide-menu">HRIS</span></a>                    
                                    <ul class="list-unstyled sub-menu hris">
                                        <li><a href="{{ url('/vigan/add-employee')}}">Add Employee</a></li>
                                        <li><a href="{{ url('/vigan/employee-record')}}">Employee Records</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children others"><a href="javascript:void(0);"><i class="list-icon material-icons">folder</i> <span class="hide-menu">Others</span></a>                    
                                    <ul class="list-unstyled sub-menu others">
                                        <li><a href="{{ url('/vigan/student-id')}}">Passer's Templates</a></li>
                                        <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li>
                                    </ul>
                                </li>
                                
                                {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons" style="margin-right: 13px;">content_paste</i>Lecturer Evaluation</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/vigan/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/vigan/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/vigan/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                </li> --}}
                                </ul>
                            
                           

                           
            </nav>
            <!-- /.sidebar-nav -->
        </aside>