  <nav class="navbar">
    <link href="{{ asset('assets/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
            <div class="container-fluid px-0 align-items-stretch">
                <!-- Logo Area -->
                <div class="navbar-header">
                    <a href="{{ url('/')}}}" class="navbar-brand">
                        <h6 class="logo-expand">CBRC - MAIN</h3>
                        <h6 class="logo-collapse">CBRC</h3>
                    </a>
                </div>
                <!-- /.navbar-header -->
                <!-- Left Menu & Sidebar Toggle -->
                <ul class="nav navbar-nav toggler">
                    <li class="sidebar-toggle dropdown"><a href="javascript:void(0)" class="ripple"><i class="material-icons list-icon md-24">menu</i></a>
                    </li>
                </ul>
                <!-- /.navbar-left -->
                <!-- Search Form -->
                
                <!-- /.navbar-search -->
                <div class="spacer"></div>
                <!-- Right Menu -->
                
                <!-- /.navbar-right -->
                <!-- User Image with Dropdown -->
                <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="javascript:void(0);" class="ripple" data-toggle="dropdown"><span class="avatar thumb-xs2"><img src="../assets/demo/users/user1.jpg" class="rounded-circle" alt=""> <i class="material-icons list-icon">expand_more</i></span></a>
                        <div
                        class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                            <div class="card">
                                
                                <ul class="list-unstyled card-body">
                                    <li><a href="{{ url('/logout')}}"><span><span class="align-middle"><i class="fa fa-sign-out"></i> Log Out</span></span></a>
                                    </li>
                                </ul>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                          </div>
            <!-- /.dropdown-card-profile -->
            </li>
            <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-nav -->
    </div>
    <!-- /.container-fluid -->
    </nav>