  <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
            <!-- User Details -->
            <div class="side-user">
                <figure class="side-user-bg" style="background-image: url(assets/demo/user-image-cropped.jpg)">
                    <img src="../assets/demo/user-image-cropped.jpg" alt="" class="d-none">
                </figure>
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <span class="avatar-text">{{substr(Auth::user()->name,0,1)}}</span>
                        <figure class="avatar-img thumb-sm mr-b-0 d-none">
                            <img src="../assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                        </figure>
                    </div>
                    <!-- /.d-inline-block -->
                    <div class="lh-14 mr-t-5 sidebar-collapse-hidden">
                        <h6 class="hide-menu side-user-heading">{{ Auth::user()->name }}</h6><small class="hide-menu">{{ Auth::user()->email}}</small>
                    </div>
                </div>
                <!-- /.col-sm-12 -->
            </div>
            <!-- /.side-user -->
            <!-- Sidebar Menu -->
            <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                    <li class="current-page admin-dashboard"><a href="{{ url('/dashboard')}}"><i class="list-icon material-icons">home</i> <span class="hide-menu">Dashboard</span></a>
                    </li>
                    <li class="menu-item-has-children branches "><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">Branches</span></a>
                        <ul class="list-unstyled sub-menu branches">

                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Abra</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/abra/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/abra/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/abra/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/abra/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/abra/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/abra/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/abra/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/abra/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/abra/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/abra/let')}}">LET</a></li>
                                            <li><a href="{{ url('/abra/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/abra/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/abra/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/abra/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/abra/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/abra/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/abra/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/abra/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/abra/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/abra/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/abra/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/abra/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/abra/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/abra/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/abra/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/abra/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/abra/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/abra/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/abra/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/abra/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/abra/books')}}">Books</a></li>
                                             <li><a href="{{ url('/abra/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/abra/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/abra/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/abra/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/abra/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/abra/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Baguio</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/baguio/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/baguio/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/baguio/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/baguio/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/baguio/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/baguio/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/baguio/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/baguio/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/baguio/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/baguio/let')}}">LET</a></li>
                                            <li><a href="{{ url('/baguio/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/baguio/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/baguio/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/baguio/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/baguio/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/baguio/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/baguio/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/baguio/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/baguio/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/baguio/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/baguio/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/baguio/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/baguio/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/baguio/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/baguio/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/baguio/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/baguio/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/baguio/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/baguio/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/baguio/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/baguio/books')}}">Books</a></li>
                                             <li><a href="{{ url('/baguio/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/baguio/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/baguio/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/baguio/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/baguio/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/baguio/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Calapan</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/calapan/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/calapan/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/calapan/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/calapan/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/calapan/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/calapan/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/calapan/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/calapan/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/calapan/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/calapan/let')}}">LET</a></li>
                                            <li><a href="{{ url('/calapan/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/calapan/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/calapan/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/calapan/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/calapan/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/calapan/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/calapan/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/calapan/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/calapan/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/calapan/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/calapan/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/calapan/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/calapan/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/calapan/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/calapan/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/calapan/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/calapan/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/calapan/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/calapan/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/calapan/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/calapan/books')}}">Books</a></li>
                                             <li><a href="{{ url('/calapan/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/calapan/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/calapan/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/calapan/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/calapan/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/calapan/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Candon</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/candon/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/candon/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/candon/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/candon/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/candon/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/candon/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/candon/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/candon/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/candon/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/candon/let')}}">LET</a></li>
                                            <li><a href="{{ url('/candon/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/candon/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/candon/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/candon/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/candon/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/candon/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/candon/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/candon/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/candon/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/candon/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/candon/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/candon/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/candon/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/candon/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/candon/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/candon/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/candon/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/candon/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/candon/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/candon/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/candon/books')}}">Books</a></li>
                                            <li><a href="{{ url('/candon/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/candon/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/candon/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/candon/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/candon/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/candon/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Dagupan</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/dagupan/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/dagupan/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/dagupan/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/dagupan/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/dagupan/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/dagupan/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/dagupan/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/dagupan/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/dagupan/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/dagupan/let')}}">LET</a></li>
                                            <li><a href="{{ url('/dagupan/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/dagupan/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/dagupan/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/dagupan/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/dagupan/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/dagupan/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/dagupan/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/dagupan/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/dagupan/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/dagupan/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/dagupan/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/dagupan/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/dagupan/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/dagupan/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/dagupan/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/dagupan/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/dagupan/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/dagupan/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/dagupan/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/dagupan/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/dagupan/books')}}">Books</a></li>
                                            <li><a href="{{ url('/dagupan/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/dagupan/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/dagupan/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/dagupan/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/dagupan/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/dagupan/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Fairview</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/fairview/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/fairview/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/fairview/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/fairview/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/fairview/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/fairview/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/fairview/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/fairview/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/fairview/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/fairview/let')}}">LET</a></li>
                                            <li><a href="{{ url('/fairview/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/fairview/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/fairview/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/fairview/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/fairview/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/fairview/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/fairview/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/fairview/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/fairview/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/fairview/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/fairview/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/fairview/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/fairview/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/fairview/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/fairview/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/fairview/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/fairview/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/fairview/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/fairview/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/fairview/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/fairview/books')}}">Books</a></li>
                                             <li><a href="{{ url('/fairview/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/fairview/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/fairview/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/fairview/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/fairview/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/fairview/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">LasPinas</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/las_pinas/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/las_pinas/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/las_pinas/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/las_pinas/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/las_pinas/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/las_pinas/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/las_pinas/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/las_pinas/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/las_pinas/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/las_pinas/let')}}">LET</a></li>
                                            <li><a href="{{ url('/las_pinas/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/las_pinas/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/las_pinas/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/las_pinas/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/las_pinas/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/las_pinas/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/las_pinas/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/las_pinas/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/las_pinas/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/las_pinas/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/las_pinas/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/las_pinas/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/las_pinas/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/las_pinas/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/las_pinas/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/las_pinas/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/las_pinas/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/las_pinas/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/las_pinas/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/las_pinas/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/las_pinas/books')}}">Books</a></li>
                                             <li><a href="{{ url('/las_pinas/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/las_pinas/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/las_pinas/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/las_pinas/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/las_pinas/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/las_pinas/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Launion</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/launion/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/launion/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/launion/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/launion/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/launion/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/launion/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/launion/book-transfer')}}">Book Transfer</a></li>

                                    <li class="current-page book-payment"><a href="{{ url('/launion/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/launion/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/launion/let')}}">LET</a></li>
                                            <li><a href="{{ url('/launion/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/launion/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/launion/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/launion/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/launion/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/launion/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/launion/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/launion/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/launion/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/launion/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/launion/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/launion/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/launion/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/launion/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/launion/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/launion/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/launion/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/launion/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/launion/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/launion/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/launion/books')}}">Books</a></li>
                                             <li><a href="{{ url('/launion/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/launion/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/launion/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/launion/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/launion/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/launion/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Manila</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/manila/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/manila/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/manila/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/manila/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/manila/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/manila/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/manila/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/manila/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/manila/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/manila/let')}}">LET</a></li>
                                            <li><a href="{{ url('/manila/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/manila/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/manila/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/manila/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/manila/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/manila/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/manila/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/manila/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/manila/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/manila/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/manila/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/manila/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/manila/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/manila/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/manila/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/manila/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/manila/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/manila/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/manila/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/manila/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/manila/books')}}">Books</a></li>
                                            <li><a href="{{ url('/manila/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/manila/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/manila/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/manila/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/manila/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/manila/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Roxas</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/roxas/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/roxas/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/roxas/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/roxas/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/roxas/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/roxas/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/roxas/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/roxas/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/roxas/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/roxas/let')}}">LET</a></li>
                                            <li><a href="{{ url('/roxas/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/roxas/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/roxas/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/roxas/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/roxas/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/roxas/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/roxas/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/roxas/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/roxas/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/roxas/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/roxas/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/roxas/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/roxas/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/roxas/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/roxas/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/roxas/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/roxas/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/roxas/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/roxas/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/roxas/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/roxas/books')}}">Books</a></li>
                                            <li><a href="{{ url('/roxas/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/roxas/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/roxas/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/roxas/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/roxas/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/roxas/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>

                            

                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Tarlac</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/tarlac/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/tarlac/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/tarlac/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/tarlac/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/tarlac/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/tarlac/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/tarlac/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/tarlac/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/tarlac/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/tarlac/let')}}">LET</a></li>
                                            <li><a href="{{ url('/tarlac/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/tarlac/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/tarlac/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/tarlac/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/tarlac/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/tarlac/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/tarlac/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/tarlac/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/tarlac/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/tarlac/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/tarlac/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/tarlac/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/tarlac/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/tarlac/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/tarlac/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/tarlac/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/tarlac/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/tarlac/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/tarlac/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/tarlac/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/tarlac/books')}}">Books</a></li>
                                            <li><a href="{{ url('/tarlac/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/tarlac/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/tarlac/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/tarlac/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/tarlac/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/tarlac/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu">Vigan</span></a>
                                <ul class="list-unstyled sub-menu">
                                    <li class="current-page dashboard"><a href="{{ url('/vigan/dashboard')}}">Dashboard</a>
                                     </li>
                                    <li class="current-page add-enrollee"><a href="{{ url('/vigan/add-enrollee')}}">Add Enrollee</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/vigan/new-payment')}}">New Payment</a></li>

                                    <li class="current-page new-payment"><a href="{{ url('/vigan/new-reservation')}}">New Reservation</a></li>

                                    <li class="current-page add-expense"><a href="{{ url('/vigan/add-expense')}}">Add Expense</a></li>

                                    <li class="current-page add-budget"><a href="{{ url('/vigan/add-budget')}}">Add Budget</a></li>

                                    <li class="current-page book-transfer"><a href="{{ url('/vigan/book-transfer')}}">Book Transfer</a></li> 

                                    <li class="current-page book-payment"><a href="{{ url('/vigan/book-payment')}}">Book Payment</a></li>

                                    <li class="current-page"><a href="{{ url('/vigan/new-remit')}}">Cash Remit</a></li>

                                    <li class="menu-item-has-children record">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Records</span></a>
                                            <ul class="list-unstyled sub-menu record">
                                             <li class="menu-item-has-children program">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Programs</span></a>
                                            <ul class="list-unstyled sub-menu program">
                                            <li class=""><a href="{{ url('/vigan/let')}}">LET</a></li>
                                            <li><a href="{{ url('/vigan/nle')}}">NLE</a></li>
                                            <li><a href="{{ url('/vigan/crim')}}">Criminology</a></li>
                                            <li><a href="{{ url('/vigan/civil')}}">Civil Service</a></li>
                                            <li><a href="{{ url('/vigan/psyc')}}">Psychometrician</a></li>
                                            <li><a href="{{ url('/vigan/nclex')}}">NCLEX</a></li>
                                            <li><a href="{{ url('/vigan/ielts')}}">IELTS</a></li>
                                            <li><a href="{{ url('/vigan/social')}}">Social Work</a></li>
                                            <li><a href="{{ url('/vigan/agri')}}">Agriculture</a></li>
                                            <li><a href="{{ url('/vigan/mid')}}">Midwifery</a></li>
                                            <li><a href="{{ url('/vigan/online')}}">Online Only</a></li>
                                            </ul>
                                        </li>
                                            <li><a href="{{ url('/vigan/tuition')}}">Tuition & Discounts</a></li>
                                            <li><a href="{{ url('/vigan/reservation')}}">Reservations</a></li>
                                            <li><a href="{{ url('/vigan/enrolled')}}">Enrolled</a></li>
                                            <li><a href="{{ url('/vigan/scholar')}}">Scholars</a></li>
                                            <li><a href="{{ url('/vigan/dropped')}}">Dropped</a></li>
                                            <li class="menu-item-has-children sale">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Sales</span></a>
                                            <ul class="list-unstyled sub-menu sale">
                                            <li><a href="{{ url('/vigan/sales-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/vigan/sales-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li class="menu-item-has-children receivable">
                                            <a href="javascript:void(0);">
                                            <span class="hide-menu">Receivables</span></a>
                                            <ul class="list-unstyled sub-menu receivable">
                                            <li><a href="{{ url('/vigan/receivable-enrollee')}}">per Enrollee</a></li>
                                            <li><a href="{{ url('/vigan/receivable-program')}}">per Program</a></li>
                                            </ul>
                                            </li>
                                            <li><a href="{{ url('/vigan/expense')}}">Expenses</a></li>
                                            <li><a href="{{ url('/vigan/books')}}">Books</a></li>
                                            <li><a href="{{ url('/vigan/bookTransferRecord')}}">Book Transfer</a></li>
                                            <li><a href="{{ url('/vigan/budget')}}">Budget</a></li>
                                            <li><a href="{{ url('/vigan/remit')}}">Cash Remittance</a></li>
                                        </ul>

                                </li>
                                    {{-- <li class="menu-item-has-children evaluate">
                                        <a href="javascript:void(0);">
                                            <span class="hide-menu"><i class="list-icon material-icons"></i>Lecturers</span></a>
                                            <ul class="list-unstyled sub-menu evaluate">
                                            <li><a href="{{ url('/vigan/add-lecturer')}}">Add Lecturer</a></li>
                                            <li><a href="{{ url('/vigan/evaluate-lecturer')}}">Evaluate Lecturer</a></li>
                                            <li><a href="{{ url('/vigan/lecturer-evaluation')}}">Evaluation Records</a></li>
                                            </ul>
                                    </li> --}}
                                </ul>
                            
                            </li>




                            {{-- end --}}
                        </ul>
                    </li>

                    <li class="menu-item-has-children reports"><a href="javascript:void(0);">
                        <i class="list-icon material-icons">apps</i> <span class="hide-menu">Reports</span></a>  
                        <ul class="list-unstyled sub-menu reports">
                        
                        
                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Abra</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/abra/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/abra/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/abra/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/abra/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/abra/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>



                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Baguio</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/baguio/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/baguio/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/baguio/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/baguio/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/baguio/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Calapan</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/calapan/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/calapan/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/calapan/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/calapan/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/calapan/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Candon</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/candon/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/candon/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/candon/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/candon/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/candon/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Dagupan</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/dagupan/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/dagupan/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/dagupan/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/dagupan/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/dagupan/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>
                            
                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Fairview</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/fairview/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/fairview/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/fairview/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/fairview/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/fairview/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>



                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Las Pinas</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/las_pinas/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/las_pinas/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/las_pinas/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/las_pinas/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/las_pinas/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Launion</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/launion/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/launion/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/launion/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/launion/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/launion/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Manila</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/manila/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/manila/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/manila/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/manila/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/manila/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Roxas</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/roxas/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/roxas/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/roxas/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/roxas/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/roxas/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>

                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Tarlac</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/tarlac/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/tarlac/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/tarlac/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/tarlac/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/tarlac/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>


                            <li class="menu-item-has-children"><a href="javascript:void(0);">
                                <span class="hide-menu sub-reports">Vigan</span></a>
                                <ul class="list-unstyled sub-menu sub-reports">
    
                               <li><a href="{{ url('/vigan/today')}}">Today Transaction</a>
                                </li>
                                
                                <li><a href="{{ url('/vigan/yesterday')}}">Yesterday Transaction</a>
                                </li>
                                 <li class="item-has-children score-card1"><a href="javascript:void(0);">
                                        <span class="hide-menu">Score Card</span></a>
                                        <ul class="list-unstyled sub-menu score-card1">
                                            <li>
                                                <a href="{{ url('/vigan/scorecard-season/1')}}">Season 1</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/vigan/scorecard-season/2')}}">Season 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/vigan/financial-report')}}">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>

                        {{-- end --}}
                        </ul>
                    </li>

            <li class="menu-item-has-children ad-record"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">Records</span></a>                    
                <ul class="list-unstyled sub-menu ad-record">

                   <li><a href="{{ url('/total-program')}}">Programs</a>
                    </li>
                    <li class="menu-item-has-children ad-enrollee">
                            <a href="javascript:void(0);">
                                <span class="hide-menu">Enrollees</span></a>
                        <ul class="list-unstyled sub-menu ad-enrollee">
                                <li><a href="{{ url('/total-let')}}">LET</a></li>
                                <li><a href="{{ url('/total-nle')}}">NLE</a></li>
                                <li><a href="{{ url('/total-crim')}}">Criminology</a></li>
                                <li><a href="{{ url('/total-civil')}}">Civil Service</a></li>
                                <li><a href="{{ url('/total-psyc')}}">Psychometrician</a></li>
                                <li><a href="{{ url('/total-nclex')}}">NCLEX</a></li>
                                <li><a href="{{ url('/total-ielts')}}">IELTS</a></li>
                                <li><a href="{{ url('/total-social')}}">Social Work</a></li>
                                <li><a href="{{ url('/total-agri')}}">Agriculture</a></li>
                                <li><a href="{{ url('/total-mid')}}">Midwifery</a></li>
                                <li><a href="{{ url('/total-online')}}">Online Only</a></li>
                            </ul>
                    </li>
                    <li><a href="{{ url('/total-dropped')}}">Dropped</a>
                    </li>
                    <li><a href="{{ url('/total-scholars')}}">Scholars</a>
                    </li>
                    <li><a href="{{ url('/tuition-fees')}}">Tuition Fees</a>
                    </li>
                    <li><a href="{{ url('/discounts')}}">Discounts</a>
                    </li>
                    <li><a href="{{ url('/total-books')}}">Books</a>
                    </li>
                    <li><a href="{{ url('/total-expense')}}">Expenses</a>
                    </li>
                    <li><a href="{{ url('/users')}}">Users</a>
                    </li>

                </ul> 
            </li>
            
            
                <li>
                    <li class="menu-item-has-children hris"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">HRIS</span></a>                    
                        <ul class="list-unstyled sub-menu hris">

                            <li class="menu-item-has-children abra"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Abra</span></a>
                                <ul class="list-unstyled sub-menu abra">
                                    <li><a href="{{ url('/abra/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/abra/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>


                            <li class="menu-item-has-children baguio"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Baguio</span></a>
                                <ul class="list-unstyled sub-menu baguio">
                                    <li><a href="{{ url('/baguio/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/baguio/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>
                            
                            <li class="menu-item-has-children calapan"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Calapan</span></a>
                                <ul class="list-unstyled sub-menu calapan">
                                    <li><a href="{{ url('/calapan/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/calapan/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>
                            

                            <li class="menu-item-has-children candon"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Candon</span></a>
                                <ul class="list-unstyled sub-menu candon">
                                    <li><a href="{{ url('/candon/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/candon/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children dagupan"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Dagupan</span></a>
                                <ul class="list-unstyled sub-menu dagupan">
                                    <li><a href="{{ url('/dagupan/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/dagupan/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children fairview"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Fairview</span></a>
                                <ul class="list-unstyled sub-menu fairview">
                                    <li><a href="{{ url('/fairview/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/fairview/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>


                            <li class="menu-item-has-children las_pinas"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Las Pinas</span></a>
                                <ul class="list-unstyled sub-menu las_pinas">
                                    <li><a href="{{ url('/las_pinas/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/las_pinas/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>
                            
                            <li class="menu-item-has-children launion"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Launion</span></a>
                                <ul class="list-unstyled sub-menu launion">
                                    <li><a href="{{ url('/launion/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/launion/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>
                            

                            <li class="menu-item-has-children manila"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Manila</span></a>
                                <ul class="list-unstyled sub-menu manila">
                                    <li><a href="{{ url('/manila/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/manila/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children roxas"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Roxas</span></a>
                                <ul class="list-unstyled sub-menu roxas">
                                    <li><a href="{{ url('/roxas/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/roxas/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children tarlac"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Tarlac</span></a>
                                <ul class="list-unstyled sub-menu tarlac">
                                    <li><a href="{{ url('/tarlac/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/tarlac/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>

                            <li class="menu-item-has-children vigan"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Vigan</span></a>
                                <ul class="list-unstyled sub-menu vigan">
                                    <li><a href="{{ url('/vigan/add-employee')}}">Add Employee</a></li>
                                    <li><a href="{{ url('/vigan/employee-record')}}">Employee Records</a></li>
                                </ul>
                            </li>



                            {{-- end --}}
                        </ul>
                    </li> 
                </li>
            {{-- <li class="menu-item-has-children ad-RADashboard"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">RA Files</span></a>                    
            <ul class="list-unstyled sub-menu evaluate">
                <li><a href="{{ url('/radashboard/evaluation')}}">Evaluation</a></li>
                <li><a href="{{ url('/radashboard/attendance')}}">Attendance</a></li>
                <li><a href="{{ url('/radashboard/result-analysis')}}">Result Analysis</a></li>
                <li><a href="{{ url('/radashboard/filter-exam')}}">Exam Monitoring</a></li>
                <li><a href="{{ url('/radashboard/onlineCompletion')}}">Online Completion</a></li>
                <li><a href="{{ url('/radashboard/viewLecturerReports')}}">Lecturer Monitoring</a></li>
            </ul>
        </li> --}}
                   
                    <li class="menu-item-has-children others"><a href="javascript:void(0);"><i class="list-icon material-icons">apps</i> <span class="hide-menu">Others</span></a>                    
                        <ul class="list-unstyled sub-menu others">

                            <li class="menu-item-has-children abra"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Abra</span></a>
                                <ul class="list-unstyled sub-menu abra">
                                    <li><a href="{{ url('/abra/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children baguio"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Baguio</span></a>
                                <ul class="list-unstyled sub-menu baguio">
                                    <li><a href="{{ url('/baguio/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children calapan"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Calapan</span></a>
                                <ul class="list-unstyled sub-menu calapan">
                                    <li><a href="{{ url('/calapan/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children candon"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Candon</span></a>
                                <ul class="list-unstyled sub-menu candon">
                                    <li><a href="{{ url('/candon/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children dagupan"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Dagupan</span></a>
                                <ul class="list-unstyled sub-menu dagupan">
                                    <li><a href="{{ url('/dagupan/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>

                            <li class="menu-item-has-children fairvie"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Fairvie</span></a>
                                <ul class="list-unstyled sub-menu fairvie">
                                    <li><a href="{{ url('/fairvie/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children las_pinas"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Las Pinas</span></a>
                                <ul class="list-unstyled sub-menu las_pinas">
                                    <li><a href="{{ url('/las_pinas/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children launion"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Launion</span></a>
                                <ul class="list-unstyled sub-menu launion">
                                    <li><a href="{{ url('/launion/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children manila"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Manila</span></a>
                                <ul class="list-unstyled sub-menu manila">
                                    <li><a href="{{ url('/manila/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children roxas"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Roxas</span></a>
                                <ul class="list-unstyled sub-menu roxas">
                                    <li><a href="{{ url('/roxas/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children tarlac"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Tarlac</span></a>
                                <ul class="list-unstyled sub-menu tarlac">
                                    <li><a href="{{ url('/tarlac/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>
    
                            <li class="menu-item-has-children vigan"><a href="javascript:void(0);"><span class="hide-menu sub-reports">Vigan</span></a>
                                <ul class="list-unstyled sub-menu vigan">
                                    <li><a href="{{ url('/vigan/student-id')}}">Passer's Templates</a></li>
                                    {{-- <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li> --}}
                                </ul>
                            </li>

                            {{-- end --}}
                        </ul>
                    </li>

                    <li class="menu-item-has-children settings"><a href="javascript:void(0);">
                        <i class="list-icon material-icons">settings</i> <span class="hide-menu">Settings</span></a>  
                        <ul class="list-unstyled sub-menu settings">
                                
                                <li><a href="{{ url('/abra/settings/admin')}}">Admin Dashboard</a></li>
                                <li><a href="{{ url('/abra/settings/member')}}">Member Dashboard</a></li>     
                                <li><a href="{{ url('expense-settings')}}">Expense Settings</a></li>
                            
                                    

                             
                        {{-- end --}}
                        </ul>
                    </li>


                      




                        
                </ul>
                <!-- /.side-menu -->
            </nav>
            <!-- /.sidebar-nav -->
        </aside>