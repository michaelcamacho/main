@extends('main')
@section('title')

@stop
@section('css')
<style type="text/css">
    input[type="file"]{
  display: none;
}
#upload{
  cursor: pointer;
  background-color:#f57327;
  color: #fff;
  padding: 10px 10px;
  margin-left: -20px; 
  border-radius: 5px;
}
#uploadId{
    margin-top: 14px;
    margin-left: 20px;
}

</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <h5>CBRC PASSER'S ID</h5>
                 </div>
                
                   <div class="col-md-6" id="demo">
                  
                       <img id="imgIds" src="../img/Capture.PNG" style="height: 500px;">

                        <img class="profile-pic img-responsive" id = "imgDisplay" style="height: 240px;width: 237px;position: absolute;z-index:1; margin-top:188px;margin-left: -478px;" src="/cover_images/noimage.jpg">
                         <h5 style="height: 238px;width: 235px;position: absolute;z-index:1; margin-top: -363px; margin-left: 385px;color:#1d3d90; ">Passers</h5>
                         <h5 id="catProg" style="height: 238px;width: 235px;position: absolute;z-index:1; margin-top: -363px; margin-left: 145px;color:#1d3d90; text-align: right; ">LET</h5>
                          <p style="position: absolute;margin-top: -235px;font-weight: bold;z-index: 1;margin-left: 280px;font-size: 15px;color:#1d3d90; text-align: center;" id="nameStud">STUDENT NAME</p>
                          <p style="position: absolute;margin-top: -155px;font-weight: bold;z-index: 1;margin-left: 280px;font-size: 12px;color:#1d3d90; text-align: center;" id="schoolStud">SCHOOL NAME</p>
                           <p style="position: absolute;margin-top: -30px;font-weight: bold;z-index: 1;margin-left: 297px;font-size: 14px;color:#fff; text-align: center;" id="schoolStud">09057544305/09399139622</p>
                         </div> 
                                       
                 <div class="col-md-5">
                     <div class="alert alert-primary" role="alert">
                   <form>
                    @csrf
                    <input type="hidden" name="branch" id="branch" value="{{Auth::user()->branch}}">
                     <div class="form-group">
                        <label>Program</label> 
                        <select class="form-control" name="program" id="program">
                            <option value="0" disable="true" selected="true">--- Select Program ---</option>
                            @if(isset($program))
                            @foreach($program as $programs)
                            <option value="{{$programs->aka}}">{{$programs->program_name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                     <div class="form-group input-has-value">
                      <label>Student Name</label>
                         <select class="form-control" name="name" id="student" required>
                              <option value="" disable="true" selected="true">--- Select Student ---</option>
                          </select>                         
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                            <label id="uploadId">
                              <span id="upload" class="text-center">Upload Files</span>
                              <input class="file-upload" name="cover_image" type="file" accept="image/*" style="margin-top: 15px;margin-left: 107px;" />
                              </label>
                          </div>
                          <div class="col-md-4">
                                <input id="btn-Preview-Image" class="btn btn-primary" type="button" value="PREVIEW" style="margin-top: 5px;" />

                            </div>  
                      </div>
                    </form>
                </div>
            </div>
          </div>
        </div>         
           <div class="container">
              <div class="row">
                  <div class="col-md-6">
                    <h5>PREVIEW IMAGE HERE !!</h5>
                    <div id="previewImage">
                    </div>
                    <div id="previewDownload">
                      
                   </div>
                  </div>
              </div>
          </div>
        </main>
@stop

@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>

<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>

<script type="text/javascript">


$(document).ready(function(e){
var element = $("#demo"); // global variable
var getCanvas; // global variable



 $("#btn-Preview-Image").on('click', function () {
html2canvas(element,{ 
  onrendered:function(canvas){
   dataURL = canvas.toDataURL("image/png");
   $("#previewImage").html(canvas);
   $('#previewDownload').html('<a id="down_button" class="btn btn-success" href="#" style="margin-top: 5px;margin-left:20px;font-weight: bold;">DOWNLOAD</a>');
      getCanvas = canvas;
   


$("#down_button").on('click', function () {

    // Now browser starts downloading it instead of just showing it
    var newData = dataURL.replace(/^data:image\/png/, "data:application/octet-stream");
     $("#down_button").attr("download", "your_pic_name.png").attr("href", newData);
    
    });
  }
});
 });

});

</script>



<script>
    $(document).ready(function(){
        $('#form').submit(function(e){
            var form = this;
            var fileInput = $(this).find("input[type=file]")[0],
            file = fileInput.files && fileInput.files[0];

            if( file ) {
                var img = new Image();

                img.src = window.URL.createObjectURL( file );

                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;

                    window.URL.revokeObjectURL( img.src );

                    if( width == 600 && height == 600) {
                        form.submit();
                    }
                    else {
                       swal({
                          title: 'Oops...',
                          text: 'Image is too large',
                          type: 'error',
                          timer: '3000'
                      });
                      $(':input[type="submit"]').removeAttr("disabled");
                    }
                };
            }            


            e.preventDefault();
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function() {

 var readURL = function(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }   
    $(".file-upload").on('change', function(){
        $('#is_upload').val(0);
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
        $(".file-upload").click();     

    });

});    
</script>
<script type="text/javascript">
  $(document).ready(function(){
$('#program').on('change', function() {
    var responseId = $(this).val();
    var prog = responseId;

    if (prog == 'lets'){
      prog = 'LETS';
    }
    else if(prog == 'nles'){
      prog = 'NLE';
    }
    else if(prog == 'crims'){
      prog = 'CRIMINOLOGY';
    }
    else if(prog == 'civils'){
      prog = 'CIVIL SERVICE'
    }
    else if(prog == 'psycs'){
      prog = 'PSYCHOMETRICIAN'
    }
    else if(prog == 'nclexes'){
      prog = 'NCLEXES'
    }
    else if(prog == 'ielts'){
      prog = 'IELTS'
    }
    else if(prog == 'socials'){
      prog = 'SOCIAL WORKS'
    }
    else if(prog == 'agris'){
      prog = 'AGRICULTURE';
    }
    else if(prog == 'mids'){
      prog = 'MIDWIFERY';
    }
    else if(prog == 'onlines'){
      prog = 'ONLINE ONLY';
    }

    $('#catProg').text(prog);
});

$('#student').on('change',function(){

  var data = $(this).val();
  var arr = data.split('*');

  var schoolStud = arr[0].toUpperCase();
  var nameStudent = arr[1].toUpperCase();

console.log(schoolStud);
console.log(nameStud);

  $('#nameStud').text(nameStudent);
  $('#schoolStud').text(schoolStud);
  
});


 $('#program').on('change', function(e){
           console.log(e);

            var program = e.target.value;
            var branch = $('#branch').val().toLowerCase();

            $.get('json-student?program=' + program,function(data){
                console.log(data);

                 $('#student').empty();
                 $('#student').append('<option value="" disable="true" selected="true">--- Select Student ---</option>');

                  $.each(data, function(index, studentObj){
                    $('#student').append('<option value="'+ studentObj.school + '*'+ studentObj.last_name +', '+ studentObj.first_name+'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
                });
                 
            });
});


  });
</script>
@stop