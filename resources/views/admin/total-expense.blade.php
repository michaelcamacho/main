@extends('main')
@section('title')
Total Expenses
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Expenses Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <br/>
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Expenses Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="sale-table">
                                    <table id="sale" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Date</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Amount</th>
                            <th>Remarks</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                            @if(isset($abra_a_sale))
                            @foreach($abra_a_sale as $abra_a_sales)
                        <tr id="row_{{$abra_a_sales->id}}">
                            <td>{{$abra_a_sales->date}}</td>
                            <td>{{$abra_a_sales->branch}}</td>
                            <td>{{$abra_a_sales->program}}</td>
                            <td>{{$abra_a_sales->category}}</td>
                            <td>{{$abra_a_sales->sub_category}}</td>
                            <td>{{$abra_a_sales->amount}}</td>
                            <td>{{$abra_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($baguio_a_sale))
                            @foreach($baguio_a_sale as $baguio_a_sales)
                        <tr id="row_{{$baguio_a_sales->id}}">
                            <td>{{$baguio_a_sales->date}}</td>
                            <td>{{$baguio_a_sales->branch}}</td>
                            <td>{{$baguio_a_sales->program}}</td>
                            <td>{{$baguio_a_sales->category}}</td>
                            <td>{{$baguio_a_sales->sub_category}}</td>
                            <td>{{$baguio_a_sales->amount}}</td>
                            <td>{{$baguio_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif


                            @if(isset($calapan_a_sale))
                            @foreach($calapan_a_sale as $calapan_a_sales)
                        <tr id="row_{{$calapan_a_sales->id}}">
                            <td>{{$calapan_a_sales->date}}</td>
                            <td>{{$calapan_a_sales->branch}}</td>
                            <td>{{$calapan_a_sales->program}}</td>
                            <td>{{$calapan_a_sales->category}}</td>
                            <td>{{$calapan_a_sales->sub_category}}</td>
                            <td>{{$calapan_a_sales->amount}}</td>
                            <td>{{$calapan_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                        
                            @if(isset($candon_a_sale))
                            @foreach($candon_a_sale as $candon_a_sales)
                        <tr id="row_{{$candon_a_sales->id}}">
                            <td>{{$candon_a_sales->date}}</td>
                            <td>{{$candon_a_sales->branch}}</td>
                            <td>{{$candon_a_sales->program}}</td>
                            <td>{{$candon_a_sales->category}}</td>
                            <td>{{$candon_a_sales->sub_category}}</td>
                            <td>{{$candon_a_sales->amount}}</td>
                            <td>{{$candon_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($dagupan_a_sale))
                            @foreach($dagupan_a_sale as $dagupan_a_sales)
                        <tr id="row_{{$dagupan_a_sales->id}}">
                            <td>{{$dagupan_a_sales->date}}</td>
                            <td>{{$dagupan_a_sales->branch}}</td>
                            <td>{{$dagupan_a_sales->program}}</td>
                            <td>{{$dagupan_a_sales->category}}</td>
                            <td>{{$dagupan_a_sales->sub_category}}</td>
                            <td>{{$dagupan_a_sales->amount}}</td>
                            <td>{{$dagupan_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($fairview_a_sale))
                            @foreach($fairview_a_sale as $fairview_a_sales)
                        <tr id="row_{{$fairview_a_sales->id}}">
                            <td>{{$fairview_a_sales->date}}</td>
                            <td>{{$fairview_a_sales->branch}}</td>
                            <td>{{$fairview_a_sales->program}}</td>
                            <td>{{$fairview_a_sales->category}}</td>
                            <td>{{$fairview_a_sales->sub_category}}</td>
                            <td>{{$fairview_a_sales->amount}}</td>
                            <td>{{$fairview_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($las_pinas_a_sale))
                            @foreach($las_pinas_a_sale as $las_pinas_a_sales)
                        <tr id="row_{{$las_pinas_a_sales->id}}">
                            <td>{{$las_pinas_a_sales->date}}</td>
                            <td>{{$las_pinas_a_sales->branch}}</td>
                            <td>{{$las_pinas_a_sales->program}}</td>
                            <td>{{$las_pinas_a_sales->category}}</td>
                            <td>{{$las_pinas_a_sales->sub_category}}</td>
                            <td>{{$las_pinas_a_sales->amount}}</td>
                            <td>{{$las_pinas_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($launion_a_sale))
                            @foreach($launion_a_sale as $launion_a_sales)
                        <tr id="row_{{$launion_a_sales->id}}">
                            <td>{{$launion_a_sales->date}}</td>
                            <td>{{$launion_a_sales->branch}}</td>
                            <td>{{$launion_a_sales->program}}</td>
                            <td>{{$launion_a_sales->category}}</td>
                            <td>{{$launion_a_sales->sub_category}}</td>
                            <td>{{$launion_a_sales->amount}}</td>
                            <td>{{$launion_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif
                            @if(isset($manila_a_sale))
                            @foreach($manila_a_sale as $manila_a_sales)
                        <tr id="row_{{$manila_a_sales->id}}">
                            <td>{{$manila_a_sales->date}}</td>
                            <td>{{$manila_a_sales->branch}}</td>
                            <td>{{$manila_a_sales->program}}</td>
                            <td>{{$manila_a_sales->category}}</td>
                            <td>{{$manila_a_sales->sub_category}}</td>
                            <td>{{$manila_a_sales->amount}}</td>
                            <td>{{$manila_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($roxas_a_sale))
                            @foreach($roxas_a_sale as $roxas_a_sales)
                        <tr id="row_{{$roxas_a_sales->id}}">
                            <td>{{$roxas_a_sales->date}}</td>
                            <td>{{$roxas_a_sales->branch}}</td>
                            <td>{{$roxas_a_sales->program}}</td>
                            <td>{{$roxas_a_sales->category}}</td>
                            <td>{{$roxas_a_sales->sub_category}}</td>
                            <td>{{$roxas_a_sales->amount}}</td>
                            <td>{{$roxas_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($tarlac_a_sale))
                            @foreach($tarlac_a_sale as $tarlac_a_sales)
                        <tr id="row_{{$tarlac_a_sales->id}}">
                            <td>{{$tarlac_a_sales->date}}</td>
                            <td>{{$tarlac_a_sales->branch}}</td>
                            <td>{{$tarlac_a_sales->program}}</td>
                            <td>{{$tarlac_a_sales->category}}</td>
                            <td>{{$tarlac_a_sales->sub_category}}</td>
                            <td>{{$tarlac_a_sales->amount}}</td>
                            <td>{{$tarlac_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif
                            @if(isset($vigan_a_sale))
                            @foreach($vigan_a_sale as $vigan_a_sales)
                        <tr id="row_{{$vigan_a_sales->id}}">
                            <td>{{$vigan_a_sales->date}}</td>
                            <td>{{$vigan_a_sales->branch}}</td>
                            <td>{{$vigan_a_sales->program}}</td>
                            <td>{{$vigan_a_sales->category}}</td>
                            <td>{{$vigan_a_sales->sub_category}}</td>
                            <td>{{$vigan_a_sales->amount}}</td>
                            <td>{{$vigan_a_sales->remarks}}</td>
                            
                        </tr>
                       @endforeach
                            @endif

                    </tbody>           
                    
                </table>
            </div>

             </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/book.js') }}"></script>
<script type="text/javascript">

@stop