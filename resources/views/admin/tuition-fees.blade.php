@extends('main')
@section('title')
 Tuition Fees
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Tuition Fees Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Tuition Fees Records</h5>
                                   <br/>
                                   
                                </div>
                <div class="widget-body clearfix">
               
            <div id="tuition-table">
                <h5 class="box-title mr-b-0">Abra</h5>
                    <table id="tuition" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                        <div class="add">
                        <a href="#" class="btn btn-primary ripple mr-3" data-toggle="modal" data-target="#add-tuition">Add New Tuition Fee</a>
                        </div>
                     <thead>
                       
                        <tr>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Tuition Fee</th>
                            <th>Facilitation Fee</th>
                            <th>Season</th>
                            <th>Year</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($abra_a_tuition))
                            @foreach($abra_a_tuition as $abra_a_tuitions)
                            <tr id="row_{{$abra_a_tuitions->id}}">
                                <td id="a_branch_{{$abra_a_tuitions->id}}">{{$abra_a_tuitions->branch}}</td>
                                <td id="a_program_{{$abra_a_tuitions->id}}">{{$abra_a_tuitions->program}}</td>
                                <td id="a_category_{{$abra_a_tuitions->id}}">{{$abra_a_tuitions->category}}</td>
                                <td id="a_tuition_{{$abra_a_tuitions->id}}">{{$abra_a_tuitions->tuition_fee}}</td>
                                <td id="a_facilitation_{{$abra_a_tuitions->id}}">{{$abra_a_tuitions->facilitation_fee}}</td>
                                <td id="a_season_{{$abra_a_tuitions->id}}">{{$abra_a_tuitions->season}}</td>
                                <td id="a_year_{{$abra_a_tuitions->id}}">{{$abra_a_tuitions->year}}</td>
                                <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$abra_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                                <td><button class="btn btn-danger delete" user-id="{{$abra_a_tuitions->id}},{{$abra_a_tuitions->branch}}">Delete</button></td>
                            </tr>
                            @endforeach
                        @endif

                        @if(isset($baguio_a_tuition))
                        @foreach($baguio_a_tuition as $baguio_a_tuitions)
                        <tr id="row_{{$baguio_a_tuitions->id}}">
                            <td id="a_branch_{{$baguio_a_tuitions->id}}">{{$baguio_a_tuitions->branch}}</td>
                            <td id="a_program_{{$baguio_a_tuitions->id}}">{{$baguio_a_tuitions->program}}</td>
                            <td id="a_category_{{$baguio_a_tuitions->id}}">{{$baguio_a_tuitions->category}}</td>
                            <td id="a_tuition_{{$baguio_a_tuitions->id}}">{{$baguio_a_tuitions->tuition_fee}}</td>
                            <td id="a_facilitation_{{$baguio_a_tuitions->id}}">{{$baguio_a_tuitions->facilitation_fee}}</td>
                            <td id="a_season_{{$baguio_a_tuitions->id}}">{{$baguio_a_tuitions->season}}</td>
                            <td id="a_year_{{$baguio_a_tuitions->id}}">{{$baguio_a_tuitions->year}}</td>
                            <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$baguio_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                            <td><button class="btn btn-danger delete" user-id="{{$baguio_a_tuitions->id}},{{$baguio_a_tuitions->branch}}">Delete</button></td>
                        </tr>
                        @endforeach
                    @endif

                    @if(isset($calapan_a_tuition))
                    @foreach($calapan_a_tuition as $calapan_a_tuitions)
                    <tr id="row_{{$calapan_a_tuitions->id}}">
                        <td id="a_branch_{{$calapan_a_tuitions->id}}">{{$calapan_a_tuitions->branch}}</td>
                        <td id="a_program_{{$calapan_a_tuitions->id}}">{{$calapan_a_tuitions->program}}</td>
                        <td id="a_category_{{$calapan_a_tuitions->id}}">{{$calapan_a_tuitions->category}}</td>
                        <td id="a_tuition_{{$calapan_a_tuitions->id}}">{{$calapan_a_tuitions->tuition_fee}}</td>
                        <td id="a_facilitation_{{$calapan_a_tuitions->id}}">{{$calapan_a_tuitions->facilitation_fee}}</td>
                        <td id="a_season_{{$calapan_a_tuitions->id}}">{{$calapan_a_tuitions->season}}</td>
                        <td id="a_year_{{$calapan_a_tuitions->id}}">{{$calapan_a_tuitions->year}}</td>
                        <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$calapan_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                        <td><button class="btn btn-danger delete" user-id="{{$calapan_a_tuitions->id}},{{$calapan_a_tuitions->branch}}">Delete</button></td>
                    </tr>
                    @endforeach
                @endif

                @if(isset($candon_a_tuition))
                @foreach($candon_a_tuition as $candon_a_tuitions)
                <tr id="row_{{$candon_a_tuitions->id}}">
                    <td id="a_branch_{{$candon_a_tuitions->id}}">{{$candon_a_tuitions->branch}}</td>
                    <td id="a_program_{{$candon_a_tuitions->id}}">{{$candon_a_tuitions->program}}</td>
                    <td id="a_category_{{$candon_a_tuitions->id}}">{{$candon_a_tuitions->category}}</td>
                    <td id="a_tuition_{{$candon_a_tuitions->id}}">{{$candon_a_tuitions->tuition_fee}}</td>
                    <td id="a_facilitation_{{$candon_a_tuitions->id}}">{{$candon_a_tuitions->facilitation_fee}}</td>
                    <td id="a_season_{{$candon_a_tuitions->id}}">{{$candon_a_tuitions->season}}</td>
                    <td id="a_year_{{$candon_a_tuitions->id}}">{{$candon_a_tuitions->year}}</td>
                    <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$candon_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                    <td><button class="btn btn-danger delete" user-id="{{$candon_a_tuitions->id}},{{$candon_a_tuitions->branch}}">Delete</button></td>
                </tr>
                @endforeach
             @endif
             @if(isset($fairview_a_tuition))
             @foreach($fairview_a_tuition as $fairview_a_tuitions)
                    <tr id="row_{{$fairview_a_tuitions->id}}">
                        <td id="a_branch_{{$fairview_a_tuitions->id}}">{{$fairview_a_tuitions->branch}}</td>
                        <td id="a_program_{{$fairview_a_tuitions->id}}">{{$fairview_a_tuitions->program}}</td>
                        <td id="a_category_{{$fairview_a_tuitions->id}}">{{$fairview_a_tuitions->category}}</td>
                        <td id="a_tuition_{{$fairview_a_tuitions->id}}">{{$fairview_a_tuitions->tuition_fee}}</td>
                        <td id="a_facilitation_{{$fairview_a_tuitions->id}}">{{$fairview_a_tuitions->facilitation_fee}}</td>
                        <td id="a_season_{{$fairview_a_tuitions->id}}">{{$fairview_a_tuitions->season}}</td>
                        <td id="a_year_{{$fairview_a_tuitions->id}}">{{$fairview_a_tuitions->year}}</td>
                        <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$fairview_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                        <td><button class="btn btn-danger delete" user-id="{{$fairview_a_tuitions->id}},{{$fairview_a_tuitions->branch}}">Delete</button></td>
                    </tr>
                    @endforeach
                @endif

                @if(isset($las_pinas_a_tuition))
                @foreach($las_pinas_a_tuition as $las_pinas_a_tuitions)
                <tr id="row_{{$las_pinas_a_tuitions->id}}">
                    <td id="a_branch_{{$las_pinas_a_tuitions->id}}">{{$las_pinas_a_tuitions->branch}}</td>
                    <td id="a_program_{{$las_pinas_a_tuitions->id}}">{{$las_pinas_a_tuitions->program}}</td>
                    <td id="a_category_{{$las_pinas_a_tuitions->id}}">{{$las_pinas_a_tuitions->category}}</td>
                    <td id="a_tuition_{{$las_pinas_a_tuitions->id}}">{{$las_pinas_a_tuitions->tuition_fee}}</td>
                    <td id="a_facilitation_{{$las_pinas_a_tuitions->id}}">{{$las_pinas_a_tuitions->facilitation_fee}}</td>
                    <td id="a_season_{{$las_pinas_a_tuitions->id}}">{{$las_pinas_a_tuitions->season}}</td>
                    <td id="a_year_{{$las_pinas_a_tuitions->id}}">{{$las_pinas_a_tuitions->year}}</td>
                    <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$las_pinas_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                    <td><button class="btn btn-danger delete" user-id="{{$las_pinas_a_tuitions->id}},{{$las_pinas_a_tuitions->branch}}">Delete</button></td>
                </tr>
                @endforeach
            @endif

                @if(isset($launion_a_tuition))
                @foreach($launion_a_tuition as $launion_a_tuitions)
                <tr id="row_{{$launion_a_tuitions->id}}">
                    <td id="a_branch_{{$launion_a_tuitions->id}}">{{$launion_a_tuitions->branch}}</td>
                    <td id="a_program_{{$launion_a_tuitions->id}}">{{$launion_a_tuitions->program}}</td>
                    <td id="a_category_{{$launion_a_tuitions->id}}">{{$launion_a_tuitions->category}}</td>
                    <td id="a_tuition_{{$launion_a_tuitions->id}}">{{$launion_a_tuitions->tuition_fee}}</td>
                    <td id="a_facilitation_{{$launion_a_tuitions->id}}">{{$launion_a_tuitions->facilitation_fee}}</td>
                    <td id="a_season_{{$launion_a_tuitions->id}}">{{$launion_a_tuitions->season}}</td>
                    <td id="a_year_{{$launion_a_tuitions->id}}">{{$launion_a_tuitions->year}}</td>
                    <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$launion_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                    <td><button class="btn btn-danger delete" user-id="{{$launion_a_tuitions->id}},{{$launion_a_tuitions->branch}}">Delete</button></td>
                </tr>
                @endforeach
            @endif

                    @if(isset($manila_a_tuition))
                    @foreach($manila_a_tuition as $manila_a_tuitions)
                    <tr id="row_{{$manila_a_tuitions->id}}">
                        <td id="a_branch_{{$manila_a_tuitions->id}}">{{$manila_a_tuitions->branch}}</td>
                        <td id="a_program_{{$manila_a_tuitions->id}}">{{$manila_a_tuitions->program}}</td>
                        <td id="a_category_{{$manila_a_tuitions->id}}">{{$manila_a_tuitions->category}}</td>
                        <td id="a_tuition_{{$manila_a_tuitions->id}}">{{$manila_a_tuitions->tuition_fee}}</td>
                        <td id="a_facilitation_{{$manila_a_tuitions->id}}">{{$manila_a_tuitions->facilitation_fee}}</td>
                        <td id="a_season_{{$manila_a_tuitions->id}}">{{$manila_a_tuitions->season}}</td>
                        <td id="a_year_{{$manila_a_tuitions->id}}">{{$manila_a_tuitions->year}}</td>
                        <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$manila_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                        <td><button class="btn btn-danger delete" user-id="{{$manila_a_tuitions->id}},{{$manila_a_tuitions->branch}}">Delete</button></td>
                    </tr>
                    @endforeach
                @endif

                @if(isset($roxas_a_tuition))
                @foreach($roxas_a_tuition as $roxas_a_tuitions)
                <tr id="row_{{$roxas_a_tuitions->id}}">
                    <td id="a_branch_{{$roxas_a_tuitions->id}}">{{$roxas_a_tuitions->branch}}</td>
                    <td id="a_program_{{$roxas_a_tuitions->id}}">{{$roxas_a_tuitions->program}}</td>
                    <td id="a_category_{{$roxas_a_tuitions->id}}">{{$roxas_a_tuitions->category}}</td>
                    <td id="a_tuition_{{$roxas_a_tuitions->id}}">{{$roxas_a_tuitions->tuition_fee}}</td>
                    <td id="a_facilitation_{{$roxas_a_tuitions->id}}">{{$roxas_a_tuitions->facilitation_fee}}</td>
                    <td id="a_season_{{$roxas_a_tuitions->id}}">{{$roxas_a_tuitions->season}}</td>
                    <td id="a_year_{{$roxas_a_tuitions->id}}">{{$roxas_a_tuitions->year}}</td>
                    <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$roxas_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                    <td><button class="btn btn-danger delete" user-id="{{$roxas_a_tuitions->id}},{{$roxas_a_tuitions->branch}}">Delete</button></td>
                </tr>
                @endforeach
            @endif

            @if(isset($tarlac_a_tuition))
            @foreach($tarlac_a_tuition as $tarlac_a_tuitions)
            <tr id="row_{{$tarlac_a_tuitions->id}}">
                <td id="a_branch_{{$tarlac_a_tuitions->id}}">{{$tarlac_a_tuitions->branch}}</td>
                <td id="a_program_{{$tarlac_a_tuitions->id}}">{{$tarlac_a_tuitions->program}}</td>
                <td id="a_category_{{$tarlac_a_tuitions->id}}">{{$tarlac_a_tuitions->category}}</td>
                <td id="a_tuition_{{$tarlac_a_tuitions->id}}">{{$tarlac_a_tuitions->tuition_fee}}</td>
                <td id="a_facilitation_{{$tarlac_a_tuitions->id}}">{{$tarlac_a_tuitions->facilitation_fee}}</td>
                <td id="a_season_{{$tarlac_a_tuitions->id}}">{{$tarlac_a_tuitions->season}}</td>
                <td id="a_year_{{$tarlac_a_tuitions->id}}">{{$tarlac_a_tuitions->year}}</td>
                <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$tarlac_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                <td><button class="btn btn-danger delete" user-id="{{$tarlac_a_tuitions->id}},{{$tarlac_a_tuitions->branch}}">Delete</button></td>
            </tr>
            @endforeach
        @endif

  
        @if(isset($vigan_a_tuition))
        @foreach($vigan_a_tuition as $vigan_a_tuitions)
        <tr id="row_{{$vigan_a_tuitions->id}}">
            <td id="a_branch_{{$vigan_a_tuitions->id}}">{{$vigan_a_tuitions->branch}}</td>
            <td id="a_program_{{$vigan_a_tuitions->id}}">{{$vigan_a_tuitions->program}}</td>
            <td id="a_category_{{$vigan_a_tuitions->id}}">{{$vigan_a_tuitions->category}}</td>
            <td id="a_tuition_{{$vigan_a_tuitions->id}}">{{$vigan_a_tuitions->tuition_fee}}</td>
            <td id="a_facilitation_{{$vigan_a_tuitions->id}}">{{$vigan_a_tuitions->facilitation_fee}}</td>
            <td id="a_season_{{$vigan_a_tuitions->id}}">{{$vigan_a_tuitions->season}}</td>
            <td id="a_year_{{$vigan_a_tuitions->id}}">{{$vigan_a_tuitions->year}}</td>
            <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$vigan_a_tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
            <td><button class="btn btn-danger delete" user-id="{{$vigan_a_tuitions->id}},{{$vigan_a_tuitions->branch}}">Delete</button></td>
        </tr>
        @endforeach
    @endif


      
        

                    </tbody>                
                </table>
            </div>



        <div id="add-tuition" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="tuition-form">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="branch">Branch</label>
                                    <select class="form-control" name="branch" id="branch" required>
                                        <option value="" disable="true" selected="true">--- Select Branch ---</option>
                                        @if(isset($branch))
                                        @foreach($branch as $branches)
                                        <option value="{{$branches->branch_name}}">{{$branches->branch_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="program">Program</label>
                                    <select class="form-control" name="program" id="program">
                                        <option value="" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->program_name}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select class="form-control" name="category" id="category">
                                        <option value="0" disable="true" selected="true">--- Select Category ---</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Scholar">Scholar</option>
                                        <option value="In House">In House</option>
                                        <option value="Final Coaching">Final Coaching</option>
                                        <option value="Repeater">Repeater</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="tuition">Tuition Fee</label>
                                    <input class="form-control" type="number" name="tuition_fee" id="tuition_fee" required>
                                </div>
                                <div class="form-group">
                                    <label for="facilitation_fee">Facilitation Fee</label>
                                    <input class="form-control" type="number" name="facilitation_fee" id="facilitation_fee" required>
                                </div>
                                <div class="form-group">
                                    <label for="season">Season</label>
                                    <select class="form-control" name="season" id="season">
                                        <option value="" disable="true" selected="true">--- Select Season ---</option>
                                        <option value="Season 1">Season 1</option>
                                        <option value="Season 2">Season 2</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <select class="form-control" name="year" id="year" required>
                                        <option value="" disable="true" selected="true">--- Select Season ---</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2020</option>
                                        <option value="2019">2021</option>
                                        <option value="2018">2022</option>
                                    </select>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <a href="#" class="btn btn-rounded btn-success ripple save" data-dismiss="modal">Save</a>
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

            <div id="update-tuition" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="tuition-form-update">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="branch" id="update_id" readonly>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="branch">Branch</label>
                                    <input type="text" class="form-control" name="branch" id="update_branch" readonly>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="program">Program</label>
                                    <select class="form-control" name="program" id="update_program">
                                        <option value="" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->program_name}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select class="form-control" name="category" id="update_category">
                                        <option value="" disable="true" selected="true">--- Select Category ---</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Scholar">Scholar</option>
                                        <option value="In House">In House</option>
                                        <option value="Final Coaching">Final Coaching</option>
                                        <option value="Repeater">Repeater</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="tuition">Tuition Fee</label>
                                    <input class="form-control" type="number" name="tuition_fee" id="update_tuition_fee" required>
                                </div>
                                <div class="form-group">
                                    <label for="facilitation_fee">Facilitation Fee</label>
                                    <input class="form-control" type="number" name="facilitation_fee" id="update_facilitation_fee" required>
                                </div>
                                <div class="form-group">
                                    <label for="season">Season</label>
                                    <select class="form-control" name="season" id="update_season">
                                        <option value="" disable="true" selected="true">--- Select Season ---</option>
                                        <option value="Season 1">Season 1</option>
                                        <option value="Season 2">Season 2</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <select class="form-control" name="year" id="update_year" required>
                                        <option value="" disable="true" selected="true">--- Select Season ---</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <a href="#" class="btn btn-rounded btn-success ripple update" data-dismiss="modal">Update</a>
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/tuition.js') }}"></script>
<script type="text/javascript">

    $('.save').on('click',function(){
              
            
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('branch', $('#branch').val());
                formData.append('program', $('#program').val());
                formData.append('category', $('#category').val());
                formData.append('tuition_fee', $('#tuition_fee').val());
                formData.append('facilitation_fee', $('#facilitation_fee').val());
                formData.append('season', $('#season').val());
                formData.append('year', $('#year').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/insert-tuition') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-tuition form')[0].reset();
                        location.reload();

                    }
                               
              });
            });

    $('.update').on('click',function(){
              
            
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', $('#update_id').val());
                formData.append('branch', $('#update_branch').val());
                formData.append('program', $('#update_program').val());
                formData.append('category', $('#update_category').val());
                formData.append('tuition_fee', $('#update_tuition_fee').val());
                formData.append('facilitation_fee', $('#update_facilitation_fee').val());
                formData.append('season', $('#update_season').val());
                formData.append('year', $('#update_year').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/update-tuition') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-tuition form')[0].reset();
                        location.reload();

                    }
                               
              });
            });
    $('.delete').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-tuition') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

    $('#tuition').on('click','.delete',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-tuition') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });
</script>
@stop