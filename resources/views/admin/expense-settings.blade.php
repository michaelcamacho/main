@extends('main')
@section('title')
Admin Settings
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5"></h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Data Settings</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-6 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">EXPENSE SETTINGS</h5>
                                   <br/>
                                    @if (Session::has('message'))
                                       <div class="alert alert-success" role="alert">
                                           {{Session::get('message')}}
                                       </div>
                                    @endif
                                  
                                   <div class="form-group">
                                                    <label for="branch">Branch</label>
                                                    <select class="form-control" name="branch" id="branch" required>
                                                        <option value="" disable="true" selected="true">--- Select Branch ---</option>
                                                        @if(isset($branch))
                                                        @foreach($branch as $branches)
                                                        <option value="{{$branches->aka}}">{{ucwords($branches->aka)}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="season">Season</label>
                                                    <select class="form-control" name="season" id="season">
                                                        <option value="" disable="true" selected="true">--- Select Season ---</option>
                                                        <option value="Season 1">Season 1</option>
                                                        <option value="Season 2">Season 2</option>
                                                    </select>
                                                </div>
                
                                                <div class="form-group">
                                                    <label for="year">Year</label>
                                                    <select class="form-control" name="year" id="year" required>
                                                        <option value="" disable="true" selected="true">--- Select Season ---</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2018">2020</option>
                                                        <option value="2019">2021</option>
                                                        <option value="2018">2022</option>
                                                    </select>
                                                </div>
                                                                        
                                                <div class="text-center mr-b-30">
                                                <button id ="save" class="btn btn-rounded btn-success ripple save" name ="submit">Save</button>
                                                </div>
                                                   
                                                    </div>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>

<script>
$('#save').on('click', function(){


        const branch = $('#branch').val();
        const season = $('#season').val();
        const year = $('#year').val();
    
    swal({
                        title: 'Would You like to Save The Setup?',
                        text: "All New Expense Transaction will be Saved Based on Setup data!",
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger m-l-10',
                        confirmButtonText: 'Yes, Add it!'
                        }).then(function () {

                            var formData = new FormData()
                            formData.append('branch',branch);
                            formData.append('season',season)
                            formData.append('year',year)
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });

                            $.ajax({
                              url: "/setup-expense",
                              type: 'POST',
                              data: formData,
                              processData:false,
                              contentType: false,       
                                }).done(function(response){
                                    console.log(response);
                                        
                                    if (response == "save") {
                                        swal(
                                        'New Setup Save!',
                                        'Setup Sucessfull Added',
                                        'success'
                                          );
                                         location.reload();
                                        
                                    } else {
                                        swal(
                                        'Setup Updated',
                                        'Setup Successfully Updated.',
                                        'success'
                                          );
                                          location.reload();
                                        
                                    }
                                    
                                });

                        },function (dismiss) {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                        if (dismiss === 'cancel') {
                                swal(
                                'Cancelled',
                                'Operation Cancelled',
                                'info'
                         )
                 }
         });

});

</script>

@stop