@extends('main')
@section('title')
 Total Sales per Program
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Sales per Program Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Sales per Program Records</h5>
                                   <br/>
                                   
                                </div>
            <div class="widget-body clearfix">
            <h5 class="box-title mr-b-0">Total Sales Abra</h5>
                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>       
                        <tr>
                            <th>Program</th>
                            <th>Enrollees</th>
                            <th>Sales Season 1</th>
                            <th>Sales Season 2</th>

                        </tr>
                    </thead>
                    <tbody>
                            <tr>
                                    <td>LET</td>
                                    <td>{{$abra_let}}</td>
                                    <td>{{$abra_let_1_sale}}</td>
                                    <td>{{$abra_let_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>NLE</td>
                                    <td>{{$abra_nle}}</td>
                                    <td>{{$abra_nle_1_sale}}</td>
                                    <td>{{$abra_nle_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Criminology</td>
                                    <td>{{$abra_crim}}</td>
                                    <td>{{$abra_crim_1_sale}}</td>
                                    <td>{{$abra_crim_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Civil Service</td>
                                    <td>{{$abra_civil}}</td>
                                    <td>{{$abra_civil_1_sale}}</td>
                                    <td>{{$abra_civil_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Psychometrician</td>
                                    <td>{{$abra_psyc}}</td>
                                    <td>{{$abra_psyc_1_sale}}</td>
                                    <td>{{$abra_psyc_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>NCLEX</td>
                                    <td>{{$abra_nclex}}</td>
                                    <td>{{$abra_nclex_1_sale}}</td>
                                    <td>{{$abra_nclex_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>IELTS</td>
                                    <td>{{$abra_ielts}}</td>
                                    <td>{{$abra_ielts_1_sale}}</td>
                                    <td>{{$abra_ielts_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Social Work</td>
                                    <td>{{$abra_social}}</td>
                                    <td>{{$abra_social_1_sale}}</td>
                                    <td>{{$abra_social_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Agriculture</td>
                                    <td>{{$abra_agri}}</td>
                                    <td>{{$abra_agri_1_sale}}</td>
                                    <td>{{$abra_agri_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Midwifery</td>
                                    <td>{{$abra_mid}}</td>
                                    <td>{{$abra_mid_1_sale}}</td>
                                    <td>{{$abra_mid_2_sale}}</td>
                                </tr>
                                <tr>
                                    <td>Online Only</td>
                                    <td>{{$abra_online}}</td>
                                    <td>{{$abra_online_1_sale}}</td>
                                    <td>{{$abra_online_2_sale}}</td>
                                </tr>
                    </tbody>                  
                </table>
            </div>


            <div class="widget-body clearfix">
                    <h5 class="box-title mr-b-0">Total Sales Baguio</h5>
                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                            <thead>       
                                <tr>
                                    <th>Program</th>
                                    <th>Enrollees</th>
                                    <th>Sales Season 1</th>
                                    <th>Sales Season 2</th>
        
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                            <td>LET</td>
                                            <td>{{$baguio_let}}</td>
                                            <td>{{$baguio_let_1_sale}}</td>
                                            <td>{{$baguio_let_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>NLE</td>
                                            <td>{{$baguio_nle}}</td>
                                            <td>{{$baguio_nle_1_sale}}</td>
                                            <td>{{$baguio_nle_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Criminology</td>
                                            <td>{{$baguio_crim}}</td>
                                            <td>{{$baguio_crim_1_sale}}</td>
                                            <td>{{$baguio_crim_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Civil Service</td>
                                            <td>{{$baguio_civil}}</td>
                                            <td>{{$baguio_civil_1_sale}}</td>
                                            <td>{{$baguio_civil_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Psychometrician</td>
                                            <td>{{$baguio_psyc}}</td>
                                            <td>{{$baguio_psyc_1_sale}}</td>
                                            <td>{{$baguio_psyc_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>NCLEX</td>
                                            <td>{{$baguio_nclex}}</td>
                                            <td>{{$baguio_nclex_1_sale}}</td>
                                            <td>{{$baguio_nclex_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>IELTS</td>
                                            <td>{{$baguio_ielts}}</td>
                                            <td>{{$baguio_ielts_1_sale}}</td>
                                            <td>{{$baguio_ielts_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Social Work</td>
                                            <td>{{$baguio_social}}</td>
                                            <td>{{$baguio_social_1_sale}}</td>
                                            <td>{{$baguio_social_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Agriculture</td>
                                            <td>{{$baguio_agri}}</td>
                                            <td>{{$baguio_agri_1_sale}}</td>
                                            <td>{{$baguio_agri_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Midwifery</td>
                                            <td>{{$baguio_mid}}</td>
                                            <td>{{$baguio_mid_1_sale}}</td>
                                            <td>{{$baguio_mid_2_sale}}</td>
                                        </tr>
                                        <tr>
                                            <td>Online Only</td>
                                            <td>{{$baguio_online}}</td>
                                            <td>{{$baguio_online_1_sale}}</td>
                                            <td>{{$baguio_online_2_sale}}</td>
                                        </tr>
                            </tbody>                  
                        </table>
                    </div>
                    
                    
                    <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Total Sales Calapan</h5>
                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                    <thead>       
                                        <tr>
                                            <th>Program</th>
                                            <th>Enrollees</th>
                                            <th>Sales Season 1</th>
                                            <th>Sales Season 2</th>
                
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                    <td>LET</td>
                                                    <td>{{$calapan_let}}</td>
                                                    <td>{{$calapan_let_1_sale}}</td>
                                                    <td>{{$calapan_let_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>NLE</td>
                                                    <td>{{$calapan_nle}}</td>
                                                    <td>{{$calapan_nle_1_sale}}</td>
                                                    <td>{{$calapan_nle_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Criminology</td>
                                                    <td>{{$calapan_crim}}</td>
                                                    <td>{{$calapan_crim_1_sale}}</td>
                                                    <td>{{$calapan_crim_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Civil Service</td>
                                                    <td>{{$calapan_civil}}</td>
                                                    <td>{{$calapan_civil_1_sale}}</td>
                                                    <td>{{$calapan_civil_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Psychometrician</td>
                                                    <td>{{$calapan_psyc}}</td>
                                                    <td>{{$calapan_psyc_1_sale}}</td>
                                                    <td>{{$calapan_psyc_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>NCLEX</td>
                                                    <td>{{$calapan_nclex}}</td>
                                                    <td>{{$calapan_nclex_1_sale}}</td>
                                                    <td>{{$calapan_nclex_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>IELTS</td>
                                                    <td>{{$calapan_ielts}}</td>
                                                    <td>{{$calapan_ielts_1_sale}}</td>
                                                    <td>{{$calapan_ielts_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Social Work</td>
                                                    <td>{{$calapan_social}}</td>
                                                    <td>{{$calapan_social_1_sale}}</td>
                                                    <td>{{$calapan_social_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Agriculture</td>
                                                    <td>{{$calapan_agri}}</td>
                                                    <td>{{$calapan_agri_1_sale}}</td>
                                                    <td>{{$calapan_agri_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Midwifery</td>
                                                    <td>{{$calapan_mid}}</td>
                                                    <td>{{$calapan_mid_1_sale}}</td>
                                                    <td>{{$calapan_mid_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Online Only</td>
                                                    <td>{{$calapan_online}}</td>
                                                    <td>{{$calapan_online_1_sale}}</td>
                                                    <td>{{$calapan_online_2_sale}}</td>
                                                </tr>
                                    </tbody>                  
                                </table>
                            </div>

                            <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Sales Candon</h5>
                                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                            <thead>       
                                                <tr>
                                                    <th>Program</th>
                                                    <th>Enrollees</th>
                                                    <th>Sales Season 1</th>
                                                    <th>Sales Season 2</th>
                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr>
                                                            <td>LET</td>
                                                            <td>{{$candon_let}}</td>
                                                            <td>{{$candon_let_1_sale}}</td>
                                                            <td>{{$candon_let_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NLE</td>
                                                            <td>{{$candon_nle}}</td>
                                                            <td>{{$candon_nle_1_sale}}</td>
                                                            <td>{{$candon_nle_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Criminology</td>
                                                            <td>{{$candon_crim}}</td>
                                                            <td>{{$candon_crim_1_sale}}</td>
                                                            <td>{{$candon_crim_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Civil Service</td>
                                                            <td>{{$candon_civil}}</td>
                                                            <td>{{$candon_civil_1_sale}}</td>
                                                            <td>{{$candon_civil_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Psychometrician</td>
                                                            <td>{{$candon_psyc}}</td>
                                                            <td>{{$candon_psyc_1_sale}}</td>
                                                            <td>{{$candon_psyc_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NCLEX</td>
                                                            <td>{{$candon_nclex}}</td>
                                                            <td>{{$candon_nclex_1_sale}}</td>
                                                            <td>{{$candon_nclex_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>IELTS</td>
                                                            <td>{{$candon_ielts}}</td>
                                                            <td>{{$candon_ielts_1_sale}}</td>
                                                            <td>{{$candon_ielts_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Social Work</td>
                                                            <td>{{$candon_social}}</td>
                                                            <td>{{$candon_social_1_sale}}</td>
                                                            <td>{{$candon_social_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Agriculture</td>
                                                            <td>{{$candon_agri}}</td>
                                                            <td>{{$candon_agri_1_sale}}</td>
                                                            <td>{{$candon_agri_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Midwifery</td>
                                                            <td>{{$candon_mid}}</td>
                                                            <td>{{$candon_mid_1_sale}}</td>
                                                            <td>{{$candon_mid_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Online Only</td>
                                                            <td>{{$candon_online}}</td>
                                                            <td>{{$candon_online_1_sale}}</td>
                                                            <td>{{$candon_online_2_sale}}</td>
                                                        </tr>
                                            </tbody>                  
                                        </table>
                                    </div>
                                    <div class="widget-body clearfix">
                                            <h5 class="box-title mr-b-0">Total Sales Dagupan</h5>
                                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                                    <thead>       
                                                        <tr>
                                                            <th>Program</th>
                                                            <th>Enrollees</th>
                                                            <th>Sales Season 1</th>
                                                            <th>Sales Season 2</th>
                                
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                            <tr>
                                                                    <td>LET</td>
                                                                    <td>{{$dagupan_let}}</td>
                                                                    <td>{{$dagupan_let_1_sale}}</td>
                                                                    <td>{{$dagupan_let_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NLE</td>
                                                                    <td>{{$dagupan_nle}}</td>
                                                                    <td>{{$dagupan_nle_1_sale}}</td>
                                                                    <td>{{$dagupan_nle_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Criminology</td>
                                                                    <td>{{$dagupan_crim}}</td>
                                                                    <td>{{$dagupan_crim_1_sale}}</td>
                                                                    <td>{{$dagupan_crim_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Civil Service</td>
                                                                    <td>{{$dagupan_civil}}</td>
                                                                    <td>{{$dagupan_civil_1_sale}}</td>
                                                                    <td>{{$dagupan_civil_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Psychometrician</td>
                                                                    <td>{{$dagupan_psyc}}</td>
                                                                    <td>{{$dagupan_psyc_1_sale}}</td>
                                                                    <td>{{$dagupan_psyc_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NCLEX</td>
                                                                    <td>{{$dagupan_nclex}}</td>
                                                                    <td>{{$dagupan_nclex_1_sale}}</td>
                                                                    <td>{{$dagupan_nclex_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>IELTS</td>
                                                                    <td>{{$dagupan_ielts}}</td>
                                                                    <td>{{$dagupan_ielts_1_sale}}</td>
                                                                    <td>{{$dagupan_ielts_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Social Work</td>
                                                                    <td>{{$dagupan_social}}</td>
                                                                    <td>{{$dagupan_social_1_sale}}</td>
                                                                    <td>{{$dagupan_social_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Agriculture</td>
                                                                    <td>{{$dagupan_agri}}</td>
                                                                    <td>{{$dagupan_agri_1_sale}}</td>
                                                                    <td>{{$dagupan_agri_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Midwifery</td>
                                                                    <td>{{$dagupan_mid}}</td>
                                                                    <td>{{$dagupan_mid_1_sale}}</td>
                                                                    <td>{{$dagupan_mid_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Online Only</td>
                                                                    <td>{{$dagupan_online}}</td>
                                                                    <td>{{$dagupan_online_1_sale}}</td>
                                                                    <td>{{$dagupan_online_2_sale}}</td>
                                                                </tr>
                                                    </tbody>                  
                                                </table>
                                            </div>
                                            <div class="widget-body clearfix">
                                                    <h5 class="box-title mr-b-0">Total Sales Fairview</h5>
                                                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                                            <thead>       
                                                                <tr>
                                                                    <th>Program</th>
                                                                    <th>Enrollees</th>
                                                                    <th>Sales Season 1</th>
                                                                    <th>Sales Season 2</th>
                                        
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                    <tr>
                                                                            <td>LET</td>
                                                                            <td>{{$fairview_let}}</td>
                                                                            <td>{{$fairview_let_1_sale}}</td>
                                                                            <td>{{$fairview_let_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>NLE</td>
                                                                            <td>{{$fairview_nle}}</td>
                                                                            <td>{{$fairview_nle_1_sale}}</td>
                                                                            <td>{{$fairview_nle_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Criminology</td>
                                                                            <td>{{$fairview_crim}}</td>
                                                                            <td>{{$fairview_crim_1_sale}}</td>
                                                                            <td>{{$fairview_crim_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Civil Service</td>
                                                                            <td>{{$fairview_civil}}</td>
                                                                            <td>{{$fairview_civil_1_sale}}</td>
                                                                            <td>{{$fairview_civil_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Psychometrician</td>
                                                                            <td>{{$fairview_psyc}}</td>
                                                                            <td>{{$fairview_psyc_1_sale}}</td>
                                                                            <td>{{$fairview_psyc_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>NCLEX</td>
                                                                            <td>{{$fairview_nclex}}</td>
                                                                            <td>{{$fairview_nclex_1_sale}}</td>
                                                                            <td>{{$fairview_nclex_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>IELTS</td>
                                                                            <td>{{$fairview_ielts}}</td>
                                                                            <td>{{$fairview_ielts_1_sale}}</td>
                                                                            <td>{{$fairview_ielts_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Social Work</td>
                                                                            <td>{{$fairview_social}}</td>
                                                                            <td>{{$fairview_social_1_sale}}</td>
                                                                            <td>{{$fairview_social_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Agriculture</td>
                                                                            <td>{{$fairview_agri}}</td>
                                                                            <td>{{$fairview_agri_1_sale}}</td>
                                                                            <td>{{$fairview_agri_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Midwifery</td>
                                                                            <td>{{$fairview_mid}}</td>
                                                                            <td>{{$fairview_mid_1_sale}}</td>
                                                                            <td>{{$fairview_mid_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Online Only</td>
                                                                            <td>{{$fairview_online}}</td>
                                                                            <td>{{$fairview_online_1_sale}}</td>
                                                                            <td>{{$fairview_online_2_sale}}</td>
                                                                        </tr>
                                                            </tbody>                  
                                                        </table>
                                                    </div>

                                                    <div class="widget-body clearfix">
                                                            <h5 class="box-title mr-b-0">Total Sales Las Pinas</h5>
                                                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                                                    <thead>       
                                                                        <tr>
                                                                            <th>Program</th>
                                                                            <th>Enrollees</th>
                                                                            <th>Sales Season 1</th>
                                                                            <th>Sales Season 2</th>
                                                
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                            <tr>
                                                                                    <td>LET</td>
                                                                                    <td>{{$las_pinas_let}}</td>
                                                                                    <td>{{$las_pinas_let_1_sale}}</td>
                                                                                    <td>{{$las_pinas_let_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>NLE</td>
                                                                                    <td>{{$las_pinas_nle}}</td>
                                                                                    <td>{{$las_pinas_nle_1_sale}}</td>
                                                                                    <td>{{$las_pinas_nle_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Criminology</td>
                                                                                    <td>{{$las_pinas_crim}}</td>
                                                                                    <td>{{$las_pinas_crim_1_sale}}</td>
                                                                                    <td>{{$las_pinas_crim_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Civil Service</td>
                                                                                    <td>{{$las_pinas_civil}}</td>
                                                                                    <td>{{$las_pinas_civil_1_sale}}</td>
                                                                                    <td>{{$las_pinas_civil_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Psychometrician</td>
                                                                                    <td>{{$las_pinas_psyc}}</td>
                                                                                    <td>{{$las_pinas_psyc_1_sale}}</td>
                                                                                    <td>{{$las_pinas_psyc_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>NCLEX</td>
                                                                                    <td>{{$las_pinas_nclex}}</td>
                                                                                    <td>{{$las_pinas_nclex_1_sale}}</td>
                                                                                    <td>{{$las_pinas_nclex_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>IELTS</td>
                                                                                    <td>{{$las_pinas_ielts}}</td>
                                                                                    <td>{{$las_pinas_ielts_1_sale}}</td>
                                                                                    <td>{{$las_pinas_ielts_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Social Work</td>
                                                                                    <td>{{$las_pinas_social}}</td>
                                                                                    <td>{{$las_pinas_social_1_sale}}</td>
                                                                                    <td>{{$las_pinas_social_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Agriculture</td>
                                                                                    <td>{{$las_pinas_agri}}</td>
                                                                                    <td>{{$las_pinas_agri_1_sale}}</td>
                                                                                    <td>{{$las_pinas_agri_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Midwifery</td>
                                                                                    <td>{{$las_pinas_mid}}</td>
                                                                                    <td>{{$las_pinas_mid_1_sale}}</td>
                                                                                    <td>{{$las_pinas_mid_2_sale}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Online Only</td>
                                                                                    <td>{{$las_pinas_online}}</td>
                                                                                    <td>{{$las_pinas_online_1_sale}}</td>
                                                                                    <td>{{$las_pinas_online_2_sale}}</td>
                                                                                </tr>
                                                                    </tbody>                  
                                                                </table>
                                                            </div>
                            <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Sales Launion</h5>
                                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                            <thead>       
                                                <tr>
                                                    <th>Program</th>
                                                    <th>Enrollees</th>
                                                    <th>Sales Season 1</th>
                                                    <th>Sales Season 2</th>
                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr>
                                                            <td>LET</td>
                                                            <td>{{$launion_let}}</td>
                                                            <td>{{$launion_let_1_sale}}</td>
                                                            <td>{{$launion_let_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NLE</td>
                                                            <td>{{$launion_nle}}</td>
                                                            <td>{{$launion_nle_1_sale}}</td>
                                                            <td>{{$launion_nle_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Criminology</td>
                                                            <td>{{$launion_crim}}</td>
                                                            <td>{{$launion_crim_1_sale}}</td>
                                                            <td>{{$launion_crim_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Civil Service</td>
                                                            <td>{{$launion_civil}}</td>
                                                            <td>{{$launion_civil_1_sale}}</td>
                                                            <td>{{$launion_civil_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Psychometrician</td>
                                                            <td>{{$launion_psyc}}</td>
                                                            <td>{{$launion_psyc_1_sale}}</td>
                                                            <td>{{$launion_psyc_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NCLEX</td>
                                                            <td>{{$launion_nclex}}</td>
                                                            <td>{{$launion_nclex_1_sale}}</td>
                                                            <td>{{$launion_nclex_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>IELTS</td>
                                                            <td>{{$launion_ielts}}</td>
                                                            <td>{{$launion_ielts_1_sale}}</td>
                                                            <td>{{$launion_ielts_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Social Work</td>
                                                            <td>{{$launion_social}}</td>
                                                            <td>{{$launion_social_1_sale}}</td>
                                                            <td>{{$launion_social_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Agriculture</td>
                                                            <td>{{$launion_agri}}</td>
                                                            <td>{{$launion_agri_1_sale}}</td>
                                                            <td>{{$launion_agri_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Midwifery</td>
                                                            <td>{{$launion_mid}}</td>
                                                            <td>{{$launion_mid_1_sale}}</td>
                                                            <td>{{$launion_mid_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Online Only</td>
                                                            <td>{{$launion_online}}</td>
                                                            <td>{{$launion_online_1_sale}}</td>
                                                            <td>{{$launion_online_2_sale}}</td>
                                                        </tr>
                                            </tbody>                  
                                        </table>
                                    </div>
                    <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Total Sales Manila</h5>
                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                    <thead>       
                                        <tr>
                                            <th>Program</th>
                                            <th>Enrollees</th>
                                            <th>Sales Season 1</th>
                                            <th>Sales Season 2</th>
                
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                    <td>LET</td>
                                                    <td>{{$manila_let}}</td>
                                                    <td>{{$manila_let_1_sale}}</td>
                                                    <td>{{$manila_let_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>NLE</td>
                                                    <td>{{$manila_nle}}</td>
                                                    <td>{{$manila_nle_1_sale}}</td>
                                                    <td>{{$manila_nle_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Criminology</td>
                                                    <td>{{$manila_crim}}</td>
                                                    <td>{{$manila_crim_1_sale}}</td>
                                                    <td>{{$manila_crim_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Civil Service</td>
                                                    <td>{{$manila_civil}}</td>
                                                    <td>{{$manila_civil_1_sale}}</td>
                                                    <td>{{$manila_civil_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Psychometrician</td>
                                                    <td>{{$manila_psyc}}</td>
                                                    <td>{{$manila_psyc_1_sale}}</td>
                                                    <td>{{$manila_psyc_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>NCLEX</td>
                                                    <td>{{$manila_nclex}}</td>
                                                    <td>{{$manila_nclex_1_sale}}</td>
                                                    <td>{{$manila_nclex_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>IELTS</td>
                                                    <td>{{$manila_ielts}}</td>
                                                    <td>{{$manila_ielts_1_sale}}</td>
                                                    <td>{{$manila_ielts_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Social Work</td>
                                                    <td>{{$manila_social}}</td>
                                                    <td>{{$manila_social_1_sale}}</td>
                                                    <td>{{$manila_social_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Agriculture</td>
                                                    <td>{{$manila_agri}}</td>
                                                    <td>{{$manila_agri_1_sale}}</td>
                                                    <td>{{$manila_agri_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Midwifery</td>
                                                    <td>{{$manila_mid}}</td>
                                                    <td>{{$manila_mid_1_sale}}</td>
                                                    <td>{{$manila_mid_2_sale}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Online Only</td>
                                                    <td>{{$manila_online}}</td>
                                                    <td>{{$manila_online_1_sale}}</td>
                                                    <td>{{$manila_online_2_sale}}</td>
                                                </tr>
                                    </tbody>                  
                                </table>
                            </div>
                            <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Sales Roxas</h5>
                                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                            <thead>       
                                                <tr>
                                                    <th>Program</th>
                                                    <th>Enrollees</th>
                                                    <th>Sales Season 1</th>
                                                    <th>Sales Season 2</th>
                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr>
                                                            <td>LET</td>
                                                            <td>{{$roxas_let}}</td>
                                                            <td>{{$roxas_let_1_sale}}</td>
                                                            <td>{{$roxas_let_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NLE</td>
                                                            <td>{{$roxas_nle}}</td>
                                                            <td>{{$roxas_nle_1_sale}}</td>
                                                            <td>{{$roxas_nle_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Criminology</td>
                                                            <td>{{$roxas_crim}}</td>
                                                            <td>{{$roxas_crim_1_sale}}</td>
                                                            <td>{{$roxas_crim_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Civil Service</td>
                                                            <td>{{$roxas_civil}}</td>
                                                            <td>{{$roxas_civil_1_sale}}</td>
                                                            <td>{{$roxas_civil_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Psychometrician</td>
                                                            <td>{{$roxas_psyc}}</td>
                                                            <td>{{$roxas_psyc_1_sale}}</td>
                                                            <td>{{$roxas_psyc_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NCLEX</td>
                                                            <td>{{$roxas_nclex}}</td>
                                                            <td>{{$roxas_nclex_1_sale}}</td>
                                                            <td>{{$roxas_nclex_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>IELTS</td>
                                                            <td>{{$roxas_ielts}}</td>
                                                            <td>{{$roxas_ielts_1_sale}}</td>
                                                            <td>{{$roxas_ielts_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Social Work</td>
                                                            <td>{{$roxas_social}}</td>
                                                            <td>{{$roxas_social_1_sale}}</td>
                                                            <td>{{$roxas_social_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Agriculture</td>
                                                            <td>{{$roxas_agri}}</td>
                                                            <td>{{$roxas_agri_1_sale}}</td>
                                                            <td>{{$roxas_agri_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Midwifery</td>
                                                            <td>{{$roxas_mid}}</td>
                                                            <td>{{$roxas_mid_1_sale}}</td>
                                                            <td>{{$roxas_mid_2_sale}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Online Only</td>
                                                            <td>{{$roxas_online}}</td>
                                                            <td>{{$roxas_online_1_sale}}</td>
                                                            <td>{{$roxas_online_2_sale}}</td>
                                                        </tr>
                                            </tbody>                  
                                        </table>
                                    </div>


                                    <div class="widget-body clearfix">
                                            <h5 class="box-title mr-b-0">Total Sales Tarlac</h5>
                                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                                    <thead>       
                                                        <tr>
                                                            <th>Program</th>
                                                            <th>Enrollees</th>
                                                            <th>Sales Season 1</th>
                                                            <th>Sales Season 2</th>
                                
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                            <tr>
                                                                    <td>LET</td>
                                                                    <td>{{$tarlac_let}}</td>
                                                                    <td>{{$tarlac_let_1_sale}}</td>
                                                                    <td>{{$tarlac_let_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NLE</td>
                                                                    <td>{{$tarlac_nle}}</td>
                                                                    <td>{{$tarlac_nle_1_sale}}</td>
                                                                    <td>{{$tarlac_nle_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Criminology</td>
                                                                    <td>{{$tarlac_crim}}</td>
                                                                    <td>{{$tarlac_crim_1_sale}}</td>
                                                                    <td>{{$tarlac_crim_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Civil Service</td>
                                                                    <td>{{$tarlac_civil}}</td>
                                                                    <td>{{$tarlac_civil_1_sale}}</td>
                                                                    <td>{{$tarlac_civil_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Psychometrician</td>
                                                                    <td>{{$tarlac_psyc}}</td>
                                                                    <td>{{$tarlac_psyc_1_sale}}</td>
                                                                    <td>{{$tarlac_psyc_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>NCLEX</td>
                                                                    <td>{{$tarlac_nclex}}</td>
                                                                    <td>{{$tarlac_nclex_1_sale}}</td>
                                                                    <td>{{$tarlac_nclex_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>IELTS</td>
                                                                    <td>{{$tarlac_ielts}}</td>
                                                                    <td>{{$tarlac_ielts_1_sale}}</td>
                                                                    <td>{{$tarlac_ielts_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Social Work</td>
                                                                    <td>{{$tarlac_social}}</td>
                                                                    <td>{{$tarlac_social_1_sale}}</td>
                                                                    <td>{{$tarlac_social_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Agriculture</td>
                                                                    <td>{{$tarlac_agri}}</td>
                                                                    <td>{{$tarlac_agri_1_sale}}</td>
                                                                    <td>{{$tarlac_agri_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Midwifery</td>
                                                                    <td>{{$tarlac_mid}}</td>
                                                                    <td>{{$tarlac_mid_1_sale}}</td>
                                                                    <td>{{$tarlac_mid_2_sale}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Online Only</td>
                                                                    <td>{{$tarlac_online}}</td>
                                                                    <td>{{$tarlac_online_1_sale}}</td>
                                                                    <td>{{$tarlac_online_2_sale}}</td>
                                                                </tr>
                                                    </tbody>                  
                                                </table>
                                            </div>

                                            <div class="widget-body clearfix">
                                                    <h5 class="box-title mr-b-0">Total Sales Vigan</h5>
                                                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                                            <thead>       
                                                                <tr>
                                                                    <th>Program</th>
                                                                    <th>Enrollees</th>
                                                                    <th>Sales Season 1</th>
                                                                    <th>Sales Season 2</th>
                                        
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                    <tr>
                                                                            <td>LET</td>
                                                                            <td>{{$vigan_let}}</td>
                                                                            <td>{{$vigan_let_1_sale}}</td>
                                                                            <td>{{$vigan_let_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>NLE</td>
                                                                            <td>{{$vigan_nle}}</td>
                                                                            <td>{{$vigan_nle_1_sale}}</td>
                                                                            <td>{{$vigan_nle_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Criminology</td>
                                                                            <td>{{$vigan_crim}}</td>
                                                                            <td>{{$vigan_crim_1_sale}}</td>
                                                                            <td>{{$vigan_crim_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Civil Service</td>
                                                                            <td>{{$vigan_civil}}</td>
                                                                            <td>{{$vigan_civil_1_sale}}</td>
                                                                            <td>{{$vigan_civil_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Psychometrician</td>
                                                                            <td>{{$vigan_psyc}}</td>
                                                                            <td>{{$vigan_psyc_1_sale}}</td>
                                                                            <td>{{$vigan_psyc_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>NCLEX</td>
                                                                            <td>{{$vigan_nclex}}</td>
                                                                            <td>{{$vigan_nclex_1_sale}}</td>
                                                                            <td>{{$vigan_nclex_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>IELTS</td>
                                                                            <td>{{$vigan_ielts}}</td>
                                                                            <td>{{$vigan_ielts_1_sale}}</td>
                                                                            <td>{{$vigan_ielts_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Social Work</td>
                                                                            <td>{{$vigan_social}}</td>
                                                                            <td>{{$vigan_social_1_sale}}</td>
                                                                            <td>{{$vigan_social_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Agriculture</td>
                                                                            <td>{{$vigan_agri}}</td>
                                                                            <td>{{$vigan_agri_1_sale}}</td>
                                                                            <td>{{$vigan_agri_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Midwifery</td>
                                                                            <td>{{$vigan_mid}}</td>
                                                                            <td>{{$vigan_mid_1_sale}}</td>
                                                                            <td>{{$vigan_mid_2_sale}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Online Only</td>
                                                                            <td>{{$vigan_online}}</td>
                                                                            <td>{{$vigan_online_1_sale}}</td>
                                                                            <td>{{$vigan_online_2_sale}}</td>
                                                                        </tr>
                                                            </tbody>                  
                                                        </table>
                                                    </div>
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
});

</script>
@stop