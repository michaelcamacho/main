@extends('main')
@section('title')
 Total Dropped
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Dropped Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Dropped Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="total-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>School</th>
                            <th>Contact #</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                   
                            @if(isset($abra_a_let))
                            @foreach($abra_a_let as $abra_a_lets)
                        <tr>
                            <td>{{$abra_a_lets->last_name}}, {{$abra_a_lets->first_name}} {{$abra_a_lets->middle_name}}</td>
                            <td>{{$abra_a_lets->cbrc_id}}</td>
                            <td>{{$abra_a_lets->branch}}</td>
                            <td>{{$abra_a_lets->program}}</td>
                            <td>{{$abra_a_lets->school}}</td>
                            <td>{{$abra_a_lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($abra_a_nle))
                            @foreach($abra_a_nle as $abra_a_nles)
                        <tr>
                            <td>{{$abra_a_nles->last_name}}, {{$abra_a_nles->first_name}} {{$abra_a_nles->middle_name}}</td>
                            <td>{{$abra_a_nles->cbrc_id}}</td>
                            <td>{{$abra_a_nles->branch}}</td>
                            <td>{{$abra_a_nles->program}}</td>
                            <td>{{$abra_a_nles->school}}</td>
                            <td>{{$abra_a_nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($abra_a_crim))
                            @foreach($abra_a_crim as $abra_a_crims)
                        <tr>
                            <td>{{$abra_a_crims->last_name}}, {{$abra_a_crims->first_name}} {{$abra_a_crims->middle_name}}</td>
                            <td>{{$abra_a_crims->cbrc_id}}</td>
                            <td>{{$abra_a_crims->branch}}</td>
                            <td>{{$abra_a_crims->program}}</td>
                            <td>{{$abra_a_crims->school}}</td>
                            <td>{{$abra_a_crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            

                        @if(isset($abra_a_civil))
                            @foreach($abra_a_civil as $abra_a_civils)
                        <tr>
                            <td>{{$abra_a_civils->last_name}}, {{$abra_a_civils->first_name}} {{$abra_a_civils->middle_name}}</td>
                            <td>{{$abra_a_civils->cbrc_id}}</td>
                            <td>{{$abra_a_civils->branch}}</td>
                            <td>{{$abra_a_civils->program}}</td>
                            <td>{{$abra_a_civils->school}}</td>
                            <td>{{$abra_a_civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($abra_a_psyc))
                            @foreach($abra_a_psyc as $abra_a_psycs)
                        <tr>
                            <td>{{$abra_a_psycs->last_name}}, {{$abra_a_psycs->first_name}} {{$abra_a_psycs->middle_name}}</td>
                            <td>{{$abra_a_psycs->cbrc_id}}</td>
                            <td>{{$abra_a_psycs->branch}}</td>
                            <td>{{$abra_a_psycs->program}}</td>
                            <td>{{$abra_a_psycs->school}}</td>
                            <td>{{$abra_a_psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                        @if(isset($abra_a_nclex))
                            @foreach($abra_a_nclex as $abra_a_nclexs)
                        <tr>
                            <td>{{$abra_a_nclexs->last_name}}, {{$abra_a_nclexs->first_name}} {{$abra_a_nclexs->middle_name}}</td>
                            <td>{{$abra_a_nclexs->cbrc_id}}</td>
                            <td>{{$abra_a_nclexs->branch}}</td>
                            <td>{{$abra_a_nclexs->program}}</td>
                            <td>{{$abra_a_nclexs->school}}</td>
                            <td>{{$abra_a_nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($abra_a_ielt))
                            @foreach($abra_a_ielt as $abra_a_ielts)
                        <tr>
                            <td>{{$abra_a_ielts->last_name}}, {{$abra_a_ielts->first_name}} {{$abra_a_ielts->middle_name}}</td>
                            <td>{{$abra_a_ielts->cbrc_id}}</td>
                            <td>{{$abra_a_ielts->branch}}</td>
                            <td>{{$abra_a_ielts->program}}</td>
                            <td>{{$abra_a_ielts->school}}</td>
                            <td>{{$abra_a_ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($abra_a_social))
                            @foreach($abra_a_social as $abra_a_socials)
                        <tr>
                            <td>{{$abra_a_socials->last_name}}, {{$abra_a_socials->first_name}} {{$abra_a_socials->middle_name}}</td>
                            <td>{{$abra_a_socials->cbrc_id}}</td>
                            <td>{{$abra_a_socials->branch}}</td>
                            <td>{{$abra_a_socials->program}}</td>
                            <td>{{$abra_a_socials->school}}</td>
                            <td>{{$abra_a_socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif


                        @if(isset($abra_a_agri))
                            @foreach($abra_a_agri as $abra_a_agris)
                        <tr>
                            <td>{{$abra_a_agris->last_name}}, {{$abra_a_agris->first_name}} {{$abra_a_agris->middle_name}}</td>
                            <td>{{$abra_a_agris->cbrc_id}}</td>
                            <td>{{$abra_a_agris->branch}}</td>
                            <td>{{$abra_a_agris->program}}</td>
                            <td>{{$abra_a_agris->school}}</td>
                            <td>{{$abra_a_agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($abra_a_mid))
                            @foreach($abra_a_mid as $abra_a_mids)
                        <tr>
                            <td>{{$abra_a_mids->last_name}}, {{$abra_a_mids->first_name}} {{$abra_a_mids->middle_name}}</td>
                            <td>{{$abra_a_mids->cbrc_id}}</td>
                            <td>{{$abra_a_mids->branch}}</td>
                            <td>{{$abra_a_mids->program}}</td>
                            <td>{{$abra_a_mids->school}}</td>
                            <td>{{$abra_a_mids->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif

                            @if(isset($abra_a_online))
                            @foreach($abra_a_online as $abra_a_onlines)
                        <tr>
                            <td>{{$abra_a_onlines->last_name}}, {{$abra_a_onlines->first_name}} {{$abra_a_onlines->middle_name}}</td>
                            <td>{{$abra_a_onlines->cbrc_id}}</td>
                            <td>{{$abra_a_onlines->branch}}</td>
                            <td>{{$abra_a_onlines->program}}</td>
                            <td>{{$abra_a_onlines->school}}</td>
                            <td>{{$abra_a_onlines->contact_no}}</td>
                        </tr>
                       @endforeach
                    @endif


                    @if(isset($baguio_a_let))
                    @foreach($baguio_a_let as $baguio_a_lets)
                <tr>
                    <td>{{$baguio_a_lets->last_name}}, {{$baguio_a_lets->first_name}} {{$baguio_a_lets->middle_name}}</td>
                    <td>{{$baguio_a_lets->cbrc_id}}</td>
                    <td>{{$baguio_a_lets->branch}}</td>
                    <td>{{$baguio_a_lets->program}}</td>
                    <td>{{$baguio_a_lets->school}}</td>
                    <td>{{$baguio_a_lets->contact_no}}</td>
                </tr>
               @endforeach
                    @endif

                    

                @if(isset($baguio_a_nle))
                    @foreach($baguio_a_nle as $baguio_a_nles)
                <tr>
                    <td>{{$baguio_a_nles->last_name}}, {{$baguio_a_nles->first_name}} {{$baguio_a_nles->middle_name}}</td>
                    <td>{{$baguio_a_nles->cbrc_id}}</td>
                    <td>{{$baguio_a_nles->branch}}</td>
                    <td>{{$baguio_a_nles->program}}</td>
                    <td>{{$baguio_a_nles->school}}</td>
                    <td>{{$baguio_a_nles->contact_no}}</td>
                </tr>
               @endforeach
                    @endif

                    

                @if(isset($baguio_a_crim))
                    @foreach($baguio_a_crim as $baguio_a_crims)
                <tr>
                    <td>{{$baguio_a_crims->last_name}}, {{$baguio_a_crims->first_name}} {{$baguio_a_crims->middle_name}}</td>
                    <td>{{$baguio_a_crims->cbrc_id}}</td>
                    <td>{{$baguio_a_crims->branch}}</td>
                    <td>{{$baguio_a_crims->program}}</td>
                    <td>{{$baguio_a_crims->school}}</td>
                    <td>{{$baguio_a_crims->contact_no}}</td>
                </tr>
               @endforeach
                    @endif

                    

                @if(isset($baguio_a_civil))
                    @foreach($baguio_a_civil as $baguio_a_civils)
                <tr>
                    <td>{{$baguio_a_civils->last_name}}, {{$baguio_a_civils->first_name}} {{$baguio_a_civils->middle_name}}</td>
                    <td>{{$baguio_a_civils->cbrc_id}}</td>
                    <td>{{$baguio_a_civils->branch}}</td>
                    <td>{{$baguio_a_civils->program}}</td>
                    <td>{{$baguio_a_civils->school}}</td>
                    <td>{{$baguio_a_civils->contact_no}}</td>
                </tr>
               @endforeach
                    @endif

                @if(isset($baguio_a_psyc))
                    @foreach($baguio_a_psyc as $baguio_a_psycs)
                <tr>
                    <td>{{$baguio_a_psycs->last_name}}, {{$baguio_a_psycs->first_name}} {{$baguio_a_psycs->middle_name}}</td>
                    <td>{{$baguio_a_psycs->cbrc_id}}</td>
                    <td>{{$baguio_a_psycs->branch}}</td>
                    <td>{{$baguio_a_psycs->program}}</td>
                    <td>{{$baguio_a_psycs->school}}</td>
                    <td>{{$baguio_a_psycs->contact_no}}</td>
                </tr>
               @endforeach
                    @endif

                @if(isset($baguio_a_nclex))
                    @foreach($baguio_a_nclex as $baguio_a_nclexs)
                <tr>
                    <td>{{$baguio_a_nclexs->last_name}}, {{$baguio_a_nclexs->first_name}} {{$baguio_a_nclexs->middle_name}}</td>
                    <td>{{$baguio_a_nclexs->cbrc_id}}</td>
                    <td>{{$baguio_a_nclexs->branch}}</td>
                    <td>{{$baguio_a_nclexs->program}}</td>
                    <td>{{$baguio_a_nclexs->school}}</td>
                    <td>{{$baguio_a_nclexs->contact_no}}</td>
                </tr>
               @endforeach
                    @endif


                @if(isset($baguio_a_ielt))
                    @foreach($baguio_a_ielt as $baguio_a_ielts)
                <tr>
                    <td>{{$baguio_a_ielts->last_name}}, {{$baguio_a_ielts->first_name}} {{$baguio_a_ielts->middle_name}}</td>
                    <td>{{$baguio_a_ielts->cbrc_id}}</td>
                    <td>{{$baguio_a_ielts->branch}}</td>
                    <td>{{$baguio_a_ielts->program}}</td>
                    <td>{{$baguio_a_ielts->school}}</td>
                    <td>{{$baguio_a_ielts->contact_no}}</td>
                </tr>
               @endforeach
                    @endif


                @if(isset($baguio_a_social))
                    @foreach($baguio_a_social as $baguio_a_socials)
                <tr>
                    <td>{{$baguio_a_socials->last_name}}, {{$baguio_a_socials->first_name}} {{$baguio_a_socials->middle_name}}</td>
                    <td>{{$baguio_a_socials->cbrc_id}}</td>
                    <td>{{$baguio_a_socials->branch}}</td>
                    <td>{{$baguio_a_socials->program}}</td>
                    <td>{{$baguio_a_socials->school}}</td>
                    <td>{{$baguio_a_socials->contact_no}}</td>
                </tr>
               @endforeach
                    @endif


                @if(isset($baguio_a_agri))
                    @foreach($baguio_a_agri as $baguio_a_agris)
                <tr>
                    <td>{{$baguio_a_agris->last_name}}, {{$baguio_a_agris->first_name}} {{$baguio_a_agris->middle_name}}</td>
                    <td>{{$baguio_a_agris->cbrc_id}}</td>
                    <td>{{$baguio_a_agris->branch}}</td>
                    <td>{{$baguio_a_agris->program}}</td>
                    <td>{{$baguio_a_agris->school}}</td>
                    <td>{{$baguio_a_agris->contact_no}}</td>
                </tr>
               @endforeach
                    @endif

                    @if(isset($baguio_a_mid))
                    @foreach($baguio_a_mid as $baguio_a_mids)
                <tr>
                    <td>{{$baguio_a_mids->last_name}}, {{$baguio_a_mids->first_name}} {{$baguio_a_mids->middle_name}}</td>
                    <td>{{$baguio_a_mids->cbrc_id}}</td>
                    <td>{{$baguio_a_mids->branch}}</td>
                    <td>{{$baguio_a_mids->program}}</td>
                    <td>{{$baguio_a_mids->school}}</td>
                    <td>{{$baguio_a_mids->contact_no}}</td>
                </tr>
               @endforeach
                    @endif

                    @if(isset($baguio_a_online))
                    @foreach($baguio_a_online as $baguio_a_onlines)
                <tr>
                    <td>{{$baguio_a_onlines->last_name}}, {{$baguio_a_onlines->first_name}} {{$baguio_a_onlines->middle_name}}</td>
                    <td>{{$baguio_a_onlines->cbrc_id}}</td>
                    <td>{{$baguio_a_onlines->branch}}</td>
                    <td>{{$baguio_a_onlines->program}}</td>
                    <td>{{$baguio_a_onlines->school}}</td>
                    <td>{{$baguio_a_onlines->contact_no}}</td>
                </tr>
               @endforeach
            @endif

            @if(isset($calapan_a_let))
            @foreach($calapan_a_let as $calapan_a_lets)
        <tr>
            <td>{{$calapan_a_lets->last_name}}, {{$calapan_a_lets->first_name}} {{$calapan_a_lets->middle_name}}</td>
            <td>{{$calapan_a_lets->cbrc_id}}</td>
            <td>{{$calapan_a_lets->branch}}</td>
            <td>{{$calapan_a_lets->program}}</td>
            <td>{{$calapan_a_lets->school}}</td>
            <td>{{$calapan_a_lets->contact_no}}</td>
        </tr>
       @endforeach
            @endif

            

        @if(isset($calapan_a_nle))
            @foreach($calapan_a_nle as $calapan_a_nles)
        <tr>
            <td>{{$calapan_a_nles->last_name}}, {{$calapan_a_nles->first_name}} {{$calapan_a_nles->middle_name}}</td>
            <td>{{$calapan_a_nles->cbrc_id}}</td>
            <td>{{$calapan_a_nles->branch}}</td>
            <td>{{$calapan_a_nles->program}}</td>
            <td>{{$calapan_a_nles->school}}</td>
            <td>{{$calapan_a_nles->contact_no}}</td>
        </tr>
       @endforeach
            @endif

            

        @if(isset($calapan_a_crim))
            @foreach($calapan_a_crim as $calapan_a_crims)
        <tr>
            <td>{{$calapan_a_crims->last_name}}, {{$calapan_a_crims->first_name}} {{$calapan_a_crims->middle_name}}</td>
            <td>{{$calapan_a_crims->cbrc_id}}</td>
            <td>{{$calapan_a_crims->branch}}</td>
            <td>{{$calapan_a_crims->program}}</td>
            <td>{{$calapan_a_crims->school}}</td>
            <td>{{$calapan_a_crims->contact_no}}</td>
        </tr>
       @endforeach
            @endif

            

        @if(isset($calapan_a_civil))
            @foreach($calapan_a_civil as $calapan_a_civils)
        <tr>
            <td>{{$calapan_a_civils->last_name}}, {{$calapan_a_civils->first_name}} {{$calapan_a_civils->middle_name}}</td>
            <td>{{$calapan_a_civils->cbrc_id}}</td>
            <td>{{$calapan_a_civils->branch}}</td>
            <td>{{$calapan_a_civils->program}}</td>
            <td>{{$calapan_a_civils->school}}</td>
            <td>{{$calapan_a_civils->contact_no}}</td>
        </tr>
       @endforeach
            @endif

        @if(isset($calapan_a_psyc))
            @foreach($calapan_a_psyc as $calapan_a_psycs)
        <tr>
            <td>{{$calapan_a_psycs->last_name}}, {{$calapan_a_psycs->first_name}} {{$calapan_a_psycs->middle_name}}</td>
            <td>{{$calapan_a_psycs->cbrc_id}}</td>
            <td>{{$calapan_a_psycs->branch}}</td>
            <td>{{$calapan_a_psycs->program}}</td>
            <td>{{$calapan_a_psycs->school}}</td>
            <td>{{$calapan_a_psycs->contact_no}}</td>
        </tr>
       @endforeach
            @endif

        @if(isset($calapan_a_nclex))
            @foreach($calapan_a_nclex as $calapan_a_nclexs)
        <tr>
            <td>{{$calapan_a_nclexs->last_name}}, {{$calapan_a_nclexs->first_name}} {{$calapan_a_nclexs->middle_name}}</td>
            <td>{{$calapan_a_nclexs->cbrc_id}}</td>
            <td>{{$calapan_a_nclexs->branch}}</td>
            <td>{{$calapan_a_nclexs->program}}</td>
            <td>{{$calapan_a_nclexs->school}}</td>
            <td>{{$calapan_a_nclexs->contact_no}}</td>
        </tr>
       @endforeach
            @endif


        @if(isset($calapan_a_ielt))
            @foreach($calapan_a_ielt as $calapan_a_ielts)
        <tr>
            <td>{{$calapan_a_ielts->last_name}}, {{$calapan_a_ielts->first_name}} {{$calapan_a_ielts->middle_name}}</td>
            <td>{{$calapan_a_ielts->cbrc_id}}</td>
            <td>{{$calapan_a_ielts->branch}}</td>
            <td>{{$calapan_a_ielts->program}}</td>
            <td>{{$calapan_a_ielts->school}}</td>
            <td>{{$calapan_a_ielts->contact_no}}</td>
        </tr>
       @endforeach
            @endif


        @if(isset($calapan_a_social))
            @foreach($calapan_a_social as $calapan_a_socials)
        <tr>
            <td>{{$calapan_a_socials->last_name}}, {{$calapan_a_socials->first_name}} {{$calapan_a_socials->middle_name}}</td>
            <td>{{$calapan_a_socials->cbrc_id}}</td>
            <td>{{$calapan_a_socials->branch}}</td>
            <td>{{$calapan_a_socials->program}}</td>
            <td>{{$calapan_a_socials->school}}</td>
            <td>{{$calapan_a_socials->contact_no}}</td>
        </tr>
       @endforeach
            @endif


        @if(isset($calapan_a_agri))
            @foreach($calapan_a_agri as $calapan_a_agris)
        <tr>
            <td>{{$calapan_a_agris->last_name}}, {{$calapan_a_agris->first_name}} {{$calapan_a_agris->middle_name}}</td>
            <td>{{$calapan_a_agris->cbrc_id}}</td>
            <td>{{$calapan_a_agris->branch}}</td>
            <td>{{$calapan_a_agris->program}}</td>
            <td>{{$calapan_a_agris->school}}</td>
            <td>{{$calapan_a_agris->contact_no}}</td>
        </tr>
       @endforeach
            @endif

            @if(isset($calapan_a_mid))
            @foreach($calapan_a_mid as $calapan_a_mids)
        <tr>
            <td>{{$calapan_a_mids->last_name}}, {{$calapan_a_mids->first_name}} {{$calapan_a_mids->middle_name}}</td>
            <td>{{$calapan_a_mids->cbrc_id}}</td>
            <td>{{$calapan_a_mids->branch}}</td>
            <td>{{$calapan_a_mids->program}}</td>
            <td>{{$calapan_a_mids->school}}</td>
            <td>{{$calapan_a_mids->contact_no}}</td>
        </tr>
       @endforeach
            @endif

            @if(isset($calapan_a_online))
            @foreach($calapan_a_online as $calapan_a_onlines)
        <tr>
            <td>{{$calapan_a_onlines->last_name}}, {{$calapan_a_onlines->first_name}} {{$calapan_a_onlines->middle_name}}</td>
            <td>{{$calapan_a_onlines->cbrc_id}}</td>
            <td>{{$calapan_a_onlines->branch}}</td>
            <td>{{$calapan_a_onlines->program}}</td>
            <td>{{$calapan_a_onlines->school}}</td>
            <td>{{$calapan_a_onlines->contact_no}}</td>
        </tr>
       @endforeach
    @endif


                        @if(isset($candon_a_let))
                        @foreach($candon_a_let as $candon_a_lets)
                    <tr>
                        <td>{{$candon_a_lets->last_name}}, {{$candon_a_lets->first_name}} {{$candon_a_lets->middle_name}}</td>
                        <td>{{$candon_a_lets->cbrc_id}}</td>
                        <td>{{$candon_a_lets->branch}}</td>
                        <td>{{$candon_a_lets->program}}</td>
                        <td>{{$candon_a_lets->school}}</td>
                        <td>{{$candon_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif

                        

                    @if(isset($candon_a_nle))
                        @foreach($candon_a_nle as $candon_a_nles)
                    <tr>
                        <td>{{$candon_a_nles->last_name}}, {{$candon_a_nles->first_name}} {{$candon_a_nles->middle_name}}</td>
                        <td>{{$candon_a_nles->cbrc_id}}</td>
                        <td>{{$candon_a_nles->branch}}</td>
                        <td>{{$candon_a_nles->program}}</td>
                        <td>{{$candon_a_nles->school}}</td>
                        <td>{{$candon_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif

                        

                    @if(isset($candon_a_crim))
                        @foreach($candon_a_crim as $candon_a_crims)
                    <tr>
                        <td>{{$candon_a_crims->last_name}}, {{$candon_a_crims->first_name}} {{$candon_a_crims->middle_name}}</td>
                        <td>{{$candon_a_crims->cbrc_id}}</td>
                        <td>{{$candon_a_crims->branch}}</td>
                        <td>{{$candon_a_crims->program}}</td>
                        <td>{{$candon_a_crims->school}}</td>
                        <td>{{$candon_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif

                        

                    @if(isset($candon_a_civil))
                        @foreach($candon_a_civil as $candon_a_civils)
                    <tr>
                        <td>{{$candon_a_civils->last_name}}, {{$candon_a_civils->first_name}} {{$candon_a_civils->middle_name}}</td>
                        <td>{{$candon_a_civils->cbrc_id}}</td>
                        <td>{{$candon_a_civils->branch}}</td>
                        <td>{{$candon_a_civils->program}}</td>
                        <td>{{$candon_a_civils->school}}</td>
                        <td>{{$candon_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif

                    @if(isset($candon_a_psyc))
                        @foreach($candon_a_psyc as $candon_a_psycs)
                    <tr>
                        <td>{{$candon_a_psycs->last_name}}, {{$candon_a_psycs->first_name}} {{$candon_a_psycs->middle_name}}</td>
                        <td>{{$candon_a_psycs->cbrc_id}}</td>
                        <td>{{$candon_a_psycs->branch}}</td>
                        <td>{{$candon_a_psycs->program}}</td>
                        <td>{{$candon_a_psycs->school}}</td>
                        <td>{{$candon_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif

                    @if(isset($candon_a_nclex))
                        @foreach($candon_a_nclex as $candon_a_nclexs)
                    <tr>
                        <td>{{$candon_a_nclexs->last_name}}, {{$candon_a_nclexs->first_name}} {{$candon_a_nclexs->middle_name}}</td>
                        <td>{{$candon_a_nclexs->cbrc_id}}</td>
                        <td>{{$candon_a_nclexs->branch}}</td>
                        <td>{{$candon_a_nclexs->program}}</td>
                        <td>{{$candon_a_nclexs->school}}</td>
                        <td>{{$candon_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif


                    @if(isset($candon_a_ielt))
                        @foreach($candon_a_ielt as $candon_a_ielts)
                    <tr>
                        <td>{{$candon_a_ielts->last_name}}, {{$candon_a_ielts->first_name}} {{$candon_a_ielts->middle_name}}</td>
                        <td>{{$candon_a_ielts->cbrc_id}}</td>
                        <td>{{$candon_a_ielts->branch}}</td>
                        <td>{{$candon_a_ielts->program}}</td>
                        <td>{{$candon_a_ielts->school}}</td>
                        <td>{{$candon_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif


                    @if(isset($candon_a_social))
                        @foreach($candon_a_social as $candon_a_socials)
                    <tr>
                        <td>{{$candon_a_socials->last_name}}, {{$candon_a_socials->first_name}} {{$candon_a_socials->middle_name}}</td>
                        <td>{{$candon_a_socials->cbrc_id}}</td>
                        <td>{{$candon_a_socials->branch}}</td>
                        <td>{{$candon_a_socials->program}}</td>
                        <td>{{$candon_a_socials->school}}</td>
                        <td>{{$candon_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif


                    @if(isset($candon_a_agri))
                        @foreach($candon_a_agri as $candon_a_agris)
                    <tr>
                        <td>{{$candon_a_agris->last_name}}, {{$candon_a_agris->first_name}} {{$candon_a_agris->middle_name}}</td>
                        <td>{{$candon_a_agris->cbrc_id}}</td>
                        <td>{{$candon_a_agris->branch}}</td>
                        <td>{{$candon_a_agris->program}}</td>
                        <td>{{$candon_a_agris->school}}</td>
                        <td>{{$candon_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif

                        @if(isset($candon_a_mid))
                        @foreach($candon_a_mid as $candon_a_mids)
                    <tr>
                        <td>{{$candon_a_mids->last_name}}, {{$candon_a_mids->first_name}} {{$candon_a_mids->middle_name}}</td>
                        <td>{{$candon_a_mids->cbrc_id}}</td>
                        <td>{{$candon_a_mids->branch}}</td>
                        <td>{{$candon_a_mids->program}}</td>
                        <td>{{$candon_a_mids->school}}</td>
                        <td>{{$candon_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                        @endif

                        @if(isset($candon_a_online))
                        @foreach($candon_a_online as $candon_a_onlines)
                    <tr>
                        <td>{{$candon_a_onlines->last_name}}, {{$candon_a_onlines->first_name}} {{$candon_a_onlines->middle_name}}</td>
                        <td>{{$candon_a_onlines->cbrc_id}}</td>
                        <td>{{$candon_a_onlines->branch}}</td>
                        <td>{{$candon_a_onlines->program}}</td>
                        <td>{{$candon_a_onlines->school}}</td>
                        <td>{{$candon_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif




                    @if(isset($dagupan_a_let))
                    @foreach($dagupan_a_let as $dagupan_a_lets)
                    <tr>
                    <td>{{$dagupan_a_lets->last_name}}, {{$dagupan_a_lets->first_name}} {{$dagupan_a_lets->middle_name}}</td>
                    <td>{{$dagupan_a_lets->cbrc_id}}</td>
                    <td>{{$dagupan_a_lets->branch}}</td>
                    <td>{{$dagupan_a_lets->program}}</td>
                    <td>{{$dagupan_a_lets->school}}</td>
                    <td>{{$dagupan_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($dagupan_a_nle))
                    @foreach($dagupan_a_nle as $dagupan_a_nles)
                    <tr>
                    <td>{{$dagupan_a_nles->last_name}}, {{$dagupan_a_nles->first_name}} {{$dagupan_a_nles->middle_name}}</td>
                    <td>{{$dagupan_a_nles->cbrc_id}}</td>
                    <td>{{$dagupan_a_nles->branch}}</td>
                    <td>{{$dagupan_a_nles->program}}</td>
                    <td>{{$dagupan_a_nles->school}}</td>
                    <td>{{$dagupan_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($dagupan_a_crim))
                    @foreach($dagupan_a_crim as $dagupan_a_crims)
                    <tr>
                    <td>{{$dagupan_a_crims->last_name}}, {{$dagupan_a_crims->first_name}} {{$dagupan_a_crims->middle_name}}</td>
                    <td>{{$dagupan_a_crims->cbrc_id}}</td>
                    <td>{{$dagupan_a_crims->branch}}</td>
                    <td>{{$dagupan_a_crims->program}}</td>
                    <td>{{$dagupan_a_crims->school}}</td>
                    <td>{{$dagupan_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($dagupan_a_civil))
                    @foreach($dagupan_a_civil as $dagupan_a_civils)
                    <tr>
                    <td>{{$dagupan_a_civils->last_name}}, {{$dagupan_a_civils->first_name}} {{$dagupan_a_civils->middle_name}}</td>
                    <td>{{$dagupan_a_civils->cbrc_id}}</td>
                    <td>{{$dagupan_a_civils->branch}}</td>
                    <td>{{$dagupan_a_civils->program}}</td>
                    <td>{{$dagupan_a_civils->school}}</td>
                    <td>{{$dagupan_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($dagupan_a_psyc))
                    @foreach($dagupan_a_psyc as $dagupan_a_psycs)
                    <tr>
                    <td>{{$dagupan_a_psycs->last_name}}, {{$dagupan_a_psycs->first_name}} {{$dagupan_a_psycs->middle_name}}</td>
                    <td>{{$dagupan_a_psycs->cbrc_id}}</td>
                    <td>{{$dagupan_a_psycs->branch}}</td>
                    <td>{{$dagupan_a_psycs->program}}</td>
                    <td>{{$dagupan_a_psycs->school}}</td>
                    <td>{{$dagupan_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($dagupan_a_nclex))
                    @foreach($dagupan_a_nclex as $dagupan_a_nclexs)
                    <tr>
                    <td>{{$dagupan_a_nclexs->last_name}}, {{$dagupan_a_nclexs->first_name}} {{$dagupan_a_nclexs->middle_name}}</td>
                    <td>{{$dagupan_a_nclexs->cbrc_id}}</td>
                    <td>{{$dagupan_a_nclexs->branch}}</td>
                    <td>{{$dagupan_a_nclexs->program}}</td>
                    <td>{{$dagupan_a_nclexs->school}}</td>
                    <td>{{$dagupan_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($dagupan_a_ielt))
                    @foreach($dagupan_a_ielt as $dagupan_a_ielts)
                    <tr>
                    <td>{{$dagupan_a_ielts->last_name}}, {{$dagupan_a_ielts->first_name}} {{$dagupan_a_ielts->middle_name}}</td>
                    <td>{{$dagupan_a_ielts->cbrc_id}}</td>
                    <td>{{$dagupan_a_ielts->branch}}</td>
                    <td>{{$dagupan_a_ielts->program}}</td>
                    <td>{{$dagupan_a_ielts->school}}</td>
                    <td>{{$dagupan_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($dagupan_a_social))
                    @foreach($dagupan_a_social as $dagupan_a_socials)
                    <tr>
                    <td>{{$dagupan_a_socials->last_name}}, {{$dagupan_a_socials->first_name}} {{$dagupan_a_socials->middle_name}}</td>
                    <td>{{$dagupan_a_socials->cbrc_id}}</td>
                    <td>{{$dagupan_a_socials->branch}}</td>
                    <td>{{$dagupan_a_socials->program}}</td>
                    <td>{{$dagupan_a_socials->school}}</td>
                    <td>{{$dagupan_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($dagupan_a_agri))
                    @foreach($dagupan_a_agri as $dagupan_a_agris)
                    <tr>
                    <td>{{$dagupan_a_agris->last_name}}, {{$dagupan_a_agris->first_name}} {{$dagupan_a_agris->middle_name}}</td>
                    <td>{{$dagupan_a_agris->cbrc_id}}</td>
                    <td>{{$dagupan_a_agris->branch}}</td>
                    <td>{{$dagupan_a_agris->program}}</td>
                    <td>{{$dagupan_a_agris->school}}</td>
                    <td>{{$dagupan_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($dagupan_a_mid))
                    @foreach($dagupan_a_mid as $dagupan_a_mids)
                    <tr>
                    <td>{{$dagupan_a_mids->last_name}}, {{$dagupan_a_mids->first_name}} {{$dagupan_a_mids->middle_name}}</td>
                    <td>{{$dagupan_a_mids->cbrc_id}}</td>
                    <td>{{$dagupan_a_mids->branch}}</td>
                    <td>{{$dagupan_a_mids->program}}</td>
                    <td>{{$dagupan_a_mids->school}}</td>
                    <td>{{$dagupan_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($dagupan_a_online))
                    @foreach($dagupan_a_online as $dagupan_a_onlines)
                    <tr>
                    <td>{{$dagupan_a_onlines->last_name}}, {{$dagupan_a_onlines->first_name}} {{$dagupan_a_onlines->middle_name}}</td>
                    <td>{{$dagupan_a_onlines->cbrc_id}}</td>
                    <td>{{$dagupan_a_onlines->branch}}</td>
                    <td>{{$dagupan_a_onlines->program}}</td>
                    <td>{{$dagupan_a_onlines->school}}</td>
                    <td>{{$dagupan_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    


                    @if(isset($fairview_a_let))
                    @foreach($fairview_a_let as $fairview_a_lets)
                    <tr>
                    <td>{{$fairview_a_lets->last_name}}, {{$fairview_a_lets->first_name}} {{$fairview_a_lets->middle_name}}</td>
                    <td>{{$fairview_a_lets->cbrc_id}}</td>
                    <td>{{$fairview_a_lets->branch}}</td>
                    <td>{{$fairview_a_lets->program}}</td>
                    <td>{{$fairview_a_lets->school}}</td>
                    <td>{{$fairview_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($fairview_a_nle))
                    @foreach($fairview_a_nle as $fairview_a_nles)
                    <tr>
                    <td>{{$fairview_a_nles->last_name}}, {{$fairview_a_nles->first_name}} {{$fairview_a_nles->middle_name}}</td>
                    <td>{{$fairview_a_nles->cbrc_id}}</td>
                    <td>{{$fairview_a_nles->branch}}</td>
                    <td>{{$fairview_a_nles->program}}</td>
                    <td>{{$fairview_a_nles->school}}</td>
                    <td>{{$fairview_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($fairview_a_crim))
                    @foreach($fairview_a_crim as $fairview_a_crims)
                    <tr>
                    <td>{{$fairview_a_crims->last_name}}, {{$fairview_a_crims->first_name}} {{$fairview_a_crims->middle_name}}</td>
                    <td>{{$fairview_a_crims->cbrc_id}}</td>
                    <td>{{$fairview_a_crims->branch}}</td>
                    <td>{{$fairview_a_crims->program}}</td>
                    <td>{{$fairview_a_crims->school}}</td>
                    <td>{{$fairview_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($fairview_a_civil))
                    @foreach($fairview_a_civil as $fairview_a_civils)
                    <tr>
                    <td>{{$fairview_a_civils->last_name}}, {{$fairview_a_civils->first_name}} {{$fairview_a_civils->middle_name}}</td>
                    <td>{{$fairview_a_civils->cbrc_id}}</td>
                    <td>{{$fairview_a_civils->branch}}</td>
                    <td>{{$fairview_a_civils->program}}</td>
                    <td>{{$fairview_a_civils->school}}</td>
                    <td>{{$fairview_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($fairview_a_psyc))
                    @foreach($fairview_a_psyc as $fairview_a_psycs)
                    <tr>
                    <td>{{$fairview_a_psycs->last_name}}, {{$fairview_a_psycs->first_name}} {{$fairview_a_psycs->middle_name}}</td>
                    <td>{{$fairview_a_psycs->cbrc_id}}</td>
                    <td>{{$fairview_a_psycs->branch}}</td>
                    <td>{{$fairview_a_psycs->program}}</td>
                    <td>{{$fairview_a_psycs->school}}</td>
                    <td>{{$fairview_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($fairview_a_nclex))
                    @foreach($fairview_a_nclex as $fairview_a_nclexs)
                    <tr>
                    <td>{{$fairview_a_nclexs->last_name}}, {{$fairview_a_nclexs->first_name}} {{$fairview_a_nclexs->middle_name}}</td>
                    <td>{{$fairview_a_nclexs->cbrc_id}}</td>
                    <td>{{$fairview_a_nclexs->branch}}</td>
                    <td>{{$fairview_a_nclexs->program}}</td>
                    <td>{{$fairview_a_nclexs->school}}</td>
                    <td>{{$fairview_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($fairview_a_ielt))
                    @foreach($fairview_a_ielt as $fairview_a_ielts)
                    <tr>
                    <td>{{$fairview_a_ielts->last_name}}, {{$fairview_a_ielts->first_name}} {{$fairview_a_ielts->middle_name}}</td>
                    <td>{{$fairview_a_ielts->cbrc_id}}</td>
                    <td>{{$fairview_a_ielts->branch}}</td>
                    <td>{{$fairview_a_ielts->program}}</td>
                    <td>{{$fairview_a_ielts->school}}</td>
                    <td>{{$fairview_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($fairview_a_social))
                    @foreach($fairview_a_social as $fairview_a_socials)
                    <tr>
                    <td>{{$fairview_a_socials->last_name}}, {{$fairview_a_socials->first_name}} {{$fairview_a_socials->middle_name}}</td>
                    <td>{{$fairview_a_socials->cbrc_id}}</td>
                    <td>{{$fairview_a_socials->branch}}</td>
                    <td>{{$fairview_a_socials->program}}</td>
                    <td>{{$fairview_a_socials->school}}</td>
                    <td>{{$fairview_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($fairview_a_agri))
                    @foreach($fairview_a_agri as $fairview_a_agris)
                    <tr>
                    <td>{{$fairview_a_agris->last_name}}, {{$fairview_a_agris->first_name}} {{$fairview_a_agris->middle_name}}</td>
                    <td>{{$fairview_a_agris->cbrc_id}}</td>
                    <td>{{$fairview_a_agris->branch}}</td>
                    <td>{{$fairview_a_agris->program}}</td>
                    <td>{{$fairview_a_agris->school}}</td>
                    <td>{{$fairview_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($fairview_a_mid))
                    @foreach($fairview_a_mid as $fairview_a_mids)
                    <tr>
                    <td>{{$fairview_a_mids->last_name}}, {{$fairview_a_mids->first_name}} {{$fairview_a_mids->middle_name}}</td>
                    <td>{{$fairview_a_mids->cbrc_id}}</td>
                    <td>{{$fairview_a_mids->branch}}</td>
                    <td>{{$fairview_a_mids->program}}</td>
                    <td>{{$fairview_a_mids->school}}</td>
                    <td>{{$fairview_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($fairview_a_online))
                    @foreach($fairview_a_online as $fairview_a_onlines)
                    <tr>
                    <td>{{$fairview_a_onlines->last_name}}, {{$fairview_a_onlines->first_name}} {{$fairview_a_onlines->middle_name}}</td>
                    <td>{{$fairview_a_onlines->cbrc_id}}</td>
                    <td>{{$fairview_a_onlines->branch}}</td>
                    <td>{{$fairview_a_onlines->program}}</td>
                    <td>{{$fairview_a_onlines->school}}</td>
                    <td>{{$fairview_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    


                    @if(isset($las_pinas_a_let))
                    @foreach($las_pinas_a_let as $las_pinas_a_lets)
                    <tr>
                    <td>{{$las_pinas_a_lets->last_name}}, {{$las_pinas_a_lets->first_name}} {{$las_pinas_a_lets->middle_name}}</td>
                    <td>{{$las_pinas_a_lets->cbrc_id}}</td>
                    <td>{{$las_pinas_a_lets->branch}}</td>
                    <td>{{$las_pinas_a_lets->program}}</td>
                    <td>{{$las_pinas_a_lets->school}}</td>
                    <td>{{$las_pinas_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($las_pinas_a_nle))
                    @foreach($las_pinas_a_nle as $las_pinas_a_nles)
                    <tr>
                    <td>{{$las_pinas_a_nles->last_name}}, {{$las_pinas_a_nles->first_name}} {{$las_pinas_a_nles->middle_name}}</td>
                    <td>{{$las_pinas_a_nles->cbrc_id}}</td>
                    <td>{{$las_pinas_a_nles->branch}}</td>
                    <td>{{$las_pinas_a_nles->program}}</td>
                    <td>{{$las_pinas_a_nles->school}}</td>
                    <td>{{$las_pinas_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($las_pinas_a_crim))
                    @foreach($las_pinas_a_crim as $las_pinas_a_crims)
                    <tr>
                    <td>{{$las_pinas_a_crims->last_name}}, {{$las_pinas_a_crims->first_name}} {{$las_pinas_a_crims->middle_name}}</td>
                    <td>{{$las_pinas_a_crims->cbrc_id}}</td>
                    <td>{{$las_pinas_a_crims->branch}}</td>
                    <td>{{$las_pinas_a_crims->program}}</td>
                    <td>{{$las_pinas_a_crims->school}}</td>
                    <td>{{$las_pinas_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($las_pinas_a_civil))
                    @foreach($las_pinas_a_civil as $las_pinas_a_civils)
                    <tr>
                    <td>{{$las_pinas_a_civils->last_name}}, {{$las_pinas_a_civils->first_name}} {{$las_pinas_a_civils->middle_name}}</td>
                    <td>{{$las_pinas_a_civils->cbrc_id}}</td>
                    <td>{{$las_pinas_a_civils->branch}}</td>
                    <td>{{$las_pinas_a_civils->program}}</td>
                    <td>{{$las_pinas_a_civils->school}}</td>
                    <td>{{$las_pinas_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($las_pinas_a_psyc))
                    @foreach($las_pinas_a_psyc as $las_pinas_a_psycs)
                    <tr>
                    <td>{{$las_pinas_a_psycs->last_name}}, {{$las_pinas_a_psycs->first_name}} {{$las_pinas_a_psycs->middle_name}}</td>
                    <td>{{$las_pinas_a_psycs->cbrc_id}}</td>
                    <td>{{$las_pinas_a_psycs->branch}}</td>
                    <td>{{$las_pinas_a_psycs->program}}</td>
                    <td>{{$las_pinas_a_psycs->school}}</td>
                    <td>{{$las_pinas_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($las_pinas_a_nclex))
                    @foreach($las_pinas_a_nclex as $las_pinas_a_nclexs)
                    <tr>
                    <td>{{$las_pinas_a_nclexs->last_name}}, {{$las_pinas_a_nclexs->first_name}} {{$las_pinas_a_nclexs->middle_name}}</td>
                    <td>{{$las_pinas_a_nclexs->cbrc_id}}</td>
                    <td>{{$las_pinas_a_nclexs->branch}}</td>
                    <td>{{$las_pinas_a_nclexs->program}}</td>
                    <td>{{$las_pinas_a_nclexs->school}}</td>
                    <td>{{$las_pinas_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($las_pinas_a_ielt))
                    @foreach($las_pinas_a_ielt as $las_pinas_a_ielts)
                    <tr>
                    <td>{{$las_pinas_a_ielts->last_name}}, {{$las_pinas_a_ielts->first_name}} {{$las_pinas_a_ielts->middle_name}}</td>
                    <td>{{$las_pinas_a_ielts->cbrc_id}}</td>
                    <td>{{$las_pinas_a_ielts->branch}}</td>
                    <td>{{$las_pinas_a_ielts->program}}</td>
                    <td>{{$las_pinas_a_ielts->school}}</td>
                    <td>{{$las_pinas_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($las_pinas_a_social))
                    @foreach($las_pinas_a_social as $las_pinas_a_socials)
                    <tr>
                    <td>{{$las_pinas_a_socials->last_name}}, {{$las_pinas_a_socials->first_name}} {{$las_pinas_a_socials->middle_name}}</td>
                    <td>{{$las_pinas_a_socials->cbrc_id}}</td>
                    <td>{{$las_pinas_a_socials->branch}}</td>
                    <td>{{$las_pinas_a_socials->program}}</td>
                    <td>{{$las_pinas_a_socials->school}}</td>
                    <td>{{$las_pinas_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($las_pinas_a_agri))
                    @foreach($las_pinas_a_agri as $las_pinas_a_agris)
                    <tr>
                    <td>{{$las_pinas_a_agris->last_name}}, {{$las_pinas_a_agris->first_name}} {{$las_pinas_a_agris->middle_name}}</td>
                    <td>{{$las_pinas_a_agris->cbrc_id}}</td>
                    <td>{{$las_pinas_a_agris->branch}}</td>
                    <td>{{$las_pinas_a_agris->program}}</td>
                    <td>{{$las_pinas_a_agris->school}}</td>
                    <td>{{$las_pinas_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($las_pinas_a_mid))
                    @foreach($las_pinas_a_mid as $las_pinas_a_mids)
                    <tr>
                    <td>{{$las_pinas_a_mids->last_name}}, {{$las_pinas_a_mids->first_name}} {{$las_pinas_a_mids->middle_name}}</td>
                    <td>{{$las_pinas_a_mids->cbrc_id}}</td>
                    <td>{{$las_pinas_a_mids->branch}}</td>
                    <td>{{$las_pinas_a_mids->program}}</td>
                    <td>{{$las_pinas_a_mids->school}}</td>
                    <td>{{$las_pinas_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($las_pinas_a_online))
                    @foreach($las_pinas_a_online as $las_pinas_a_onlines)
                    <tr>
                    <td>{{$las_pinas_a_onlines->last_name}}, {{$las_pinas_a_onlines->first_name}} {{$las_pinas_a_onlines->middle_name}}</td>
                    <td>{{$las_pinas_a_onlines->cbrc_id}}</td>
                    <td>{{$las_pinas_a_onlines->branch}}</td>
                    <td>{{$las_pinas_a_onlines->program}}</td>
                    <td>{{$las_pinas_a_onlines->school}}</td>
                    <td>{{$las_pinas_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    


                    @if(isset($launion_a_let))
                    @foreach($launion_a_let as $launion_a_lets)
                    <tr>
                    <td>{{$launion_a_lets->last_name}}, {{$launion_a_lets->first_name}} {{$launion_a_lets->middle_name}}</td>
                    <td>{{$launion_a_lets->cbrc_id}}</td>
                    <td>{{$launion_a_lets->branch}}</td>
                    <td>{{$launion_a_lets->program}}</td>
                    <td>{{$launion_a_lets->school}}</td>
                    <td>{{$launion_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($launion_a_nle))
                    @foreach($launion_a_nle as $launion_a_nles)
                    <tr>
                    <td>{{$launion_a_nles->last_name}}, {{$launion_a_nles->first_name}} {{$launion_a_nles->middle_name}}</td>
                    <td>{{$launion_a_nles->cbrc_id}}</td>
                    <td>{{$launion_a_nles->branch}}</td>
                    <td>{{$launion_a_nles->program}}</td>
                    <td>{{$launion_a_nles->school}}</td>
                    <td>{{$launion_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($launion_a_crim))
                    @foreach($launion_a_crim as $launion_a_crims)
                    <tr>
                    <td>{{$launion_a_crims->last_name}}, {{$launion_a_crims->first_name}} {{$launion_a_crims->middle_name}}</td>
                    <td>{{$launion_a_crims->cbrc_id}}</td>
                    <td>{{$launion_a_crims->branch}}</td>
                    <td>{{$launion_a_crims->program}}</td>
                    <td>{{$launion_a_crims->school}}</td>
                    <td>{{$launion_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($launion_a_civil))
                    @foreach($launion_a_civil as $launion_a_civils)
                    <tr>
                    <td>{{$launion_a_civils->last_name}}, {{$launion_a_civils->first_name}} {{$launion_a_civils->middle_name}}</td>
                    <td>{{$launion_a_civils->cbrc_id}}</td>
                    <td>{{$launion_a_civils->branch}}</td>
                    <td>{{$launion_a_civils->program}}</td>
                    <td>{{$launion_a_civils->school}}</td>
                    <td>{{$launion_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($launion_a_psyc))
                    @foreach($launion_a_psyc as $launion_a_psycs)
                    <tr>
                    <td>{{$launion_a_psycs->last_name}}, {{$launion_a_psycs->first_name}} {{$launion_a_psycs->middle_name}}</td>
                    <td>{{$launion_a_psycs->cbrc_id}}</td>
                    <td>{{$launion_a_psycs->branch}}</td>
                    <td>{{$launion_a_psycs->program}}</td>
                    <td>{{$launion_a_psycs->school}}</td>
                    <td>{{$launion_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($launion_a_nclex))
                    @foreach($launion_a_nclex as $launion_a_nclexs)
                    <tr>
                    <td>{{$launion_a_nclexs->last_name}}, {{$launion_a_nclexs->first_name}} {{$launion_a_nclexs->middle_name}}</td>
                    <td>{{$launion_a_nclexs->cbrc_id}}</td>
                    <td>{{$launion_a_nclexs->branch}}</td>
                    <td>{{$launion_a_nclexs->program}}</td>
                    <td>{{$launion_a_nclexs->school}}</td>
                    <td>{{$launion_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($launion_a_ielt))
                    @foreach($launion_a_ielt as $launion_a_ielts)
                    <tr>
                    <td>{{$launion_a_ielts->last_name}}, {{$launion_a_ielts->first_name}} {{$launion_a_ielts->middle_name}}</td>
                    <td>{{$launion_a_ielts->cbrc_id}}</td>
                    <td>{{$launion_a_ielts->branch}}</td>
                    <td>{{$launion_a_ielts->program}}</td>
                    <td>{{$launion_a_ielts->school}}</td>
                    <td>{{$launion_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($launion_a_social))
                    @foreach($launion_a_social as $launion_a_socials)
                    <tr>
                    <td>{{$launion_a_socials->last_name}}, {{$launion_a_socials->first_name}} {{$launion_a_socials->middle_name}}</td>
                    <td>{{$launion_a_socials->cbrc_id}}</td>
                    <td>{{$launion_a_socials->branch}}</td>
                    <td>{{$launion_a_socials->program}}</td>
                    <td>{{$launion_a_socials->school}}</td>
                    <td>{{$launion_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($launion_a_agri))
                    @foreach($launion_a_agri as $launion_a_agris)
                    <tr>
                    <td>{{$launion_a_agris->last_name}}, {{$launion_a_agris->first_name}} {{$launion_a_agris->middle_name}}</td>
                    <td>{{$launion_a_agris->cbrc_id}}</td>
                    <td>{{$launion_a_agris->branch}}</td>
                    <td>{{$launion_a_agris->program}}</td>
                    <td>{{$launion_a_agris->school}}</td>
                    <td>{{$launion_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($launion_a_mid))
                    @foreach($launion_a_mid as $launion_a_mids)
                    <tr>
                    <td>{{$launion_a_mids->last_name}}, {{$launion_a_mids->first_name}} {{$launion_a_mids->middle_name}}</td>
                    <td>{{$launion_a_mids->cbrc_id}}</td>
                    <td>{{$launion_a_mids->branch}}</td>
                    <td>{{$launion_a_mids->program}}</td>
                    <td>{{$launion_a_mids->school}}</td>
                    <td>{{$launion_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($launion_a_online))
                    @foreach($launion_a_online as $launion_a_onlines)
                    <tr>
                    <td>{{$launion_a_onlines->last_name}}, {{$launion_a_onlines->first_name}} {{$launion_a_onlines->middle_name}}</td>
                    <td>{{$launion_a_onlines->cbrc_id}}</td>
                    <td>{{$launion_a_onlines->branch}}</td>
                    <td>{{$launion_a_onlines->program}}</td>
                    <td>{{$launion_a_onlines->school}}</td>
                    <td>{{$launion_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif




                    @if(isset($manila_a_let))
                    @foreach($manila_a_let as $manila_a_lets)
                    <tr>
                    <td>{{$manila_a_lets->last_name}}, {{$manila_a_lets->first_name}} {{$manila_a_lets->middle_name}}</td>
                    <td>{{$manila_a_lets->cbrc_id}}</td>
                    <td>{{$manila_a_lets->branch}}</td>
                    <td>{{$manila_a_lets->program}}</td>
                    <td>{{$manila_a_lets->school}}</td>
                    <td>{{$manila_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($manila_a_nle))
                    @foreach($manila_a_nle as $manila_a_nles)
                    <tr>
                    <td>{{$manila_a_nles->last_name}}, {{$manila_a_nles->first_name}} {{$manila_a_nles->middle_name}}</td>
                    <td>{{$manila_a_nles->cbrc_id}}</td>
                    <td>{{$manila_a_nles->branch}}</td>
                    <td>{{$manila_a_nles->program}}</td>
                    <td>{{$manila_a_nles->school}}</td>
                    <td>{{$manila_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($manila_a_crim))
                    @foreach($manila_a_crim as $manila_a_crims)
                    <tr>
                    <td>{{$manila_a_crims->last_name}}, {{$manila_a_crims->first_name}} {{$manila_a_crims->middle_name}}</td>
                    <td>{{$manila_a_crims->cbrc_id}}</td>
                    <td>{{$manila_a_crims->branch}}</td>
                    <td>{{$manila_a_crims->program}}</td>
                    <td>{{$manila_a_crims->school}}</td>
                    <td>{{$manila_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($manila_a_civil))
                    @foreach($manila_a_civil as $manila_a_civils)
                    <tr>
                    <td>{{$manila_a_civils->last_name}}, {{$manila_a_civils->first_name}} {{$manila_a_civils->middle_name}}</td>
                    <td>{{$manila_a_civils->cbrc_id}}</td>
                    <td>{{$manila_a_civils->branch}}</td>
                    <td>{{$manila_a_civils->program}}</td>
                    <td>{{$manila_a_civils->school}}</td>
                    <td>{{$manila_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($manila_a_psyc))
                    @foreach($manila_a_psyc as $manila_a_psycs)
                    <tr>
                    <td>{{$manila_a_psycs->last_name}}, {{$manila_a_psycs->first_name}} {{$manila_a_psycs->middle_name}}</td>
                    <td>{{$manila_a_psycs->cbrc_id}}</td>
                    <td>{{$manila_a_psycs->branch}}</td>
                    <td>{{$manila_a_psycs->program}}</td>
                    <td>{{$manila_a_psycs->school}}</td>
                    <td>{{$manila_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($manila_a_nclex))
                    @foreach($manila_a_nclex as $manila_a_nclexs)
                    <tr>
                    <td>{{$manila_a_nclexs->last_name}}, {{$manila_a_nclexs->first_name}} {{$manila_a_nclexs->middle_name}}</td>
                    <td>{{$manila_a_nclexs->cbrc_id}}</td>
                    <td>{{$manila_a_nclexs->branch}}</td>
                    <td>{{$manila_a_nclexs->program}}</td>
                    <td>{{$manila_a_nclexs->school}}</td>
                    <td>{{$manila_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($manila_a_ielt))
                    @foreach($manila_a_ielt as $manila_a_ielts)
                    <tr>
                    <td>{{$manila_a_ielts->last_name}}, {{$manila_a_ielts->first_name}} {{$manila_a_ielts->middle_name}}</td>
                    <td>{{$manila_a_ielts->cbrc_id}}</td>
                    <td>{{$manila_a_ielts->branch}}</td>
                    <td>{{$manila_a_ielts->program}}</td>
                    <td>{{$manila_a_ielts->school}}</td>
                    <td>{{$manila_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($manila_a_social))
                    @foreach($manila_a_social as $manila_a_socials)
                    <tr>
                    <td>{{$manila_a_socials->last_name}}, {{$manila_a_socials->first_name}} {{$manila_a_socials->middle_name}}</td>
                    <td>{{$manila_a_socials->cbrc_id}}</td>
                    <td>{{$manila_a_socials->branch}}</td>
                    <td>{{$manila_a_socials->program}}</td>
                    <td>{{$manila_a_socials->school}}</td>
                    <td>{{$manila_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($manila_a_agri))
                    @foreach($manila_a_agri as $manila_a_agris)
                    <tr>
                    <td>{{$manila_a_agris->last_name}}, {{$manila_a_agris->first_name}} {{$manila_a_agris->middle_name}}</td>
                    <td>{{$manila_a_agris->cbrc_id}}</td>
                    <td>{{$manila_a_agris->branch}}</td>
                    <td>{{$manila_a_agris->program}}</td>
                    <td>{{$manila_a_agris->school}}</td>
                    <td>{{$manila_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($manila_a_mid))
                    @foreach($manila_a_mid as $manila_a_mids)
                    <tr>
                    <td>{{$manila_a_mids->last_name}}, {{$manila_a_mids->first_name}} {{$manila_a_mids->middle_name}}</td>
                    <td>{{$manila_a_mids->cbrc_id}}</td>
                    <td>{{$manila_a_mids->branch}}</td>
                    <td>{{$manila_a_mids->program}}</td>
                    <td>{{$manila_a_mids->school}}</td>
                    <td>{{$manila_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($manila_a_online))
                    @foreach($manila_a_online as $manila_a_onlines)
                    <tr>
                    <td>{{$manila_a_onlines->last_name}}, {{$manila_a_onlines->first_name}} {{$manila_a_onlines->middle_name}}</td>
                    <td>{{$manila_a_onlines->cbrc_id}}</td>
                    <td>{{$manila_a_onlines->branch}}</td>
                    <td>{{$manila_a_onlines->program}}</td>
                    <td>{{$manila_a_onlines->school}}</td>
                    <td>{{$manila_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif




                    @if(isset($roxas_a_let))
                    @foreach($roxas_a_let as $roxas_a_lets)
                    <tr>
                    <td>{{$roxas_a_lets->last_name}}, {{$roxas_a_lets->first_name}} {{$roxas_a_lets->middle_name}}</td>
                    <td>{{$roxas_a_lets->cbrc_id}}</td>
                    <td>{{$roxas_a_lets->branch}}</td>
                    <td>{{$roxas_a_lets->program}}</td>
                    <td>{{$roxas_a_lets->school}}</td>
                    <td>{{$roxas_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($roxas_a_nle))
                    @foreach($roxas_a_nle as $roxas_a_nles)
                    <tr>
                    <td>{{$roxas_a_nles->last_name}}, {{$roxas_a_nles->first_name}} {{$roxas_a_nles->middle_name}}</td>
                    <td>{{$roxas_a_nles->cbrc_id}}</td>
                    <td>{{$roxas_a_nles->branch}}</td>
                    <td>{{$roxas_a_nles->program}}</td>
                    <td>{{$roxas_a_nles->school}}</td>
                    <td>{{$roxas_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($roxas_a_crim))
                    @foreach($roxas_a_crim as $roxas_a_crims)
                    <tr>
                    <td>{{$roxas_a_crims->last_name}}, {{$roxas_a_crims->first_name}} {{$roxas_a_crims->middle_name}}</td>
                    <td>{{$roxas_a_crims->cbrc_id}}</td>
                    <td>{{$roxas_a_crims->branch}}</td>
                    <td>{{$roxas_a_crims->program}}</td>
                    <td>{{$roxas_a_crims->school}}</td>
                    <td>{{$roxas_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($roxas_a_civil))
                    @foreach($roxas_a_civil as $roxas_a_civils)
                    <tr>
                    <td>{{$roxas_a_civils->last_name}}, {{$roxas_a_civils->first_name}} {{$roxas_a_civils->middle_name}}</td>
                    <td>{{$roxas_a_civils->cbrc_id}}</td>
                    <td>{{$roxas_a_civils->branch}}</td>
                    <td>{{$roxas_a_civils->program}}</td>
                    <td>{{$roxas_a_civils->school}}</td>
                    <td>{{$roxas_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($roxas_a_psyc))
                    @foreach($roxas_a_psyc as $roxas_a_psycs)
                    <tr>
                    <td>{{$roxas_a_psycs->last_name}}, {{$roxas_a_psycs->first_name}} {{$roxas_a_psycs->middle_name}}</td>
                    <td>{{$roxas_a_psycs->cbrc_id}}</td>
                    <td>{{$roxas_a_psycs->branch}}</td>
                    <td>{{$roxas_a_psycs->program}}</td>
                    <td>{{$roxas_a_psycs->school}}</td>
                    <td>{{$roxas_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($roxas_a_nclex))
                    @foreach($roxas_a_nclex as $roxas_a_nclexs)
                    <tr>
                    <td>{{$roxas_a_nclexs->last_name}}, {{$roxas_a_nclexs->first_name}} {{$roxas_a_nclexs->middle_name}}</td>
                    <td>{{$roxas_a_nclexs->cbrc_id}}</td>
                    <td>{{$roxas_a_nclexs->branch}}</td>
                    <td>{{$roxas_a_nclexs->program}}</td>
                    <td>{{$roxas_a_nclexs->school}}</td>
                    <td>{{$roxas_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($roxas_a_ielt))
                    @foreach($roxas_a_ielt as $roxas_a_ielts)
                    <tr>
                    <td>{{$roxas_a_ielts->last_name}}, {{$roxas_a_ielts->first_name}} {{$roxas_a_ielts->middle_name}}</td>
                    <td>{{$roxas_a_ielts->cbrc_id}}</td>
                    <td>{{$roxas_a_ielts->branch}}</td>
                    <td>{{$roxas_a_ielts->program}}</td>
                    <td>{{$roxas_a_ielts->school}}</td>
                    <td>{{$roxas_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($roxas_a_social))
                    @foreach($roxas_a_social as $roxas_a_socials)
                    <tr>
                    <td>{{$roxas_a_socials->last_name}}, {{$roxas_a_socials->first_name}} {{$roxas_a_socials->middle_name}}</td>
                    <td>{{$roxas_a_socials->cbrc_id}}</td>
                    <td>{{$roxas_a_socials->branch}}</td>
                    <td>{{$roxas_a_socials->program}}</td>
                    <td>{{$roxas_a_socials->school}}</td>
                    <td>{{$roxas_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($roxas_a_agri))
                    @foreach($roxas_a_agri as $roxas_a_agris)
                    <tr>
                    <td>{{$roxas_a_agris->last_name}}, {{$roxas_a_agris->first_name}} {{$roxas_a_agris->middle_name}}</td>
                    <td>{{$roxas_a_agris->cbrc_id}}</td>
                    <td>{{$roxas_a_agris->branch}}</td>
                    <td>{{$roxas_a_agris->program}}</td>
                    <td>{{$roxas_a_agris->school}}</td>
                    <td>{{$roxas_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($roxas_a_mid))
                    @foreach($roxas_a_mid as $roxas_a_mids)
                    <tr>
                    <td>{{$roxas_a_mids->last_name}}, {{$roxas_a_mids->first_name}} {{$roxas_a_mids->middle_name}}</td>
                    <td>{{$roxas_a_mids->cbrc_id}}</td>
                    <td>{{$roxas_a_mids->branch}}</td>
                    <td>{{$roxas_a_mids->program}}</td>
                    <td>{{$roxas_a_mids->school}}</td>
                    <td>{{$roxas_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($roxas_a_online))
                    @foreach($roxas_a_online as $roxas_a_onlines)
                    <tr>
                    <td>{{$roxas_a_onlines->last_name}}, {{$roxas_a_onlines->first_name}} {{$roxas_a_onlines->middle_name}}</td>
                    <td>{{$roxas_a_onlines->cbrc_id}}</td>
                    <td>{{$roxas_a_onlines->branch}}</td>
                    <td>{{$roxas_a_onlines->program}}</td>
                    <td>{{$roxas_a_onlines->school}}</td>
                    <td>{{$roxas_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif




                    @if(isset($tarlac_a_let))
                    @foreach($tarlac_a_let as $tarlac_a_lets)
                    <tr>
                    <td>{{$tarlac_a_lets->last_name}}, {{$tarlac_a_lets->first_name}} {{$tarlac_a_lets->middle_name}}</td>
                    <td>{{$tarlac_a_lets->cbrc_id}}</td>
                    <td>{{$tarlac_a_lets->branch}}</td>
                    <td>{{$tarlac_a_lets->program}}</td>
                    <td>{{$tarlac_a_lets->school}}</td>
                    <td>{{$tarlac_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($tarlac_a_nle))
                    @foreach($tarlac_a_nle as $tarlac_a_nles)
                    <tr>
                    <td>{{$tarlac_a_nles->last_name}}, {{$tarlac_a_nles->first_name}} {{$tarlac_a_nles->middle_name}}</td>
                    <td>{{$tarlac_a_nles->cbrc_id}}</td>
                    <td>{{$tarlac_a_nles->branch}}</td>
                    <td>{{$tarlac_a_nles->program}}</td>
                    <td>{{$tarlac_a_nles->school}}</td>
                    <td>{{$tarlac_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($tarlac_a_crim))
                    @foreach($tarlac_a_crim as $tarlac_a_crims)
                    <tr>
                    <td>{{$tarlac_a_crims->last_name}}, {{$tarlac_a_crims->first_name}} {{$tarlac_a_crims->middle_name}}</td>
                    <td>{{$tarlac_a_crims->cbrc_id}}</td>
                    <td>{{$tarlac_a_crims->branch}}</td>
                    <td>{{$tarlac_a_crims->program}}</td>
                    <td>{{$tarlac_a_crims->school}}</td>
                    <td>{{$tarlac_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($tarlac_a_civil))
                    @foreach($tarlac_a_civil as $tarlac_a_civils)
                    <tr>
                    <td>{{$tarlac_a_civils->last_name}}, {{$tarlac_a_civils->first_name}} {{$tarlac_a_civils->middle_name}}</td>
                    <td>{{$tarlac_a_civils->cbrc_id}}</td>
                    <td>{{$tarlac_a_civils->branch}}</td>
                    <td>{{$tarlac_a_civils->program}}</td>
                    <td>{{$tarlac_a_civils->school}}</td>
                    <td>{{$tarlac_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($tarlac_a_psyc))
                    @foreach($tarlac_a_psyc as $tarlac_a_psycs)
                    <tr>
                    <td>{{$tarlac_a_psycs->last_name}}, {{$tarlac_a_psycs->first_name}} {{$tarlac_a_psycs->middle_name}}</td>
                    <td>{{$tarlac_a_psycs->cbrc_id}}</td>
                    <td>{{$tarlac_a_psycs->branch}}</td>
                    <td>{{$tarlac_a_psycs->program}}</td>
                    <td>{{$tarlac_a_psycs->school}}</td>
                    <td>{{$tarlac_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($tarlac_a_nclex))
                    @foreach($tarlac_a_nclex as $tarlac_a_nclexs)
                    <tr>
                    <td>{{$tarlac_a_nclexs->last_name}}, {{$tarlac_a_nclexs->first_name}} {{$tarlac_a_nclexs->middle_name}}</td>
                    <td>{{$tarlac_a_nclexs->cbrc_id}}</td>
                    <td>{{$tarlac_a_nclexs->branch}}</td>
                    <td>{{$tarlac_a_nclexs->program}}</td>
                    <td>{{$tarlac_a_nclexs->school}}</td>
                    <td>{{$tarlac_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($tarlac_a_ielt))
                    @foreach($tarlac_a_ielt as $tarlac_a_ielts)
                    <tr>
                    <td>{{$tarlac_a_ielts->last_name}}, {{$tarlac_a_ielts->first_name}} {{$tarlac_a_ielts->middle_name}}</td>
                    <td>{{$tarlac_a_ielts->cbrc_id}}</td>
                    <td>{{$tarlac_a_ielts->branch}}</td>
                    <td>{{$tarlac_a_ielts->program}}</td>
                    <td>{{$tarlac_a_ielts->school}}</td>
                    <td>{{$tarlac_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($tarlac_a_social))
                    @foreach($tarlac_a_social as $tarlac_a_socials)
                    <tr>
                    <td>{{$tarlac_a_socials->last_name}}, {{$tarlac_a_socials->first_name}} {{$tarlac_a_socials->middle_name}}</td>
                    <td>{{$tarlac_a_socials->cbrc_id}}</td>
                    <td>{{$tarlac_a_socials->branch}}</td>
                    <td>{{$tarlac_a_socials->program}}</td>
                    <td>{{$tarlac_a_socials->school}}</td>
                    <td>{{$tarlac_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($tarlac_a_agri))
                    @foreach($tarlac_a_agri as $tarlac_a_agris)
                    <tr>
                    <td>{{$tarlac_a_agris->last_name}}, {{$tarlac_a_agris->first_name}} {{$tarlac_a_agris->middle_name}}</td>
                    <td>{{$tarlac_a_agris->cbrc_id}}</td>
                    <td>{{$tarlac_a_agris->branch}}</td>
                    <td>{{$tarlac_a_agris->program}}</td>
                    <td>{{$tarlac_a_agris->school}}</td>
                    <td>{{$tarlac_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($tarlac_a_mid))
                    @foreach($tarlac_a_mid as $tarlac_a_mids)
                    <tr>
                    <td>{{$tarlac_a_mids->last_name}}, {{$tarlac_a_mids->first_name}} {{$tarlac_a_mids->middle_name}}</td>
                    <td>{{$tarlac_a_mids->cbrc_id}}</td>
                    <td>{{$tarlac_a_mids->branch}}</td>
                    <td>{{$tarlac_a_mids->program}}</td>
                    <td>{{$tarlac_a_mids->school}}</td>
                    <td>{{$tarlac_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($tarlac_a_online))
                    @foreach($tarlac_a_online as $tarlac_a_onlines)
                    <tr>
                    <td>{{$tarlac_a_onlines->last_name}}, {{$tarlac_a_onlines->first_name}} {{$tarlac_a_onlines->middle_name}}</td>
                    <td>{{$tarlac_a_onlines->cbrc_id}}</td>
                    <td>{{$tarlac_a_onlines->branch}}</td>
                    <td>{{$tarlac_a_onlines->program}}</td>
                    <td>{{$tarlac_a_onlines->school}}</td>
                    <td>{{$tarlac_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif




                    @if(isset($vigan_a_let))
                    @foreach($vigan_a_let as $vigan_a_lets)
                    <tr>
                    <td>{{$vigan_a_lets->last_name}}, {{$vigan_a_lets->first_name}} {{$vigan_a_lets->middle_name}}</td>
                    <td>{{$vigan_a_lets->cbrc_id}}</td>
                    <td>{{$vigan_a_lets->branch}}</td>
                    <td>{{$vigan_a_lets->program}}</td>
                    <td>{{$vigan_a_lets->school}}</td>
                    <td>{{$vigan_a_lets->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($vigan_a_nle))
                    @foreach($vigan_a_nle as $vigan_a_nles)
                    <tr>
                    <td>{{$vigan_a_nles->last_name}}, {{$vigan_a_nles->first_name}} {{$vigan_a_nles->middle_name}}</td>
                    <td>{{$vigan_a_nles->cbrc_id}}</td>
                    <td>{{$vigan_a_nles->branch}}</td>
                    <td>{{$vigan_a_nles->program}}</td>
                    <td>{{$vigan_a_nles->school}}</td>
                    <td>{{$vigan_a_nles->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($vigan_a_crim))
                    @foreach($vigan_a_crim as $vigan_a_crims)
                    <tr>
                    <td>{{$vigan_a_crims->last_name}}, {{$vigan_a_crims->first_name}} {{$vigan_a_crims->middle_name}}</td>
                    <td>{{$vigan_a_crims->cbrc_id}}</td>
                    <td>{{$vigan_a_crims->branch}}</td>
                    <td>{{$vigan_a_crims->program}}</td>
                    <td>{{$vigan_a_crims->school}}</td>
                    <td>{{$vigan_a_crims->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    @if(isset($vigan_a_civil))
                    @foreach($vigan_a_civil as $vigan_a_civils)
                    <tr>
                    <td>{{$vigan_a_civils->last_name}}, {{$vigan_a_civils->first_name}} {{$vigan_a_civils->middle_name}}</td>
                    <td>{{$vigan_a_civils->cbrc_id}}</td>
                    <td>{{$vigan_a_civils->branch}}</td>
                    <td>{{$vigan_a_civils->program}}</td>
                    <td>{{$vigan_a_civils->school}}</td>
                    <td>{{$vigan_a_civils->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($vigan_a_psyc))
                    @foreach($vigan_a_psyc as $vigan_a_psycs)
                    <tr>
                    <td>{{$vigan_a_psycs->last_name}}, {{$vigan_a_psycs->first_name}} {{$vigan_a_psycs->middle_name}}</td>
                    <td>{{$vigan_a_psycs->cbrc_id}}</td>
                    <td>{{$vigan_a_psycs->branch}}</td>
                    <td>{{$vigan_a_psycs->program}}</td>
                    <td>{{$vigan_a_psycs->school}}</td>
                    <td>{{$vigan_a_psycs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($vigan_a_nclex))
                    @foreach($vigan_a_nclex as $vigan_a_nclexs)
                    <tr>
                    <td>{{$vigan_a_nclexs->last_name}}, {{$vigan_a_nclexs->first_name}} {{$vigan_a_nclexs->middle_name}}</td>
                    <td>{{$vigan_a_nclexs->cbrc_id}}</td>
                    <td>{{$vigan_a_nclexs->branch}}</td>
                    <td>{{$vigan_a_nclexs->program}}</td>
                    <td>{{$vigan_a_nclexs->school}}</td>
                    <td>{{$vigan_a_nclexs->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($vigan_a_ielt))
                    @foreach($vigan_a_ielt as $vigan_a_ielts)
                    <tr>
                    <td>{{$vigan_a_ielts->last_name}}, {{$vigan_a_ielts->first_name}} {{$vigan_a_ielts->middle_name}}</td>
                    <td>{{$vigan_a_ielts->cbrc_id}}</td>
                    <td>{{$vigan_a_ielts->branch}}</td>
                    <td>{{$vigan_a_ielts->program}}</td>
                    <td>{{$vigan_a_ielts->school}}</td>
                    <td>{{$vigan_a_ielts->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($vigan_a_social))
                    @foreach($vigan_a_social as $vigan_a_socials)
                    <tr>
                    <td>{{$vigan_a_socials->last_name}}, {{$vigan_a_socials->first_name}} {{$vigan_a_socials->middle_name}}</td>
                    <td>{{$vigan_a_socials->cbrc_id}}</td>
                    <td>{{$vigan_a_socials->branch}}</td>
                    <td>{{$vigan_a_socials->program}}</td>
                    <td>{{$vigan_a_socials->school}}</td>
                    <td>{{$vigan_a_socials->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif


                    @if(isset($vigan_a_agri))
                    @foreach($vigan_a_agri as $vigan_a_agris)
                    <tr>
                    <td>{{$vigan_a_agris->last_name}}, {{$vigan_a_agris->first_name}} {{$vigan_a_agris->middle_name}}</td>
                    <td>{{$vigan_a_agris->cbrc_id}}</td>
                    <td>{{$vigan_a_agris->branch}}</td>
                    <td>{{$vigan_a_agris->program}}</td>
                    <td>{{$vigan_a_agris->school}}</td>
                    <td>{{$vigan_a_agris->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($vigan_a_mid))
                    @foreach($vigan_a_mid as $vigan_a_mids)
                    <tr>
                    <td>{{$vigan_a_mids->last_name}}, {{$vigan_a_mids->first_name}} {{$vigan_a_mids->middle_name}}</td>
                    <td>{{$vigan_a_mids->cbrc_id}}</td>
                    <td>{{$vigan_a_mids->branch}}</td>
                    <td>{{$vigan_a_mids->program}}</td>
                    <td>{{$vigan_a_mids->school}}</td>
                    <td>{{$vigan_a_mids->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif

                    @if(isset($vigan_a_online))
                    @foreach($vigan_a_online as $vigan_a_onlines)
                    <tr>
                    <td>{{$vigan_a_onlines->last_name}}, {{$vigan_a_onlines->first_name}} {{$vigan_a_onlines->middle_name}}</td>
                    <td>{{$vigan_a_onlines->cbrc_id}}</td>
                    <td>{{$vigan_a_onlines->branch}}</td>
                    <td>{{$vigan_a_onlines->program}}</td>
                    <td>{{$vigan_a_onlines->school}}</td>
                    <td>{{$vigan_a_onlines->contact_no}}</td>
                    </tr>
                    @endforeach
                    @endif



                    </tbody>           
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
});

</script>
@stop