@extends('main')
@section('title')
BULLETIN BOARD
@stop
@section('css')
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css"/>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

 
<style type="text/css">
.fc-center h2{
  font-size: 20px;

}
.btn-group{
  margin-top: -30px;
}
.fc-scroller {
   overflow-y: hidden !important;
}
.owl-stage{
        display: flex;
}
.fc-day-header{
  background-color:#512f50;
  color:#fff;
}


#wrapper-carousel {
            position: relative;
            width: 100%;
            height: 200px;
            margin: 0 auto;
            overflow: hidden;
            display:flex;
        }
        #main-carousel {

            position: relative;
            width: 100%;
            height: 200px;
            overflow:hidden;
        }
         #container-carousel {

            position: absolute;
            width: 100%;
            height: 200px;
        }
        .child {
            
            width: 170px;
            height: 200px;
            padding-top: 35px;
            float: left;
            text-align: center;
            font-size: 16px;
        }
      
</style>
@stop

@section('main-content')
   
<main class="main-wrapper clearfix">
     <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <p style="margin-left: 10px;font-size: 18px;font-weight: bold;">Novaliches</p>                      
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Notification</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
    <div class="container">
      <div class="row">
        <div class="col-md-7" style="padding-right: 0px;">
           <div class="card shadow-sm p-3 mx-sm-1 p-3 mt-10" style="margin-top: 10px;margin-bottom: 0px;">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100 img-responsive" src="../img/large.png" alt="First slide" style="max-height: 445px; max-width: 100%;">
                    </div>
                    <div class="carouselCbrc" id="carouselCbrc">
                      
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
          </div>
        </div>
         <div class="col-md-5" style="padding-left: 0px;">
           <div class="card shadow-sm p-3 mx-sm-1 p-3 mt-10" style="margin-top: 10px;">
                <div id='calendar'></div>
            </div>
        </div>
      </div>
    </div> <!-- end of container -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
           <div class="card shadow-sm p-3  mx-sm-1 p-3" style="margin-top: 10px; margin-bottom: 10px;">
                <div class="col-md-12"> 
                  <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;"><i class="fas fa-user-circle"></i> Birthday of the Month</p>
                <hr>
                <div id="wrapper-carousel">
                  <div id="main-carousel">
                      <div id="container-carousel">
                         
                    </div>
                  </div>
              </div>
          </div>
        </div>       
      </div>
    </div>
  </div> <!-- end of container -->
   <div class="container">
      <div class="row">
        <div class="col-md-12">
           <div class="card shadow-sm p-3 mx-sm-1 p-3">
               <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;"><i class="fa fa-calendar-check"></i> Task and Priorities <span style="float: right;margin-right: 30px;"><a href="taskHistory"> History</a></span></p>
            <hr>
              <table class="table" id="tasktable">
                <thead>
                  <tr>
                    <th width="25%" scope="col">title</th>
                    <th width ="35%" scope="col">Description</th>
                    <th width="15%" scope="col">Date Posted</th>
                    <th scope="col">Deadline</th>
                    <th scope="col">Status</th>
                  </tr>
                </thead>
                <tbody id="task">
                </tbody>
            </table> 
        </div>       
      </div>
    </div>
  </div> <!-- end of container -->
 <div class="container">
      <div class="row">
        <div class="col-md-12">
           <div class="card shadow-sm p-3 mx-sm-1 p-3" style="margin-top: 10px;">
             <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;"><i class="fas fa-poll"></i> Poll Event</p>
               <hr>
               <div class="row">
                      <div class="col-md-4">
                           <div class="form-group">
                               <p style="margin-bottom: 5px;">Choose Poll Event</p>
                                <select class="form-control" name="poll" type="text" id="poll" required>                                   
                                </select> 
                                   <input type="hidden" name="eventIds" value="" id="eventIds">
                             </div>
                        <div id="choices">
                          <h6 id="title"></h6>
                          <p id="Question" style="margin-bottom: 5px;"></p>
                          <div id="category"></p>
                          </div>
                        </div>            
                      </div>
                  <div class="col-lg-8" style="margin-bottom: 10px;">
                    <div id="ewan">
                <canvas id="myChart"></canvas>
                  </div> <!-- end col -->  
               </div>         
          </div>       
        </div>
      </div>
  </div>
</div> <!-- end of container -->
  <!--================= Calendar modal =========================-->

<div id="calendarModal" class="modal fade">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
           <div class="container" style="font-family: Verdana,Arial,sans-serif;color:black;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <p id="modalTitle" class="modal-title" style="font-size: 30px;font-weight: bold;margin-top: 5px;"></p>
                <hr>    
              <p style="font-size: 20px; margin-bottom: 0px;">Start : <span id="start"></span></p>
              <p style="font-size: 20px;">End : <span id="end"></span></p>
              <p id="modalDescription" style="font-size: 17px;"></p>
            </div>
        </div>
    </div>
</div>
</main>

@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>
    <script src="{{ asset('/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('/js/data-table.js') }}"></script>


<script >//Birthday Carousel api
var firstval = 0;
var runSlider;


function CarouselBday() {
clearTimeout(runSlider);
    firstval += 2;
    parent = document.getElementById('container-carousel');
    parent.style.left = "-" + firstval + "px";
    if (!(firstval % 130)) {
        setTimeout(CarouselBday, 3000);
        firstval = 0;
        var firstChild = parent.firstElementChild;
         parent.appendChild(firstChild);
        parent.style.left= 0;
        return;
    }
    runCarousel = setTimeout( CarouselBday, 10);
}
CarouselBday();


function leftClickBday(){
firstval += 2;
    parent = document.getElementById('container-carousel');
    parent.style.left = "-" + firstval + "px";
    
    if (!(firstval % 130)) {
        
        firstval = 0;
        var firstChild = parent.firstElementChild;
        parent.appendChild(firstChild);
        parent.style.left= 0;
        return;
    }
    runSlider = setTimeout(leftClickBday, 10);
}

function rightClickBday(){
firstval += 2;
    parent = document.getElementById('container-carousel');
    parent.style.left =  firstval + "px";
    
    if (!(firstval % 130)) {
        
        firstval = 0;
        var firstChild = parent.firstElementChild;
        parent.appendChild(firstChild);
        parent.style.left= 0;
        return;
    }
    runSlider = setTimeout(rightClickBday, 10);
}


  
        $(document).on('ready', function () {

            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3
            });
        });
        
</script>

<script>
  $(window).on('load',function(){
  var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
  $.ajax({
     url: 'https://beta.cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = JSON.stringify(response);
  var apiTokenParse = JSON.parse(apiToString);
  var apiToken = apiTokenParse.access_token;
  console.log(apiToken);  

  // ================ bannerads diplay through API Access ==========================
     $.get('https://cbrc.solutions/api/main/bannerads?token='+apiToken,function(data){
              $.each(data, function (index, ObjCarousel) {
                 $('#carouselCbrc').append('<div class="carousel-item"><img class="d-block w-100 img-responsive" src="https://cbrc.solutions/upload/1280x1024-q2kjur%20(1).png_1552615496.webp" style="max-height: 445px; max-width: 100%;"></div>');
          });   
      });


 // ================= calendar event display Using API ======================
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next',
      center: 'title',
      right: 'month,agendaDay,list',
    },
    defaultDate: new Date(),
    editable: true,
    contentHeight: 390,
    droppable: true,
    eventLimit: false,
    eventColor: '#C83530',
    events: {
      url: "https://cbrc.solutions/api/main/announcements?token="+apiToken,
      textColor: 'white',
    },
    eventClick:  function(event, jsEvent, view) {
        $('#modalTitle').html(event.title);
        $('#start').html(moment(event.start).format('MMM Do h:mm A'));
        $('#end').html(moment(event.end).format('MMM Do h:mm A'));
        $('#modalDescription').html(event.description);
        $('#calendarModal').modal();

      }
  });

  $('.close').on('click',function(){
       $("#calendarModal").modal("hide"); 
  });

// ================  get Birthday of employee Today using API ===============================
  $.get('https://cbrc.solutions/api/main/employee/bday?token='+apiToken,function(data){
    $.each(data.data,function(index, Objbdy){
  $('#container-carousel').append(' <div class="child"><img src="'+Objbdy.Image+'" style="max-width:100%; height:100px;" class="rounded-circle"><p style="font-size:15px;margin-bottom:0px;">'+Objbdy.Firstname+' '+Objbdy.Lastname+'</p><p style="margin-top:-10px;margin-bottom:0px;">'+Objbdy.Branch_Name+'</p><p style="margin-top:-10px;">'+Objbdy.Birthday+'</p> </div>');
                   
    });    
});

// ================== task event to specific Branch ===================================
 $.get("https://cbrc.solutions/api/main/tasks/Novaliches?token="+apiToken,function(data){
     console.log(data);
        var today = new Date();
        var dd = String(today.getDate());
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        var x = 0;
        if(dd<10) {
            dd = '0'+dd
        } 
        if(mm<10) {
            mm = '0'+mm
        } 
        today = yyyy + '-' + mm + '-' + dd; 
$.each(data, function(index, objtask){
    
   // console.log(objtask.id)
    
  $('#task').append('<tr id="task'+ data.id+x+'"> </tr>');
    x++;
});
x=0;  
    $.each(data, function(index, objtask){
            if(today > objtask.end_date){
                $('#task'+data.id+x).append('<td>'+ objtask.title +'</td><td>'+ objtask.description +'</td><td>'+ objtask.date_start+'</td><td>'+objtask.end_date+'</td><td><button class="btn btn-success taskStatus" task_id="'+objtask.id +'" style="border-radius: 0px; "  task_status= "'+objtask.status+'">Done</button></td>');
                $('#tasktable').DataTable({
                   "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]]
                });
                  $('#task'+data.id+x).css('background-color','#D94F41');
                  $('#task'+data.id+x).css('color','white'); 
            } else{
                $('#task'+data.id+x).append('<td>'+ objtask.title +'</td><td>'+ objtask.description +'</td><td>'+ objtask.date_start+'</td><td>'+objtask.end_date+'</td><td><button class="btn btn-success taskStatus" task_id="'+objtask.id +'" style="border-radius: 0px; "  task_status= "'+objtask.status+'">Done</button></td>');
                $('#tasktable').DataTable({
                   "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]]
                });
              } 
           x++;
    });
 }).done(function(response){
// ===================== update Status of Task Event  ===========================
    $('#task').on('click', '.taskStatus', function(){
       var task_id = $(this).attr('task_id');
       var task_status = $(this).attr('task_status');
       var csrf_token = $('meta[name="csrf-token"]').attr('content');

       var formdata = new FormData();
           formdata.append('_token',csrf_token);
           formdata.append('Task_id',task_id);

       $.ajax({
        type: 'POST',
        url:  "https://cbrc.solutions/api/main/doneTask?token=" + apiToken,
        data:formdata,
        processData:false,
        contentType: false,
         success:function(){
          swal({
                  title: 'Success!',
                  text: data.message,
                  type: 'success',
                  timer: '3000'
              });
              location.reload();
              }, error: function(msg) {
              swal("Error!", "Something went wrong.", "error");
              alert(msg.status);
        }
       });
    });
 });

//================ onload poll Event Vote Display API ====================================

 $.get('https://cbrc.solutions/api/main/getpollevent?token=' +apiToken,function(data){
        $.each(data,function(index, Objpoll){
           $('#poll').append('<option value="'+Objpoll.event_id+'">'+Objpoll.event_title+'</option>');

    }); 
  }).done(function(response){
    var events = $('#poll').val();
    $('#eventIds').val(events);
      var eid = $('#eventIds').val();
      var isVoted = 0;
      $.get('https://cbrc.solutions/api/main/chkpoll/'+eid+'/Novaliches?token='+apiToken,function(data){
        isVoted = data;
      }).done(function(response) {
        $.get('https://cbrc.solutions/api/main/GetEventQ/'+eid+'?token=' +apiToken,function(data){
        $.each(data,function(view, ObjAns){
          $('#category').append('<input type="radio" class="pollAns" name="pollAns" value="'+ObjAns.aid+'"> '+ObjAns.Answer+'<br>' );
        });       
        if(isVoted == 1) {
          $('#category').append('<button  class="voteBtn  btn btn-warning" id="'+eid+'" hidden>Submit</button>');


        } else {
          $('#category').append('<button  class="voteBtn btn btn-warning" id="'+eid+'">Submit</button>');
        }
        
        });
      });
  });  

var ctx = document.getElementById('myChart').getContext('2d');
var ans = [];
var total = [];
var vote = [];
var title = [];
$.getJSON('https://cbrc.solutions/api/main/displayVote?token='+apiToken,function(data){
  $.each(data,function(view, ObjResult){
$('#title').text(ObjResult.event_title);
$('#Question').text(ObjResult.Question);
ans.push(ObjResult.Answer);
title.push(ObjResult.event_title);
total.push(ObjResult.total);

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels:ans,
        datasets: [{
            label: '# of Votes',
            data: total,
            backgroundColor: [
              'rgba(225,99,132,0.6)',
               'rgba(54,162,235,0.6)',
               'rgba(225,206,86,0.6)',
               'rgba(75,192,92,0.6)',
               'rgba(153,102,255,0.6)',
               'rgba(225,159,64,0.6)',
               'rgba(225,99,132,0.6)'

            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
              display: true,
              text: ObjResult.event_title,
              position: 'top',
              fontSize:20
              },
               legend: {
            display: false
         },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
      });      
  });
});

  }); //========end of response of apiToken =================
}); // ===== end of window on load =============

// ============ display Category and question of poll Event using OnchangeEvent ================

 var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
  $.ajax({
     url: 'https://cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = JSON.stringify(response);
  var apiTokenParse = JSON.parse(apiToString);
  var apiToken = apiTokenParse.access_token;
$('#poll').on('change',function(e){
var id = e.target.value;
$('#eventIds').val(id);
var isVoted = 0;
$('#ewan').empty();
$('#category').empty();
$('#title').empty();
$('#Question').empty();
$('#ewan').append('<canvas id="barChart"></canvas>');
var canvas = document.getElementById("barChart");

var ctx = canvas.getContext('2d');
var ans = [];
var total = [];
var vote = [];
var title = [];
// Global Options:
 Chart.defaults.global.defaultFontColor = 'black';
 Chart.defaults.global.defaultFontSize = 12;
$.get('https://cbrc.solutions/api/main/chkpoll/'+id+'/Novaliches?token='+apiToken,function(data){
      isVoted = data;
      }).done(function(response) {
$.get('https://cbrc.solutions/api/main/displaySpecVote/'+id+'?token='+apiToken,function(data){
      $.each(data,function(view, ObjChkvote){
     $('#title').text(ObjChkvote.event_title);
$('#Question').text(ObjChkvote.Question);
 $('#category').append('<input type="radio" class = "pollAns" name="pollAns" value="'+ObjChkvote.aid+'"> '+ObjChkvote.Answer+'<br>' );


ans.push(ObjChkvote.Answer);
title.push(ObjChkvote.event_title);
total.push(ObjChkvote.total);

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels:ans,
        datasets: [{
            label: '# of Votes',
            data: total,
            backgroundColor: [
              'rgba(225,99,132,0.6)',
               'rgba(54,162,235,0.6)',
               'rgba(225,206,86,0.6)',
               'rgba(75,192,92,0.6)',
               'rgba(153,102,255,0.6)',
               'rgba(225,159,64,0.6)',
               'rgba(225,99,132,0.6)'

            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
              display: true,
              text: ObjChkvote.event_title,
              position: 'top',
              fontSize:20
              },
               legend: {
            display: false
         },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
      }); 
    });
    
     if(isVoted == 1) {
          $('#category').append('<button  class="voteBtn btn btn-warning" id="'+id+'" hidden>Submit</button>');
        } else {
          $('#category').append('<button  class="voteBtn btn btn-warning" id="'+id+'">Submit</button>');
        }
  });
});
}); //end of onChange

// ================ Click button to save Votes =========================

 $('#category').on('click', '.voteBtn', function(e){
  var eventTitle = $('input[name=eventIds]').val();
  
  var formSelected = {
    "Branch_Name":"Novaliches",
    "Answer": $("input[name='pollAns']:checked").val(),
    "event_id": eventTitle
    
  };
  $.ajax({
    url:"https://cbrc.solutions/api/main/saveVote?token="+apiToken,
    method:"POST",
     data:formSelected,
      success:function(){
          swal({
                  title: 'Success!',
                  text: data.message,
                  type: 'success',
                  timer: '3000'
              });
              location.reload();
              }, error: function(msg) {
              swal("Error!", "Something went wrong.", "error");
              alert(msg.status);
        }
    });
  });
});

</script>
@stop