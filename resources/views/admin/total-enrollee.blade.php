@extends('main')
@section('title')
 Total {{$program}} Enrollee
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total {{$program}} Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total {{$program}} Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="total-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Branch</th>
                            <th>Contact #</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>School</th>
                            <th>Birthdate</th>                           
                            <th>Program</th>
                            <th>Section</th>
                            <th>Major</th>
                            <th>Take</th>
                            <th>NOA #</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Emer. Contact</th>
                            <th>Emer. #</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                            @if(isset($abra_a_prog))
                            @foreach($abra_a_prog as $abra_a_progs)
                                <tr>
                                    <td>{{$abra_a_progs->last_name}}, {{$abra_a_progs->first_name}} {{$abra_a_progs->middle_name}}</td>
                                    <td>{{$abra_a_progs->cbrc_id}}</td>
                                    <td>{{$abra_a_progs->branch}}</td>
                                    <td>{{$abra_a_progs->contact_no}}</td>
                                    <td>{{$abra_a_progs->email}}</td>
                                    <td>{{$abra_a_progs->address}}</td>
                                    <td>{{$abra_a_progs->school}}</td>
                                    <td>{{$abra_a_progs->birthdate}}</td>
                                    <td>{{$abra_a_progs->course}}</td>
                                    <td>{{$abra_a_progs->section}}</td>
                                    <td>{{$abra_a_progs->major}}</td>
                                    <td>{{$abra_a_progs->take}}</td>
                                    <td>{{$abra_a_progs->noa_no}}</td>                            
                                    <td>{{$abra_a_progs->username}}</td>
                                    <td>{{$abra_a_progs->password}}</td>
                                    <td>{{$abra_a_progs->contact_person}}</td>
                                    <td>{{$abra_a_progs->contact_details}}</td>
                                </tr>
                            @endforeach
                        @endif


                        @if(isset($baguio_a_prog))
                        @foreach($baguio_a_prog as $baguio_a_progs)
                            <tr>
                                <td>{{$baguio_a_progs->last_name}}, {{$baguio_a_progs->first_name}} {{$baguio_a_progs->middle_name}}</td>
                                <td>{{$baguio_a_progs->cbrc_id}}</td>
                                <td>{{$baguio_a_progs->branch}}</td>
                                <td>{{$baguio_a_progs->contact_no}}</td>
                                <td>{{$baguio_a_progs->email}}</td>
                                <td>{{$baguio_a_progs->address}}</td>
                                <td>{{$baguio_a_progs->school}}</td>
                                <td>{{$baguio_a_progs->birthdate}}</td>
                                <td>{{$baguio_a_progs->course}}</td>
                                <td>{{$baguio_a_progs->section}}</td>
                                <td>{{$baguio_a_progs->major}}</td>
                                <td>{{$baguio_a_progs->take}}</td>
                                <td>{{$baguio_a_progs->noa_no}}</td>                            
                                <td>{{$baguio_a_progs->username}}</td>
                                <td>{{$baguio_a_progs->password}}</td>
                                <td>{{$baguio_a_progs->contact_person}}</td>
                                <td>{{$baguio_a_progs->contact_details}}</td>
                            </tr>
                        @endforeach
                    @endif


                    @if(isset($calapan_a_prog))
                    @foreach($calapan_a_prog as $calapan_a_progs)
                        <tr>
                            <td>{{$calapan_a_progs->last_name}}, {{$calapan_a_progs->first_name}} {{$calapan_a_progs->middle_name}}</td>
                            <td>{{$calapan_a_progs->cbrc_id}}</td>
                            <td>{{$calapan_a_progs->branch}}</td>
                            <td>{{$calapan_a_progs->contact_no}}</td>
                            <td>{{$calapan_a_progs->email}}</td>
                            <td>{{$calapan_a_progs->address}}</td>
                            <td>{{$calapan_a_progs->school}}</td>
                            <td>{{$calapan_a_progs->birthdate}}</td>
                            <td>{{$calapan_a_progs->course}}</td>
                            <td>{{$calapan_a_progs->section}}</td>
                            <td>{{$calapan_a_progs->major}}</td>
                            <td>{{$calapan_a_progs->take}}</td>
                            <td>{{$calapan_a_progs->noa_no}}</td>                            
                            <td>{{$calapan_a_progs->username}}</td>
                            <td>{{$calapan_a_progs->password}}</td>
                            <td>{{$calapan_a_progs->contact_person}}</td>
                            <td>{{$calapan_a_progs->contact_details}}</td>
                        </tr>
                    @endforeach
                @endif


                @if(isset($candon_a_prog))
                @foreach($candon_a_prog as $candon_a_progs)
                    <tr>
                        <td>{{$candon_a_progs->last_name}}, {{$candon_a_progs->first_name}} {{$candon_a_progs->middle_name}}</td>
                        <td>{{$candon_a_progs->cbrc_id}}</td>
                        <td>{{$candon_a_progs->branch}}</td>
                        <td>{{$candon_a_progs->contact_no}}</td>
                        <td>{{$candon_a_progs->email}}</td>
                        <td>{{$candon_a_progs->address}}</td>
                        <td>{{$candon_a_progs->school}}</td>
                        <td>{{$candon_a_progs->birthdate}}</td>
                        <td>{{$candon_a_progs->course}}</td>
                        <td>{{$candon_a_progs->section}}</td>
                        <td>{{$candon_a_progs->major}}</td>
                        <td>{{$candon_a_progs->take}}</td>
                        <td>{{$candon_a_progs->noa_no}}</td>                            
                        <td>{{$candon_a_progs->username}}</td>
                        <td>{{$candon_a_progs->password}}</td>
                        <td>{{$candon_a_progs->contact_person}}</td>
                        <td>{{$candon_a_progs->contact_details}}</td>
                    </tr>
                @endforeach
            @endif


            @if(isset($fairview_a_prog))
            @foreach($fairview_a_prog as $fairview_a_progs)
                <tr>
                    <td>{{$fairview_a_progs->last_name}}, {{$fairview_a_progs->first_name}} {{$fairview_a_progs->middle_name}}</td>
                    <td>{{$fairview_a_progs->cbrc_id}}</td>
                    <td>{{$fairview_a_progs->branch}}</td>
                    <td>{{$fairview_a_progs->contact_no}}</td>
                    <td>{{$fairview_a_progs->email}}</td>
                    <td>{{$fairview_a_progs->address}}</td>
                    <td>{{$fairview_a_progs->school}}</td>
                    <td>{{$fairview_a_progs->birthdate}}</td>
                    <td>{{$fairview_a_progs->course}}</td>
                    <td>{{$fairview_a_progs->section}}</td>
                    <td>{{$fairview_a_progs->major}}</td>
                    <td>{{$fairview_a_progs->take}}</td>
                    <td>{{$fairview_a_progs->noa_no}}</td>                            
                    <td>{{$fairview_a_progs->username}}</td>
                    <td>{{$fairview_a_progs->password}}</td>
                    <td>{{$fairview_a_progs->contact_person}}</td>
                    <td>{{$fairview_a_progs->contact_details}}</td>
                </tr>
            @endforeach
        @endif

        
        @if(isset($las_pinas_a_prog))
        @foreach($las_pinas_a_prog as $las_pinas_a_progs)
            <tr>
                <td>{{$las_pinas_a_progs->last_name}}, {{$las_pinas_a_progs->first_name}} {{$las_pinas_a_progs->middle_name}}</td>
                <td>{{$las_pinas_a_progs->cbrc_id}}</td>
                <td>{{$las_pinas_a_progs->branch}}</td>
                <td>{{$las_pinas_a_progs->contact_no}}</td>
                <td>{{$las_pinas_a_progs->email}}</td>
                <td>{{$las_pinas_a_progs->address}}</td>
                <td>{{$las_pinas_a_progs->school}}</td>
                <td>{{$las_pinas_a_progs->birthdate}}</td>
                <td>{{$las_pinas_a_progs->course}}</td>
                <td>{{$las_pinas_a_progs->section}}</td>
                <td>{{$las_pinas_a_progs->major}}</td>
                <td>{{$las_pinas_a_progs->take}}</td>
                <td>{{$las_pinas_a_progs->noa_no}}</td>                            
                <td>{{$las_pinas_a_progs->username}}</td>
                <td>{{$las_pinas_a_progs->password}}</td>
                <td>{{$las_pinas_a_progs->contact_person}}</td>
                <td>{{$las_pinas_a_progs->contact_details}}</td>
            </tr>
        @endforeach
    @endif

    
                        @if(isset($launion_a_prog))
                        @foreach($launion_a_prog as $launion_a_progs)
                            <tr>
                                <td>{{$launion_a_progs->last_name}}, {{$launion_a_progs->first_name}} {{$launion_a_progs->middle_name}}</td>
                                <td>{{$launion_a_progs->cbrc_id}}</td>
                                <td>{{$launion_a_progs->branch}}</td>
                                <td>{{$launion_a_progs->contact_no}}</td>
                                <td>{{$launion_a_progs->email}}</td>
                                <td>{{$launion_a_progs->address}}</td>
                                <td>{{$launion_a_progs->school}}</td>
                                <td>{{$launion_a_progs->birthdate}}</td>
                                <td>{{$launion_a_progs->course}}</td>
                                <td>{{$launion_a_progs->section}}</td>
                                <td>{{$launion_a_progs->major}}</td>
                                <td>{{$launion_a_progs->take}}</td>
                                <td>{{$launion_a_progs->noa_no}}</td>                            
                                <td>{{$launion_a_progs->username}}</td>
                                <td>{{$launion_a_progs->password}}</td>
                                <td>{{$launion_a_progs->contact_person}}</td>
                                <td>{{$launion_a_progs->contact_details}}</td>
                            </tr>
                        @endforeach
                    @endif

                    @if(isset($manila_a_prog))
                    @foreach($manila_a_prog as $manila_a_progs)
                        <tr>
                            <td>{{$manila_a_progs->last_name}}, {{$manila_a_progs->first_name}} {{$manila_a_progs->middle_name}}</td>
                            <td>{{$manila_a_progs->cbrc_id}}</td>
                            <td>{{$manila_a_progs->branch}}</td>
                            <td>{{$manila_a_progs->contact_no}}</td>
                            <td>{{$manila_a_progs->email}}</td>
                            <td>{{$manila_a_progs->address}}</td>
                            <td>{{$manila_a_progs->school}}</td>
                            <td>{{$manila_a_progs->birthdate}}</td>
                            <td>{{$manila_a_progs->course}}</td>
                            <td>{{$manila_a_progs->section}}</td>
                            <td>{{$manila_a_progs->major}}</td>
                            <td>{{$manila_a_progs->take}}</td>
                            <td>{{$manila_a_progs->noa_no}}</td>                            
                            <td>{{$manila_a_progs->username}}</td>
                            <td>{{$manila_a_progs->password}}</td>
                            <td>{{$manila_a_progs->contact_person}}</td>
                            <td>{{$manila_a_progs->contact_details}}</td>
                        </tr>
                    @endforeach
                @endif
                
            @if(isset($roxas_a_prog))
            @foreach($roxas_a_prog as $roxas_a_progs)
                <tr>
                    <td>{{$roxas_a_progs->last_name}}, {{$roxas_a_progs->first_name}} {{$roxas_a_progs->middle_name}}</td>
                    <td>{{$roxas_a_progs->cbrc_id}}</td>
                    <td>{{$roxas_a_progs->branch}}</td>
                    <td>{{$roxas_a_progs->contact_no}}</td>
                    <td>{{$roxas_a_progs->email}}</td>
                    <td>{{$roxas_a_progs->address}}</td>
                    <td>{{$roxas_a_progs->school}}</td>
                    <td>{{$roxas_a_progs->birthdate}}</td>
                    <td>{{$roxas_a_progs->course}}</td>
                    <td>{{$roxas_a_progs->section}}</td>
                    <td>{{$roxas_a_progs->major}}</td>
                    <td>{{$roxas_a_progs->take}}</td>
                    <td>{{$roxas_a_progs->noa_no}}</td>                            
                    <td>{{$roxas_a_progs->username}}</td>
                    <td>{{$roxas_a_progs->password}}</td>
                    <td>{{$roxas_a_progs->contact_person}}</td>
                    <td>{{$roxas_a_progs->contact_details}}</td>
                </tr>
            @endforeach
        @endif
                        
                        @if(isset($tarlac_a_prog))
                        @foreach($tarlac_a_prog as $tarlac_a_progs)
                            <tr>
                                <td>{{$tarlac_a_progs->last_name}}, {{$tarlac_a_progs->first_name}} {{$tarlac_a_progs->middle_name}}</td>
                                <td>{{$tarlac_a_progs->cbrc_id}}</td>
                                <td>{{$tarlac_a_progs->branch}}</td>
                                <td>{{$tarlac_a_progs->contact_no}}</td>
                                <td>{{$tarlac_a_progs->email}}</td>
                                <td>{{$tarlac_a_progs->address}}</td>
                                <td>{{$tarlac_a_progs->school}}</td>
                                <td>{{$tarlac_a_progs->birthdate}}</td>
                                <td>{{$tarlac_a_progs->course}}</td>
                                <td>{{$tarlac_a_progs->section}}</td>
                                <td>{{$tarlac_a_progs->major}}</td>
                                <td>{{$tarlac_a_progs->take}}</td>
                                <td>{{$tarlac_a_progs->noa_no}}</td>                            
                                <td>{{$tarlac_a_progs->username}}</td>
                                <td>{{$tarlac_a_progs->password}}</td>
                                <td>{{$tarlac_a_progs->contact_person}}</td>
                                <td>{{$tarlac_a_progs->contact_details}}</td>
                            </tr>
                        @endforeach
                    @endif
                        
                    
            @if(isset($vigan_a_prog))
            @foreach($vigan_a_prog as $vigan_a_progs)
                <tr>
                    <td>{{$vigan_a_progs->last_name}}, {{$vigan_a_progs->first_name}} {{$vigan_a_progs->middle_name}}</td>
                    <td>{{$vigan_a_progs->cbrc_id}}</td>
                    <td>{{$vigan_a_progs->branch}}</td>
                    <td>{{$vigan_a_progs->contact_no}}</td>
                    <td>{{$vigan_a_progs->email}}</td>
                    <td>{{$vigan_a_progs->address}}</td>
                    <td>{{$vigan_a_progs->school}}</td>
                    <td>{{$vigan_a_progs->birthdate}}</td>
                    <td>{{$vigan_a_progs->course}}</td>
                    <td>{{$vigan_a_progs->section}}</td>
                    <td>{{$vigan_a_progs->major}}</td>
                    <td>{{$vigan_a_progs->take}}</td>
                    <td>{{$vigan_a_progs->noa_no}}</td>                            
                    <td>{{$vigan_a_progs->username}}</td>
                    <td>{{$vigan_a_progs->password}}</td>
                    <td>{{$vigan_a_progs->contact_person}}</td>
                    <td>{{$vigan_a_progs->contact_details}}</td>
                </tr>
            @endforeach
        @endif
                    </tbody>           
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
  $('.ad-enrollee').addClass('active');
  $('.ad-enrollee').addClass('collapse in');
});

</script>
@stop