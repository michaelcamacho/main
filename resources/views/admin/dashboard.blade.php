@extends('main')
@section('title')
DASHBOARD
@stop
@section('main-content')

@if ($flash = session('log'))
<input type="hidden" value="{{$flash}}" id="logstat" />
<input type="hidden" value="" id="ip" />
<input type="hidden" value="" id="branch" />
<input type="hidden" value="" id="user" />
<input type="hidden" value="" id="user_id" />

@endif
<main class="main-wrapper clearfix">
        <!-- Page Title Area -->
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Summary</h6>
                    {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html">Dashboard</a>
                        </li>
                        {{--<li class="breadcrumb-item active">Home</li>--}}
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
        </div>
        <!-- /.container-fluid -->
        <!-- =================================== -->
        <!-- Different data widgets ============ -->
        <!-- =================================== -->



        <div class="container-fluid">
            <div class="widget-list row">
                <div class="widget-holder widget-full-height widget-flex col-lg-8">
                    
                </div>

                <div class="widget-holder col-lg-4">

                </div>
                <!-- /.widget-holder -->
                <div class="widget-holder widget-sm col-md-4 widget-full-height">
                    <div class="widget-bg bg-primary text-inverse">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Total Sales for Season 1</p>
                                    <span class="counter-title-a d-block">
                                        Php 
                                    <span class="counter">
                                    {{$total_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{ url('/total-program')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <!-- /.widget-holder -->
                <div class="widget-holder widget-sm col-md-4 widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #85d1f2">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Total Sales for Season 2</p>
                                    <span class="counter-title-a d-block">
                                        Php
                                    <span class="counter">
                                    {{$total_s2 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{ url('/total-program')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View List</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <div class="widget-holder widget-sm col-md-4 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Facilitation Balance</p>
                                        <span class="counter-title-a d-block">
                                            Php
                                        <span class="counter">
                                        {{$facilitation or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{ url('/total-program')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View List</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                 <div class="widget-holder widget-sm col-md-3 widget-full-height">
                    <div class="widget-bg text-inverse bg-primary">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Total Enrollees</p>
                                    <span class="counter-title-a d-block">
                                        
                                        <span class="counter">
                                        {{$total_enrollee or 0}}
                                        </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Records</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                 <div class="widget-holder widget-sm col-md-3 widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6bd8a1">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Total Expenses</p>
                                    <span class="counter-title-a d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('total-expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col-md-3 widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Total Receivables</p>
                                    <span class="counter-title-a d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_receivable or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col-md-3 widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Total Discounts</p>
                                    <span class="counter-title-a d-block">
                                    Php 
                                    <span class="counter">
                                    {{$discount or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                
             
              
                <!-- /.widget-holder -->
            </div>
            <!-- /.widget-list -->
        </div>


        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Branch Sales Season 1</h6>
                    {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="widget-list row">
                <div class="widget-holder widget-full-height widget-flex col-lg-8">
                    
                </div>

                <div class="widget-holder col-lg-4">

                </div>

                 <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg bg-primary text-inverse">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Abra Sales</p>
                                    <span class="counter-title d-block">
                                        Php 
                                    <span class="counter">
                                    {{$total_abra_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('abra/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <!-- /.widget-holder -->
               

                 <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Baguio Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_baguio_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('baguio/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Calapan Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_calapan_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('calapan/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Candon Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                        <span class="counter">
                                        {{$total_candon_s1 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('candon/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Dagupan Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                            <span class="counter">
                                            {{$total_dagupan_s1 or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <a href="{{url('dagupan/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Fairview Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_fairview_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('fairview/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Las Pinas Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_las_pinas_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('las_pinas/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Launion Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_launion_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('launion/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Manila Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_manila_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('manila/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Roxas Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_roxas_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('roxas/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Tarlac Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_tarlac_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('tarlac/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Vigan Sales</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$total_vigan_s1 or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('vigan/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>


            </div>
            <!-- /.widget-list -->
        </div>

        

        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Branch Sales Season 2</h6>
                    {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                </div>
            </div>
        </div>
        
        <div class="container-fluid">
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                    </div>
    
                    <div class="widget-holder col-lg-4">
    
                    </div>
    
                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Abra Sales</p>
                                        <span class="counter-title d-block">
                                            Php 
                                        <span class="counter">
                                        {{$total_abra_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('abra/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                   
    
                     <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Baguio Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_baguio_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('baguio/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Calapan Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_calapan_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('calapan/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                            <div class="widget-bg text-inverse" style="background: #85d1f2">
                                <div class="widget-body">
                                    <div class="counter-w-info media">
                                        <div class="media-body w-50">
                                            <p class="text-muted mr-b-5 fw-600">Candon Sales</p>
                                            <span class="counter-title d-block">
                                                Php 
                                            <span class="counter">
                                            {{$total_candon_s2 or 0}}
                                            </span></span>
                                            <!-- /.counter-title --> <a href="{{url('candon/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                        </div>
                                        <!-- /.media-body -->
                                        <div class="pull-right align-self-center">
                                            <div class="mr-t-20">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.counter-w-info -->
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
        
                         <div class="widget-holder widget-sm col widget-full-height">
                            <div class="widget-bg text-inverse" style="background: #6bd8a1">
                                <div class="widget-body">
                                    <div class="counter-w-info media">
                                        <div class="media-body w-50">
                                            <p class="text-muted mr-b-5 fw-600">Dagupan Sales</p>
                                            <span class="counter-title d-block">
                                                Php 
                                                <span class="counter">
                                                {{$total_dagupan_s2 or 0}}
                                                </span></span>
                                            <!-- /.counter-title --> <a href="{{url('dagupan/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                        </div>
                                        <!-- /.media-body -->
                                        <div class="pull-right align-self-center">
                                            <div class="mr-t-20">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.counter-w-info -->
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Fairview Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_fairview_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('fairview/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Las Pinas Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_las_pinas_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('las_pinas/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Launion Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_launion_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('launion/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Manila Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_manila_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('manila/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Roxas Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_roxas_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('roxas/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Tarlac Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_tarlac_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('tarlac/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Vigan Sales</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_vigan_s2 or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('vigan/sales-enrollee')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
    
                </div>
                <!-- /.widget-list -->
            </div>  
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Branch Enrollees</h6>
                    {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="widget-list row">
                <div class="widget-holder widget-full-height widget-flex col-lg-8">
                    
                </div>

                <div class="widget-holder col-lg-4">

                </div>
                 <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg bg-primary text-inverse">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Abra Enrollees</p>
                                    <span class="counter-title d-block">
                                         
                                    <span class="counter">
                                    {{$total_abra or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <!-- /.widget-holder -->
                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #85d1f2">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Candon Enrollees</p>
                                    <span class="counter-title d-block"><span class="counter">
                                    {{$total_candon or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                 <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6bd8a1">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Dagupan Enrollees</p>
                                    <span class="counter-title d-block">
                                        
                                        <span class="counter">
                                        {{$total_dagupan or 0}}
                                        </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                 <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Baguio Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_baguio or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Calapan Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_calapan or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Fairview Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_fairview or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Las Pinas Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_las_pinas or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Launion Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_launion or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Manila Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_manila or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Roxas Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_roxas or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Tarlac Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_tarlac or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6a55ee">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Vigan Enrollees</p>
                                    <span class="counter-title d-block">
                                    
                                    <span class="counter">
                                    {{$total_vigan or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <!-- /.widget-list -->
        </div>

        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Branch Expenses</h6>
                    {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="widget-list row">
                <div class="widget-holder widget-full-height widget-flex col-lg-8">
                    
                </div>

                <div class="widget-holder col-lg-4">

                </div>
              
                 <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg bg-primary text-inverse">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Abra Expenses</p>
                                    <span class="counter-title d-block">
                                        Php 
                                    <span class="counter">
                                    {{$abra_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('abra/expense')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <!-- /.widget-holder -->
                <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Baguio Expenses</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$baguio_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('baguio/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
    
                    <div class="widget-holder widget-sm col widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Calapan Expenses</p>
                                        <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$calapan_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('calapan/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #85d1f2">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Candon Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$candon_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('candon/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                 <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #6bd8a1">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Dagupan Expenses</p>
                                    <span class="counter-title d-block">
                                        Php 
                                        <span class="counter">
                                        {{$dagupan_expense or 0}}
                                        </span></span>
                                    <!-- /.counter-title --> <a href="{{url('dagupan/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Fairview Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$fairview_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('fairview/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Las Pinas Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$las_pinas_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('las_pinas/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Launion Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$launion_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('launion/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Manila Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$manila_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('manila/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Roxas Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$roxas_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('roxas/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Tarlac Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$tarlac_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('tarlac/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>

                <div class="widget-holder widget-sm col widget-full-height">
                    <div class="widget-bg text-inverse" style="background: #e8b335">
                        <div class="widget-body">
                            <div class="counter-w-info media">
                                <div class="media-body w-50">
                                    <p class="text-muted mr-b-5 fw-600">Vigan Expenses</p>
                                    <span class="counter-title d-block">
                                    Php 
                                    <span class="counter">
                                    {{$vigan_expense or 0}}
                                    </span></span>
                                    <!-- /.counter-title --> <a href="{{url('vigan/expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                </div>
                                <!-- /.media-body -->
                                <div class="pull-right align-self-center">
                                    <div class="mr-t-20">
                                    </div>
                                </div>
                            </div>
                            <!-- /.counter-w-info -->
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
            </div>
            <!-- /.widget-list -->
        </div>
        <!-- /.container-fluid -->
    </main>



@stop
@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
	

<script type="text/javascript">
    $('.admin-dashboard').addClass('active');

</script>

<script>
        $( window ).on("load", function() {
               
        
                const stat = $('#logstat').val();
        
        
                if (stat != null) {
                     console.log(stat);
                     //this get function fetch the authenticated user 
                      
                        $.get('/fetch-userinfo/'+stat, function(data){
                            var formData = new FormData()
                             
                          
                        
        
                            $.getJSON('https://api.ipify.org?format=json', function(ips){
                                    formData.append('ipaddress',ips.ip);
                                       formData.append('action','Login');
                                       
                                       $('#ip').val(ips.ip);
                                                
                                        
                                          $.each(data, function(index,objuser){              
                                              
                                              
                                              $('#branch').val(objuser.branch);
                                              $('#user').val(objuser.name);
                                              $('#user_id').val(objuser.id);
                                              
                                                    formData.append('branch',objuser.branch);
                                                    formData.append('user',objuser.name);
                                                    formData.append('user_id',objuser.id); 
                                                    
                                                    
                                                 }); 
                                                 
                                                 
                                 
                            }); 
                            
                        });
                        
                
                }
        });
        
        </script>
        <script>
        
        setTimeout(function(){
         //your code here
        
            
                    // var tip = $('#ip').val();
                    
                    // alert(tip);
                    
                    //      var items = {
                    //         "action":'Login',
                    //         "branch":$('#branch').val(),
                    //         "user":$('#user').val(),
                    //         "user_id":$('#user_id').val(),
                    //         "ipaddress":$('#ip').val()
                                
                    //         };
                            
                    //           var FormToString = JSON.stringify(items);
                    //   var formToJson = JSON.parse(FormToString);
                    //   alert(FormToString);
                      
                            var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"};
                            
                           
                    $.ajax({
                      url: 'https://cbrc.solutions/api/auth/login',
                      method: 'POST',
                      data: auth
                      }).done(function(response){
                      var tokenResponse = JSON.stringify(response);
                      var token = JSON.parse(tokenResponse);
                      var urlToken = token.access_token;   
                      
                      console.log(urlToken);
                      
                       
                             var items = {  
                                 
                            "action":"Login",
                            "branch":$('#branch').val(),
                            "user":$('#user').val(),
                            "user_id":$('#user_id').val(),
                            "ipaddress":$('#ip').val(),
                                
                            };
                            
                               var FormToString = JSON.stringify(items);
                      var formToJsons = JSON.parse(FormToString);
                      var str = JSON.stringify(formToJsons);
                      var formToJson = JSON.parse(str);
                      
                    //   alert(FormToString);
                    //   alert(formToJson.action);
                    //   alert(formToJson.branch);
                    //   alert(formToJson.user);
                      
                
                        $.ajax({
                            url: 'https://cbrc.solutions/api/main/system-log?token='+urlToken,
                            type: 'POST',
                            data: formToJson,
                                }).done(function(response){
                                     console.log(response);
                            });
                         });
                   
                      //});
                    
                    
                    
                    
        }, 2000);
        
                            
                           
        </script>
@stop
