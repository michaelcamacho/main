@extends('main')
@section('title')
Financial Report
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
.btn-group {
    top: 0px;
}

@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>

<link rel="stylesheet" style="text/css" href="{{ asset('css/financialreport.css') }}" />
@stop
@section('main-content')

<main class="main-wrapper clearfix">
        <!-- Page Title Area -->
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} Financial Report</h6>
                    
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Records</li>
                    <li class="breadcrumb-item active">{{ $branch }} Financial Report</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
        </div>
        <!-- /.page-title -->

        <!-- page content strarts here -->
        <div class="container-fluid">
                <div class="widget-list">
                    <div class="row" >
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                
                                <!--content start -->
                                <div class="widget-body clearfix">
                                  
                                        <input type="button" class="btn btn-primary" value="Download" onclick="downloadPdf()" />
                                            
                                            <br/><br/>

                                            <div style="background:white" id="financialReportPage">
                        
                                            <br/>
                                            
                                            <b>  
                                                Carl Balita Review Center<br/>										
                                                {{ $branch }} Branch<br/>										
                                                Statement of Reciepts & Disbursements<br/>										
                                                As of &nbsp;{{ date('M d,Y') }}
                                            </b>	
                                            
                                                <hr id="line1"/>                                        
                                            
                                            <br/>                                        
                                                <b>RECIEPTS:</b>
                                                <br/> 
                                                <div id="receiptSeason1">
                                                <b>Season 1</b><br/>
                                                    {{ number_format($letS1,2)}}<br/>
                                                    {{ number_format($nleS1,2)}}<br/>
                                                    {{ number_format($crimS1,2)}}<br/>
                                                    {{ number_format($civilS1,2)}}<br/>
                                                    {{ number_format($psychoS1,2)}}<br/>
                                                    {{ number_format($nclexS1,2)}}<br/>
                                                    {{ number_format($ieltsS1,2)}}<br/>
                                                    {{ number_format($socialsS1,2)}}<br/>
                                                    {{ number_format($agriS1,2)}}<br/>
                                                    {{ number_format($midwiferyS1,2)}}<br/>
                                                    {{ number_format($onlineS1,2)}}<br/>
                                                    &#8369; {{ number_format($totSeason1,2)}}<br/>
                                                </div><!-- end of receiptSeason1 --> 
                                            
                                                <div id="receiptSeason2">
                                                <b>Season 2</b><br/>
                                                {{ number_format($letS2,2)}}<br/>
                                                {{ number_format($nleS2,2)}}<br/>
                                                {{ number_format($crimS2,2)}}<br/>
                                                {{ number_format($civilS2,2)}}<br/>
                                                {{ number_format($psychoS2,2)}}<br/>
                                                {{ number_format($nclexS2,2)}}<br/>
                                                {{ number_format($ieltsS2,2)}}<br/>
                                                {{ number_format($socialsS2,2)}}<br/>
                                                {{ number_format($agriS2,2)}}<br/>
                                                {{ number_format($midwiferyS2,2)}}<br/>
                                                {{ number_format($onlineS2,2)}}<br/>
                                                    &#8369; {{ number_format($totSeason2,2)}}<br/>
                                                </div><!-- end of receiptSeason2 --> 
                                            
                                                <div id="programlist">  				 						
                                                <b>Review Fee</b>	        <br/>						
                                                LET							<br/>
                                                NLE 						<br/>
                                                Criminology					<br/>		
                                                Civil Service				<br/>		
                                                Psychometrician				<br/>			
                                                NCLEX						<br/>	
                                                IELTS						<br/>	
                                                Social Service				<br/>			
                                                Agriculture					<br/>		
                                                Midwifery					<br/>		
                                                Online Only					<br/>		
                                                </div><!-- end of programlist --> 
                                                <hr id="line2" />
                                                <hr id="line4" />

                                                <div id="totbookSale">  <b>Books</b><br/>
                                                    {{ number_format($bookLET,2)}}<br/>
                                                    {{ number_format($bookNLE,2)}}<br/>
                                                    {{ number_format($bookCrim,2)}}<br/>
                                                    {{ number_format($bookCS,2)}}<br/>
                                                    {{ number_format($bookPsycho,2)}}<br/>
                                                    {{ number_format($bookNCLEX,2)}}<br/>
                                                    {{ number_format($bookIELTS,2)}}<br/>
                                                    {{ number_format($bookSW,2)}}<br/>
                                                    {{ number_format($bookAgri,2)}}<br/>
                                                    {{ number_format($bookMid,2)}}<br/>
                                                    {{ number_format($bookOnline,2)}}<br/>
                                                        &#8369; {{ number_format($booksSale,2)}}<br/>
                                                </div>
                                                
                                                <b id="totRevenue"> &#8369;{{ number_format($revenue,2)}} Total Revenue</b>
                                                <b id="totReciept">Total Reciepts</b>
                                                
                                            <br/><br/>
                                            
                                            <b>DISBURSEMENTS:</b>
                                            
                                            <br/>									
                                            
                                            <div id="disbursmentlist">
                                                <b>Operating Expenses</b>
                                                <br/>									
                                                    <div id="operatingexpense">
                                                    Professional Fee			                    <br/>
                                                    Lecturer Allowance - Food and Transpo			<br/>	
                                                    Lecturer - Accommodation				        <br/> 				
                                                    Review Materials								<br/>
                                                    Facilitation Fee 								<br/>
                                                    Final Coaching								    <br/>
                                                    Rental - Venue				                    <br/>			
                                                    Marketing Expenses				                <br/>				
                                                    Courrier and Shipments				            <br/>				
                                                    Advertisement & Promotional Expenses			<br/>		
                                                    Representation				    		        <br/>
                                                    </div>
                                               
                                                <div id="operatingexpensedata">
                                                         {{  number_format($lecTax + $lecProfFee,2) }} <br/>
                                                         {{  number_format($lecAllow + $lecReimburse + $lecTranspo,2) }} <br/>
                                                         {{  number_format($lecAccommodation,2)}} <br/>
                                                         {{  number_format(0,2)}} <br/>
                                                         {{  number_format($faciFee,2)}} <br/>
                                                         {{  number_format($finalCoaching,2)}} <br/>
                                                         {{ number_format($utilVenue,2) }} <br/>
                                                         {{ number_format($mktingAllowance + $mktingGasoline + $mktingMeals + $mktingProfFee + $mktingTranspo,2) }} <br/>
                                                         {{ number_format($mktingDailyExpWaybill,2) }} <br/>
                                                         {{ number_format($mktingPostSignageFlyers,2) }} <br/>
                                                         {{ number_format($mktingGifts,2) }} <br/>
                                                </div>
                                                <!-- end of operatingexpense --> 

                                                <br/>
                                                <b>Administrative Expenses</b>					<br/>					
                                                    <div id="administrativeexpense">
                                                    Salaries & Wages				            <br/>				
                                                    Employee Gov't Contribution				    <br/>				
                                                    Rental - Office				                <br/>			
                                                    Light & Water				                <br/>			
                                                    Retainer's Fee								<br/>
                                                    PT & Communication				            <br/>			
                                                    Repairs & Maintenance				        <br/>			
                                                    Transportation Expense				        <br/>				
                                                    Supplies				                    <br/>				
                                                    Taxes, Licenses & Fees				        <br/> 				
                                                    Office Equipment				            <br/> 				
                                                    Furniture and Fixtures				        <br/>				
                                                    Miscellaneous (others, interest-bank)		<br/> 				
                                                    </div>
                                            
                                                <div id="administrativeexpensedata">
                                                         {{ number_format($admnsalary,2)}} <br/>
                                                         {{ number_format($contributionsss + $contributionpagibig + $contributionphilhealth,2) }} <br/>
                                                         {{ number_format($utilityrent,2) }} <br/>
                                                         {{ number_format($utilitywater + $utilitysound + $utilityelectricity,2) }} <br/>
                                                         {{ number_format(0,2)}} <br/>
                                                         {{ number_format($utilityinternet + $dailyexpload + $utilitytelephone,2) }} <br/>
                                                         {{ number_format($dailyexpmaintenance,2) }} <br/>
                                                         {{ number_format($dailyexptranspo + $dailyexpgasoline,2) }} <br/>
                                                         {{ number_format($dailyexpofficeSupply + $dailyexpgrocery,2) }} <br/>
                                                         {{ number_format($insurance + $taxAndLicense,2) }} <br/>
                                                         {{ number_format($investOfficeEquip,2) }} <br/>
                                                         {{ number_format($investFurnitureAndfixture,2) }} <br/>
                                                         {{ number_format($dailyexpOthers + $dailyexpMeals + $dailyexpAllow,2) }} <br/>
                                                </div>
                                                <!-- end of administrativeexpense --> 

                                                <br/>	
                                                <b>Other Disbursements	</b>
                                                <br/>	
                                                <div id="otherdisbursment">							
                                                    Books<br/>								
                                                    Loans<br/>	
                                                </div> 
                                                
                                                <div id="otherdisbursmentdata">
                                                         {{ number_format($totBookDisbursement,2) }} <br/>
                                                         {{ number_format($totLoanDisbursment,2) }} <br/>
                                                </div>
                                                <!-- end of administrativeexpense --> 

                                            </div><!-- end of disbusment -->                                      
                                                                                      
                                            <br/>	
                                            <div id="totDisburseNetExcess">                                       
                                            <b >Total Disbursements:</b><br/>									
                                            	                                       
                                            <b >Net Excess/(Deficit)</b>						 
                                            </div>
                                            <hr id="line3"/>
                                            <div id="netincome">
                                                
                                                {{ number_format($totDisbursement,2) }}
                                                <br/>
                                                
                                                @if(number_format($netExcessIncome,2) >= 0)
                                                     {{ number_format(abs($netExcessIncome),2) }} 
                                                @else
                                                {{ "(".number_format(abs($netExcessIncome),2).")" }} 
                                                @endif
                                                
                                                <br/>
                                                @if(number_format($netExcessIncome,2) >= 0)
                                                &#8369; {{ number_format(abs($netExcessIncome),2) }} 
                                                @else
                                                &#8369;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                @endif
                                                
                                            </div>		
                                            <b id="netIncomelbl">Net Income</b>
                                            <br/>
                                            <br/>
                                           
                                    </div><!-- financialReportPage end -->


                        

                                </div>
                                <!-- content end -->
                            </div>
                        </div>
                    </div><!-- page row ends here --> 
                </div><!-- page content-fluid ends here --> 
        </div>
        <!-- page content ends here -->        
    </main>
    <!-- end of main -->
@stop

@section('js')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('.reports').addClass('active');
  $('.reports').addClass('collapse in');

  });

function downloadPdf(){
    var pdf = new jsPDF('p','pt','legal');

    pdf.addHTML($('#financialReportPage'),function() {
    pdf.save('{{$branch}}financialreport.pdf');
});
}

</script>




@stop