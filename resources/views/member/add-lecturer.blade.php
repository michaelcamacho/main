@extends('main')
@section('title')
ADD LECTURER
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly]] {
    color: #000;
}
</style>
<link href="{{ asset('css/select.css') }}" rel="stylesheet" />
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{Auth::user()->branch}}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Lecturer Evaluation</li>
                            <li class="breadcrumb-item active">Add Lecturer</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Add Lecturer</h5>
                                   <br/>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position.'insert-lecturer'}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" 
                                                    value="" name="lecturer" id="lecturer" required>
                                                    <label>Name of reviewer</label>
                                                </div>
                                            </div>
                                         
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control"
                                                    name="program" id="program" required>
                                                    <option value="">--- Select Program ---</option>
                                                    @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                         
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control"
                                                    name="class" id="class" required>
                                                    <option value="">--- Select Class ---</option>
                                                    </select>
                                                    <label>Class</label>
                                                </div>
                                            </div>
                                         
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control"
                                                    name="subject" id="subject" required>
                                                    <option value="">--- Select Subject ---</option>
                                                    </select>
                                                    <label>Subject</label>
                                                </div>
                                            </div>
                                         
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="number" 
                                                    value="" name="section" id="section" min="1" required>
                                                    <label>Section</label>
                                                </div>
                                            </div>
                                         
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" 
                                                    value="" name="ambassador" id="ambassador">
                                                    <label>Name of review ambassador</label>
                                                </div>
                                            </div>
                                         
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group input-focused">
                                                    <input class="form-control" type="date" 
                                                    value="" name="date" id="date" required>
                                                    <label>Date of lecture</label>
                                                </div>
                                            </div>
                                         
                                        </div>
                                       
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" type="submit" id="button-prevent">Add Lecturer</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
{{--<script src="{{ asset('/js/book-fetch.js') }}"></script>--}}
<script type="text/javascript">
    $(document).ready(function() {
  $('.evaluate').addClass('active');
  $('.evaluate').addClass('collapse in');
});


    $('#program').on('change', function(e){
            console.log(e);

            var program = e.target.value;
            
            $.get({{Auth::user()->position}}'json-class?program=' + program,function(data){
                console.log(data);

                 $('#class').empty();
                 $('#class').append('<option value="" disable="true" selected="true">--- Select Class ---</option>');

                 
                 $('#subject').empty();
                 $('#subject').append('<option value="" disable="true" selected="true">--- Select Subject ---</option>');
                 
                 $('#section').val('');
                 

                  $.each(data, function(index, lecObj){
                    $('#class').append('<option value="'+ lecObj.aka +'*'+ lecObj.class +'">'+ lecObj.class +'</option>');
                })

            });
        });

    $('#class').on('change', function(e){
            console.log(e);

            var classes = e.target.value;

            var clas = classes.split("*");
            
            var program = $('#program').val();
            

            $.get({{Auth::user()->position}}'json-subject?program=' + program + '&class=' + clas[0],function(data){
                console.log(data);

                 
                 $('#subject').empty();
                 $('#subject').append('<option value="" disable="true" selected="true">--- Select Subject ---</option>');
                 
                 $('#section').val('');
                 

                  $.each(data, function(index, lecObj){
                    $('#subject').append('<option value="'+ lecObj.aka +'*'+ lecObj.subject +'">'+ lecObj.subject +'</option>');
                })

            });
        });




</script>

@stop