@extends('main')
@section('title')
 Books Transfer Record
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Books Transfer Record</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Books Transfer Record</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="book-table">
                                    <table id="booktransact" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                        
                     <thead>
                       
                        <tr>
                            <th>Trans ID</th>
                            <th>Date</th>
                            <th>Remarks</th>
                            <th>Branch Sender</th>
                            <th>Branch Reciever</th>
                            <th>List of Books</th>
                            
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($book))
                            @foreach($book as $books)
                        <tr id="row_{{$books->id}}">
                            <td id="branch_{{$books->id}}">{{$books->id}}</td>
                            <td id="branch_{{$books->id}}">{{$books->book_date}}</td>
                            <td id="book_title_{{$books->id}}">{{$books->book_remarks}}</td>
                            <td id="sender_{{$books->id}}">{{$books->book_branchSender}}</td>
                            <td id="reciever_{{$books->id}}">{{$books->book_branchReciever}}</td>
                            
                            <input type="hidden" name="date" value="{{$books->book_date}}">
                            <input type="hidden" name="remarks" value="{{$books->book_remarks}}">
                            <input type="hidden" name="sender" value="{{$books->book_branchSender}}">
                            <input type="hidden" name="reciever" value="{{$books->book_branchReciever}}">
                            <td>
                                    <button class="btn btn-primary a_view" onclick="modals(this)" id="a_view{{$books->id}}" value="{{$books->id}}" >View</button>
                                </td>
                        </tr>
                       @endforeach
                            @endif


                    </tbody>  
                           
                    
                </table>
                <div id="view" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
                        <div class="modal-dialog   modal-lg ">
                                <div class="modal-content ">
                                    <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                                    <div class="modal-body">
                                            <p id="transactionInfo"></p>

                                            <table id="bookModal" class="table table-bordered  nowrap display" >
                                                
                                                        <thead>
                                                                <th>Program</th>
                                                                <th>Title</th>
                                                                <th>Author</th>
                                                                <th>Quantity</th>
                                                                <th>Stock<br>Initial</th>
                                                                <th>Stock<br>Final</th>
                                                        </thead>
                                                        <tbody>                                                                
                                                            
                                                        </tbody>
                                            </table>
                                    </div>
                                </div>  
                            </div> 
                        </div>  
            </div>


                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->

        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/book.js') }}"></script>

<script>

    function modals(d){

        var id = d.value;       
        var date = $('#date').val();     
        var remarks = $('#remarks').val();     
        var sender = $('#sender').val();     
        var reciever = $('#reciever').val();     

$('#view').modal('show');
        
      $.ajax({
               type: 'GET',
               url: "/fetch-book-list/"+id,
               
        }).done(function(response){
            console.log(response);
            $('#bookModal tbody').empty();
            
            

            $.each(response,function(key,value){

            
                $('#bookModal tbody').append('<tr>'+
                    '<td>'+value.book_program+'</td>'+
                    '<td>'+value.book_title+'</td>'+
                    '<td>'+value.book_author+'</td>'+
                    '<td>'+value.book_quantity+'</td>'+
                    '<td>'+value.book_stockInitial+'</td>'+
                    '<td>'+value.book_stockFinal+'</td>'+
                    '</tr>');
                });
       
            });
    }
    </script>

<script type="text/javascript">





    $('#clear').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'clear-book'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });





</script>
@stop