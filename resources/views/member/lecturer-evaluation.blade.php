@extends('main')
@section('title')
Lecturer Evaluation
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
.btn-group {
    top: -14px;
}
div.dataTables_wrapper div.dataTables_info {
    display: none;
}
#table-header{
    float: right;
}
#lec3_filter{
    display: none;
}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{Auth::user()->branch}}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Lecturer Evaluation</li>
                            <li class="breadcrumb-item active">Lecturer Evaluation Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Lecturer Evaluation Records</h5>
                                   
                                </div>
                                <div class="widget-body clearfix">

                                  <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Branch</label>
                                                    <input class="form-control" type="text" 
                                                    value="{{Auth::user()->branch}}" name="branch" id="branch" readonly>
                                                    
                                                </div>
                                            </div>

                                </div>

                                <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Program</label>
                                                    <select class="form-control"
                                                    name="program" id="program" required>
                                                    <option value="">--- Select Program ---</option>
                                                    @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}*{{$programs->program_name}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    
                                                </div>
                                            </div>
                                </div>
                                <div class="row">    
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Class</label>
                                                    <select class="form-control" 
                                                     name="class" id="class" readonly>
                                                     <option value="">--- Select Class ---</option>
                                                  </select>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>

                                <div class="row">  
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Subject</label>
                                                    <select class="form-control" 
                                                     name="subject" id="subject" readonly>
                                                     <option value="">--- Select Subject ---</option>
                                                  </select>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>
                                    <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Section</label>
                                                    <select class="form-control" 
                                                     name="section" id="section" readonly>
                                                     <option value="">--- Select Section ---</option>
                                                  </select>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Name of reviewer</label>
                                                    <select class="form-control" 
                                                     name="lecturer" id="lecturer" readonly>
                                                     <option value="">--- Select Lecturer ---</option>
                                                  </select>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>

                                    <div class="row" id="form2">

                                            <div class="col-lg-6">
                                                <div class="form-group">

                                                    <a class="btn btn-success search scroll" style="width: 100%; margin-top: 31px;" href="#form2">Search Lecturer</a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <br/><br/>
                            @include('template.hidden')


                            <table id="lecturers" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important"></table>


               
                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->

            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Lecturer Comments Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                            <table id="lec3" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Lecturer</th>
                            <th>Program</th>
                            <th>Class</th>
                            <th>Subject</th>
                            <th>Section</th>
                            <th>Likes</th>
                            <th>Dislikes</th>
                            <th>Comment</th>
                            <th>Date Of Lecturer</th>
                            <th>Student Name</th>
                            
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($comment))
                            @foreach($comment as $comments)
                        <tr>
                            <td>{{$comments->lecturer}}</td>
                            <td>{{$comments->program}}</td>
                            <td>{{$comments->class}}</td>
                            <td>{{$comments->subject}}</td>
                            <td>{{$comments->section}}</td>
                            <td>{{$comments->like}}</td>
                            <td>{{$comments->dislike}}</td>
                            <td>{{$comments->comment}}</td>
                            <td>{{$comments->date}}</td>
                            <td>{{$comments->name}}</td>
                        </tr>
                       @endforeach
                            @endif

                            </tbody>                  
                    
                </table>

                @role('admin')
                <br/>
                <a class="btn btn-danger clear" id="clear">Clear Lecturer Tables</a>
                @endrole
                            </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/evaluate.js') }}"></script>
@include('js.evaluation')
<script src="{{ asset('/js/lecturer-evaluation.js') }}"></script>
@stop