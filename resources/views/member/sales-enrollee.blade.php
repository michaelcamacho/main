@extends('main')
@section('title')
Sales per Enrollee Records
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Sales per Enrollee Records</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Sales per Enrollee Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Sales Per Enrollee Records Season 1</h5>
                                <br/>  
                                </div>
                                <div class="widget-body clearfix">
                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                        <tr>
                            <th>Trans.#</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Amount Paid</th>
                            <th>Balance</th>
                            <th>Discount</th>
                            <th>Tuition Fee</th>
                            <th>Facilitation Fee</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Discount Category</th>
                            <th>Year</th>
                             @role('admin')
                             <th>DELETE</th>
                             @endrole
                            
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($sale))
                            @foreach($sale as $sales)
                        <tr>
                            
                            <td>00{{$sales->id}}</td>
                            <td>{{$sales->date}}</td>
                            <td>{{$sales->name}}</td>
                            <td>{{$sales->amount_paid}}</td>
                            <td>{{$sales->balance}}</td>
                            <td>{{$sales->discount}}</td>
                            <td>{{$sales->tuition_fee}}</td>
                            <td>{{$sales->facilitation_fee}}</td>
                            <td>{{$sales->program}}</td>
                            <td>{{$sales->category}}</td>
                            <td>{{$sales->discount_category}}</td>
                            <td>{{$sales->year}}</td>
                             @role('admin')
                             
                            <td>
                                <a  href="delete-sale/s1/{{$sales->id}}" id="delete" class="btn btn-danger delete"  name="{{$sales->id}}">DELETE</button>
                            </td>
                            @endrole
                        </tr>
                       @endforeach
                            @endif
                    </tbody>                  
                    
                </table>
                @role('admin')
                <br/>
                <a class="btn btn-danger clear" id="clear1">Clear table</a>
                @endrole
                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->

            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Sales Per Enrollee Records Season 2</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="exam2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Trans.#</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Amount Paid</th>
                            <th>Balance</th>
                            <th>Discount</th>
                            <th>Tuition Fee</th>
                            <th>Facilitation Fee</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Discount Category</th>
                            @role('admin')
                             <th>DELETE</th>
                             @endrole
                            
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($sale2))
                            @foreach($sale2 as $sale2s)
                        <tr>
                            <td>00{{$sale2s->id}}</td>
                            <td>{{$sale2s->date}}</td>
                            <td>{{$sale2s->name}}</td>
                            <td>{{$sale2s->amount_paid}}</td>
                            <td>{{$sale2s->balance}}</td>
                            <td>{{$sale2s->discount}}</td>
                            <td>{{$sale2s->tuition_fee}}</td>
                            <td>{{$sale2s->facilitation_fee}}</td>
                            <td>{{$sale2s->program}}</td>
                            <td>{{$sale2s->category}}</td>
                            <td>{{$sale2s->discount_category}}</td>
                            @role('admin')
                             
                            <td>
                                <a  href="delete-sale/s2/{{$sale2s->id}}" id="delete2" class="btn btn-danger delete"  name="{{$sale2s->id}}">DELETE</button>
                            </td>
                            @endrole
                        </tr>
                       @endforeach
                            @endif
                    </tbody>                  
                    
                </table>
                @role('admin')
                <br/>
                <a class="btn btn-danger clear" id="clear2">Clear table</a>
                @endrole
                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
           
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.record').addClass('active');
  $('.record').addClass('collapse in');
});

  
          $('#clear1').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'clear-sale-season1'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

      $('#clear2').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'clear-sale-season2'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });


</script>
@stop