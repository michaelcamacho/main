@extends('main')
@section('title')
Evaluate Lecturer
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
.btn-group {
    top: -14px;
}
div.dataTables_wrapper div.dataTables_info {
    display: none;
}
.form-check-input{
    position: relative !important;
    margin-right: 10px;
}
.checkes{
    padding: 10px 20px 0 30px !important;
    text-align: center;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 10px;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{Auth::user()->branch}}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Lecturer Evaluation</li>
                            <li class="breadcrumb-item active">Evaluate Lecturer</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Evaluate Lecturer</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix" id="clear">

                                    <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Branch</label>
                                                    <input class="form-control" type="text" 
                                                    value="{{Auth::user()->branch}}" name="branch" id="branch"  required>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Student Name</label>
                                                    <select class="form-control js-example-basic-single"
                                                    name="student" id="student" required>
                                                    <option value="">--- Select Student ---</option>
                                                    </select>
                                                    
                                                </div>
                                            </div>

                                        
                                </div>

                                <div class="row">
                                            
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Program</label>
                                                    <select class="form-control"
                                                    name="program" id="program" required>
                                                    <option value="">--- Select Program ---</option>
                                                    @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}*{{$programs->program_name}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                
                                            </div>
                                </div>

                                  <div class="row">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Class</label>
                                                    <select class="form-control"
                                                    name="class" id="class" required>
                                                    <option value="">--- Select Class ---</option>
                                                    </select>
                                                    
                                                </div>
                                            </div>
                                        
                                         
                                        </div>

                                    
                                    <div class="row">
                                            <div class="col-lg-6">
                                               <div class="form-group">
                                                    <label>Subject</label>
                                                    <select class="form-control"
                                                    name="subject" id="subject" required>
                                                    <option value="">--- Select Subject ---</option>
                                                    </select>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>


                                        <div class="row">
                                        
                                            <div class="col-lg-6">
                                                
                                                 <div class="form-group">
                                                    <label>Section</label>
                                                    <select class="form-control"
                                                    name="section" id="section" required>
                                                    <option value="">--- Select Section ---</option>
                                                    </select>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>

                                        <div class="row">
                                            
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Name of reviewer</label>
                                                    <select class="form-control" 
                                                     name="lecturer" id="lecturer" required>
                                                     <option value="">--- Select Lecturer ---</option>
                                                  </select>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>

                                    <div class="row" id="form2">

                                            <div class="col-lg-6">
                                                <div class="form-group">

                                                    <a class="btn btn-success eval scroll" style="width: 100%; margin-top: 31px;" href="#form2">Evaluate Lecturer</a>
                                                    
                                                </div>
                                            </div>
                                         
                                        </div>
                                     
                                        <br/><br/>
        <form id="form">
                @csrf
        <div style="overflow-x:auto;">
            <table id="lec2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">

                        <tr>
                            <th>STUDENT:</th>
                            <th><input type="text" class="readonly"  id="eval_student" name="eval_student"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                           
                        </tr>

                        <tr>
                            <th>CLASS:</th>
                            <th><input type="text" class="readonly"  id="eval_class" name="eval_class" required></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                           
                        </tr>
                   
                      <tr>
                            <th><strong>SUBJECT:</strong></th>
                            <th><input type="text" class="readonly"  id="eval_subj" name="eval_subj" required></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                     
                         <tr>
                            <th><strong>PROGRAM:</strong></th>
                            <th><input type="text" class="readonly"  id="eval_program" name="eval_program" required></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                        <tr>
                            <th><strong>SECTION:</strong></th>
                            <th><input type="text" class="readonly"  id="eval_section" name="eval_section" required></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                        <tr>
                            <th><strong>NAME OF REVIEWER:</strong></th>
                            <th>
                                <input type="text" class="readonly"  id="eval_lecturer" name="eval_lecturer" required>
                                <input type="hidden" id="eval_id" name="eval_id">
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                        <tr>
                            <th><strong>DATE OF LECTURE:</strong></th>
                            <th>
                                <input type="text" class="readonly"  id="eval_date" name="eval_date" required>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                        <tr>
                            <th><strong>NAME OF REVIEW AMBASSADOR:</strong></th>
                            <th><input type="text" class="readonly"  id="eval_ambassador" name="eval_ambassador"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                           
                        </tr>

                         <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                       
                    
                        <tr>
                            <th></th>
                            <th>Excellent</th>
                            <th>Good</th>
                            <th>Fair</th>
                            <th>Poor</th>
                            <th>Very Poor</th>
                            
                        </tr>
    
                      <tr>
                        <td>
                          <strong>CONTENT</strong>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                       
                      </tr>
                      <tr>
                        <td>
                          <p>Comprehensiveness – shows knowledge of the subject matter</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label"> 
                            <input type="radio" name="qA" id="qA" value="excellentA" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qA" id="qA" value="goodA" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qA" id="qA" value="fairA" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qA" id="qA" value="poorA" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qA" id="qA" value="verypoorA" class="form-check-input">very poor
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <p>Substantiveness – delivers enough and substantial concepts</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qB" id="qB" value="excellentB" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qB" id="qB" value="goodB" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qB" id="qB" value="fairB" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qB" id="qB" value="poorB" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qB" id="qB" value="verypoorB" class="form-check-input">very poor
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <p>Relevance – provides topics that are relevant to the actual exam</p>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qC" id="qC" value="excellentC" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qC" id="qC" value="goodC" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qC" id="qC" value="fairC" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qC" id="qC" value="poorC" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qC" id="qC" value="verypoorC" class="form-check-input">very poor
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <p>Organization – executes the topic in an organized manner</p>
                        </td>
                        
                       <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qD" id="qD" value="excellentD" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qD" id="qD" value="goodD" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qD" id="qD" value="fairD" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qD" id="qD" value="poorD" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qD" id="qD" value="verypoorD" class="form-check-input">very poor
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <strong>MASTERY</strong>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Authority –  exhibits confidence and authority in presenting the topic</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qE" id="qE" value="excellentE" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qE" id="qE" value="goodE" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qE" id="qE" value="fairE" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qE" id="qE" value="poorE" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qE" id="qE" value="verypoorE" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Ability to Answer Questions – able to answer students questions satisfactorily</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qF" id="qF" value="excellentF" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qF" id="qF" value="goodF" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qF" id="qF" value="fairF" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qF" id="qF" value="poorF" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qF" id="qF" value="verypoorF" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Preparedness – well-prepared to discuss the lecture</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qG" id="qG" value="excellentG" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qG" id="qG" value="goodG" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qG" id="qG" value="fairG" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qG" id="qG" value="poorG" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qG" id="qG" value="verypoorG" class="form-check-input">very poor
                          </label>
                        </td>
                       
                      </tr>
                      <tr>
                        <td>
                          <strong>DELIVERY</strong>
                        </td>
                         <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Enthusiasm  - shows energy that stimulates participation</p>
                        </td>
                       <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qH" id="qH" value="excellentH" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qH" id="qH" value="goodH" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qH" id="qH" value="fairH" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qH" id="qH" value="poorH" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qH" id="qH" value="verypoorH" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Pacing – moves to one topic to another understandable to the students</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qI" id="qI" value="excellentI" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qI" id="qI" value="goodI" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qI" id="qI" value="fairI" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qI" id="qI" value="poorI" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qI" id="qI" value="verypoorI" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Voice Projection –has effetive and clear voice</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qJ" id="qJ" value="excellentJ" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qJ" id="qJ" value="goodJ" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qJ" id="qJ" value="fairJ" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qJ" id="qJ" value="poorJ" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qJ" id="qJ" value="verypoorJ" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Approach/ Strategy – has dynamic technique in discussing</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qK" id="qK" value="excellentK" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qK" id="qK" value="goodK" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qK" id="qK" value="fairK" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qK" id="qK" value="poorK" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qK" id="qK" value="verypoorK" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <strong>MOTIVATIONAL</strong>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Inspiration – inspires me to strive harder and makes the topic more interesting</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qL" id="qL" value="excellentL" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qL" id="qL" value="goodL" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qL" id="qL" value="fairL" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qL" id="qL" value="poorL" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qL" id="qL" value="verypoorL" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Self-Esteem – boosts me to be a better person</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qM" id="qM" value="excellentM" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qM" id="qM" value="goodM" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qM" id="qM" value="fairM" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qM" id="qM" value="poorM" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qM" id="qM" value="verypoorM" class="form-check-input">very poor
                          </label>
                        </td>
                       
                      </tr>
                      <tr>
                        <td>
                          <p>Authenticity – shows sincerity and authenticity in presenting himself/herself</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qN" id="qN" value="excellentN" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qN" id="qN" value="goodN" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qN" id="qN" value="fairN" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qN" id="qN" value="poorN" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qN" id="qN" value="verypoorN" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <strong>IMAGING</strong>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Appearance & Grooming – shows neatness, proper posture, and an aesthetic appeal</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qO" id="qO" value="excellentO" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qO" id="qO" value="goodO" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qO" id="qO" value="fairO" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qO" id="qO" value="poorO" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qO" id="qO" value="verypoorO" class="form-check-input">very poor
                          </label>
                        </td>
                       
                      </tr>
                      <tr>
                        <td>
                          <p>Character & Personality - shows uniqueness and carried himself/herself very well</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qP" id="qP" value="excellentP" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qP" id="qP" value="goodP" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qP" id="qP" value="fairP" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qP" id="qP" value="poorP" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qP" id="qP" value="verypoorP" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                      <tr>
                        <td>
                          <p>Impact – has stage presence, charisma, and a contagious aura</p>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qQ" id="qQ" value="excellentQ" class="form-check-input" required>excellent
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qQ" id="qQ" value="goodQ" class="form-check-input">good
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qQ" id="qQ" value="fairQ" class="form-check-input">fair
                          </label>
                        </td>
                        <td class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qQ" id="qQ" value="poorQ" class="form-check-input">poor
                          </label>
                        </td>
                        <td  class="checkes">
                            <label class="form-check-label">
                            <input type="radio" name="qQ" id="qQ" value="verypoorQ" class="form-check-input">very poor
                          </label>
                        </td>
                        
                      </tr>
                  
            </tr>
        </table>
    </div>
                    <br/><br/>
                    <div class="row">
                        <div class="col-lg-12" style="margin-left: 16px;">
                            <div class="form-group">
                                <label>LIKES</label>
                                <textarea class="form-control" name="like" id="like"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="margin-left: 16px;">
                            <div class="form-group">
                                <label>DISLIKES</label>
                                <textarea class="form-control" name="dislike" id="dislike"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="margin-left: 16px;">
                            <div class="form-group">
                                <label>COMMENTS</label>
                                <textarea class="form-control" name="comment" id="comment"></textarea>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12" style="margin-left: 16px;">
                            <button type="submit" class="btn btn-success add-eval" style="width: 100%; margin-top: 31px;">Add Lecturer Evaluation
                            </button>

                        </div>

                    </div>

            </form>
        

               
                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<!--<script src="{{ asset('/datatable/datatables.min.js') }}"></script>-->
<!--<script src="{{ asset('/js/data-table.js') }}"></script>-->
<script src="{{ asset('/js/evaluate.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.evaluate').addClass('active');
  $('.evaluate').addClass('collapse in');
});
    $('#program').on('change', function(e){
            console.log(e);

            var prog = e.target.value;
            var program = prog.split("*");

            $.get({{Auth::user()->position}}'json-class?program=' + program[0],function(data){
                console.log(data);

                 $('#class').empty();
                 $('#class').append('<option value="" disable="true" selected="true">--- Select Class ---</option>');

                 
                 $('#subject').empty();
                 $('#subject').append('<option value="" disable="true" selected="true">--- Select Subject ---</option>');
                 
                 $('#section').empty();
                 $('#section').append('<option value="" disable="true" selected="true">--- Select Section ---</option>');

                 $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');
                 

                  $.each(data, function(index, lecObj){
                    $('#class').append('<option value="'+ lecObj.aka +'*'+ lecObj.class +'">'+ lecObj.class +'</option>');
                })

            });
        });

    $('#class').on('change', function(e){
            console.log(e);

            var classes = e.target.value;

            var clas = classes.split("*");
            

            var prog = $('#program').val();
            var program = prog.split("*");

            $.get({{Auth::user()->position}}'json-subject?program=' + program[0] + '&class=' + clas[0],function(data){
                console.log(data);

                 
                 $('#subject').empty();
                 $('#subject').append('<option value="" disable="true" selected="true">--- Select Subject ---</option>');
                 
                 $('#section').empty();
                 $('#section').append('<option value="" disable="true" selected="true">--- Select Section ---</option>');

                 $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');
                 

                  $.each(data, function(index, lecObj){
                    $('#subject').append('<option value="'+ lecObj.aka +'*'+ lecObj.subject +'">'+ lecObj.subject +'</option>');
                })

            });
        });

     $('#subject').on('change', function(e){
            console.log(e);
            var subj = e.target.value;
            var subject = subj.split("*");

            var classes = $('#class').val();
            var clas = classes.split("*");

            var prog = $('#program').val();
            var program = prog.split("*");

            $.get({{Auth::user()->position}}'json-section?program=' + program[0] + '&class=' + clas[0] + '&subject=' +subject[0],function(data){
                console.log(data);

                $('#section').empty();
                 $('#section').append('<option value="" disable="true" selected="true">--- Select Section ---</option>');

                 $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');


                  $.each(data, function(index, lecObj){
                    $('#section').append('<option value="'+ lecObj.section +'">'+ lecObj.section +'</option>');
                })

            });
        });

    $('#section').on('change', function(e){
            console.log(e);
            var section = e.target.value;

            var classes = $('#class').val();
            var clas = classes.split("*");

            var subj = $('#subject').val();
            var subject = subj.split("*");

            var prog = $('#program').val();
            var program = prog.split("*");

            $('#lecturer').empty();
                 $('#lecturer').append('<option value="" disable="true" selected="true">--- Select Lecturer ---</option>');

            $.get({{Auth::user()->position}}'json-lecturer?program=' + program[0] + '&class=' + clas[0] + '&subject=' +subject[0] + '&section=' +section,function(data){
                console.log(data);


                  $.each(data, function(index, lecObj){
                    $('#lecturer').append('<option value="'+ lecObj.lecturer +'*'+ lecObj.id +'">'+ lecObj.lecturer +'</option>');
                })

            });
        });

    $('.eval').on('click', function(){
           
            var student = $('#student').val();

            var section = $('#section').val();

            var classes = $('#class').val();
            var clas = classes.split("*");

            var subj = $('#subject').val();
            var subject = subj.split("*");

            var prog = $('#program').val();
            var program = prog.split("*");

            var lec = $('#lecturer').val();
            var lecturer = lec.split("*");

            $.get({{Auth::user()->position}}'json-lecturer?program=' + program[0] + '&class=' + clas[0] + '&subject=' +subject[0] + '&section=' +section,function(data){
                console.log(data);


                  $.each(data, function(index, lecObj){
                    $('#eval_date').val(lecObj.date);
                });

                  $.each(data, function(index, lecObj){
                    $('#eval_ambassador').val(lecObj.review_ambassador);
                });

                  $('#eval_program').val(program[1]);
                  $('#eval_class').val(clas[1]);
                  $('#eval_subj').val(subject[1]);
                  $('#eval_section').val(section);
                  $('#eval_lecturer').val(lecturer[0]);
                  $('#eval_id').val(lecturer[1]);
                  if(student == 0){
                    $('#eval_student').val("");
                  }
                  if(student != 0){
                  $('#eval_student').val(student);
                }


            });
        });

    $('#program').on('change', function(e){
            console.log(e);

            var prog = e.target.value;
            var program = prog.split("*");
            var branch = $('#branch').val().toLowerCase();

            $.get({{Auth::user()->position}}'json-student?program=' + program[0],function(data){
                console.log(data);

                 $('#student').empty();
                 $('#student').append('<option value="0" disable="true" selected="true">--- Select Student ---</option>');


                  $.each(data, function(index, studentObj){
                    $('#student').append('<option value="'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
                });
                 
            });
        });

    $('#form').submit(function(event){
        event.preventDefault();
        event.stopPropagation();
              
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('student', $('#eval_student').val());
                formData.append('class', $('#eval_class').val());
                formData.append('subject', $('#eval_subj').val());
                formData.append('program', $('#eval_program').val());
                formData.append('lecturer', $('#eval_lecturer').val());
                formData.append('section', $('#eval_section').val());
                formData.append('date', $('#eval_date').val());
                formData.append('id', $('#eval_id').val());
                formData.append('qA', $('#qA:checked').val());
                formData.append('qB', $('#qB:checked').val());
                formData.append('qC', $('#qC:checked').val());
                formData.append('qD', $('#qD:checked').val());
                formData.append('qE', $('#qE:checked').val());
                formData.append('qF', $('#qF:checked').val());
                formData.append('qG', $('#qG:checked').val());
                formData.append('qH', $('#qH:checked').val());
                formData.append('qI', $('#qI:checked').val());
                formData.append('qJ', $('#qJ:checked').val());
                formData.append('qK', $('#qK:checked').val());
                formData.append('qL', $('#qL:checked').val());
                formData.append('qM', $('#qM:checked').val());
                formData.append('qN', $('#qN:checked').val());
                formData.append('qO', $('#qO:checked').val());
                formData.append('qP', $('#qP:checked').val());
                formData.append('qQ', $('#qQ:checked').val());
                formData.append('like', $('#like').val());
                formData.append('dislike', $('#dislike').val());
                formData.append('comment', $('#comment').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{Auth::user()->position. 'insert-evaluation'}}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success'
                            }).then(function(){

                        $('input:radio[name="qA"]').prop('checked',false);
                        $('input:radio[name="qB"]').prop('checked',false);
                        $('input:radio[name="qC"]').prop('checked',false);
                        $('input:radio[name="qD"]').prop('checked',false);
                        $('input:radio[name="qE"]').prop('checked',false);
                        $('input:radio[name="qF"]').prop('checked',false);
                        $('input:radio[name="qG"]').prop('checked',false);
                        $('input:radio[name="qH"]').prop('checked',false);
                        $('input:radio[name="qI"]').prop('checked',false);
                        $('input:radio[name="qJ"]').prop('checked',false);
                        $('input:radio[name="qK"]').prop('checked',false);
                        $('input:radio[name="qL"]').prop('checked',false);
                        $('input:radio[name="qM"]').prop('checked',false);
                        $('input:radio[name="qN"]').prop('checked',false);
                        $('input:radio[name="qO"]').prop('checked',false);
                        $('input:radio[name="qP"]').prop('checked',false);
                        $('input:radio[name="qQ"]').prop('checked',false);
                        $('#like').val("");
                        $('#dislike').val("");
                        $('#comment').val("");
                        $('#eval_student').val("");
                        $('#eval_class').val("");
                        $('#eval_subj').val("");
                        $('#eval_program').val("");
                        $('#eval_lecturer').val("");
                        $('#eval_ambassador').val("");
                        $('#eval_section').val("");
                        $('#eval_date').val("");
                        $('#eval_id').val("");
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                      })
                        


                    }
                               
              });
                
            });



</script>
@stop