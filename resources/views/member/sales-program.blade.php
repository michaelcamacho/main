@extends('main')
@section('title')
Sales per Program
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Sales per Program Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Sales per Program Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Program</th>
                            <th>Enrollees</th>
                            <th>Sales Season 1</th>
                            <th>Sales Season 2</th>

                        </tr>
                       
                    </thead>
                    <tbody>

                        <tr>
                            <td>LET</td>
                            <td>{{$let}}</td>
                            <td>{{$let_1_sale}}</td>
                            <td>{{$let_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>NLE</td>
                            <td>{{$nle}}</td>
                            <td>{{$nle_1_sale}}</td>
                            <td>{{$nle_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>Criminology</td>
                            <td>{{$crim}}</td>
                            <td>{{$crim_1_sale}}</td>
                            <td>{{$crim_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>Civil Service</td>
                            <td>{{$civil}}</td>
                            <td>{{$civil_1_sale}}</td>
                            <td>{{$civil_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>Psychometrician</td>
                            <td>{{$psyc}}</td>
                            <td>{{$psyc_1_sale}}</td>
                            <td>{{$psyc_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>NCLEX</td>
                            <td>{{$nclex}}</td>
                            <td>{{$nclex_1_sale}}</td>
                            <td>{{$nclex_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>IELTS</td>
                            <td>{{$ielts}}</td>
                            <td>{{$ielts_1_sale}}</td>
                            <td>{{$ielts_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>Social Work</td>
                            <td>{{$social}}</td>
                            <td>{{$social_1_sale}}</td>
                            <td>{{$social_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>Agriculture</td>
                            <td>{{$agri}}</td>
                            <td>{{$agri_1_sale}}</td>
                            <td>{{$agri_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>Midwifery</td>
                            <td>{{$mid}}</td>
                            <td>{{$mid_1_sale}}</td>
                            <td>{{$mid_2_sale}}</td>
                        </tr>
                        <tr>
                            <td>Online Only</td>
                            <td>{{$online}}</td>
                            <td>{{$online_1_sale}}</td>
                            <td>{{$online_2_sale}}</td>
                        </tr>


                    </tbody>                  
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.ad-record').addClass('active');
  $('.ad-record').addClass('collapse in');
});

</script>
@stop